%\documentstyle{mybook}
%\input{latexmac}
%
%\setcounter{chapter}{0}
%\setcounter{section}{0}
%\setcounter{subsection}{0}
%
\thispagestyle{plain}
%\setcounter{page}{1}\pagenumbering{roman}
%\begin{document}
\bigskip
\vspace*{1in}
\noindent{\Huge\bf Preface}
\bigskip
\par\noindent
This book helps students to master the material of a standard 
undergraduate linear algebra course.

The material is standard in that the topics covered are
Gaussian reduction, 
vector spaces, linear maps,
determinants, and eigenvalues and eigenvectors.
The audience is also standard: 
sophmores or juniors, usually with a background 
of at least
one semester of Calculus and perhaps with as much as three semesters. 

The help that it gives to students comes from taking a developmental 
approach\Dash 
this book's presentation emphasizes motivation and naturalness, 
driven home by a wide variety of
examples and extensive, careful, exercises.
The developmental approach is what sets this book apart, 
so some expansion of the term is appropriate here.

Courses in the beginning of most Mathematics programs
reward students less for understanding the theory and more for correctly
applying formulas and algorithms.
Later courses
ask for mathematical maturity:~the ability to follow different 
types of arguments, 
a familiarity with
the themes that underly many mathematical investigations like
elementary set and function facts,
and a capacity for some independent reading and thinking. 
Linear algebra is an ideal spot to work on the transistion between
the two kinds of courses.
It comes early in a program so that progress made here pays off later,
but also comes late enough that
students are often majors and minors.
The material is coherent, accessible, and elegant.
There are a variety of argument styles\Dash proofs by
contradiction, if and only if statements, and proofs by induction, for
instance\Dash and examples are plentiful.
%This text helps an instructor raise the students' level of
%mathematical sophistication.

So, the aim of this book's exposition is to help students develop from 
being successful at their present level, 
in classes where a majority of the members are interested mainly in
applications in science or engineering,
to being successful at the next level, that of serious students of 
the subject of mathematics itself.

Helping students
make this transition means taking the mathematics seriously, so
all of the results in this book are proved.
On the other hand, we cannot
assume that students have already arrived,
and so 
in contrast with more abstract texts, 
we give many examples
and they are often quite detailed.

In the past, linear algebra texts commonly made this transistion abrubtly. 
They began with extensive computations of linear systems, 
matrix multiplications, 
and determinants.
When the concepts\Dash 
vector spaces and linear maps\Dash finally appeared, 
and definitions and proofs started, often the change
brought students to a stop.
In this book, while we start with
a computational topic, linear reduction, from the first
we do more than compute.
We do linear systems quickly but completely,
including the proofs needed to justify what we are computing.
Then, with the linear systems work as motivation
and at a point where the study of linear combinations seems natural,
the second chapter starts with the definition of a real vector space.
This occurs by the end of the third week.

Another example of our emphasis on motivation and naturalness
is that the third chapter on linear maps
does not begin with the definition of homomorphism, 
but with that of isomorphism.
That's because this definition is easily motivated
by the observation that some spaces are ``just like'' others.
After that, 
the next section takes the reasonable step of defining homomorphism by
isolating the operation-preservation idea.
This approach loses mathematical slickness, 
but it is a good trade because it comes 
in return for a large gain in sensibility to students.

One aim of a developmental approach is 
that students should feel throughout the presentation that 
they can see how the ideas arise, and perhaps picture themselves
doing the same type of work.

The clearest example of the developmental approach taken here\Dash 
and the feature that most recommends this book\Dash is the
exercises.
A student progresses most while doing the exercises, so they have
been selected with great care.
Each problem set ranges from
simple checks to resonably involved proofs.
Since an instructor usually assigns about a dozen exercises
after each lecture,
each section ends with about twice that many, 
thereby providing a selection.
There are even a few problems that are challenging puzzles
taken from various journals, competitions, or
problems collections. 
(These are marked with a
`\puzzlemark' and 
as part of the fun, the original wording
has been retained as much as possible.)
In total, the exercises are aimed to both build an ability at,
and help students experience the pleasure of, 
\emph{doing} mathematics.



% In most mathematics programs,
% linear algebra comes in the first or second 
% year, following or along with at least one course in calculus.
% While the location of this course is stable, lately the content
% has been under discussion.
% Some instructors have experimented with varying the traditional topics
% and others have tried courses focused on applications or on computers.
% Despite this healthy debate,
% most instructors are still convinced, I think, that the right core material is
% vector spaces, linear maps, determinants, and eigenvalues and eigenvectors.
% Applications and code have a part to play,
% but the themes of the course should remain unchanged. 

% Not that all is fine with the traditional course.
% Many of us believe that the
% standard text type could do with a change.
% Introductory texts have traditionally started with extensive computations of 
% linear reduction, matrix multiplication, and determinants, which
% take up half of the course.
% Then, when vector spaces and linear maps finally appear 
% and definitions and proofs start,
% the nature of the course takes a sudden turn.
% The computation drill was there in the past because, as future practitioners, 
% students needed to be fast and accurate.
% But that has changed.
% Being a whiz at $\nbyn{5}$ determinants just isn't important anymore.
% Instead, the availability of computers gives us an opportunity
% to move toward a focus on concepts.

% This is an opportunity that we should seize.
% The courses at the start of most mathematics programs work at having 
% students apply formulas and algorithms.
% Later courses ask for mathematical maturity:~reasoning skills 
% that are developed enough to follow different types of arguments, 
% a familiarity with
% the themes that underly many mathematical investigations like
% elementary set and function facts,
% and an ability to do some independent reading and thinking. 
% Where do we work on the transition?

% Linear algebra is an ideal spot.
% It comes early in a program so that progress made here pays off later.
% But, it is also placed far enough into a program that
% the students are serious about mathematics, often majors and minors.
% The material is straightforward, elegant, and accessible.
% There are a variety of argument styles\Dash proofs by
% contradiction, if and only if statements, and proofs by induction, for
% instance\Dash and examples are plentiful.

% The goal of this text is,
% along with the presentation of undergraduate linear algebra,
% to help an instructor raise the students' level of
% mathematical sophistication. 
% Most of the differences between this book and others follow straight from that
% goal.

% One consequence of this goal of development is that, 
% unlike in many computational texts, 
% all of the results here are proved.
% On the other hand, in contrast with more abstract texts, 
% many examples are given,
% and they are often quite detailed.

% Another consequence of the goal
% is that while we start with
% a computational topic, linear reduction, from the first
% we do more than just compute.
% The solution of linear systems is done quickly but completely,
% proving everything,
% all the way through the uniqueness of reduced echelon form.
% And, right in this first chapter 
% a few induction proofs appear, 
% where the arguments are just verifications of details, so
% that when induction is needed later
% (e.g., 
% to prove that all bases of a finite dimensional vector space have the
% same number of members) it will be familiar.

% Still another consequence of the goal of development is that
% the second chapter starts (using the linear systems work as motivation) 
% with the definition of a real vector space.
% This typically occurs by the end of the third week.
% We do not stop to introduce matrix multiplication and determinants as
% rote computations.
% Instead, those topics appear naturally in the
% development, after the definition of linear maps.

% Throughout the book
% the presentation stresses motivation and naturalness.
% An example is the third chapter, on linear maps.
% It does not begin with the definition of a homomorphism, as is
% the case in other books, but with that of an isomorphism.
% That's because isomorphism is easily motivated
% by the observation that some spaces are just like each other.
% After that, 
% the next section takes the reasonable step of defining homomorphisms by
% isolating the operation-preservation idea.
% Some mathematical slickness is lost, but it is 
% in return for a large gain in sensibility to students.
% %This emphasis on motivation and naturalness
% %helps students make the transition from earlier courses.

% Having extensive motivation in the text also helps with time pressures.
% I ask students to, before each class, look ahead in the book. 
% They follow the classwork better because they have some prior exposure
% to the material.
% For example, I can start the linear independence class with the 
% definition because I know students have some idea of what it is about.
% No book can take the place of an instructor
% but a helpful book gives the instructor 
% more class time for examples and questions.

% Much of a student's progress takes place while doing the exercises;
% the exercises here work with the rest of the text.
% Besides computations, there are many proofs.
% In each subsection they are spread over an approachability range, from
% simple checks to some much more involved arguments.
% There are even a few that are challenging puzzles
% taken from various journals, competitions, or
% problems collections 
% (these are marked with a
% \puzzlemark; as part of the fun, the original wording
% has been retained as much as possible).
% In total, the exercises are aimed to both build an ability at,
% and help students experience the pleasure of, 
% \emph{doing} mathematics.



%\vspace*{.5in}
\medskip
\noindent{\bf Applications, and Computers.}
%\smallskip
The point of view taken here, that linear algebra is about vector spaces
and linear maps, is not taken to the complete exclusion of others.
Applications and the role of the computer are important and vital aspects 
of the subject.
Consequently, each of this book's chapters closes with a few 
application or computer-related topics.
Some are:~network flows, the speed and accuracy of
computer linear reductions, Leontief Input/Output analysis,
dimensional analysis, Markov chains, voting paradoxes,
analytic projective geometry, and difference equations.

These topics are brief enough to be done in a day's class 
or to be given as independent projects for individuals or small groups.
Most simply give a reader
a taste of the subject, discuss how linear algebra comes in,
point to some further reading, and give a few exercises. 
In short, these topics invite 
readers to see for themselves that linear algebra is a tool
that a professional must have. 

%The computer work is not tied to any single software package.
%Sample code is given from a variety of packages 
%to underscore that the discussion is not about some particular 
%package; rather, the software is a tool to
%develop the ideas, and to quickly and correctly compute answers.   




%\vspace*{.5in}
\medskip
\noindent{\bf For people reading this book on their own.}
%\smallskip
%
\newcommand{\classday}[1]{\textsc{#1}}
\newcommand{\colwidth}{1.35in}
This book's emphasis on motivation and development make it
a good choice for self-study.
But, while a professional instructor can judge what pace and topics suit a
class, if you are an independent student 
then perhaps you would find some advice helpful.

Here are two timetables for a semester.
The first focuses on core material.
\begin{center}
   \begin{tabular}{r|*{2}{p{\colwidth}}l}
      \textit{week}  
       &\textit{Monday}          
       &\textit{Wednesday}            
       &\textit{Friday}        \\ \hline
       1    &One.I.1         &One.I.1, 2        &One.I.2, 3         \\
       2    &One.I.3         &One.II.1          &One.II.2         \\
       3    &One.III.1, 2    &One.III.2         &Two.I.1         \\
       4    &Two.I.2         &Two.II            &Two.III.1         \\
       5    &Two.III.1, 2    &Two.III.2         &\classday{exam}          \\
       6    &Two.III.2, 3    &Two.III.3         &Three.I.1        \\
       7    &Three.I.2         &Three.II.1          &Three.II.2         \\
       8    &Three.II.2        &Three.II.2          &Three.III.1          \\
       9    &Three.III.1       &Three.III.2         &Three.IV.1, 2       \\
      10    &Three.IV.2, 3, 4  &Three.IV.4          &\classday{exam}          \\
      11    &Three.IV.4, Three.V.1 &Three.V.1, 2        &Four.I.1, 2         \\
      12    &Four.I.3         &Four.II            &Four.II       \\
      13    &Four.III.1       &Five.I             &Five.II.1         \\
      14    &Five.II.2        &Five.II.3          &\classday{review}        
   \end{tabular}
\end{center}
The second timetable is more ambitious
(it supposes that you know One.II, the elements of vectors, 
usually covered in third semester calculus).
\begin{center}
   \begin{tabular}{r|*{2}{p{\colwidth}}l}
      \textit{week}  
         &\textit{Monday}          
         &\textit{Wednesday}            
         &\textit{Friday}        \\ \hline
       1    &One.I.1         &One.I.2           &One.I.3         \\
       2    &One.I.3         &One.III.1, 2      &One.III.2         \\
       3    &Two.I.1         &Two.I.2           &Two.II           \\
       4    &Two.III.1       &Two.III.2         &Two.III.3         \\
       5    &Two.III.4       &Three.I.1           &\classday{exam}          \\
       6    &Three.I.2         &Three.II.1          &Three.II.2         \\
       7    &Three.III.1       &Three.III.2         &Three.IV.1, 2      \\
       8    &Three.IV.2        &Three.IV.3          &Three.IV.4         \\
       9    &Three.V.1         &Three.V.2           &Three.VI.1         \\
      10    &Three.VI.2        &Four.I.1           &\classday{exam}          \\
      11    &Four.I.2         &Four.I.3           &Four.I.4         \\
      12    &Four.II          &Four.II, Four.III.1   &Four.III.2, 3      \\
      13    &Five.II.1, 2     &Five.II.3          &Five.III.1         \\
      14    &Five.III.2       &Five.IV.1, 2       &Five.IV.2         
   \end{tabular}
\end{center} 
See the table of contents for the titles of these subsections.

To help you make time trade-offs, 
in the table of contents
I have marked subsections as optional if
some instructors will pass over them in favor of spending more time elsewhere. 
You might also try picking one or two topics that appeal to you 
from the end of each chapter.
You'll get more from these
if you have access to computer software that can do the
big calculations.

The most important advice is: do many exercises.
I have marked a good sample with \recommendationmark's.
(The answers are available.)
You should be aware, however, 
that few inexperienced people can write correct proofs.
Try to find a knowledgeable person to work with
you on this.

\bigskip
Finally, if I may, a caution for all students, independent or not:~I 
cannot overemphasize how much the statement
that I sometimes hear, ``I understand the material, but it's only 
that I have trouble with the problems''\spacefactor=1000\ %
reveals a lack of understanding of what we are up to.
Being able to do things with the ideas is their point.
The quotes below express this sentiment admirably.
They state what I believe is the key to both the beauty and the power
of mathematics and the sciences in general, 
and of linear algebra in particular (I took the liberty of formatting
them as poems).

\bigskip
\par\noindent\begin{tabular}[t]{@{}l@{}}
  \textit{I know of no better tactic}                     \\
  \textit{\ than the illustration of exciting principles} \\
  \textit{by well-chosen particulars.}                    \\
  \hspace*{1in}\textit{--Stephen Jay Gould}
\end{tabular}

\bigskip
\par\noindent\begin{tabular}[t]{@{}l@{}}   
\textit{If you really wish to learn}                     \\
   \textit{\ then you must mount the machine}  \\ 
   \textit{\ and become acquainted with its tricks} \\
   \textit{by actual trial.}                    \\
   \hspace*{1in}\textit{--Wilbur Wright}
\end{tabular}

\par\ \hfill\begin{tabular}[t]{@{}l@{}}
                       Jim Hef{}feron            \\
                       Mathematics, Saint Michael's College \\ 
                       Colchester, Vermont\ USA 05439  \\     
                       \texttt{http://joshua.smcvt.edu} \\
                       2006-May-20
                    \end{tabular}

\vfill
\par\noindent\textit{Author's Note.}
Inventing a good exercise, one that enlightens as well as tests, 
is a creative act, and hard work.
%At least half of the the effort on this text has gone into exercises 
%and solutions.
The inventor deserves recognition.
But for some reason texts have traditionally not given attributions for
questions.
I have changed that here where I was sure of the source.
I would greatly appreciate hearing from anyone who can help me to correctly
attribute others of the questions.   
%\end{document}
