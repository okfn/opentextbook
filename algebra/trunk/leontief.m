# Octave commands for _Linear Algebra_ by Jim Hefferon,
#  Topic: leontif.tex 
a=[(25448-5395)/25448  -2664/30346;
    -48/25448          (30346-9030)/30346];
b=[17589;
   21243];
ans=a \ b;
printf("The answer to the first system is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
b=[17489;
   21243];
ans=a \ b;
printf("The answer to the second system is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
# question 1
b=[17789;
   21243];
ans=a \ b;
printf("The answer to question (1a) is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
b=[17689;
   21443];
ans=a \ b;
printf("The answer to question (1b) is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
b=[17789;
   21443];
ans=a \ b;
printf("The answer to question (1c) is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
# question 2
printf("Current ratio for use of steel by auto is %0.4f\n",2664/30346);
a=[(25448-5395)/25448  -0.0500;
    -48/25448          (30346-9030)/30346];
b=[17589;
   21243];
ans=a \ b;
printf("The answer to 2(a) is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
b=[17589;
   21500];
ans=a \ b;
printf("The answer to 2(b) is s=%0.0f and a=%0.0f\n",ans(1),ans(2));
# question 3
printf("The value of steel used by others is %0.2f\n",18.69-(6.90+1.28));
printf("The value of autos used by others is %0.2f\n",14.27-(0+4.40));
a=[(18.69-6.90)/18.69  -1.28/14.27;
    -0/18.69          (14.27-4.40)/14.27];
b=[1.10*(18.69-(6.90+1.28));
   1.15*(14.27-(0+4.40))];
ans=a \ b;
printf("The answer to 3(a) is s=%0.2f and a=%0.2f\n",ans(1),ans(2));
printf("The 1947 ratio of steel used by steel is %0.2f\n",(18.69-6.90)/18.69);
printf("The 1947 ratio of steel used by autos is %0.2f\n",1.28/14.27);
printf("The 1947 ratio of autos used by steel is %0.2f\n",0/18.69);
printf("The 1947 ratio of autos used by autos is %0.2f\n",(14.27-4.40)/14.27);
printf("The 1958 ratio of steel used by steel is %0.2f\n",(25448-5395)/25448);
printf("The 1958 ratio of steel used by autos is %0.2f\n",2664/30346);
printf("The 1958 ratio of autos used by steel is %0.2f\n",48/25448);
printf("The 1958 ratio of autos used by autos is %0.2f\n",(30346-9030)/30346);
b=[17.598/1.30;
   21.243/1.30];
ans=a \ b;
newans=1.30 * ans;
printf("The answer to 3(c) is (in billions of 1947 dollars) s=%0.2f and a=%0.2f\n  and in billions of 1958 dollars it is s=%0.2f and a=%0.2f\n",ans(1),ans(2),newans(1),newans(2));
