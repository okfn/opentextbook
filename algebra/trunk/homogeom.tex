% Chapter 3, Topic _Linear Algebra_ Jim Hefferon
%  http://joshua.smcvt.edu/linalg.html
%  2001-Jun-12
\topic{Geometry of Linear Maps} 
\index{Geometry of Linear Maps|(}
%The geometry of linear maps %$\map{h}{\Re^n}{\Re^m}$ 
%is appealing both for its simplicity and for its usefulness.
The pictures below contrast 
\( f_1(x)=e^x \) and \( f_2(x)=x^2 \), which are nonlinear,
with \( h_1(x)=2x \) and \( h_2(x)=-x \), which are linear.
Each of the four pictures shows the domain $\Re^1$ on the left 
mapped to the codomain $\Re^1$ on the right. 
Arrows trace out where each map sends
$x=0$, $x=1$, $x=2$, $x=-1$, and $x=-2$.
Note how the nonlinear maps distort
the domain in transforming it into the range.
For instance,
\( f_1(1) \) is further from
$f_1(2)$ than it is from $f_1(0)$ \Dash  the map is spreading 
the domain out unevenly so that  
an interval near $x=2$ is spread apart more 
than is an interval near $x=0$
when they are carried over to the range.
\begin{center}
  \includegraphics{ch3.46}
  \hspace*{3em}
  \includegraphics{ch3.47}
\end{center}
The linear maps are nicer, more regular, 
in that for each map all of the domain is 
spread by the same factor.
\begin{center}
  \includegraphics{ch3.48}
  \hspace*{3em}
  \includegraphics{ch3.49}
\end{center}

The only linear maps from $\Re^1$ to $\Re^1$ are multiplications by a scalar. 
In higher dimensions more can happen. 
For instance, this linear transformation of $\Re^2$, 
rotates vectors counterclockwise, and is not just a scalar 
multiplication.\index{rotation}\index{linear map!rotation}
\begin{center}
  \includegraphics{ch3.50}
\end{center}
The transformation of $\Re^3$ 
which projects vectors into the $xz$-plane
is also not just a rescaling.
\begin{center}
 \includegraphics{ch3.51}
\end{center}
Nonetheless, even in higher dimensions the situation isn't too
complicated.

Below, we use the standard bases to represent 
each linear map $\map{h}{\Re^n}{\Re^m}$ by a matrix $H$.
Recall that any $H$ can be factored $H=PBQ$, 
where $P$ and $Q$ are nonsingular and $B$ is a partial-identity matrix.
Further, recall that nonsingular matrices
factor into elementary matrices\index{matrix!elementary reduction}\index{elementary!matrix}
$PBQ=T_nT_{n-1}\cdots T_jBT_{j-1}\cdots T_1$,
which are matrices that
are obtained from the identity $I$ with one Gaussian step
\begin{equation*}
  I\grstep{k\rho_i}M_i(k) 
  \qquad 
  I\grstep{\rho_i\leftrightarrow\rho_j}P_{i,j}  
  \qquad
  I\grstep{k\rho_i+\rho_j}C_{i,j}(k) 
\end{equation*}
($i\neq j$, $k\neq 0$).
So if we understand the effect of a linear map described
by a partial-identity matrix, and the effect of linear mapss
described by the elementary matrices, then we will in some sense
understand the effect of any linear map.
(The pictures below stick to transformations of $\Re^2$ for ease of drawing, 
but the statements hold for maps from any $\Re^n$ to any $\Re^m$.)

The geometric effect of the linear transformation represented by a  
partial-identity matrix is projection.
\begin{equation*}
  \colvec{x \\ y  \\ z}
  \quad\begin{array}[b]{c}
    \text{\scriptsize $\begin{pmatrix}
      1  &0  &0   \\
      0  &1  &0   \\
      0  &0  &0   
    \end{pmatrix}_{\text{\makebox[0pt][l]{$\stdbasis_3,\stdbasis_3$}}}$ } \\[1.5ex]
    \longrightarrow
  \end{array}\qquad
  \colvec{x \\ y  \\ 0}
\end{equation*}

For the $M_i(k)$ matrices, 
the geometric action of a transformation represented by such a  
matrix (with respect to the standard basis) is to  
stretch vectors by a factor of $k$ along the $i$-th axis.
This map stretches by a factor of $3$ along the $x$-axis.
\begin{center}
  \includegraphics{ch3.52}
\end{center}
Note that if $0\leq k<1$ or if $k<0$ then the $i$-th
component goes the other way; here, toward the left.
\begin{center}
  \includegraphics{ch3.53}
\end{center}
Either of these is a 
\definend{dilation}.\index{dilation}\index{linear map!dilation}

The action of a transformation represented by a $P_{i,j}$ permutation matrix
is to interchange the $i$-th and $j$-th axes; this is a particular kind of
reflection. 
\begin{center}
  \includegraphics{ch3.54}
\end{center}
In higher dimensions, 
permutations involving many axes can be decomposed into a combination 
of swaps of pairs of axes\Dash see \nearbyexercise{exer:PermIsCompSwaps}.

The remaining  case is that of matrices of the form $C_{i,j}(k)$.
Recall that, for instance, that $C_{1,2}(2)$ performs $2\rho_1+\rho_2$. 
\begin{equation*}
  \colvec{x  \\  y}
  \quad\begin{array}[b]{c}
      \text{\scriptsize $\begin{pmatrix}
                1  &0  \\
                2  &1  
          \end{pmatrix}_{\text{\makebox[0pt][l]{$\stdbasis_2,\stdbasis_2$}}}$ } \\[1.5ex]
          \longrightarrow
        \end{array}
   \qquad
   \colvec{x \\ 2x+y}
\end{equation*}
In the picture below, 
the vector $\vec{u}$ with the first component of $1$ is affected less 
than the vector $\vec{v}$ with the first component of $2$\Dash 
$h(\vec{u})$ is only $2$ higher than $\vec{u}$ while 
$h(\vec{v})$ is $4$ higher than $\vec{v}$.
\begin{center}
  \includegraphics{ch3.55}
\end{center}
Any vector with a first component of $1$ would be affected as is $\vec{u}$;
it would be slid up by $2$.
And any vector with a first component of $2$ would be slid up $4$, 
as was $\vec{v}$.
That is, the transformation represented by 
$C_{i,j}(k)$ affects vectors depending on their $i$-th component.

Another way to see this same point is to consider the action of this map 
on the unit square.
In the next picture,
vectors with a first component of $0$, like the origin, are not pushed 
vertically at all but vectors with a positive first component are slid up.
Here, all vectors with a first component of $1$\Dash the entire 
right side of the square\Dash is affected to the same extent.
More generally, vectors on the same vertical line are slid up the same amount,
namely, they are slid up by twice their first component.
The resulting shape, a rhombus, has the same base and height as the square
(and thus the same area) but the right angles are gone.
\begin{center}
  \includegraphics{ch3.56}
\end{center}
For contrast the next picture shows the effect of the map represented by 
$C_{2,1}(1)$.
In this case, vectors are affected according to their  
second component.
The vector $\binom{x}{y}$ is slid horozontally by twice $y$.
\begin{center}
  \includegraphics{ch3.57}
\end{center}
%In general, for any $C_{i,j}(k)$, the
%sliding happens in such a way that vectors with the same $i$-th component
%are slid by the same amount.
Because of this action, this kind of map is called a 
\definend{skew}.\index{skew}\index{linear map!skew}

With that, we have covered the geometric effect of the four types 
of components in the expansion
$H=T_nT_{n-1}\cdots T_jBT_{j-1}\cdots T_1$,
the partial-identity projection $B$ and the elementary $T_i$'s.
Since we understand its components, we in some sense 
understand the action of any $H$.
As an illustration of this assertion, 
recall that under a linear map, the image of a subspace is a subspace
and thus the linear transformation $h$ represented by $H$ maps lines 
through the origin to lines through the origin.
(The dimension of the image space cannot be greater than 
the dimension of the domain space, so a line can't map onto, say, a plane.)
We will extend that to show that any line, 
not just those through the origin, 
is mapped by $h$ to a line.
The proof is simply
that the partial-identity projection $B$ and the elementary $T_i$'s
each turn a line input into a line output 
(verifying the four cases is \nearbyexercise{exer:ImageLinSurIsLinSur}),
and therefore their composition also preserves lines.
Thus, by understanding its components we can understand arbitrary square 
matrices $H$, in the sense that we can prove things about them.

An understanding of the geometric effect of linear transformations 
on $\Re^n$ is very important in mathematics. 
Here is a familiar application from calculus.
On the left is a picture
of the action of the nonlinear function \( y(x)=x^2+x \).
As at that start of this Topic, overall the geometric effect of this map is
irregular in that at different domain points it has different effects
(e.g., as the domain point $x$ goes from $2$ to $-2$, the associated range
point $f(x)$ at first decreases, then pauses instantaneously,
and then increases).
\begin{center}
  \includegraphics{ch3.58}
\end{center}
But in calculus we don't focus on the map overall, 
we focus instead on the local effect of the map.
%The picture below looks closely at what this map
%does near $x=1$.
At $x=1$ the derivative is \( y^\prime(1)=3 \),
so that near \( x=1 \) 
we have \( \Delta y\approx 3\cdot\Delta x \).
%; in other words, \( (1.001^2+1.001)-(1^2+1)\approx 3\cdot (0.001) \).
That is, in a neighborhood of $x=1$,
in carrying the domain to the codomain this map causes it to grow by
a factor of $3$ \Dash  it is, locally, 
approximately, a dilation.
%The map $y$ is locally regular.
The picture below shows a small interval 
in the domain $(x-\Delta x\,..\,x+\Delta x)$
carried over to an interval in the codomain $(y-\Delta y\,..\,y+\Delta y)$
that is three times as wide:~$\Delta y \approx 3\cdot \Delta x$.
\begin{center}
  \includegraphics{ch3.59}
\end{center}
(When the above picture is drawn in the traditional cartesian way
then the prior sentence about the rate of growth of $y(x)$ is usually
stated:~the derivative $y^\prime(1)=3$ gives the slope of the 
line tangent to the graph at the point $(1,2)$.)

In higher dimensions, the idea is the same but the approximation
is not just the $\Re^1$-to-$\Re^1$ scalar multiplication case.
Instead, for 
a function \( \map{y}{\Re^n}{\Re^m} \) and a point \( \vec{x}\in\Re^n \),
the derivative is defined to be the 
linear map \( \map{h}{\Re^n}{\Re^m} \) best approximating
how \( y \) changes near \( y(\vec{x}) \).
So the geometry studied above applies.
%Calculus considers the map 
%that locally approximates the change \( \Delta x\mapsto 3\cdot\Delta x \)
%(instead of the actual change map
%\( \Delta x\mapsto y(1+\Delta x)-y(1) \)) because the local map 
%is easier.
%It is easier in that, if the
%input change is doubled, or tripled, etc., then the
%resulting output change will double, or triple, etc.
%\begin{equation*}
%  3(r\,\Delta x)=r\,(3\Delta x)
%\end{equation*}
%(for $r\in\Re$), 
%and it is easier in that 
%adding changes in input adds the resulting output changes.
%\begin{equation*}
%  3(\Delta x_1+\Delta x_2)=3\Delta x_1+3\Delta x_2
%\end{equation*}
%In short, what's
%easy about \( \Delta x\mapsto 3\cdot\Delta x \) is that
%it is linear.

We will close this Topic by remarking how
this point of view makes clear an often-misunderstood, but very important, 
result about derivatives:~the derivative of the composition of two functions
is computed by using the Chain Rule for combining their derivatives.
Recall that (with suitable conditions on the two functions)
\begin{equation*}
  \frac{d\,(\composed{g}{f})}{dx}(x) = 
  \frac{dg}{dx}(f(x))\cdot\frac{df}{dx}(x)
\end{equation*} 
so that, for instance, the derivative of $\sin(x^2+3x)$ is
$\cos(x^2+3x)\cdot(2x+3)$.
How does this combination arise?
From this picture of the action of the composition.  
\begin{center}
  \includegraphics{ch3.60}
\end{center}
The first map $f$ dilates the neighborhood of $x$ by a factor of 
\begin{equation*}
  \frac{df}{dx}(x) 
\end{equation*}
and the second map $g$ dilates some more, this time 
dilating a neighborhood of $f(x)$ by a factor of 
\begin{equation*}
  \frac{dg}{dx}(\,f(x)\,) 
\end{equation*}
and as a result, the composition dilates by the product of these two.

In higher dimensions 
the map expressing how a function changes near a point is a linear map,
and is expressed as a matrix.
(So we understand the basic geometry of higher-dimensional derivatives;
they are compositions of dilations, interchanges of axes, shears, and 
a projection).
And, the Chain Rule just multiplies the matrices.

Thus, the geometry of linear maps $\map{h}{\Re^n}{\Re^m}$ 
is appealing both for its simplicity and for its usefulness.

\begin{exercises}
  \item 
    Let $\map{h}{\Re^2}{\Re^2}$ be the transformation that rotates
    vectors clockwise by $\pi/4$~radians.
    \begin{exparts}
      \partsitem Find the matrix $H$ representing 
         $h$ with respect to the standard bases.
         Use Gauss' method to reduce $H$ to the identity.
      \partsitem Translate the row reduction to to a matrix equation
        $T_jT_{j-1}\cdots T_1H=I$
        (the prior item shows both that $H$ is similar to $I$, and that
        no column operations are needed to derive $I$ from $H$).
      \partsitem Solve this matrix equation for $H$.
      \partsitem Sketch the geometric effect matrix, that is, sketch how
        $H$ is expressed as a 
        combination of dilations, flips, skews, and projections
        (the identity is a trivial projection). 
    \end{exparts}
    \begin{answer}
      \begin{exparts}
        \partsitem  To represent $H$, recall that rotation counterclockwise by 
          $\theta$~radians is represented with respect to the standard basis
          in this way.
          \begin{equation*}
            \rep{h}{\stdbasis_2,\stdbasis_2}
            =\begin{pmatrix}
              \cos\theta  &-\sin\theta  \\
              \sin\theta  &\cos\theta
             \end{pmatrix}
          \end{equation*}
          A clockwise angle is the negative of a counterclockwise
          one.  
          \begin{equation*}
            \rep{h}{\stdbasis_2,\stdbasis_2}
            =\begin{pmatrix}
              \cos(-\pi/4)  &-\sin(-\pi/4)  \\
              \sin(-\pi/4)  &\cos(-\pi/4)
            \end{pmatrix}
            =\begin{pmatrix}
              \sqrt{2}/2  &\sqrt{2}/2  \\
              -\sqrt{2}/2 &\sqrt{2}/2
            \end{pmatrix}
          \end{equation*}
          This Gauss-Jordan reduction
          \begin{equation*}
            \grstep{\rho_1+\rho_2}
            \begin{pmatrix}
              \sqrt{2}/2  &\sqrt{2}/2  \\
              0           &\sqrt{2}
            \end{pmatrix}
            \grstep[(1/\sqrt{2})\rho_2]{(2/\sqrt{2})\rho_1}
            \begin{pmatrix}
              1  &1  \\
              0  &1
            \end{pmatrix}
            \grstep{-\rho_2+\rho_1}
            \begin{pmatrix}
              1  &0  \\
              0  &1
            \end{pmatrix}
          \end{equation*}
          produces the identity matrix 
          so there is no need for column-swapping operations
          to end with a partial-identity.
        \partsitem The reduction is expressed in matrix multiplication 
          as
          \begin{equation*}
            \begin{pmatrix}
              1  &-1 \\
              0  &1
            \end{pmatrix}
            \begin{pmatrix}
              2/\sqrt{2}  &0         \\
              0           &1/\sqrt{2}
            \end{pmatrix}
            \begin{pmatrix}
              1  &0 \\
              1  &1
            \end{pmatrix}
            H
            =I
          \end{equation*}
          (note that composition of the Gaussian operations is performed
          from right to left).
        \partsitem  Taking inverses 
          \begin{equation*}
            H
            =
            \underbrace{
              \begin{pmatrix}
                1  &0 \\
                -1  &1
              \end{pmatrix}
              \begin{pmatrix}
                \sqrt{2}/2  &0         \\
                0           &\sqrt{2}
              \end{pmatrix}
              \begin{pmatrix}
                1  &1 \\
                0  &1
              \end{pmatrix}
             }_P
            I
          \end{equation*}
          gives the desired factorization of $H$ (here, the partial
          identity is $I$, and $Q$ is trivial, that is, it is also an identity
          matrix).
        \partsitem Reading the composition from right to left (and ignoring the
          identity matrices as trivial) gives that $H$ has the same
          effect as first performing this skew 
          \begin{center}
            \includegraphics{ch3.95}
         \end{center}
         followed by a dilation that multiplies all first components by 
         $\sqrt{2}/2$ (this is a ``shrink'' in that $\sqrt{2}/2\approx0.707$) 
         and all second components by $\sqrt{2}$,
         followed by another skew. 
          \begin{center}
            \includegraphics{ch3.96}
         \end{center}
         For instance, the effect of $H$ on the unit vector whose angle with
         the $x$-axis is $\pi/3$ is this.
          \begin{center}
            \includegraphics{ch3.97}
         \end{center}
         Verifying that the resulting vector has unit length and forms an
         angle of $-\pi/6$ with the $x$-axis is routine. 
      \end{exparts}
    \end{answer}
  \item 
    What combination of dilations, flips, skews, and projections
    produces a rotation counterclockwise by $2\pi/3$ radians?
    \begin{answer}
      We will first represent the map with a matrix $H$,
      perform the row operations and, if needed, column operations
      to reduce it to a partial-identity matrix.
      We will then translate that into a factorization $H=PBQ$.
      Subsitituting into the general matrix
          \begin{equation*}
            \rep{r_\theta}{\stdbasis_2,\stdbasis_2}
            \begin{pmatrix}
              \cos\theta  &-\sin\theta  \\
              \sin\theta  &\cos\theta
            \end{pmatrix}
          \end{equation*}
          gives this representation.
          \begin{equation*}
            \rep{r_{2\pi/3}}{\stdbasis_2,\stdbasis_2}
            \begin{pmatrix}
              -1/2        &-\sqrt{3}/2  \\
              \sqrt{3}/2  &-1/2
            \end{pmatrix}
          \end{equation*}
          Gauss' method is routine.
          \begin{equation*}
            \grstep{\sqrt{3}\rho_1+\rho_2}
            \begin{pmatrix}
              -1/2        &-\sqrt{3}/2  \\
               0          &-2
            \end{pmatrix}
            \grstep[(-1/2)\rho_2]{-2\rho_1}
            \begin{pmatrix}
               1          &\sqrt{3}    \\
               0          &1
            \end{pmatrix}
            \grstep{-\sqrt{3}\rho_2+\rho_1}
            \begin{pmatrix}
               1          &0   \\
               0          &1
            \end{pmatrix}
          \end{equation*}
          That translates to a matrix equation in this way.
          \begin{equation*}
            \begin{pmatrix}
              1  &-\sqrt{3}  \\
              0  &1
            \end{pmatrix}
            \begin{pmatrix}
              -2  &0    \\
               0  &-1/2
            \end{pmatrix}
            \begin{pmatrix}
               1         &0  \\
               \sqrt{3}  &1
            \end{pmatrix}
            \begin{pmatrix}
              -1/2        &-\sqrt{3}/2  \\
              \sqrt{3}/2  &-1/2
            \end{pmatrix}
            =I
          \end{equation*}
          Taking inverses to solve for $H$ yields this factorization.
          \begin{equation*}
            \begin{pmatrix}
              -1/2        &-\sqrt{3}/2  \\
              \sqrt{3}/2  &-1/2
            \end{pmatrix}
            =
            \begin{pmatrix}
                1         &0  \\
               -\sqrt{3}  &1
            \end{pmatrix}
            \begin{pmatrix}
              -1/2  &0    \\
               0    &-2
            \end{pmatrix}
            \begin{pmatrix}
              1  &\sqrt{3}  \\
              0  &1
            \end{pmatrix}
            I
          \end{equation*}
    \end{answer}
  \item 
    What combination of dilations, flips, skews, and projections
    produces the map $\map{h}{\Re^3}{\Re^3}$ 
    represented with respect to the standard bases by this matrix?
    \begin{equation*}
      \begin{pmatrix}
        1  &2  &1  \\
        3  &6  &0  \\
        1  &2  &2
      \end{pmatrix}
    \end{equation*}
    \begin{answer}
      This Gaussian reduction
      \begin{equation*}
        \grstep[-\rho_1+\rho_3]{-3\rho_1+\rho_2}
        \begin{pmatrix}
          1  &2  &1  \\
          0  &0  &-3 \\
          0  &0  &1
        \end{pmatrix}
        \grstep{(1/3)\rho_2+\rho_3}
        \begin{pmatrix}
          1  &2  &1  \\
          0  &0  &-3 \\
          0  &0  &0
        \end{pmatrix}
        \grstep{(-1/3)\rho_2}
        \begin{pmatrix}
          1  &2  &1  \\
          0  &0  &1 \\
          0  &0  &0
        \end{pmatrix}
        \grstep{-\rho_2+\rho_1}
        \begin{pmatrix}
          1  &2  &0  \\
          0  &0  &1 \\
          0  &0  &0
        \end{pmatrix}
      \end{equation*}
      gives the reduced echelon form of the matrix.
      Now the two column operations of taking $-2$ times the first column 
      and adding it to the second, and then of swapping columns two and three
      produce this partial identity. 
      \begin{equation*} 
        B=\begin{pmatrix}
          1  &0  &0  \\
          0  &1  &0  \\ 
          0  &0  &0
        \end{pmatrix}
      \end{equation*}
      All of that translates into matrix terms as:~where
      \begin{equation*}
        P=
        \begin{pmatrix}
          1  &-1    &0  \\
          0  &1     &0  \\
          0  &0     &1         
        \end{pmatrix}
        \begin{pmatrix}
          1  &0    &0  \\
          0  &-1/3 &0  \\
          0  &0    &1
        \end{pmatrix}
        \begin{pmatrix}
          1  &0    &0  \\
          0  &1    &0  \\
          0  &1/3  &1         
        \end{pmatrix}
        \begin{pmatrix}
          1  &0  &0  \\
          0  &1  &0  \\
         -1  &0  &1         
        \end{pmatrix}
        \begin{pmatrix}
          1  &0  &0  \\
         -3  &1  &0  \\
          0  &0  &1         
        \end{pmatrix}
      \end{equation*}
      and 
      \begin{equation*}
        Q=
        \begin{pmatrix}
          1  &-2    &0  \\
          0  &1     &0  \\
          0  &0     &1         
        \end{pmatrix}
        \begin{pmatrix}
          0  &1     &0  \\
          1  &0     &0  \\
          0  &0     &1         
        \end{pmatrix}
      \end{equation*}
      the given matrix factors as $PBQ$.
    \end{answer}
  \item \label{exer:RToRIsScalMult} 
    Show that any linear transformation of $\Re^1$ is the map
    that multiplies by a scalar $x\mapsto kx$.
    \begin{answer}
      Represent it with respect to the 
      standard bases $\stdbasis_1,\stdbasis_1$, then the
      only entry in the resulting $\nbyn{1}$ matrix is the scalar $k$.
    \end{answer}
  \item \label{exer:PermIsCompSwaps}
    Show that for any permutation  
    (that is, reordering) $p$ of the numbers
    $1$, \ldots, $n$, the map 
    \begin{equation*}
      \colvec{x_1 \\ x_2 \\ \vdots \\ x_n}
      \mapsto
      \colvec{x_{p(1)} \\ x_{p(2)} \\ \vdots \\ x_{p(n)}}
    \end{equation*}
    can be accomplished with a composition of maps, 
    each of which only swaps a single pair of coordinates.
    \textit{Hint:} it can be done by induction on $n$.
    (\textit{Remark:}~in the fourth chapter we will show this and we will also 
    show that the parity of the number of swaps used is determined by $p$.
    That is, although a particular
    permutation could be accomplished in two different ways
    with two different numbers of swaps, either both ways use an even number of
    swaps, or both use an odd number.)
    \begin{answer}
      We can show this by induction on the number of components in the 
      vector.
      In the $n=1$ base case the only permutation is the trivial one,
      and the map
      \begin{equation*}
        \colvec{x_1}
        \mapsto
        \colvec{x_1}
      \end{equation*}
      is indeed expressible as a composition of swaps\Dash as zero swaps.
      For the inductive step we assume that the map induced by 
      any permutation of fewer than
      $n$ numbers can be expressed with swaps only, and we consider the map
      induced by a 
      permutation $p$ of $n$ numbers.
      \begin{equation*}
        \colvec{x_1 \\ x_2 \\ \vdots \\ x_n}
        \mapsto
        \colvec{x_{p(1)} \\ x_{p(2)} \\ \vdots \\ x_{p(n)}}
      \end{equation*}
      Consider the number~$i$ such that $p(i)=n$.
      The map      
      \begin{equation*}
        \colvec{x_1      \\ x_2      \\ \vdots \\ x_i      \\ \vdots \\ x_n}
        \mapsunder{\hat{p}}
        \colvec{x_{p(1)} \\ x_{p(2)} \\ \vdots \\ x_{p(n)} \\ \vdots  \\ x_{n}}
      \end{equation*}
      will, when followed by the swap of the $i$-th and $n$-th components, 
      give the map~$p$.
      Now, the inductive hypothesis gives that $\hat{p}$ is achievable as 
      a composition of swaps.
    \end{answer}
  \item \label{exer:ImageLinSurIsLinSur} 
    Show that linear maps preserve the linear structures of a space.
    \begin{exparts}
      \partsitem Show that for any linear map from $\Re^n$ to $\Re^m$,
         the image of any line is a line.
         The image may be a degenerate line, that is, a single point.
      \partsitem Show that the image of any linear surface is a linear surface.
         This generalizes the result that under a linear map the image of
         a subspace is a subspace.
      \partsitem Linear maps preserve other linear ideas.
         Show that linear maps preserve ``betweeness'':~if the point
         $B$ is between $A$ and $C$ then the image of $B$ is between the
         image of $A$ and the image of $C$.
    \end{exparts}
    \begin{answer}
      \begin{exparts}
        \partsitem A line is a subset of $\Re^n$ of the form
          $\set{\vec{v}=\vec{u}+t\cdot\vec{w}\suchthat t\in\Re}$.
          The image of a point on that line is 
          $h(\vec{v})=h(\vec{u}+t\cdot\vec{w})=h(\vec{u})+t\cdot h(\vec{w})$,
          and the set of such vectors, as $t$ ranges over the reals, is
          a line (albeit, degenerate if $h(\vec{w})=\zero$).
        \partsitem This is an obvious extension of the prior argument.
        \partsitem If the point~$B$ is between the points~$A$ and~$C$ then the
          line from $A$ to $C$ has $B$ in it.
          That is, there is a $t\in (0\,..\,1)$ such that
          $\vec{b}=\vec{a}+t\cdot (\vec{c}-\vec{a})$ (where $B$ is the
          endpoint of $\vec{b}$, etc.).
          Now, as in the argument of the first item, linearity shows that
          $h(\vec{b})=h(\vec{a})+t\cdot h(\vec{c}-\vec{a})$.  
      \end{exparts}
    \end{answer}
  \item 
    Use a picture like the one 
    that appears in the discussion of the Chain Rule
    to answer:~if a function $\map{f}{\Re}{\Re}$ has an inverse,
    what's the relationship between how the function \Dash locally, 
    approximately \Dash  dilates space, and
    how its inverse dilates space (assuming, of course, that it has an 
    inverse)?
    \begin{answer}
      The two are inverse.
      For instance, for a fixed $x\in\Re$,
      if $f^\prime (x)=k$ (with $k\neq 0$) then 
      $(f^{-1})^\prime (x)=1/k$.
      \begin{center}
        \includegraphics{ch3.98}
     \end{center}
    \end{answer}
\end{exercises}
\index{Geometry of Linear Maps|)}
\endinput


