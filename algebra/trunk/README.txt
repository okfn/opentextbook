Author(s): Jim Hefferon

Title: Linear Algebra

Source URL: <http://joshua.smcvt.edu/linalg.html/>

Download URL(s): <ftp://joshua.smcvt.edu/pub/hefferon/book/linear_algebra.zip>

License(s): CC-BY-SA, GFDL

Tags: mathematics


#############################################################
# Mirrored at Open Text Book <http://www.opentextbook.org>. #
#############################################################
