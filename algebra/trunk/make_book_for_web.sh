#!/bin/sh
#  Make the book for the web (hyperlinks, PDF, etc.)
latex "\def\dvidrv{dvipdfm}\def\pbsifont{yes}\def\hrefout{yes}\input book"
makeindex -s book.isty -p odd book.idx
latex "\def\dvidrv{dvipdfm}\def\pbsifont{yes}\def\hrefout{yes}\input book"
dvipdfm book

# edit jhanswer.tex to set \input to bookans
latex "\def\dvidrv{dvipdfm}\def\pbsifont{yes}\def\hrefout{yes}\input jhanswer"
latex "\def\dvidrv{dvipdfm}\def\pbsifont{yes}\def\hrefout{yes}\input jhanswer"
dvipdfm jhanswer
