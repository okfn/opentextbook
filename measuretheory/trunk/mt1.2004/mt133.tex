\filename{mt133.tex}
\versiondate{21.12.03}
\copyrightdate{1994}
     
\def\chaptername{Complements}
\def\sectionname{Wider concepts of integration}
     
\newsection{133}
     
There are various contexts in which it is useful to be able to assign a
value to the integral of a function which is not quite covered by the
basic definition in 122M.   In this section I offer suggestions
concerning the assignment of the values $\pm\infty$ to integrals of
real-valued functions (133A), the integration
of complex-valued functions (133C-133H) and upper and lower integrals
(133I-133K).   In \S135 below I will discuss a further elaboration of
the ideas of Chapter 12.
     
\leader{133A}{Infinite integrals} It is normal to restrict the phrase
`$f$ is integrable' to functions $f$ to which a finite integral
$\int f$ can
be assigned\comment{ (just as a series is called `summable' only when
a finite
sum can be assigned to it)}.   But for non-negative functions it is
sometimes convenient to write `$\int f=\infty$' if, in some sense, the
only way in which $f$ fails to be integrable is that the integral is too
large;  that is, $f$ is defined almost everywhere, is $\mu$-virtually
measurable, and either
     
\Centerline{$\{x:x\in\dom f,\,f(x)\ge\epsilon\}$}
     
\noindent includes a set of infinite measure for some $\epsilon>0$, or
     
\Centerline{$\sup\{\int h:h\text{ is simple},\,h\leae f\}=\infty$.}
     
\noindent (Compare 122J.)   Under this rule, we shall still have
     
\Centerline{$\int f_1+f_2=\int f_1+\int f_2$,
\quad$\int cf=c\int f$}
     
\noindent whenever $c\in\coint{0,\infty}$ and $f_1$, $f_2$, $f$ are
non-negative functions for which $\int f_1$, $\int f_2$, $\int f$ are
defined in $[0,\infty]$.
     
We can therefore\comment{ repeat the definition 122M and} say that
     
\Centerline{$\int f_1-f_2=\int f_1-\int f_2$}
     
\noindent whenever $f_1$, $f_2$ are real-valued functions such that
$\int f_1$, $\int f_2$ are defined in $[0,\infty]$ and are not both
infinite\comment{;  the last condition being imposed to avoid the
possibility of being asked to calculate $\infty-\infty$}.
     
We still have the rules that
     
\Centerline{$\int f+g=\int f+\int g$,
\quad$\int(cf)=c\int f$,
\quad$\int|f|\ge|\int f|$}
     
\noindent at least when the right-hand-sides can be interpreted,
allowing $0\cdot\infty=0$, but not allowing any interpretation of
$\infty-\infty$;  and $\int f\le\int g$ whenever both integrals are
defined and $f\leae g$.   \comment{(But of course it is now possible
to have $f\le g$ and $\int
f=\int g=\pm\infty$ without $f$ and $g$ being equal almost everywhere.)}
     
Setting $f^+(x)=\max(f(x),0)$, $f^-(x)=\max(-f(x),0)$ for $x\in\dom f$,
then
     
\Centerline{$\int f=\infty\,\iff\,\int f^+=\infty$ and $f^-$ is
integrable,}
     
\Centerline{$\int f=-\infty\,\iff\,f^+$ is integrable and
$\int f^-=\infty$.}
     
\comment{(For further ideas in this direction, see \S135 below.)}
     
     
\leader{133B}{Functions with exceptional values} It is also convenient
to allow as `integrable' functions $f$ which take occasional values
which are not real\comment{ -- typically, where a formula for $f(x)$
allows the
value `$\infty$' on some convention}.   For such a function I will
write $\int f=\int\tilde f$ if $\int\tilde f$ is defined, where
     
\Centerline{$\dom\tilde f=\{x:x\in\dom f,\,f(x)\in\Bbb R\}$,
\quad$\tilde f(x)=f(x)$ for $x\in\dom\tilde f$.}
     
\comment{\noindent Since in this convention I still require $\tilde f$
to be
defined almost everywhere in $X$, the set $\{x:x\in\dom
f,\,f(x)\notin\Bbb R\}$ will have to be negligible.}
     
\comment{\leader{133C}{Complex-valued functions} All the theory of
measurable and
integrable functions so far developed has been devoted to real-valued
functions.   There are no substantial new ideas required to deal with
complex-valued functions, but perhaps I should spell out some of the
details, since
there are many applications in which complex-valued functions are the
most natural context in which to work.
}%end of comment
     
\leader{133D}{Definitions (a)} Let $X$ be a set and $\Sigma$ a
$\sigma$-algebra of subsets of $X$.   If $D\subseteq X$ and
$f:D\to\Bbb C$ is a function, then we
say that $f$ is {\bf measurable} if its real and imaginary parts
$\Real f$, $\Imag f$ are measurable in the sense of 121B-121C.
     
\medskip
     
{\bf (b)} Let $(X,\Sigma,\mu)$ be a measure space.   If $f$ is a
complex-valued function defined on a conegligible
subset of $X$, we say that $f$ is {\bf integrable} if its real and
imaginary parts are integrable, and then
     
\Centerline{$\int f=\int\Real f+i\int\Imag f$.}
     
\medskip
     
{\bf (c)} Let $(X,\Sigma,\mu)$ be a measure space, $H\in\Sigma$ and $f$
a complex-valued function defined on a subset of $X$.   Then
$\int_Hf$ is $\int(f\restr H)d\mu_H$ if this is defined in the sense of
(b), taking the subspace measure $\mu_H$ to be that of 131A-131B.
     
\ifnum\stylenumber=10\ifresultsonly\eject\fi\fi
     
\leader{133E}{Lemma} (a) If $X$ is a set, $\Sigma$ is a $\sigma$-algebra
of subsets of $X$, and $f$ and $g$ are measurable complex-valued
functions with domains $\dom f$, $\dom g\subseteq X$, then
     
\quad(i) $f+g:\dom f\cap\dom g\to\Bbb C$ is measurable;
     
\quad(ii) $cf:\dom f\to\Bbb C$ is measurable, for every $c\in\Bbb C$;
     
\quad(iii) $f\times g:\dom f\cap\dom g\to\Bbb C$ is measurable;
     
\quad(iv) $f/g:\{x:x\in\dom f\cap\dom g,\,g(x)\ne 0\}\to\Bbb C$ is
measurable;
     
\quad(v) $|f|:\dom f\to\Bbb R$ is measurable.
     
{(b)} If $\sequencen{f_n}$ is a sequence of measurable
complex-valued functions defined on subsets of $X$, then
$f=\lim_{n\to\infty}f_n$ is measurable, if we take $\dom f$ to be
     
$$\eqalign{\{x:x\in\bigcup_{n\in\Bbb N}&\bigcap_{m\ge n}\dom f_m,
  \,\lim_{n\to\infty}f_n(x)\text{ exists in }\Bbb C\}\cr
&=\dom(\lim_{n\to\infty}\Real f_n)
  \cap\dom(\lim_{n\to\infty}\Imag f_n).\cr}$$
     
\proof{{\bf (a)} All are immediate from 121E, if you write down
the formulae for the real and imaginary parts of $f+g,\ldots,|f|$ in terms of the real and imaginary parts of $f$ and $g$.
     
\medskip
     
{\bf (b)} Use 121Fa.
}%end of proof of 133E
     
\leader{133F}{Proposition} Let $(X,\Sigma,\mu)$ be a measure space.
     
(a) If $f$ and $g$ are integrable
complex-valued functions defined
on conegligible subsets of $X$, then $f+g$ and $cf$ are integrable, for
every $c\in\Bbb C$, and $\int f+g=\int f+\int g$, $\int cf=c\int f$.
     
{(b)} If $f$ is a complex-valued function defined on a conegligible
subset of $X$, then $f$ is integrable iff $|f|$ is integrable and $f$ is
$\mu$-virtually measurable.
     
\proof{{\bf (a)} Use 122Oa-122Ob.
     
\medskip
     
{\bf (b)} The point is that $|\Real f|$,
$|\Imag f|\le|f|\le|\Real f|+|\Imag f|$;  now we need only apply 122P an adequate number of times.
}%end of proof of 133F
     
\leader{133G}{Lebesgue's Dominated Convergence Theorem} Let
$(X,\Sigma,\mu)$
be a measure space and $\langle f_n\rangle_{n\in\Bbb N}$ a sequence of
integrable complex-valued functions on $X$ such that
$f(x)=\lim_{n\to\infty}f_n(x)$
exists in $\Bbb C$ for almost every $x\in X$.   Suppose moreover that
there is a real-valued
integrable function $g$ on $X$ such that $|f_n|\leae g$ for each
$n$.   Then $f$ is integrable and $\lim_{n\to\infty}\int f_n$ exists and is equal to $\int f$.
     
\proof{Apply 123C to the sequences $\sequencen{\Real f_n}$,
$\sequencen{\Imag f_n}$.
}%end of proof of 133G
     
\leader{133H}{Corollary} Let $(X,\Sigma,\mu)$ be a measure space and
$\ooint{a,b}$ a non-empty open interval in $\Bbb R$.   Let
$f:X\times\ooint{a,b}\to\Bbb C$ be a function such that
     
\inset{(i) the integral $F(t)=\int f(x,t)dx$ is defined for every
$t\in\ooint{a,b}$;}
     
\inset{(ii) the partial derivative $\pd{f}{t}$ of $f$ with respect to
the second variable is defined everywhere in $X\times\ooint{a,b}$;}
     
\inset{(iii) there is an integrable function $g:X\to\coint{0,\infty}$
such that $|\pd{f}{t}(x,t)|\le g(x)$ for every $x\in X$, $t\in\ooint{a,b}$.}
     
\noindent Then the derivative $F'(t)$ and the integral $\int
\pd{f}{t}(x,t)dx$ exist for every $t\in\ooint{a,b}$, and are equal.
     
\proof{Apply 123D to $\Real f$, $\Imag f$.}
     
     
\leader{133I}{Upper and lower integrals}\comment{ I return now to
real-valued functions.}   Let $(X,\Sigma,\mu)$ be a
measure space and $f$ a real-valued function defined almost everywhere
in $X$.   Its {\bf upper integral} is
     
\Centerline{$\textfont3=\twelveex\overline{\intop}f=\inf\{\int g:g$ is integrable, $f\leae g\}$,}
     
\noindent allowing $\infty$ for $\inf\emptyset$ and $-\infty$ for
$\inf\Bbb R$.   Similarly, the {\bf lower integral} of $f$ is
     
\Centerline{$\textfont3=\twelveex\underline{\intop}f
=\sup\{\int g:g$ is integrable, $f\geae g\}$,}
     
\noindent allowing $-\infty$ for $\sup\emptyset$ and $\infty$ for
$\sup\Bbb R$.
     
\leader{133J}{Proposition} Let $(X,\Sigma,\mu)$ be a measure space.
     
(a) Let $f$ be a real-valued function defined almost everywhere in $X$.
     
\quad (i) If $\overline{\int}f$ is finite, then there is
an integrable $g$ such that $f\leae g$ and
$\int g=\overline{\int}f$.
     
\quad (ii) If $\underline{\int}f$ is finite,
then there is an integrable $h$ such that $h\leae f$ and
$\int h=\underline{\int}f$.
     
{(b)} For any real-valued functions $f$, $g$ defined on conegligible
subsets of $X$ and any $c\ge 0$,
     
\vskip 2pt
     
\quad(i) $\underline{\int}f\le\overline{\int}f$,
     
\vskip2pt
     
\quad (ii) $\overline{\int}f+g\le\overline{\int}f+\overline{\int}g$,
     
\vskip2pt
     
\quad (iii) $\overline{\int}cf=c\overline{\int}f$,
     
\vskip2pt
     
\quad (iv) $\underline{\int}(-f)=-\overline{\int}f$,
     
\vskip2pt
     
\quad (v) $\underline{\int}f+g\ge\underline{\int}f+\underline{\int}g$,
     
\vskip2pt
     
\quad (vi) $\underline{\int}cf=c\underline{\int}f$
     
\vskip2pt
     
\noindent whenever the right-hand-sides do not involve adding $\infty$
to $-\infty$.
     
(c) If $f\leae g$ then $\overline{\int}f\le\overline{\int}g$ and
$\underline{\int}f\le\underline{\int}g$.
     
(d) A real-valued function $f$ defined almost everywhere in $X$ is
integrable iff
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f=\underline{\intop}f=a\in\Bbb R$,}
     
\noindent and in this case $\int f=a$.
     
\proof{{\bf (a)(i)} For each
$n\in\Bbb N$, choose an integrable function $g_n$ such that $f\leae g_n$
and $\int g_n\le\overline{\int}f+2^{-n}$.   Set $h_n=\inf_{i\le
n}g_i$ for each $n$;  then $h_n$ is integrable (because
$|h_n-g_0|\le\sum_{i=0}^n|g_i-g_0|$ on $\bigcap_{i\le n}\dom g_i$), and
$f\leae h_n$, so
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f\le\int h_n\le\int g_n\le\overline{\intop}f+2^{-n}$.}
     
\noindent   By B.Levi's theorem (123A), applied to $\sequencen{-h_n}$,
$g(x)=\inf_{n\in\Bbb N}h_n(x)\in\Bbb R$ for almost every $x$, and
$\int g=\inf_{n\in\Bbb N}\int h_n=\overline{\int}f$;  also, of course,
$f\leae g$.
     
\medskip
     
\quad{\bf (ii)} Argue similarly, or use (b-iv).
     
\medskip
     
{\bf (b)(i)} If either $\underline{\int}f=-\infty$ or
$\overline{\int}f=\infty$ this is trivial.   Otherwise it follows at
once from the fact that if $g$, $h$ are integrable, $g\leae f$ and
$f\leae h$, then $\int g\le\int h$.
     
\medskip
     
\quad{\bf (ii)} If $a>\overline{\int}f+\overline{\int}g$, there must be integrable functions $f_1$, $g_1$ such that $f\leae f_1$,
$g\leae g_1$ and $\int f_1+\int g_1\le a$.   Now $f+g\leae f_1+g_1$, so
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f+g\le\int f_1+g_1\le a$.}
     
\noindent As $a$ is arbitrary, we have the result.
     
\medskip
     
\quad{\bf (iii)($\pmb{\alpha}$)} If $c=0$ this is trivial.   {\bf
($\pmb{\beta}$)} If $c>0$ and $a>c\overline{\int}f$, there must be an
integrable $f_1$ such that $f\leae f_1$ and $c\int f_1\le a$.   Now
$cf\leae cf_1$ and $\int cf_1\le a$, so $\overline{\int}cf\le a$.
As $a$ is arbitrary, $\overline{\int}cf\le c\overline{\int}f$.
{\bf($\pmb{\gamma}$)}  Still supposing that $c>0$, we also have
     
\Centerline{$\textfont3=\twelveex
c\overline{\intop}f=c\overline{\intop}c^{-1}cf
\le cc^{-1}\overline{\intop}cf=\overline{\intop}cf$,}
     
\noindent so we get equality.
     
\medskip
     
\quad{\bf (iv)} This is just because $\int(-f_1)=-\int f_1$ for any
integrable function $f_1$.
     
\medskip
     
\quad{\bf (v)-(vi)} Use (iv) to turn $\underline{\int}$ into
$\overline{\int}$, and apply (ii) or (iii).
     
\medskip
     
{\bf (c)} These are immediate from the definitions, because (for
instance) if $g\leae h$ then $f\leae h$.
     
\medskip
     
{\bf (d)} If $f$ is integrable, then
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f=\int f=\underline{\intop}f$}
     
\noindent by 122Od.   If
$\overline{\int}f=\underline{\int}f=a\in\Bbb R$, then, by (a), there are integrable $g$, $h$ such that $g\leae f\leae h$
and $\int g=\int h=a$, so that $g\eae h$, by 122Rc, and $g\eae f\eae h$ and $f$ is integrable, by 122Rb.
}%end of proof of 133J
     
\comment{\medskip
     
\noindent{\bf Remark} I hope that the formulae here remind you of
$\limsup$, $\liminf$.}
     
\leader{133K}{Convergence theorems for upper
\dvrocolon{integrals}}\comment{ We have the
following versions of B.Levi's theorem and Fatou's Lemma.
     
\medskip
     
\noindent}{\bf Proposition} Let $(X,\Sigma,\mu)$ be a measure space, and
$\sequencen{f_n}$ a sequence of real-valued functions defined almost
everywhere in $X$.
     
(a) If, for each $n$, $f_n\leae f_{n+1}$, and
$-\infty<\sup_{n\in\Bbb N}\overline{\int}f_n<\infty$, then
$f(x)=\sup_{n\in\Bbb N}f_n(x)$ is defined in $\Bbb R$ for almost every
$x\in X$, and
$\overline{\int}f=\sup_{n\in\Bbb N}\overline{\int}f_n$.
     
(b) If, for each $n$, $f_n\geae 0$, and
$\liminf_{n\to\infty}\overline{\int}f_n<\infty$, then
$f(x)=\liminf_{n\to\infty}f_n(x)$ is defined in $\Bbb R$ for almost
every $x\in X$, and
$\overline{\int}f\le\liminf_{n\to\infty}\overline{\int}f_n$.
     
\proof{{\bf (a)} Set $c=\sup_{n\in\Bbb N}\overline{\int}f_n$.    For
each $n$, there is an integrable function $g_n$ such that $f_n\leae g_n$
and $\int g_n=\overline{\int}f_n$ (133Ja).   Set $g'_n=\min(g_n,g_{n+1})$;
then $g'_n$ is integrable and $f_n\leae g'_n\leae g_n$, so
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f_n\le\int g'_n
\le\int g_n=\overline{\intop}f_n$}
     
\noindent and $g'_n$ must be equal to $g_n$ a.e.   Consequently
$g_n\leae g_{n+1}$, for each $n$, while
$\sup_{n\in\Bbb N}\int g_n=c<\infty$.
By B.Levi's theorem, $g=\sup_{n\in\Bbb N}g_n$ is defined, as a
real-valued function, almost everywhere in $X$, and $\int g=c$.   Now of
course $f(x)$ is defined, and not greater than $g(x)$, for all $x\in\dom
g\cap\bigcap_{n\in\Bbb N}\dom f_n$ such that $f_n(x)\le g_n(x)$ for
every $n$, that is, for almost every $x$;  so
$\overline{\int}f\le\int g=c$.   On the other hand, $f_n\leae f$, so $\overline{\int}f_n\le\overline{\int}f$,
for every $n\in\Bbb N$;  it follows that $\overline{\int}f$ must be at
least $c$, and is therefore equal to $c$, as required.
     
\medskip
     
{\bf (b)} The argument follows that of 123B.   Set
$c=\liminf_{n\to\infty}\overline{\int}f_n$.   For each $n$, set
$g_n=\inf_{m\ge n}f_n$;  then $\overline{\int}g_n\le\inf_{m\ge
n}\overline{\int}f_m\le c$.   We have $g_n(x)\le g_{n+1}(x)$ for every
$x\in\dom g_n$, that is, almost everywhere, for each $n$;  so, by (a),
     
\Centerline{$\textfont3=\twelveex\overline{\intop}g
=\sup_{n\in\Bbb N}\overline{\intop}g_n\le c$,}
     
\noindent where
     
\Centerline{$g=\sup_{n\in\Bbb N}g_n\eae\liminf_{n\to\infty}f_n$,}
     
\noindent so $\overline{\int}\liminf_{n\to\infty}f_n\le c$, as claimed.
}%end of proof of 133K
     
     
\exercises{
\leader{133X}{Basic exercises $\pmb{>}$(a)}
%\spheader 133Xa
Let $(X,\Sigma,\mu)$ be a measure space, and $f:X\to\coint{0,\infty}$ a measurable function.   Show that
     
$$\eqalign{\int fd\mu
&=\sup_{n\in\Bbb N}2^{-n}\sum_{k=1}^{4^n}\mu\{x:f(x)\ge 2^{-n}k\}\cr
&=\lim_{n\to\infty}2^{-n}\sum_{k=1}^{4^n}\mu\{x:f(x)\ge 2^{-n}k\}\cr}$$
     
\noindent in $[0,\infty]$.
%133A
     
\spheader 133Xb
Let $(X,\Sigma,\mu)$ be a measure space and $f$
a complex-valued function defined on a subset of $X$.   (i) Show that if
$E\in\Sigma$, then $f\restr E$ is $\mu_E$-integrable iff $\tilde f$ is
$\mu$-integrable, writing $\mu_E$ for the subspace measure on $E$ and
$\tilde f(x)=f(x)$ if $x\in E\cap\dom f$, $0$ if $x\in X\setminus E$;
and in this case $\int_Efd\mu_E=\int \tilde fd\mu$.   (ii) Show that if
$E\in\Sigma$ and $f$ is defined $\mu$-almost everywhere, then
$f\restr E$ is
$\mu_E$-integrable iff $f\times\chi E$ is $\mu$-integrable, and in this
case $\int_Ef=\int f\times\chi E$.   (iii) Show that if $\int_Ef=0$ for
every $E\in\Sigma$, then $f\eae 0$.
%133E
     
\spheader 133Xc Suppose that $(X,\Sigma,\mu)$ is a measure space
and that $G$ is an open subset of $\Bbb C$, that is, a set such that for
every $w\in G$ there is a $\delta>0$ such that
$\{z:|z-w|<\delta\}\subseteq G$.   Let $f:X\times G\to\Bbb C$ be a
function, and suppose that the derivative $\pd{f}{z}$ of $f$ with
respect to the
second variable exists for all $x\in X$, $z\in G$.   Suppose
moreover that (i) $F(z)=\int f(x,z)dx$ exists for every
$z\in G$ (ii) there is an integrable function $g$ such that
$|\pd{f}{z}(x,z)|\le g(x)$ for every $x\in X$, $z\in G$.
Show that the derivative $F'$ of $F$ exists everywhere in $G$, and
$F'(z)=\int\pd{f}{z}(x,z)dx$ for every $z\in G$.   ({\it Hint\/}:  you
will need to check that
$|f(x,z)-f(x,w)|\le|z-w|g(x)$ whenever $x\in X$, $z\in G$ and $w$ is close to $z$.)
%133F
     
\sqheader 133Xd Let $f$ be a complex-valued function defined
almost everywhere
on $\coint{0,\infty}$, endowed as usual with Lebesgue measure.   Its
{\bf Laplace transform} is the function $F$ defined by writing
     
\Centerline{$F(s)=\int_0^{\infty}e^{-sx}f(x)dx$}
     
\noindent for all those complex numbers $s$ for which the integral is
defined in $\Bbb C$.
     
\quad(i) Show that if $s\in\dom F$ and $\Real s'\ge\Real s$ then
$s'\in\dom F$ (because $|e^{-s'x}e^{sx}|\le 1$ for all $x$).
     
\quad(ii) Show that $F$ is analytic (that is, differentiable as a
function of a complex variable) on the interior of its domain.
\Hint{133Xc.}
     
\quad(iii) Show that if $F$ is defined anywhere then $\lim_{\Real
s\to\infty}F(s)=0$.
     
\quad(iv) Show that if $f$, $g$ have Laplace transforms $F$, $G$ then
the Laplace transform of $f+g$ is $F+G$, at least on $\dom F\cap \dom
G$.
%133Xc 133F
     
\sqheader 133Xe Let $f$ be an integrable complex-valued function
defined almost everywhere in $\Bbb R$, endowed as usual with Lebesgue
measure.   Its {\bf Fourier transform} is the function $\varhatf$
defined by
     
\Centerline{$\varhatf(s)=\Bover{1}{\sqrt{2\pi}}
\int_{-\infty}^{\infty}e^{-isx}f(x)dx$}
     
\noindent for all real $s$.
     
\quad(i) Show that $\varhatf$ is continuous.   \Hint{use Lebesgue's
Dominated Convergence Theorem on sequences of the form
$f_n(x)=e^{-is_nx}f(x)$.}
     
\quad(ii) Show that if $f$, $g$ have Fourier transforms $\varhatf$,
$\varhat g$ then the Fourier transform of $f+g$ is $\varhatf+\varhat g$.
     
\quad(iii) Show that if $\int xf(x)dx$ exists then $\varhatf$ is
differentiable, with
$\varhatf\vthsp'(s)=-\Bover{i}{\sqrt{2\pi}}\int xe^{-isx}f(x)dx$ for
every $s$.
%133Xc 133F
     
\spheader 133Xf Let $(X,\Sigma,\mu)$ be a measure space and
$\sequencen{f_n}$ a sequence of real-valued functions each defined
almost everywhere in $X$.   Suppose that there is an integrable
real-valued function $g$ such that $|f_n|\leae g$ for each $n$.
Show that
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}\liminf_{n\to\infty}f_n
\le\liminf_{n\to\infty}\overline{\intop}f_n$,\quad
$\textfont3=\twelveex\underline{\intop}\limsup_{n\to\infty}
\ge\limsup_{n\to\infty}\underline{\intop}f_n$.}
%133K
     
\leader{133Y}{Further exercises (a)}
%\spheader 133Ya
Use the ideas of 133C-133H to
develop a theory of measurable and
integrable functions taking values in $\BbbR^r$, where $r\ge 2$.
%133H
     
\spheader 133Yb Let $X$ be a set and $\Sigma$ a $\sigma$-algebra
of subsets of $X$.   Let $Y$ be a subset of $X$ and $f:Y\to\Bbb C$ a
$\Sigma_Y$-measurable function, where $\Sigma_Y=\{E\cap Y:E\in\Sigma\}$.
Show that there is a $\Sigma$-measurable function $\tilde f:X\to\Bbb C$
extending $f$.  \Hint{121I.}
%133F
     
\spheader 133Yc Let $f$ be an integrable complex-valued function
defined almost everywhere in $\BbbR^r$, endowed as usual with Lebesgue
measure, where $r\ge 1$.   Its {\bf Fourier transform} is the function
$\varhatf$ defined by
     
\Centerline{$\varhatf(s)=\Bover{1}{(\sqrt{2\pi})^r}
\int_{-\infty}^{\infty}
e^{-is\dotproduct x}f(x)dx$}
     
\noindent for all $s\in\BbbR^r$, writing $s\dotproduct x$ for
$\sigma_1\xi_1+\ldots+\sigma_r\xi_r$ if $s=(\sigma_1,\ldots,\sigma_r)$,
$x=(\xi_1,\ldots,\xi_r)\in\BbbR^r$.
     
\quad(i) Show that $\varhatf$ is continuous.   \Hint{use
Lebesgue's Dominated Convergence Theorem on sequences of the form
$f_n(x)=e^{-is_n\dotproduct x}f(x)$.}
     
\quad(ii) Show that if $f$, $g$ have Fourier transforms $\varhatf$,
$\varhat g$ then the Fourier transform of $f+g$ is $\varhatf+\varhat g$.
     
\quad (iii) Show that if $\int\|x\|f(x)dx$ exists
(taking $\|x\|=\sqrt{\xi_1^2+\ldots+\xi_r^2}$ if
$x=(\xi_1,\dots,\xi_r)$), then $\varhatf$ is differentiable, with
     
\Centerline{$\Pd{\varhatf}{\sigma_k}(s)
=-\Bover{i}{(\sqrt{2\pi})^r}\int\xi_ke^{-is\dotproduct x}f(x)dx$}
     
\noindent for every $s\in\BbbR^r$, $k\le r$.
%133Xe 133F
     
\spheader 133Yd Recall the definition of `quasi-simple'
function from 122Yd.   Show that for any measure space $(X,\Sigma,\mu)$ and any real-valued function $f$ defined almost everywhere in $X$,
     
\Centerline{$\textfont3=\twelveex
\overline{\intop}f=\inf\{\int g:g$ is quasi-simple, $f\leae g\}$,}
     
\Centerline{$\textfont3=\twelveex
\underline{\intop}f=\sup\{\int g:g$ is quasi-simple, $f\geae g\}$,}
     
\noindent allowing $\infty$ for $\inf\emptyset$ and $\sup\Bbb R$ and
$-\infty$ for $\inf\Bbb R$ and $\sup\emptyset$.
%133J
     
\spheader 133Ye State and prove a similar result concerning the
`pseudo-simple' functions of 122Ye.
%133Yd 133J
}%end of exercises
     
\endnotes{
\Notesheader{133} I have spelt this section out in
detail, even though there is nothing that can really be called a new
idea in it, because it gives us an opportunity to review the previous
work, and because the manipulations which are by now, I hope, becoming
`obvious' to you are in fact justifiable only through difficult
theorems, and I believe that it is at least some of the time right to
look back to the exact points at which justifications were written out.
     
You may have noticed similarities between results involving `upper
integrals', as described here, and those of \S132 concerning `outer
measure' (132Ae and 133Ka, for instance, or 132Xe and 133Kb).   These
are not a coincidence;  an explanation of sorts can be found in 252Yh in
Volume 2.
}%end of notes
     
\discrpage
     
    
