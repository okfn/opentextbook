\filename{mt10.tex}
\versiondate{16.6.01}
%\def\vtmp#1{#1}
%\def\vtmpb#1{(#1)}
%\def\vtmp#1{}
\def\vtmpb#1{}

\newif\iflargelogo

\def\pagereference#1#2{\hfill\hbox{\ifresultsonly#2\qquad \hskip 1truein
                                                 \else#1\qquad \fi}}
\long\def\section#1#2#3#4#5#6{\ifresultsonly
    \vbox{\hskip1truein\qquad #1 #2\pagereference{#4}{#5}}
  \else\vskip 2pt plus 0pt minus 0pt
    \vbox{\qquad #1 #2\pagereference{#4}{#5}\par
    \abstr{#6}
    }\vskip -1pt plus 0pt minus 0pt
  \fi}
\long\def\chapintrosection#1#2#3{\ifresultsonly
    \hskip1truein\qquad Introduction\pagereference{#2}{#3}
  \else\vskip 1pt plus 1pt minus 0pt
    \qquad Introduction\pagereference{#2}{#3}
    \vskip 0pt plus 1pt minus 0pt
  \fi}

\def\abstr#1{\ifresultsonly{}
   \else{\advance\leftskip by\parindent
     \advance\rightskip by\parindent
     \advance\rightskip by\parindent
     {\advance\leftskip by\parindent
      \advance\rightskip by\parindent
     {\advance\leftskip by\parindent
     {\advance\leftskip by\parindent
     {\vskip1pt plus 0pt minus0pt
     \parindent=0pt
     \smallerfonts #1
     \vskip1pt plus 0pt minus0pt }}}}}\fi}

\Loadfourteens
\hyphenation{Dede-kind}

\vbox{\vskip 2truein

\centerline{\fourteenbf MEASURE THEORY}
}%end of vbox

\bigskip

\centerline{\fourteenbf Volume 1}

%\bigskip

%\centerline{The Irreducible Minimum}

\ifresultsonly
   \bigskip

   \centerline{\fourteenit (results only)}
   \fi

\vskip 2truein

\centerline{\fourteenrm D.H.Fremlin}

\vskip 2truein

\largelogofalse
\input mtlogo

\vfill\eject

\vbox{\vskip 2truein

\noindent By the same author:

{\it Topological Riesz Spaces and Measure Theory}, Cambridge University
Press, 1974.

{\it Consequences of Martin's Axiom}, Cambridge University Press, 1982.
}%end of vbox

\bigskip

\noindent Companions to the present volume:

{\it Measure Theory}, vol.\ 1, \ifresultsonly full version;\else results
only;\fi

{\it Measure Theory}, vol.\ 2, Torres Fremlin, 2001;

{\it Measure Theory}, vol.\ 3, Torres Fremlin, 2002;

{\it Measure Theory}, vol.\ 4, Torres Fremlin, 2003.

\vfill

\Centerline{\it First printing May 2000}

\Centerline{\it Second printing corrected September 2001}

\Centerline{\it Third printing corrected February 2004}

\vfill\eject
\def\Loadtwenties{
      \font\twentyrm=cmr10 scaled \magstep4
      \font\twentybf=cmbx10 scaled \magstep4
      \font\twentyit=cmsl10 scaled \magstep4}

\Loadtwenties

\vbox{\vskip 1.5truein

\centerline{\twentybf MEASURE THEORY}
}%end of vbox

\bigskip\bigskip

\centerline{\twentybf Volume 1}

\bigskip\bigskip

\centerline{\twentyrm The Irreducible Minimum}

\ifresultsonly
   \bigskip\bigskip

   \centerline{\twentyit (results only)}
   \fi

\vskip 1truein
\vfill

\centerline{\twentyrm D.H.Fremlin}

\bigskip\bigskip

\centerline{\fourteenit Research Professor in Mathematics, University of
Essex}

\vskip 1truein
\vfill

\largelogotrue
\input mtlogo

\eject

\vbox{\vskip 5truecm

\Centerline{\fourteenit Dedicated by the Author}

\Centerline{\fourteenit to the Publisher}
}%end of vbox

\vfill

\inset{\noindent This book may be ordered from the
publisher at the address below.   For price and means of payment see the
author's Web page {\tt
http://www.essex.ac.uk/maths/staff/\penalty-100fremlin/mtsales.htm}, or
enquire
from {\tt fremdh\@essex.ac.uk}.
}%end of inset

\vfill

\bigskip

\noindent First published in 2000

by Torres Fremlin, 25 Ireton Road, Colchester CO3 3AT, England

\medskip

\noindent\copyright\ D.H.Fremlin 2000

\noindent The right of D.H.Fremlin to be identified as author of this
work has been asserted in accordance with the Copyright, Designs and
Patents Act 1988.
This work is issued under the terms of the Design Science License as
published in
{\tt http://dsl.org/copyleft/dsl.txt}.
For the source files see {\tt
http://www.essex.ac.uk/\penalty-
100maths/staff/fremlin/mt1.2004/index.htm}.

\medskip

\noindent Library of Congress classification QA312.F72

\medskip

\noindent AMS 2000 classification 28A99

\medskip

\noindent ISBN \ifresultsonly 0-9538129-1-X \else 0-9538129-0-1\fi

\medskip

\noindent Typeset by \AmSTeX

\medskip

\noindent Printed in England by Biddles Short Run Books, King's Lynn

\eject

\pageno=5

\vbox{\ifresultsonly\vskip1truein\fi

\centerline{\bf Contents}}

%\bigskip
\medskip

\ifresultsonly\hskip 1truein\fi
General Introduction \pagereference{6}{6}

\input mt01

\wheader{}{46}{22}{22}{300pt}

\gdef\topparagraph{}
   \gdef\bottomparagraph{{\it General introduction}}

\bigskip

\noindent{\bf General Introduction}

\medskip

In this treatise I aim to give a
comprehensive description of modern abstract measure theory, with some
indication of its principal applications.   The first two volumes are
set at an introductory level;  they are intended for students with a
solid grounding in the concepts of real analysis, but possibly with
rather limited detailed knowledge.   The emphasis throughout is on the
mathematical ideas involved, which in this subject are mostly to be
found in the details of the proofs.

My intention is that the book should be usable both as a first
introduction to the subject and as a reference work.   For the sake of
the first aim, I try to limit the ideas of the early volumes to those
which are really essential to the development of the basic theorems.
For the sake of the second aim, I try to express these ideas in their
full natural generality, and in particular I take care to avoid
suggesting any unnecessary restrictions in their applicability.   Of
course these principles are to to some extent contradictory.
Nevertheless, I find that most of the time they are very nearly
reconcilable, {\it provided} that I indulge in a certain degree of
repetition.   For instance, right at the beginning, the puzzle arises:
should one develop Lebesgue measure first on the real line, and then in
spaces of higher dimension, or should one go straight to the
multidimensional case?   I believe that there is no single correct
answer to this question.   Most students will find the one-dimensional
case easier, and it therefore seems more appropriate for a first
introduction, since even in that case the technical problems can be
daunting.   But certainly every student of measure theory must at a
fairly early stage
come to terms with Lebesgue area and volume as well as length;
and with the correct formulations, the multidimensional case differs
from the one-dimensional case only in a definition and a (substantial)
lemma.   So what I have done is to write them both out (in
\S\S114-115), so that you can pass over the higher dimensions at first
reading (by omitting \S115) and at the same time have a complete
and uncluttered argument for them (if you omit section \S114).
In the same spirit, I have been uninhibited, when setting out exercises,
by the fact that many of the results I invite students to look for will
appear in later chapters;  I believe that throughout mathematics one has
a better chance of understanding a theorem if one has previously
attempted something similar alone.

As I write this Introduction (June 2001), the plan of the work is
as follows:

\medskip

\qquad\qquad Volume 1:  The Irreducible Minimum

\qquad\qquad Volume 2:  Broad Foundations

\qquad\qquad Volume 3:  Measure Algebras

\qquad\qquad Volume 4:  Topological Measure Spaces

\qquad\qquad Volume 5:  Set-theoretic Measure Theory.

\medskip

\noindent Volume 1 is intended for those with no prior knowledge of
measure theory, but competent in the elementary techniques of real
analysis.  I hope that it will be found useful by undergraduates meeting
Lebesgue measure for the first time.    Volume 2 aims to lay out some of
the fundamental results of pure measure theory (the
Radon-Nikod\'ym theorem, Fubini's theorem), but also gives short
introductions to some of the most important applications of measure
theory (probability theory, Fourier analysis).   While I should like to
believe that most of it is written at a level accessible to anyone who
has mastered the contents of Volume 1, I should not myself have the
courage to try to cover it in an undergraduate course, though I would
certainly attempt to include some parts of it.   Volumes 3 and 4 are
set at a rather higher level, suitable to postgraduate courses;  while
Volume 5 will assume a wide-ranging competence over large parts of
analysis and set theory.

There is a disclaimer which I ought to make in a place where you might
see it in time to avoid paying for this book.   I make no attempt to
describe the history of the subject.   This is not because I think the
history uninteresting or unimportant;  rather, it is because I have no
confidence of saying anything which would not be seriously misleading.
Indeed I have very little confidence in anything I have ever read
concerning the history of ideas.   So while I am happy to honour the
names of Lebesgue and Kolmogorov and Maharam in more or less appropriate
places, and I try to include in the bibliographies the works which I
have myself consulted, I leave any consideration of the details to those
bolder and better qualified than myself.

The work as a whole is not yet complete;  and when it is finished, it
will undoubtedly be too long to be printed as a single volume in any
reasonable format.   I am therefore publishing it one part at a time.
However, drafts of most of the rest are available from my website;  see
{\tt http://www.essex.ac.uk/maths/staff/\penalty-100fremlin/mtcont.htm}
for detailed contents with links to \TeX files.   For the time being,
at least, printing will be in short runs.   I hope that readers will be
energetic in commenting on errors and omissions, since it should be
possible to correct these relatively promptly.   An inevitable
consequence of this is that paragraph references may go out of date
rather quickly.   I shall be most flattered if anyone chooses to rely on
this book as a source for basic material;  and I am willing to attempt
to maintain a concordance to such references, indicating where migratory
results have come to rest for the moment, if authors will supply me with
copies of papers which use them.

This volume is available in two forms:  `full' and `results-only'.
\ifresultsonly
The one you now have in your hand is the `results-only' version of
Volume 1.   I hope that you will find that it is useful for reference
and revision, while being cheaper and easier to handle than the `full'
version.   It is not however intended as a self-teaching text.
\else
The one you now have in your hand is the `full' version of Volume 1;
the `results-only' version omits proofs, exercises and notes.   I hope
that it will be found useful for reference and revision, while being
cheaper and easier to handle.   Only a quite exceptional student would
be able to learn the subject from the abridged version without a great
deal of help;  but a teacher who wished to use this text as the basis
for a course might find it useful to ask students to obtain the
results-only version.
\fi

I mention some minor points concerning the layout of the material.
\ifresultsonly\else
Most sections conclude with lists of `basic exercises' and
`further exercises', which I hope will be generally instructive and
occasionally entertaining.   How many of these you should attempt must
be for you and your teacher, if any, to decide, as no two students will
have quite the same needs.   I mark with a $\pmb{>}$ those which seem to
me to be particularly important.   But while you may not need to write
out solutions to all the `basic exercises', if you are in any doubt
as to your capacity to do so you should take this as a warning to slow
down a bit.   The `further exercises' are unbounded in difficulty,
and are unified only by a presumption that each has at least one
solution based on ideas already introduced.

\fi
The impulse to write this treatise is in large part a desire to present
a unified account of the subject.   Cross-references are correspondingly
abundant and wide-ranging.   In order to be able to refer freely across
the whole
text, I have chosen a reference system which gives the same code name to
a paragraph wherever it is being called from.   Thus 132E is the fifth
paragraph in the second section of Chapter 13, which is itself the
third chapter of this volume, and is referred to by that name
throughout.
Let me emphasize that cross-references are supposed to help the reader,
not distract him.   Do not take the interpolation `(121A)' as an
instruction, or even a recommendation, to turn back to \S121.
If you are happy with an argument as it stands,
independently of the reference, then carry on.   If, however, I seem to
have made rather a large jump, or the notation has suddenly become
opaque, local cross-references may help you to fill in the gaps.

Each volume will have an appendix of `useful facts', in which I set
out material which is called on somewhere in that volume, and which I do
not feel I can take for granted.   Typically the arrangement of material
in these appendices is directed very narrowly at the particular
applications I have in mind, and is unlikely to be a satisfactory
substitute for conventional treatments of the topics touched on.
Moreover, the ideas may well be needed only on rare and isolated
occasions.   So as a rule I
recommend you to ignore the appendices until you have some
direct reason to suppose that a fragment may be useful to you.

During the extended gestation of this project I have been helped by many
people, and I hope that my friends and colleagues will be pleased when
they recognise their ideas scattered through the pages below.   But I am
especially grateful to those who have taken the trouble to read through
earlier drafts and comment on obscurities and errors.   In particular, I
should like to single out F.Nazarov, whose thorough reading of the
present volume corrected many faults.

\wheader{{\it Introduction to Volume 1}}{18}{6}{6}{100pt}

\noindent{\bf Introduction to Volume 1}

\medskip

\input mt1

\bigskip

\noindent{\bf Note on second and third printings}

\medskip

For the second printing of this volume I made a few corrections, with a
handful of new exercises.   For the third printing I have done the same;
in addition, I have given an elementary extra result and formal
definitions of some almost standard terms.   I have also allowed myself,
in a couple of cases, to rearrange a set of exercises into what now
seems to me a more natural order.

