Author(s): Charles M. Grinstead, J. Laurie Snell

Title: Introduction to Probability

Source URL: <http://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/book.html>

Download URL(s): <http://math.dartmouth.edu/~prob/prob/prob.tar.gz>

License(s): GFDL

Tags: mathematics


#############################################################
# Mirrored at Open Text Book <http://www.opentextbook.org>. #
#############################################################
