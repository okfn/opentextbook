\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{0.1,0.6,0.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate Methods) Week 10 Factor Analysis}
\author{Paul Hewson}
\date{11th December 2008}


\newtheorem{df}{Definition}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}

\maketitle

\tableofcontents

\section{Outline}

\subsection{Aims of this week}

\begin{itemize}
\item To introduce ``Factor analysis''
\item To consider the ``principal component'' factoring method in some detail, describe ``principal factoring'' and introduce ``maximum likelihood factor analysis''
\item To make sure we are clear on the difference between factor analysis and principal co-ordinate analysis
\item To consider the number and r\^ole of factors
\item To consider factor rotation as an aid to interpretation
\end{itemize}



\newpage

\subsection{R\^ole of factor analysis}


\begin{itemize}
\item Most development outside the statistical community, especially Psychometrics
\item Aim is to produce a small number of ``latent variables'' which have some substantive interpretation
\item There are related techniques for discrete responses which we don't cover
\end{itemize}



\newpage

\subsubsection{Factor analysis versus principal component analysis}

\begin{itemize}
\item The (one) aim of principal components analysis is to produce a projection explaining the greatest variance: factor analysis is about modelling the covariance structure
\item Factor analysis is based on a probability model
\item \emph{Reification}: if a number of observed correlated variables are all manifestations of some underlying phenomenon, the claim is that factor analysis recovers this underlying structure
\end{itemize}


\newpage

\section{Fitting the Factor analysis model}


None of these methods leads to a unique solution!

\begin{itemize}
\item Principal component factor analysis
\begin{itemize}
\item Principal factoring (iterative modification)
\end{itemize}
\item Maximum likelihood
\end{itemize}

\emph{\color{red}Do not confuse PCFA with pca!}

\newpage

\subsection{Idealised factor analysis model?}

\begin{picture}(100,200)(0,0)

\put(0,150){\fbox{f1}}
\put(30,150){\vector(2,-1){95}} 
\put(30,150){\vector(2,1){95}} 
\put(30,150){\vector(1,-1){95}} 

\put(0,50){\fbox{f2}}
\put(30,50){\vector(2,-1){95}} 
\put(30,50){\vector(2,1){95}} 
\put(30,50){\vector(1,1){95}}  

\put(150,0){\fbox{X1}}
\put(150,100){\fbox{X2}}
\put(150,200){\fbox{X3}}

\end{picture}

\newpage


\subsection{Some key words}

\begin{itemize}
\item  We have $p$ observed or \emph{manifest} variables
\item We wish to represent these by $q<p$ mutually uncorrelated common factors.
\item We have some uncorrelated residuals  specific to each of the observed variables, which is not correlated with any of the remaining $p-1$ variables. That part of the variance of the manifest variable explained by the $q<p$ latent variables is known as the \emph{commonality}.   
\item It is possible to \emph{rotate} the $q$ axes of common factors to new orthogonal or oblique axes
\end{itemize}


\newpage

\subsubsection{The factor model}

The orthogonal model underlying Factor Analysis can be described as follows:

\begin{displaymath}
\label{factanal}
{\color{blue}\boldsymbol{x} = \boldsymbol{\mu} + \boldsymbol{\Gamma} \boldsymbol{\phi} + \boldsymbol{\zeta}}
\end{displaymath}

\begin{itemize}
\item $\boldsymbol{x}$ is an $1 \times p$ random vector.   
\item $\boldsymbol{\mu}$ represents a vector of unknown constants (mean values),\item  $\boldsymbol{\Gamma}$ is an unknown $p \times q$ matrix of constants referred to as the \textit{loadings}.   
\item $\boldsymbol{\phi}$ is a $q \times 1$ unobserved random vector referred to as the \textit{scores} assumed to have mean $\boldsymbol{0}$ and covariance $\boldsymbol{\Sigma}_{\phi}$, 
\item $\boldsymbol{\Sigma}_{\phi} = \boldsymbol{I}$.   
\item $\boldsymbol{\zeta}$ is $1 \times p$ unobserved random error vector having mean $\boldsymbol{0}$ and by assumption a diagonal covariance $\boldsymbol{\psi}$ referred to as the \textit{uniqueness} or \textit{specific variance}.   
\end{itemize}


\newpage

\subsubsection{Consequences}

 With the above assumptions:

\begin{itemize}
\item, $cov(\boldsymbol{\phi}, \boldsymbol{\zeta}) = 0$, 
\item if $\boldsymbol{\Sigma}_{\phi} = \boldsymbol{I}$ then $cov(\boldsymbol{x}, \boldsymbol{\phi}) = \boldsymbol{\Gamma}$.   
\end{itemize}

And our \emph{manifest} variables have the following distributional form:

\begin{displaymath}
\boldsymbol{x} \sim Normal(\boldsymbol{\mu}, \boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi})
\end{displaymath}

\newpage

It may be slightly clearer to consider the way a vector of observations $\boldsymbol{x} = x_{1}, \ldots, x_{p}$ are modelled in factor analysis:

\begin{eqnarray*}
x_{1} &=& \mu_{1} + \sum_{k=1}^{q} \gamma_{1k} \phi_{k}  + \zeta_{1}\\
x_{2} &=& \mu_{2} + \sum_{k=1}^{q} \gamma_{2k} \phi_{k}  + \zeta_{2}\\
&\vdots&\\
x_{p} &=& \mu_{p} + \sum_{k=1}^{q} \gamma_{pk} \phi_{k}  + \zeta_{p}
\end{eqnarray*}


\newpage

\subsubsection{A key result}


\begin{equation}
\label{communalities}
var(x_{j}) = \underbrace{\gamma_{j1}^{2} + \gamma_{j2}^{2} + \ldots + \gamma_{jq}^{2}}_{communalities} + \underbrace{var(\zeta_{j})}_{Uniqueness}
\end{equation} 

A lot of parameters for the amount of data!


\begin{itemize}
\item The covariance matrix $\boldsymbol{\Sigma}$ has $p(p+1)/2$ parameters, 
\item The factor model $\boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi}$ has $qp - q(q-1)/2 + p$ parameters.  
\end{itemize}

So, $p(p+1)/2 \geq qp - q(q-1)/2 + p$, or:

\begin{equation}
\label{qp}
q \leq \frac{2p + 1 - \sqrt{8p-1}}{2}
\end{equation}



This gives some maximum values of $q$ for given values of $p$:


\begin{tabular}{r|rrrrrrrrrr}
\hline
$p$ &    1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
max $q$ & 0 & 0 & 1 & 1 & 2 & 3 & 3 & 4 & 5 & 6 \\
\hline
\end{tabular}

(any problems with the schematic put up earlier)


\newpage

\subsubsection{On a related note \ldots}

Degrees of freedom after fitting a $q$ factor model:

\begin{equation}
\label{dffact}
df = \frac{p(p+1)}{2} - qp + \frac{q(q-1)}{2} - p = \frac{(p-q)^{2} - (d+m)}{2}
\end{equation}

\newpage


\subsection{Centering or scaling}

Either centre the data:

\begin{equation}
\label{facentre}
x_{j} - \mu_{j} = \sum_{k=1}^{q} \gamma_{k} \phi_{k} + \zeta_{j}; j = 1, \ldots, p
\end{equation}

Or even standardise (i.e. model the correlation matrix rather than the covariance matrix):   

\begin{equation}
\label{fastandardise}
\frac{x_{j} - \mu_{j}}{\sigma_{jj}} = \sum_{k=1}^{q} \gamma_{k} \phi_{k} + \zeta_{j}; j = 1, \ldots, p
\end{equation}


So, regardless of the data matrix used, factor analysis is essentially a model for $\boldsymbol{\Sigma}$, the covariance matrix of $\boldsymbol{x}$, 

\begin{displaymath}
\label{covdecomp}
\boldsymbol{\Sigma} = \boldsymbol{\Gamma}\boldsymbol{\Gamma}^{T} + \boldsymbol{\psi}
\end{displaymath}


\newpage

\subsection{Factor indeterminacy}

\subsubsection{Identifiability}

\begin{itemize}
\item A very indeterminate model, 
\item specifically it is unchanged if we replace $\boldsymbol{\Gamma}$ by $\boldsymbol{K} \boldsymbol{\Gamma}$ for any orthogonal matrix $\boldsymbol{K}$.   
\item This can be turned to our advantage, with sensible choice of a suitable orthogonal matrix $\boldsymbol{K}$ we can achieve a rotation that may yield a more interpretable answer.  
\end{itemize}


\newpage

\subsection{Strategy for factor analysis}

 To fit the model, we therefore need to:

\begin{itemize}
\item Estimate the number of common factors $q$.   
\item Estimate the factor loadings $\boldsymbol{\Gamma}$
\item Estimate the specific variances $\boldsymbol{\psi}^{2}$
\item On occasion, estimate the factor scores $\boldsymbol{\phi}$
\end{itemize}



\newpage

\section{Principal component extraction}


We have already used the spectral decomposition to obtain one possible factoring of the covariance matrix $\boldsymbol{\Sigma}$.

\begin{displaymath}
\boldsymbol{\Sigma} = \boldsymbol{E} \boldsymbol{\Lambda}\boldsymbol{E}^{T}
\end{displaymath}


\begin{eqnarray*}
\boldsymbol{\Sigma} &=& \lambda_{1} \boldsymbol{e}_{1} \boldsymbol{e}_{1}^{T} +  \lambda_{2} \boldsymbol{e}_{2} \boldsymbol{e}_{2}^{T} + \ldots  \lambda_{p} \boldsymbol{e}_{p} \boldsymbol{e}_{p}^{T} \\
 &=& \left( \sqrt{\lambda_{1}} \boldsymbol{e}_{1}, \sqrt{\lambda_{2}} \boldsymbol{e}_{2}, \ldots, \sqrt{\lambda_{p}} \boldsymbol{e}_{p} \right) \left( \begin{array}{c} \sqrt{\lambda_{1}} \boldsymbol{e}_{1} \\  \sqrt{\lambda_{2}} \boldsymbol{e}_{2} \\ \vdots \\  \sqrt{\lambda_{p}} \boldsymbol{e}_{p} \end{array} \right)
\end{eqnarray*}

In practice we don't know $\boldsymbol{\Sigma}$ and we use  $S$ (or we standardise the variables and use $R$)

\newpage

Spectral decomposition in full \ldots yields linear principal components as follows:

\begin{eqnarray*}
z_{1} &=& e_{11}x_{1} + e_{12}x_{2} + \ldots + e_{1p} x_{p}; var(z_{1})=\lambda_{1}\\
z_{2} &=& e_{21}x_{2} + e_{22}x_{2} + \ldots + e_{2p} x_{p}; var(z_{2})=\lambda_{2}\\
&\vdots&\\
z_{p} &=& e_{p1}x_{1} + e_{p2}x_{1} + \ldots + e_{1p} x_{p}; var(z_{p})=\lambda_{p}
\end{eqnarray*}

\newpage

Or in matrix notation:

\begin{equation}
\label{pcfact}
\boldsymbol{Z} = \boldsymbol{E}\boldsymbol{X}
\end{equation}

\begin{itemize}
\item $\boldsymbol{Z} = \left( \begin{array}{c} z_{1} \\ z_{2} \\ \vdots \\ z_{p} \end{array} \right)$,  $\boldsymbol{X} = \left( \begin{array}{c} X_{1} \\ X_{2} \\ \vdots \\ X_{p} \end{array} \right)$ and  $\boldsymbol{E} = \left( \begin{array}{cccc} e_{11} & e_{12} & \hdots & e_{1p} \\ 
e_{21}& e_{22} & \hdots & e_{2p} \\
 \vdots & \vdots & \ddots & \vdots \\ 
e_{p1} & e_{p2} & \hdots & e_{pp}  \end{array} \right)$.   
\end{itemize}
So, multiplying both sides of \ref{pcfact} by $\boldsymbol{E}^{-1}$gives:

\begin{equation}
\boldsymbol{E}^{-1} \boldsymbol{Z} = \boldsymbol{X}
\end{equation}


\newpage

\subsection{Finding $\boldsymbol{E}^{-1}$}

We know orthogonal matrices generally that $\boldsymbol{E}^{-1} = \boldsymbol{E}^{T}$ so we can invert the transformation by using

\begin{equation}
\boldsymbol{X} = \boldsymbol{E}^{T} \boldsymbol{Z}
\end{equation}

which can be expanded as:

\begin{eqnarray*}
x_{1} &=& e_{11}z_{1} + e_{21} z_{2}  + \ldots + e_{p} z_{p}  \\
x_{2} &=& e_{12}z_{2} + e_{22} z_{2} + \ldots + e_{p} z_{p}  \\
&\vdots& \\
x_{p} &=& e_{1p}z_{1} + e_{2p} z_{2} + \ldots + e_{pp} z_{p}  
\end{eqnarray*}

\newpage

which we could express as;

\begin{eqnarray*}
x_{1} &=& (e_{11} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{21} \sqrt{\lambda_{2}}) \frac{z_{2}}{\sqrt{\lambda_{2}}} + \ldots +  (e_{p1} \sqrt{\lambda_{p}}) \frac{z_{p}}{\sqrt{\lambda_{p}}} \\
x_{2} &=& (e_{12} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{12} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + \ldots +  (e_{p2} \sqrt{\lambda_{p}}) \frac{z_{p}}{\sqrt{\lambda_{p}}} \\
&\vdots& \\
x_{p} &=& (e_{1p} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{2p} \sqrt{\lambda_{2}}) \frac{z_{2}}{\sqrt{\lambda_{2}}} + \ldots +  (e_{pp} \sqrt{\lambda_{p}}) \frac{z_{p}}{\sqrt{\lambda_{p}}} 
\end{eqnarray*}


\newpage

\subsubsection{The model}

\begin{itemize}
\item Set $\gamma_{jk} = (e_{jk} \sqrt{\lambda_{j}})$ and $\phi_{j} = z_{j} / \sqrt{\lambda_{j}}$ 
\end{itemize}

A clear link with the factor analysis model.  Our loadings matrix $\boldsymbol{\Gamma}$ is the $p \times p$ matrix where the $j$th column is given by  $\sqrt{\lambda_{j}} \boldsymbol{e}_{j}$ so that:

\begin{displaymath}
\boldsymbol{S} = \boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T}
\end{displaymath}


\newpage


\subsubsection{But \ldots the problem}


\begin{eqnarray*}
x_{1} &=& e_{11}z_{1} + e_{21} z_{2}  + \ldots + e_{q1} z_{q} +  e_{q+1,1} z_{q+1} + \ldots +  e_{p1} z_{p} \\
x_{2} &=& e_{12}z_{2} + e_{22} z_{2} + \ldots + e_{q2} z_{q} +  e_{q+1,2} z_{q+1} + \ldots + e_{p2} z_{p} \\
&\vdots& \\
x_{p} &=& e_{1p}z_{1} + e_{2p} z_{2} + \ldots + e_{qp} z_{q} + e_{q+1,p} z_{q+1} + \ldots + e_{pp} z_{p} 
\end{eqnarray*}

\newpage

if we set $ e_{q+1,j} z_{q+1} + \ldots +  e_{pj} z_{p} = \zeta_{j}; j = 1, \ldots, p$ we can rewrite this as:


\begin{eqnarray*}
x_{1} &=& e_{11} z_{1} + e_{21} z_{2} + \ldots + e_{q1} z_{q} +  \zeta_{1}\\
x_{2} &=& e_{12} z_{1} + e_{22} z_{2} + \ldots + e_{q2} z_{q} +  \zeta_{2}\\
&\vdots& \\
x_{p} &=& e_{1p} z_{1} + e_{2p} z_{1} + \ldots + e_{qp} z_{q} + \zeta_{p}
\end{eqnarray*}

As earlier, we can expressed this as:

\begin{eqnarray*}
x_{1} &=& (e_{11} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{21} \sqrt{\lambda_{2}}) \frac{z_{2}}{\sqrt{\lambda_{2}}} + \ldots +  (e_{q1} \sqrt{\lambda_{q}}) \frac{z_{q}}{\sqrt{\lambda_{q}}} +  \zeta_{1}\\
x_{2} &=& (e_{12} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{12} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + \ldots +  (e_{q2} \sqrt{\lambda_{q}}) \frac{z_{q}}{\sqrt{\lambda_{q}}} +  \zeta_{2}\\
&\vdots& \\
x_{p} &=& (e_{1p} \sqrt{\lambda_{1}}) \frac{z_{1}}{\sqrt{\lambda_{1}}} + (e_{2p} \sqrt{\lambda_{2}}) \frac{z_{2}}{\sqrt{\lambda_{2}}} + \ldots +  (e_{qp} \sqrt{\lambda_{q}}) \frac{z_{q}}{\sqrt{\lambda_{q}}} +  \zeta_{p}
\end{eqnarray*}

where $\gamma_{jk} = (e_{jk} \sqrt{\lambda_{j}})$ and $\phi_{i} = z_{i} / \sqrt{\lambda_{i}}$ as before, notice as stated at the outset that $var(\boldsymbol{\zeta}) = \boldsymbol{\psi}$.


 If we consider this in terms of the decomposition of the covariance matrix we have:

\begin{equation}
\boldsymbol{\Sigma} = \left( \sqrt{\lambda_{1}} \boldsymbol{e}_{1}, \sqrt{\lambda_{2}} \boldsymbol{e}_{2}, \ldots, \sqrt{\lambda_{q}} \boldsymbol{e}_{q} \right) \left( \begin{array}{r} \sqrt{\lambda_{1}} \boldsymbol{e}_{1} \\  \sqrt{\lambda_{2}} \boldsymbol{e}_{2} \\ \vdots \\  \sqrt{\lambda_{q}} \boldsymbol{e}_{q} \end{array} \right) + 
\left[ \begin{array}{rrrr} \psi_{1} & 0 & \hdots & 0\\
0 & \psi_{2} & \hdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \hdots & \psi_{p}
\end{array} \right]
\end{equation}

Where now $\psi_{j} = var(\zeta_{j}) =  \sigma_{jj} - \sum_{k=1}^{q} \gamma_{jk}^{2}$ for $k = 1, 2, \ldots, q$. 

\newpage

\subsection{Uniquenesses}

Estimates of the specific variances are given by diagonal elements of the matrix $\boldsymbol{\hat{\Sigma}} - \boldsymbol{\hat{\Gamma}}\boldsymbol{\hat{\Gamma}}^{T}$, i.e:

\begin{equation}
\boldsymbol{\hat{\psi}} = 
\left[ \begin{array}{rrrr} \psi_{1} & 0 & \hdots & 0\\
0 & \psi_{2} & \hdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \hdots & \psi_{p}
\end{array} \right]
\mbox{with}  \psi_{j} = \sigma_{jj} - \sum_{k=1}^{q} \gamma_{jk}^{2}
\end{equation}

\newpage

\subsection{Common factors}


So, when using the principal component solution of $\boldsymbol{\hat{\Sigma}}$, it is specified in terms of eigenvalue-eigenvector pairs ($\hat{\lambda}_{1}, \hat{\boldsymbol{e}}_{1}$), ($\hat{\lambda}_{2}, \hat{\boldsymbol{e}}_{2}$), $\ldots$, ($\hat{\lambda}_{p}, \hat{\boldsymbol{e}}_{p}$), where $\hat{\lambda}_{1} \geq \hat{\lambda}_{2} \geq \ldots \geq \hat{\lambda}_{p}$.   If we wish to find a $q<p$ solution of common factors, then the estimated factor loadings are given by:

\begin{displaymath}
\boldsymbol{\hat{\Gamma}} = \left( \sqrt{\lambda_{1}} \boldsymbol{e}_{1}, \sqrt{\lambda_{2}} \boldsymbol{e}_{2}, \ldots, \sqrt{\lambda_{q}} \boldsymbol{e}_{q} \right) 
\end{displaymath}

\subsubsection{Common factors have identity covariance matrix}

As with the factor analysis model given earlier, the factors $\boldsymbol{\phi}$ have identity covariance matrix

\begin{displaymath}
var(\boldsymbol{\phi}) = var \left(\sqrt{\Lambda_{1}} \boldsymbol{\Gamma}_{1}^{T}(\boldsymbol{x} - \boldsymbol{\mu}) \right) = \boldsymbol{I}_{q},
\end{displaymath}



\subsection{Common factors are uncorrelated with the residuals}

\begin{displaymath}
cov(\boldsymbol{\phi}, \boldsymbol{\zeta}) = cov \left( \sqrt{\Lambda_{1}} \boldsymbol{\Gamma}_{1}^{T}(\boldsymbol{x} - \boldsymbol{\mu}),  \boldsymbol{\Gamma}_{2}\boldsymbol{\Gamma}_{2}^{T}(\boldsymbol{x} - \boldsymbol{\mu}) \right) = \sqrt{\boldsymbol{\Lambda_{1}}} \boldsymbol{\Gamma}_{1}^{T} \boldsymbol{\Sigma} \boldsymbol{\Gamma}_{2} \boldsymbol{\Gamma}_{2}^{T} = 0
\end{displaymath}



\newpage

\subsection{But the uniquenesses aren't independent}

Each $\zeta_{i}$ contains the same $z_{i}$ so they are not mutually unrelated.   Hence the latent variables obtained using the principal component method do not explain all the correlation structure in our data $\boldsymbol{X}$.   The covariance matrix for the errors is now:

\begin{displaymath}
var(\boldsymbol{\zeta}) = \boldsymbol{\Gamma}_{2} \boldsymbol{\Lambda}_{2} \boldsymbol{\Gamma}_{2}^{T}
\end{displaymath}


\newpage


\subsubsection{A ``residual'' matrix}

\begin{equation}
\boldsymbol{\epsilon} = \boldsymbol{{S}} - \left(\boldsymbol{L}\boldsymbol{L}^{T} + \boldsymbol{\psi} \right)
\end{equation}

By construction, the diagonal elements of this residual matrix will be zero.   A decision to retain a particular $q$ factor model could be made depending on the size of the off-diagonal elements.   Rather conveniently, there is an inequality which gives us:

\begin{equation}
\left[ \boldsymbol{\epsilon} = \boldsymbol{\hat{\Sigma}} - \left(\boldsymbol{L}\boldsymbol{L}^{T} + \boldsymbol{\psi} \right) \right] \leq \hat{\lambda}_{q+1}^{2} + \cdots + \hat{\lambda}_{p}^{2}
\end{equation}

So it is possible to check the acceptability of fit in terms of a small sum of squares of neglected eigenvalues.

\newpage

\subsection{Proportion of variance explained}

Instead of examining discarded components we could examine those we intend to retain.   Bearing in mind that $trace(\boldsymbol{\Sigma}) = \sigma_{11} + \sigma_{22} + \ldots + \sigma_{pp}$, we know that the amount of variation explained by the first factor $\gamma_{11}^{2} + \gamma_{21}^{2} + \ldots + \gamma_{p1}^{2} = (\sqrt{\lambda_{1}} \boldsymbol{e}_{1})^{T}(\sqrt{\lambda_{1}} \boldsymbol{e}_{1}) = \lambda^{1}$.

So we know that the $j$-th factor explains the following proportion of total sample variance:

\begin{equation}
\frac{\lambda_{j}}{trace(\boldsymbol{S})}
\end{equation}

which reduces to $\frac{\lambda_{j}}{p}$ when using standardised variables (the correlation matrix).

Kaiser criterion! 

\newpage


\subsubsection{Exercise}

Consider the \textbf{R} data \texttt{ability.cov}, originally reported by  Smith, G. A. and Stanley G. (1983) ``Clocking g: relating
     intelligence and measures of timed performance'' \textit{Intelligence},
     \textbf{7}:353-368 which measures $p=6$ tests given to $n=112$ individuals.  
The eigenvalues of the correlation matrix are: 3.08, 1.14, 0.82, 0.41,  0.36, 0.20.    The corresponding eigenvectors of the correlation matrix are given below:

\begin{table}[ht]
\begin{center}
\begin{tabular}{rrrrrrr}
  \hline
 & $e_1$& $e_2$ & $e_3$ & $e_4$ & $e_5$ & $e_6$ \\
  \hline
General & -0.47 & 0.00 & 0.07 & 0.86 & 0.04 & -0.16 \\
Picture & -0.36 & 0.41 & 0.59 & -0.27 & 0.53 & 0.00 \\
Blocks & -0.43 & 0.40 & 0.06 & -0.20 & -0.78 & 0.05 \\
Maze & -0.29 & 0.40 & -0.79 & -0.10 & 0.33 & 0.05 \\
Reading & -0.44 & -0.51 & -0.01 & -0.10 & 0.06 & 0.73 \\
Vocab & -0.43 & -0.50 & -0.09 & -0.35 & 0.02 & -0.66 \\
   \hline
\end{tabular}
\end{center}
\end{table}


\newpage

\begin{shortquiz}{PCFA}
\begin{questions}
\item Given the above information, how many factors do you retain according to the Kaiser criteria
\begin{answers}[kaiser]{3}
\Ans0 1 &
\Ans1 2 &
\Ans0 3 \\
\Ans0 4 &
\Ans0 5 &
\Ans0 6
\end{answers}
\begin{solution}
The Kaiser criteria suggests that we retain all factors where the eigenvalues explain greater than the average amount of variance.   For a correlation matrix this reduces to retaining all factors where the eigenvalues are greater than 1.
\end{solution}
\item We have six eigenvalues and six eigenvectors.  What is the maximum number of factors we could expect to find
\begin{answers}[howmanyfactors]{3}
\Ans0 1 &
\Ans0 2 &
\Ans1 3 \\
\Ans0 4 &
\Ans0 5 &
\Ans0 6
\end{answers}
\begin{solution}
Using the formula
\begin{equation}
q \leq \frac{2p + 1 - \sqrt{8p-1}}{2}
\end{equation}
we have $p=6$ and so $q \leq \frac{2 \times 6 + 1 - sqrt{8 \times 6 -1}}{2}$ giving $q=3$ as the maximum possible whole number of factors supported by three manifest variables
\end{solution}
\newpage
\item Find the loading for Maze on factor 1
\begin{answers}[maze1]{3}
\Ans0 -0.89 &
\Ans1 -0.51 &
\Ans0 -0.29
\end{answers}
\begin{solution}
The first eigenvalue is $3.07$, the element of the first eigenvector corresponding to Maze is $-0.29$ and so we have the first loading $=\sqrt{3.07} \times -0.29$
\end{solution}
\item Now find the loading for Maze on factor 2
\begin{answers}[maze2]{3}
\Ans1 0.43 &
\Ans0 0.46 &
\Ans0 0.40
\end{answers}
\begin{solution}
The second eigenvalue is $1.14$, the element of the first eigenvector corresponding to Maze is $0.40$ and so we have the second loading $=\sqrt{1.14} \times 0.40$
\end{solution}
\item Now find the commonality for Maze assuming a two factor solution
\begin{answers}[common]{3}
\Ans0 0.51 &
\Ans0 0.43 &
\Ans1 0.45 
\end{answers}
\begin{solution}
We want $-0.51^2 + 0.43^2$ to give us the commonality on a two factor solution
\end{solution}
\item Now find the uniqueness for Maze assuming a two factor solution
\begin{answers}[unique]{3}
\Ans0 0.49 &
\Ans1 0.55 &
\Ans0 0.57
\end{answers}
\begin{solution}
The uniqueness is basically $1-\mbox{commonality}$, hence $0.55$
\end{solution}
\item Is this commonality ``good''
\begin{answers}[good]{2}
\Ans0 Yes &
\Ans1 No
\end{answers}
\begin{solution}
There are no hard and fast rules here, but explanining less than half the variance in the Maze variable doesn't sound too convincing to me
\end{solution}
\end{questions}
\end{shortquiz}


\newpage


\section{Maximum likelihood solutions}
\label{mlfact}



Obvious conclusions might be drawn by noting that R only offers this method of fitting factor analysis models, see the help-file for the relevant function \texttt{?factanal} as well as Venables and Ripley.  
 

\begin{itemize}
\item Invariant to changes in scale i.e it doesn't matter whether the correlation or the covariance matrix are used, or indeed whether any other scale changes are applied.  
\item  There are a number of other advantages associated with maximum likelihood fitting, but the problem of Heywood cases still remains, whereby some of the unique variances are estimated with a negative value.   
\item We also need to impose an additional assumption over and above the factor analysis assumptions set out earlier, namely that the following matrix:
\end{itemize}

\begin{equation}
\label{diagconstraint}
\boldsymbol{\Gamma}^{T} \boldsymbol{\Psi}^{-1} \boldsymbol{\Gamma}
\end{equation}

must be diagonal to enable model fitting.



\newpage

\subsection{Developing the maximum likelihood solution}

\begin{equation}
\label{mvnlike}
L(\boldsymbol{x}; \boldsymbol{\mu}, \boldsymbol{\Sigma}) = (2 \pi)^{-\frac{np}{2}} |\boldsymbol{\Sigma}|^{-\frac{n}{2}} e^{-\frac{1}{2}tr\left( \boldsymbol{\Sigma}^{-1} ( \sum_{i=1}^{n}(\boldsymbol{x}_{i} - \boldsymbol{\bar{x}})(\boldsymbol{x}_{i} - \boldsymbol{\bar{x}})^{T} + n(\boldsymbol{\bar{x}} - \boldsymbol{\mu})(\boldsymbol{\bar{x}} - \boldsymbol{\mu})^{T} ) \right)}
\end{equation}
we wish to solve this in terms of our factor analysis model and therefore need to find an expression for the likelihood of $L(\boldsymbol{x}; \boldsymbol{\mu}, \boldsymbol{\Gamma}, \boldsymbol{\psi})$ where $\boldsymbol{\mu}$ is a nuisance parameter for our purposes here, we can either get rid of it by using the estimate $\boldsymbol{\hat{\mu}} = \boldsymbol{\bar{x}}$ and hence use the profile likelihood to find $\boldsymbol{\hat{\Gamma}}$ and  $\boldsymbol{\hat{\psi}}$ , or we can factorise the likelihood as  $L(\boldsymbol{S}; \boldsymbol{\bar{x}}, \boldsymbol{\Sigma}) L(\boldsymbol{\bar{x}}; \boldsymbol{\mu}, \boldsymbol{\Sigma})$.   In this latter case, $\boldsymbol{bar{x}}$ and $\boldsymbol{S}$ are the joint sufficient statistics for $\boldsymbol{\mu}$ and $\boldsymbol{\Sigma}$ respectively, for the purposes of factor analysis we only require the first part of the factorised likelihood which can be estimated by conditional maximum likelihood.   Note that as $\boldsymbol{bar{x}}$ and $\boldsymbol{S}$ are independent this is also the marginal likelihood.




\subsubsection{The log likelihood}


Taking logs of \ref{mvnlike}, and collecting constant terms into $c_{1}$ and $c_{2}$ we can say that we wish to maximise:

\begin{equation}
\label{fall}
\ln L = c_{1} - c_{2} \left( \ln |\boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi}| + trace(\boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi})^{-1}\boldsymbol{S} \right)
\end{equation}



\subsubsection{Maximising the log-likelihood (subject to diagonality constraint)}

An initial estimate of $\widetilde{\boldsymbol{\psi}}$ has to be made as before, for fixed $\boldsymbol{\psi}>0$, the likelihood equations require:

\begin{equation}
\boldsymbol{\hat{\Gamma}} = \sqrt{\boldsymbol{\psi}} \boldsymbol{E}_{1} \sqrt{(\boldsymbol{\Lambda}_{1} - \boldsymbol{I})}
\end{equation}

where $\boldsymbol{\Lambda}_{1}$ contains the $q$ largest eigenvalues of $\sqrt{\boldsymbol{\psi}} \boldsymbol{S} \sqrt{\boldsymbol{\psi}}$, and  $\boldsymbol{E}_{1}$ the corresponding eigenvectors.   This is used to estimate $\boldsymbol{\hat{\Gamma}}$ given a value of $\boldsymbol{\hat{\psi}}$.   Now, the log likelihood is maximised with respect to $\boldsymbol{\hat{\psi}}$ given an estimate of  $\boldsymbol{\hat{\Gamma}}$.


\newpage

\subsection{Hypothesis testing}

\begin{eqnarray*}
H_{0}&:& \boldsymbol{\Sigma} = \boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi}\\
H_{1}&:&  \boldsymbol{\Sigma}\ is\ any\ other\ positive\ definite\ matrix
\end{eqnarray*}

This (eventually) yields a likelihood ratio statistic:

\begin{equation}
-2 \ln \Lambda = -2 \ln \left(  \frac{|\boldsymbol{\hat{\Sigma}}|}{|\boldsymbol{S}|} \right) + n \left( tr(\boldsymbol{\hat{\Sigma}}^{-1}\boldsymbol{S}) - p \right)
\end{equation}

with $\frac{1}{2} \left( (p-q)^{2} - p - q \right)$ degrees of freedom.


It can be shown that 
$ tr(\boldsymbol{\hat{\Sigma}}^{-1}\boldsymbol{S}) - p = 0$ at the maximum likelihood  so this term can be removed and we can consider that

\begin{equation}
\label{faqtest}
-2 \ln \Lambda = n \ln \left(  \frac{|\boldsymbol{\hat{\Sigma}}|}{|\boldsymbol{S}|} \right)
\end{equation}

which requires only a Bartlett correction replacing $n$ with:

\begin{displaymath}
n - 1 - \frac{2p + 5}{6} - \frac{2q}{3}
\end{displaymath}

%JW $n - 1 - \frac{(2p + 4q + 5)}{6}$.
%WK $n - \frac{2p + 11}{6} - \frac{2q}{3}$

\newpage

\subsubsection{Hence we are going to test:}

\begin{equation}
n - 1 - \frac{2p + 5}{6} - \frac{2q}{3} \ln  \left(  \frac{|\boldsymbol{\hat{\Gamma}} \boldsymbol{\hat{\Gamma}}^{T} + \boldsymbol{\hat{\psi}}|}{|\boldsymbol{S}|} \right) > \chi^{2}_{\left((p-q)^{2} - p - q \right) / 2, \alpha}
\end{equation}

\begin{itemize}
\item Start with $q$ small (anticipating the rejection of $H_{0}$), and increase $q$ until $H_{0}$ is no longer rejected.   
\item Do note, there are many reasons for rejecting $H_{0}$, not all of these may concern us. 
\item Johnson and Wichern suggest that if $n$ is large and $q$ is small relative to $p$, it will tend to reject $H_{0}$ even though $\boldsymbol{\hat{\Sigma}}$ is close to $\boldsymbol{S}$.  So the situation can arise whereby we can claim ``statistical significance'' for the inclusion of additional factors in our model, but they actually add little to the model.   This tends to reinforces the exploratory aspects of multivariate analysis (for some sense of exploratory).
\end{itemize}


\newpage

\section{Rotation}

Having added a constraint to limit rotation

We are now going to rotate:

%\begin{displaymath}
%\boldsymbol{T} = \left( \begin{array}{rr} \cos \phi & \sin \phi \\ - \sin \phi & \cos \phi \end{array} \right)
%\end{displaymath}

%for rotation angle $\phi: -\pi \leq \phi \leq \phi$.   All we need to do is find a suitable value for $\phi$.   This becomes slightly more difficult in every sense where $q>2$, indeed it is possible to carry out the whole procedure by eye with pencil and paper (do you remember what they are).


\begin{itemize}
\item \emph{Orthogonal rotations}: two objective criteria are most commonly used to determine the optimal rotation: the Varimax procedure and the Quartimax procedure.
\end{itemize}

Varimax rotation seeks to maximise:

\begin{equation}
V = \frac{1}{p^{2}} \sum_{k=1}^{q} \left( p \sum_{j=1}^{p} \left[\frac{\gamma_{jk}^{2}}{\xi_{j}^{2}} \right]^{4} - \left[ \sum_{j=1}^{p} \left[\frac{\gamma_{jk}^{2}}{\xi_{j}^{2}}\right] \right]^{2} \right)
\end{equation}

where $\xi_{i}^{2} = \sum_{k=1}^{q} \gamma_{jk}^{2}$ is the communality for each of the $j$ variables as before.


%Another index to be maximised is the so-called quartimax rotation \cite{Neuhaus+Wrigley:1954}
%
%\begin{equation}
%Q = \sum_{j=1}^{p} \sum_{k=1}^{q} \gamma_{jk}^{4} - \frac{1}{pq} \left(\sum_{j=1}^{p} \sum_{k=1}^{q} \gamma_{jk}^{2} \right)^{2}
%\end{equation}

%It needs to be stressed that this whole point of this exercise is interpret-ability!.   



\begin{figure}
\includegraphics[width = 0.5\textwidth]{../images/farotation}
\caption{Plot overlaying the co-ordinates of factor loadings 1 and 2 before and after rotation optimised by the varimax criterion}
\label{farotation}
\end{figure}

Although is is more difficult to see what is going on with $q=4$, we can see for example that the eighth variable (Social and Personal Services) has in increased loading in terms of $\gamma_{18}$, and a much decreased loading in terms of the second factor ($\gamma_{28}$ )is virtually zero.   Thus we may feel that we have achieved some simplification of our factor structure.


\newpage

\begin{shortquiz}{mlfa}
\begin{questions}
\item When fitting a maximum likelihood factor analysis model to the \texttt{ability} data, a significance test resulting from a single factor model has a $\chi^{2}$ test statistic of 75.18 of $\nu=9$ degrees of freedom.   Does this tell us:
\begin{answers}[ho1]{1}
\Ans0 A one factor model is adequate
\Ans1 A one factor model is inadequate
\end{answers}
\begin{solution}
Do note that $75.18$ is a huge value for a $\chi^{2}_{9}$ distribution, and we therefore reject $H_{0}$ that $\Gamma \Gamma^{T} + \psi$ is an adequate representation of $\Sigma$.   But it is also worth noting that there could be other reasons for rejecting $H_{0}$, such as the distribution being severely non-normal
\end{solution}
\item When fitting a two factor model to the \texttt{ability} data we obtain a $\chi^{2}$ test statistic of  $6.11$ on $4$ degrees of freedom. 
Does this tell us:
\begin{answers}[ho1]{1}
\Ans1 A two factor model is adequate
\Ans0 A two factor model is inadequate
\end{answers}
\begin{solution}
6.11 is below the critical value for a $\chi^{2}_{4}$, and so we have no reason to reject the null hypothesis.
\end{solution}
\newpage
\item A \emph{promax} rotation of the factor solutions has been carried out, and gives the following loadings:

\begin{tabular}{lrr}
&        Factor1 & Factor2 \\
general  & 0.364  &  0.470  \\
picture  & &        0.671\\ 
blocks & &          0.932\\ 
maze    &&         0.508 \\
reading & 1.023 & \\         
vocab  &   0.811 & 
\end{tabular}

 Would you describe the first factor as a measure of:
\newpage
\begin{answers}[interpret]{1}
\Ans0 Language skills \\
\Ans0 Mathematical skills \\
\Ans1 General and language skills \\    
\Ans0 General and mathematical skills \\
\Ans0 General, spatial and mathematical skills \\
\Ans0 General, spatial and language skills
\end{answers}
\begin{solution}
Of course, this is all somewhat subjective.   It's well worth taking 5 minutes in \textbf{R} to run $\texttt{example(ability.cov)}$ and try a few different rotation options.   But I think the first component is all about language skills (reading and vocabulary) as well as G-intellegence (general intelligence).
\end{solution}
\newpage
\item Would you describe the second factor as a measure of:
\begin{answers}[interpret2]{1}
\Ans0 Language skills \\
\Ans0 Mathematical skills \\
\Ans0 General and language skills \\    
\Ans0 General and mathematical skills  \\
\Ans1 General, spatial and mathematical skills \\
\Ans0 General, spatial and language skills
\end{answers}
\begin{solution}
Again, this is all highly subjective.   And do check out the \textbf{R} demonstration as the effects of rotation are rather frightening.   But I would classify ``blocks'' and ``maze'' as being somewhat mathematical.   You probably even argue that picture skills were ``mathematical'', but I've described them as spatial.
\end{solution}
\item The example presented here used ``promax'' rotation.  What kind of rotation is this:
\begin{answers}[promax]{2}
\Ans0 Orthogonal &
\Ans1 Oblique
\end{answers}
\item And what about varimax rotation:
\begin{answers}[varimax]{2}
\Ans1 Orthogonal &
\Ans0 Oblique
\end{answers}
\item What is the varimax trying to do:
\begin{answers}[varimaxrationale]{1}
\Ans1 To force factor loadings as close to $0$ or $1$ as possible \\
\Ans0 To maximise the size of the commonalities \\
\Ans0 To increase the proportion of variance explained by the factor model
\end{answers}
\end{questions}
\end{shortquiz}

\newpage
\section{Scoring}


\subsection{The weighted least squares estimates are:}

\begin{equation}
\boldsymbol{\hat{\phi}}_{i} = (\boldsymbol{\Gamma}^{T} \boldsymbol{\Psi}^{-1} \boldsymbol{\Gamma})\boldsymbol{\Gamma}^{T} \boldsymbol{\Psi}(\boldsymbol{x}_{i} - \boldsymbol{\bar{x}})
\end{equation}



\subsection{Or}

Based on assuming that both $\boldsymbol{\phi}$ and $\boldsymbol{\zeta}$ are multivariate normal, thus a concatenation of the manifest ($\boldsymbol{x}$) and latent ( $\boldsymbol{\phi}$) variables $\boldsymbol{y}^{T} = (\boldsymbol{\phi}^{T}, \boldsymbol{x}^{T})$ will also be normal with dispersion matrix:

\begin{displaymath}
var(\boldsymbol{y}) = \left( \begin{array}{cc} \boldsymbol{I} & \boldsymbol{\Gamma}^{T} \\
\boldsymbol{\Gamma} & \boldsymbol{\Gamma} \boldsymbol{\Gamma}^{T} + \boldsymbol{\psi} \end{array} \right)
\end{displaymath}


The mean of $\boldsymbol{\phi}$ is zero by definition, therefore:

\begin{displaymath}
E(\boldsymbol{z} | \boldsymbol{x}_{0}) =  \boldsymbol{\Gamma}^{T}(\boldsymbol{\Gamma})\boldsymbol{\Gamma}^{T} + \boldsymbol{\Psi})^{-1}(\boldsymbol{x}_{o} - \boldsymbol{\mu})
\end{displaymath}

which gives the estimate for the scores as:

\begin{equation}
\boldsymbol{z} =  \boldsymbol{\hat{\Gamma}}^{T}(\boldsymbol{\hat{\Gamma}})\boldsymbol{\hat{\Gamma}}^{T} + \boldsymbol{\hat{\psi}})^{-1}(\boldsymbol{x}_{i} - \boldsymbol{\hat{mu}})
\end{equation}


\newpage

\subsection{Bayesian methods}

It might be clear that factor scoring takes no account of uncertainty in the estimates of $\boldsymbol{\hat{\Gamma}}$ and $ \boldsymbol{\hat{\psi}}$, this is one area where Bayesian methods are coming to the fore



\newpage

\section{Summary}


\begin{itemize}
\item A method for finding ``latent variables'' which explain the dependence (correlation) structure in some data
\item Unfortunately, solutions based on PCFA are still common and you ought to know a little about them
\item Maximum likelihood based methods have been available since 1972 - explicit probability model and hence hypothesis testing possible
\item Interpretation of the loadings is usually the end-goal.   If you don't like the loadings you get you can rotate them (using a suitable criteria to find the ``optimal'' rotation)
\item You can check the quality of fit by examining the commonalities/uniqueness, as well as hypothesis tests (where appropriate) and all the usual eigen value procedures apply
\end{itemize}


\newpage

\subsection{Typical Exam type questions}
  
\begin{itemize}
\item Given some loadings $\boldsymbol{a}$ and eigenvalues, calculate the value of factor loadings (using PCFA)
\item Calculate and interpret the commonality and uniqueness
\item Explaining difference between PCFA and MLFA - why do loadings differ (often higher in PCFA), why are uniquenesses from PCFA flawed.
\item Interpreting rotated and unrotated factor solutions.  Explaining a little about criteria for optimising a rotation.   Explaining difference between orthogonal and oblique rotation.
\item Anything else you would like to suggest
\end{itemize}

\newpage

\subsection{Labwork}
  
  \begin{itemize}
  \item Worked example of discriminant analysis with the wines data - look at the effect of removing variables on the APER, plot the discriminant functions
\item Worked example with the iris data - to look at the way you can use a test and training set
\end{itemize}

\NaviBarOff


\end{document} 