\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage{mathrsfs}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate) Week 8: (Fisher's) Discriminant Analysis}
\author{Paul Hewson}
\date{27th November 2008}

\newtheorem{df}{Definition}
\newtheorem{theorem}{Theorem}


\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}


\maketitle


\section{Aims of this week}

\begin{itemize}
\item To introduce ``Discriminant analysis''
\item To consider \emph{one} method for Discriminant analysis in some detail
\item To \emph{briefly} outline the difference between other methods of discriminant analysis
\end{itemize}



\subsection{Reasons for discriminant analysis}


\begin{itemize}
\item Discrimination: to describe differences between two (or more) populations
\item Classification: to develop a rule which allows us to classify future observations
\end{itemize}

Note that this forms part of a much larger world known as \emph{supervised} classification


\newpage

\subsection{Range of methods for discriminant analysis}

There are methods which:

\begin{itemize}
\item can use prior information on proportion in each group
\item can incorporate information on the cost of misclassification
\item can use information on the probability density of the population
  (e.g., Multivariate Normal)

\end{itemize}

Note, when prior information assumes proportion in each group is the same, and misclassification costs are the same then this reduces to Fisher / Canonical discrimination

There are also a range of types of method:

\begin{itemize}
\item Linear discriminant analysis
\item Quadratic discriminant analysis
\item Logistic discriminant analysis
\end{itemize}



\newpage

\section{Fisher Discriminant Analysis}

We shall only consider one type, Fisher/Canonical discriminant
analysis.



This method was originally developed for investigating:

\begin{itemize}
\item the difference between two populations.
\item Does not assume normality, but does assume covariance is the same in each group
\item Relies on us finding linear combinations of the variables which best separate the two groups
\end{itemize}

\begin{equation}
z = a_{1} x_{1} + a_{2} x_{2} + \ldots + a_{p} x_{p}
\end{equation}


\newpage

\subsection{An idealised discriminant function}

\begin{figure}
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/discrim}
\caption{Idealised discrimant function}
\label{discrim}
\end{center}
\end{figure}


\newpage

\includegraphics[width = 0.6\textwidth]{../images/x1da}

No separation on $x_{1}$


\includegraphics[width = 0.6\textwidth]{../images/x2da}


Little separation on $x_{2}$

\includegraphics[width = 0.6\textwidth]{../images/z1da}

Very good separation on $z_{1} = a_{1}x_{1} + a_{2}x_{2}$

\newpage


In order to carry out Fisher's discriminant analysis, we need a method
which will find a suitable value of $\boldsymbol{a}$

We want to maximise the separation:

\begin{displaymath}
d(z_{1},z_{2}) = \frac{\lvert \bar{z}_{1} -  \bar{z}_{2} \rvert}{s_{z}}
\end{displaymath}

where $s_{z}$ is the pooled variance:

\begin{displaymath}
s_{z}^{2} = \frac{\sum_{i=1}^{n_{1}} (z_{1i} - \bar{z}_{1 \cdot})^{2} + \sum_{i=1}^{n_{2}} (z_{2i} - \bar{z}_{2 \cdot})^{2}}{n_{1} + n_{2} - 2}
\end{displaymath}

\newpage

In order to find $\boldsymbol{a}$, we note that:

\begin{displaymath}
\frac{\lvert \bar{z}_{1} -  \bar{z}_{2}\rvert}{s_{z}} = \frac{(\boldsymbol{a}^{T}\bar{\boldsymbol{x}}_{1} - \boldsymbol{a}^{T}\bar{\boldsymbol{x}}_{2})^{2}}{\boldsymbol{a} \boldsymbol{S}_{pooled} \boldsymbol{a}}
\end{displaymath}

Remembering the extended Cauchy-Schwarz Inequality:   

\begin{df}{Extended Cauchy Schwarz} 
for any non-zero vectors $\boldsymbol{u} \in \mathbb{R}$ and $\boldsymbol{v} \in \mathbb{R}$, with any positive definite $p \times p$ matrix $\boldsymbol{S}$:
\begin{displaymath}
\langle \boldsymbol{u}, \boldsymbol{v} \rangle^{2} \leq (\boldsymbol{u}^{T}\boldsymbol{S} \boldsymbol{u})(\boldsymbol{v}^{T}\boldsymbol{S}^{-1} \boldsymbol{v}), \mbox{ for all } \boldsymbol{u}, \boldsymbol{v} \in \mathbb{R}
\end{displaymath}
with equality if and only if $\boldsymbol{u} = \lambda \boldsymbol{S}\boldsymbol{v}$ for some $\lambda \in \mathbb{R}$. 
\end{df}



If we now rearrange this:
\begin{displaymath}
\mbox{max}\frac{(\boldsymbol{u}^{T}\boldsymbol{v})^{2}}{\boldsymbol{u}^{T} \boldsymbol{S} \boldsymbol{u}} = \boldsymbol{v}^{T} \boldsymbol{S}^{-1} \boldsymbol{v}
\end{displaymath}


And so in order to find $\boldsymbol{a}$ we need to \emph{ maximise} this distance:
\begin{displaymath}
\frac{(\boldsymbol{a}^{T}\bar{\boldsymbol{x}}_{1} - \boldsymbol{a}^{T}\bar{\boldsymbol{x}}_{2})^{2}}{\boldsymbol{a} \boldsymbol{S}_{pooled} \boldsymbol{a}} = \frac{\left(\boldsymbol{a}^{T}(\bar{\boldsymbol{x}}_{1} - \bar{\boldsymbol{x}}_{2}) \right)^{2}}{\boldsymbol{a} \boldsymbol{S}_{pooled} \boldsymbol{a}}
\end{displaymath}

but look at the extended Cauchy Schwartz:

\begin{displaymath}
\mbox{max}\frac{(\boldsymbol{u}^{T}\boldsymbol{v})^{2}}{\boldsymbol{u}^{T} \boldsymbol{S} \boldsymbol{u}} = \boldsymbol{v}^{T} \boldsymbol{S}^{-1} \boldsymbol{v}
\end{displaymath}
where $\boldsymbol{u} = \boldsymbol{a}$, $\boldsymbol{v} = (\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2})$ and $\boldsymbol{S}$ is the pooled covariance matrix.



We find that the \emph{Mahalanobis distance} is the maximum possible given $\boldsymbol{a}^{T} \boldsymbol{S} \boldsymbol{a} = 1$



This makes finding $\boldsymbol{a}$ rather simple.   So, to find $\boldsymbol{a}$ which maximises $\lvert\boldsymbol{a} (\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2} ) \rvert$ subject to $\boldsymbol{a}^{T}\boldsymbol{S}\boldsymbol{a} = 1$ we only need to solve:

\begin{displaymath}
\boldsymbol{a} = \boldsymbol{S}^{-1}(\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2} )
\end{displaymath}



%%
%%Need an example here
%%



\begin{df}{Two group linear discriminant function}
And so we form the linear discriminant function  by:

\begin{displaymath}
z = {\color{red}\underbrace{(\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2} )^{T}\boldsymbol{S}^{-1}}_{\boldsymbol{a}}} \boldsymbol{x}
\end{displaymath}
\end{df}

\newpage


\subsubsection{Worked example}

Johnson and Wichern report a study on Haemophila A carriers.   Two
biochemical markers are available, 

\begin{itemize}
\item $X_1 = \log_{10} (\mbox{AHF activity})$
\item $X_2 = \log_{10} (\mbox{AHF-like antigen})$
\end{itemize}


The summary statistics we have are $\boldsymbol{\bar{x}}_{Control}^{T}
= (-0.0065, -0.0390)$ and  $\boldsymbol{\bar{x}}_{Carrier}^{T}
= (-0.2483, 0.0262)$.  

\begin{shortquiz}{Kirk}
Given
\begin{displaymath}
\boldsymbol{S}_{pooled}^{-1} = \left( \begin{array}{rr} 131.158
    & -90.423 \\ -90.423 & 108.147 \end{array} \right)
\end{displaymath}
you should find $\boldsymbol{a}$
\begin{answers}[disc1]{2}
\Ans0 $35.61 x_{1} - 27.92 x_{2}$ &
\Ans1 $37.61 x_{1} - 28.92 x_{2}$ \\
\Ans0 $39.61 x_{1} - 29.92 x_{2}$ &
\Ans0 $41.61 x_{1} - 30.92 x_{2}$
\end{answers}
\begin{solution}

Noting that we need to form:

\begin{eqnarray*}
\boldsymbol{z}_{ij} &=& \boldsymbol{a}^{T}\boldsymbol{x}\\
&=& ( \boldsymbol{x}_{Control} - \boldsymbol{x}_{Carrier} )
  \boldsymbol{S}_{pooled}^{-1}\boldsymbol{x}_{i}
\end{eqnarray*}

In our case we need to find:

\begin{displaymath}
( 0.2418, -0.0652) \left( \begin{array}{rr} 131.158 & -90.423 \\
    -90.423 & 108.147 \end{array} \right) = \left( \begin{array}{r}
    37.61 \\ -28.92 \end{array} \right)
\end{displaymath}

So the values of $\boldsymbol{a}$ we are interested in are $37.61$ and
$-28.92$.   We therefore form the linear discriminant function as:

\begin{displaymath}
\boldsymbol{z}_{i} = 37.61 x_{i1} - 28.92 x_{i2}
\end{displaymath}
\end{solution}
\end{shortquiz}


\newpage

\subsubsection{Classification}

In this case, we are more interested in the potential for using the
discriminant functions as classifiers than we are in using them to
explore the differences between the two groups.


For classification, the obvious cut-off would be the midpoint between the mean value of $z$ for group 1 and 2.   But we can add intercepts, and scale the coefficients.


\begin{shortquiz}{Piccard}
If we had an observation on an individual such that
$\boldsymbol{x}_{new} = (-0.01, 0.01)$, to which group would they
belong:
\begin{answers}[class1]{2}
\Ans1 $Control$ &
\Ans0 $Carrier$
\end{answers}
\begin{solution}
Note that
\begin{displaymath}
\boldsymbol{\bar{z}}_{Control} =
\boldsymbol{a}^{T}\boldsymbol{\bar{x}}_{Control} = (37.61, -28.92) \left(
  \begin{array}{r} -0.0065 \\ -0.0390 \end{array} \right) = 0.88
\end{displaymath}
and
\begin{displaymath}
\boldsymbol{\bar{z}}_{Carrier} =
\boldsymbol{a}^{T}\boldsymbol{\bar{x}}_{Carrier} = (37.61, -28.92) \left(
  \begin{array}{r} -0.2483 \\ 0.0262 \end{array} \right) = -10.10
\end{displaymath}

So the midpoint between these two is $\frac{1}{2}(0.88 - 10.10) =
-4.61$.


We score our new observation as follows:
\begin{displaymath}
\boldsymbol{\bar{z}}_{Bew} =
\boldsymbol{a}^{T}\boldsymbol{x}_{New} = (37.61, -28.92) \left(
  \begin{array}{r} -0.01 \\ 0.01 \end{array} \right) = -0.6653
\end{displaymath}

This is clearly closer to the mean value for the Carrier group (it is
larger than the cut-off) so we would assign this observation to the
Carrier group.
\end{solution}
\end{shortquiz}


\newpage

\subsection{A worked example from Flury}

These results are set out to  clarify a couple of points about the
calculations and uses the midge data (also two species, two variables)
and very similar to the ride on lawnmower example in Johnson and Wichern.

See \texttt{?midge} for details on the data.

\begin{verbatim}
library(Flury)
data(midge)
## extract the group specific means and covariances
mu <- by(midge[,-1], midge[,1], mean)
s <- by(midge[,-1], midge[,1], cov)
\end{verbatim}

And from these we can get the difference in means and the pooled covariance (note $8$ observations from \texttt{Af}, $5$ from \texttt{Apf}):

The mean differences and pooled covariance matrix:

\begin{verbatim} 
mudiff <- mu[1]$Af - mu[2]$Apf
spool <- 1/13 * (8 * s[1]$Af + 5 * s[2]$Apf)
\end{verbatim}

The inverse of the pooled covariance matrix:
\begin{verbatim}
a <- solve(spool) %*% mudiff
a
                 [,1]
Ant.Length   58.23644
Wing.Length -38.05867
\end{verbatim}

and checking the Mahalanobis distance is easy:

\begin{verbatim}
sqrt(t(mudiff) %*% solve(spool) %*% mudiff)
        [,1]
[1,] 3.93985
\end{verbatim}

So the simplest form of classification rule we could construct would be to fin d the mid point in $z$:

\begin{verbatim}
z1 <- mu[1]$Af %*% a
z2 <- mu[2]$Apf %*% a
midpoint <- (z1+z2)/2
midpoint
         [,1]
[1,] 5.871534
\end{verbatim}

And for any observation find $x\boldsymbol{a}$, if is below this midpoint classify into group 1 and if it is above this classify as group 2


\newpage

\subsection{The \texttt{lda()} function}

We might prefer to use an inbuilt function in R, in the \texttt{MASS} library:

\begin{verbatim}
 midge.lda <- lda(midge[,1] ~ midge[,2] + midge[,3], 
     prior = c(1,1)/2)
midge.lda$scaling
                 LD1
midge[, 2] -14.78139
midge[, 3]   9.65993 
\end{verbatim}


\subsection{Why the difference from our values?  }


\begin{itemize}
\item Note that $58.23644 \div 3.93985 = 14.78139$ and
\item  $38.05867 \div 3.93985 = -9.65993$.
\end{itemize}

Do remember (note) that the sign of an eigenvalue (and everything related to it) is arbitrary



In fact, any constant $c$ multiplied by our weightings $\boldsymbol{a}$ is as good as any other.  The \texttt{lda()} function has scaled the coefficients by the Mahalanobis distance (which is the standard deviation of the discriminant scores $z$)

Some software also incorporates a constant into the discriminant function, so that values below 0 are in one group, values above 0 are in the other:

\begin{displaymath}
V^{*} = \frac{1}{D}(V-midpoint) = \frac{V-5.87}{3.94} = 1.94 + 0.1478 X_{1} + 0.0966X_{2}
\end{displaymath}


\newpage

\section{Canonical discriminant analysis with more than two groups}



The first thing to note is that there is more than one discriminant
function is available.  

\begin{df}{Discriminant functions}
 In total, there are:
\begin{equation}
s = min(p, g-1)
\end{equation}
discriminant functions available, where $p$ is the number of variables, and $g$ is the number of groups.
\end{df}

So in general, where we have $g > 2$ groups, and $p > 2$ variables, we are looking for the following discriminant functions:


\begin{eqnarray*}
z_{1} &=& a_{11}x_{1} + a_{12}x_{2} + \ldots + a_{1p}x_{p}\\
z_{2} &=& a_{21}x_{1} + a_{22}x_{2} + \ldots + a_{2p}x_{p}\\
\ldots\\
z_{s} &=& a_{s1}x_{1} + a_{s2}x_{2} + \ldots + a_{sp}x_{p}
\end{eqnarray*}




%Similar idea, only instead of the difference between two groups we now have the Sum of Squares and Crossproducts matrix for the difference between more than one group:

%\begin{displaymath}
%\boldsymbol{SSCP}_{Between} = \sum_{h=1}^{g}(\boldsymbol{\mu}_{h} - \boldsymbol{\bar{\mu}})(\boldsymbol{\mu}_{h} - %\boldsymbol{\bar{\mu}})^{T}
%\end{displaymath} 
%where $\bar{\mu}$ is the grand mean.   

Here's a couple of interesting examples:

\subsubsection{Wines data}
  
%  \includegraphics[width = 0.7\textwidth]{../images/savineyard}
 
 Wines from three different countries (you know what wine looks like?)
 - click \href{http://www.wineanorak.com/sa1.htm}{\color{blue}HERE}
   for a picture of a randomly selected South African Vineyard.
  \newpage
\subsubsection{Anderson's Iris data}

As discussed last week, we are interested to learn from the Iris data
whether we do indeed have three distinct species, and perhaps whether
we could classify future observations.   

%We have (old) data on petal length, petal width, sepal length and sepal width.
%\begin{figure}
%\includegraphics[width = 0.2\textwidth]{../images/versic}
%\includegraphics[width = 0.2\textwidth]{../images/virginica}
%\includegraphics[width = 0.2\textwidth]{../images/setosa}
%\caption{From left to right; \textit{Iris versicolor, I. virginica, I. setosa}}
%\end{figure}



\newpage

\subsection{Canonical discriminant analysis}

\begin{itemize}
\item Again, we form $z = \boldsymbol{a}^{T}\boldsymbol{X}$, where $E(Z) = \boldsymbol{a^{T}\mu}$ and $Var(Z) = \boldsymbol{a^{T}\Sigma a}$.   
\item In practice, we need sample estimates $\bar{x}$, $\bar{x_{h}}$ and $\boldsymbol{S}_{pooled}$ (more later)
\end{itemize}





\subsubsection{Firstly, remember ANOVA?}

It may be worth re-revisiting ANOVA.  Imagine an ANOVA of each linear
combination $z$ in turn (first canonical discriminant function, second
canonical discriminant function and so on).

\begin{tabular}{llll}
Source & d.f. & Mean Square & F ratio \\
\hline
Between groups & $g-1$ & $M_{B}$ & $M_{B} / M_{W}$\\
Within groups & $N-g$ & $M_{W}$ & \\
 & N-1 & & \\
\end{tabular}

So, when we have more than two groups, we need:

\begin{itemize}
\item to find $\boldsymbol{a}$ to give us as $z$ with as large an F ratio as possible, 
\item i.e. hence the coefficients $a_{1}, \ldots, a_{p}$ need to be chosen to maximise this value.  
\item We impose a side condition that each subsequent set of $\boldsymbol{a}$ shall be orthogonal to the previous
\end{itemize} 

So, we need to find a linear combination $z = \boldsymbol{a}^{T} \boldsymbol{x}$ which maximised the ratio of \emph{between} group variance to \emph{within} group variance.   So consider:

\begin{itemize}
\item Within sample matrix of sum of squares and cross products: 

$\boldsymbol{SSCP}_{Within}$, 
\item Total sample matrix of sum of squares and cross products:

$\boldsymbol{SSCP}_{Total}$, 

\item Between-groups sample matrix sum of squares and cross products:

\begin{equation}
\boldsymbol{SSCP}_{Between} = \boldsymbol{SSCP}_{Total} - \boldsymbol{SSCP}_{Within}
\end{equation}
\end{itemize}


Having found these matrices, we wish to maximise:

\begin{equation}
\frac{ \boldsymbol{a}^{T} \boldsymbol{SSCP}_{Between} \boldsymbol{a}}{ \boldsymbol{a}^{T} \boldsymbol{SSCP}_{Within} \boldsymbol{a}} 
\end{equation}

subject to the condition that $\boldsymbol{a}^{T} \boldsymbol{SSCP}_{Within} \boldsymbol{a}$ = 1.   


\newpage

\subsection{Sample estimates}
  
(Recognise these?   Same as last week but this time they are in matrix notation)

  \begin{equation}
  \boldsymbol{SSCP}_{Within} = \frac{(\boldsymbol{X} - \boldsymbol{G}\boldsymbol{M})^{T}(\boldsymbol{X} - \boldsymbol{G}\boldsymbol{M})}{n-g}
\end{equation}

\begin{equation}
\boldsymbol{SSCP}_{Between} =
\frac{(\boldsymbol{G}\boldsymbol{\bar{x}_{k}} -
  \boldsymbol{1}\boldsymbol{\bar{x}})^{T}(\boldsymbol{G}\boldsymbol{\bar{x}_{k}}
  - \boldsymbol{1}\boldsymbol{\bar{x}})}{g-1}
\end{equation}

Where $\boldsymbol{G}$ is an $n \times g$ class indicator matrix, $\boldsymbol{M}$ is a $g \times p$ matrix of class means.   You will note that Johnson and Wichern give this in more conventional notation.


\newpage

As it happens, $\boldsymbol{S}_{Pooled} =
\frac{\boldsymbol{SSCP}_{Within}}{n_{1} + n_{2} + \ldots + n_{g}}$.
We have already seen that our discriminant coefficients
$\boldsymbol{a}$ can be multiplied by any arbitrary constant $c$.
Consquently, it turns out that the values of $\boldsymbol{a}$ which maximise 

\begin{equation}
\frac{ \boldsymbol{a}^{T} \boldsymbol{SSCP}_{Between} \boldsymbol{a}}{ \boldsymbol{a}^{T} \boldsymbol{SSCP}_{Within} \boldsymbol{a}} 
\end{equation}

are the same values which maximise:

\begin{equation}
\frac{ \boldsymbol{a}^{T} \boldsymbol{SSCP}_{Between} \boldsymbol{a}}{ \boldsymbol{a}^{T} \boldsymbol{S}_{Pooled} \boldsymbol{a}} 
\end{equation}

\newpage

\subsection{How to maximise}


\begin{itemize}
\item Find the eigenvalues and corresponding eigenvectors of 

$\boldsymbol{SSCP}_{Within}^{-1}\boldsymbol{SSCP}_{Between}$.   
\item The ordered eigenvalues $\lambda_{1}, \ldots, \lambda_{s}$ are the ratio of \emph{between} groups to \emph{within} groups sum of squares and cross products for each linear combination $z_{1}, \ldots, z_{s}$, 
\item the corresponding eigenvectors, $\boldsymbol{a}_{1}, \ldots, \boldsymbol{a}_{s}$, where $\boldsymbol{a}_{i} = \left( \begin{array}{c} a_{i1} \\ \vdots \\ a_{ip} \end{array} \right)$ are the \emph{orthogonal} coefficients.
\end{itemize}

\newpage

\subsubsection{For the \emph{very} curious}
  
  We wish to maximise:
  
  \begin{displaymath}
  \frac{\boldsymbol{a}^{T}\boldsymbol{B}\boldsymbol{a}}{\boldsymbol{a}^{T}\boldsymbol{\Sigma}\boldsymbol{a}}
  \end{displaymath}
  
  Decompose $\boldsymbol{\Sigma}$ as $\boldsymbol{E}^{T}\boldsymbol{\Lambda}\boldsymbol{E}$, and therefore consider a symmetric matrix square root as follows:

\begin{displaymath}
\boldsymbol{\Sigma}^{\frac{1}{2}} = \boldsymbol{E}^{T}\boldsymbol{\Lambda}^{\frac{1}{2}}\boldsymbol{E}
\end{displaymath}


If $\boldsymbol{u} = \boldsymbol{\Sigma}^{\frac{1}{2}} \boldsymbol{a}$, then we could say we wish to maximise:


  \begin{displaymath}
  \frac{(\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a})^{T}\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{B}\boldsymbol{\Sigma}^{\frac{1}{2}}
 (\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a})}{(\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a})^{T}(\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a})}
  \end{displaymath}
  
Note that the maximum of $\frac{\boldsymbol{u}^{T}\boldsymbol{M}\boldsymbol{u}}{\boldsymbol{u}^{T}\boldsymbol{u}}$ is given by $\lambda_{1}$ when $\boldsymbol{u}$ comprises the corresponding eigenvectors.   Note therefore that $(\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a})^{T}(\boldsymbol{\Sigma}^{\frac{1}{2}}\boldsymbol{a}) = 1$


\newpage

\subsection{Interpreting the loadings}


\begin{verbatim}
Coefficients of linear discriminants:
                    LD1        LD2
Sepal.Length  0.7760999  0.6977274
Sepal.Width   1.7473156 -2.3042725
Petal.Length -2.3760012  0.8819810
Petal.Width  -2.7421144 -3.3932192

Proportion of trace:
   LD1    LD2 
0.9927 0.0073 
\end{verbatim}


(For computational reasons these are actually derived from the singular value decomposition)

\begin{verbatim}
iris.lda$svd^2
[1] 1268.955889    9.326774
\end{verbatim}


But these loadings give us:
  
  
  
  \includegraphics[width = 0.6\textwidth]{../images/irisda}
  



  
  \includegraphics[width = 0.6\textwidth]{../images/winesda}
  

\section*{}

  \subsection*{M.S. Bartlett}
  
%  \includegraphics[width = 0.5\textwidth]{../images/bartlett}
Information on M.S. Bartlett can be found \href{http://en.wikipedia.org/wiki/M._S._Bartlett}{\color{blue}HERE}. 
 
  18 June 1910 to 8 Jan 2002 (died in Exmouth).   Went to Queen's University Cambdrige, worked with Egon Pearson, Neyman and R.A. Fisher.   Received DSc. from {\color{red}\emph{Hull}} University.   Many many contributions to Multivariate Statistics, you will meet numerous Bartlett Corrections (how to turn something strange into something approximating a distribution we understand)



\subsection{Assumptions}


\begin{itemize}
\item That observations are a random sample, 
\item that they are normally distributed, but relatively resistant to some departures, e.g. skewness, but not others e.g. outliers,
\item that the variance is the same for each group (i.e. that $\boldsymbol{SSCP}_{Within}$ is the same for each group)
\end{itemize}




\subsection{A couple of points about discriminant functions}

\begin{itemize}
\item There is no need to standardise the variables
\item The solution is invariant to any constant $c$ applied to the discriminant function, i.e. $z = c \boldsymbol{a}^{T}\boldsymbol{x}$
\end{itemize}

\newpage

\subsection{How to determine group membership}

\begin{itemize}
\item Find the group centroids, i.e. $\bar{z}_{g} = \boldsymbol{a}\bar{\boldsymbol{x}}_{g}$
\item Determine the Euclidean distance of any point $z_{i} = \boldsymbol{a}\bar{\boldsymbol{z}}_{i}$ from these centroids
\item Allocate an observation to the ``nearest'' centroid
\end{itemize}


\newpage


\begin{shortquiz}{Lt.Data}
Assume we have group centroids $\boldsymbol{\bar{x}}_{1}^{T} = (2,3,2)$,
$\boldsymbol{\bar{x}}_{2}^{T} = (4,6,2)$ and
$\boldsymbol{\bar{x}}_{3}^{T} = (8,7,10)$.\\[1cm]

Also, assume that the linear discriminant functions are given by 
$\boldsymbol{a}_{1} = (1,-1,1)$ and 
$\boldsymbol{a}_{2} = (-1,1,1)$\\[1cm]


Given a new observation $x_{new}^{T} = (5,5,5)$, determine the group
to which it should be assigned:
\begin{answers}[centroids]{3}
\Ans1 1 &
\Ans0 2 &
\Ans0 3
\end{answers}
\begin{solution}
The first thing we need to do is to project the group centroids into
discriminant space.

So, 
\begin{displaymath}
\bar{z}_{11} = \boldsymbol{\bar{x}}_{1}^{T}\boldsymbol{a}_{1} =
 (2,3,2) \left( 
\begin{array}{r} 1 \\ -1 \\ 1 \end{array} \right) 
= 1
\end{displaymath}

And
\begin{displaymath}
\bar{z}_{12} = \boldsymbol{\bar{x}}_{1}^{T}\boldsymbol{a}_{2} =
(2,3,2) \left( \begin{array}{r} -1 \\ 1 \\ 1 \end{array} \right)=
3
\end{displaymath}
hence $\boldsymbol{\bar{z}}_{1} = (1,3)$.   In the same way we can
find $\boldsymbol{\bar{z}}_{2} = (0,4)$ and $\boldsymbol{\bar{z}}_{3} = (11,9)$

We now need to project the new observation into discriminant space:

\begin{displaymath}
\bar{z}_{new1} = \boldsymbol{x}_{new}^{T}\boldsymbol{a}_{1} =
 (5,5,5) \left( \begin{array}{r} 1 \\ -1 \\ 1 \end{array} \right) =
 5
\end{displaymath}
 and

\begin{displaymath}
\bar{z}_{new2} = \boldsymbol{x}_{new}^{T}\boldsymbol{a}_{2} = (5,5,5)
\left( \begin{array}{r} -1 \\ 1 \\ 1 \end{array} \right) =
5
\end{displaymath}


All we need to do now is to assess the three euclidean distances to
see whether $\boldsymbol{z}_{new}$ lies closer to
$\boldsymbol{\bar{z}}_1$, $\boldsymbol{\bar{z}}_2$ or $\boldsymbol{\bar{z}}_3$ 

The distance between $\boldsymbol{\bar{z}}_1$ and
$\boldsymbol{z}_{new}$ is
\begin{displaymath}
d() = \sqrt{(1-5)^2 + (3-5)^2} = 4.47
\end{displaymath}

Likewise, the distance between $\boldsymbol{\bar{z}}_2$ and
$\boldsymbol{z}_{new}$ is
\begin{displaymath}
d() = \sqrt{(0-5)^2 + (4-5)^2} = 45.10
\end{displaymath}

And finally, the distance between $\boldsymbol{\bar{z}}_3$ and
$\boldsymbol{z}_{new}$ is
\begin{displaymath}
d() = \sqrt{(11-5)^2 + (9-5)^2} = 7.21
\end{displaymath}


So we would assign $\bar{x}_{new}$ to group 1!
\end{solution}
\end{shortquiz}

\newpage
\section{Accuracy of discrimination}  


If we are interested in building a classifier from our discriminant
analysis, we need to determine how well it performs.



\subsection{Apparent error rates}

\begin{itemize}
\item Several ways of measuring accuracy
\item Overfitting: we can fit a classifier really really well to our existing data but it doesn't work well next time we carry out a data collection exercise
\item $\therefore$ use 
\begin{itemize}
\item training and 
\item testing sets, 
\end{itemize}
i.e. split the data into two groups, and use one part to build a classifier, and the other to test it.  
\end{itemize}

Johnson and Wichern also refer to ``Lachenbruch's Hold-Out procedure'', which is essential cross-validation, and should be easy to implement in R (see the helpfiles for \texttt{?lda}).


\newpage

\subsection{Confusion matrix}

\begin{itemize}
\item Crosstabulate predicted versus actual classes
\item Correct values on the diagonal
\item Apparent Error Rate (APER) is the proportion of entries on the off-diagonal
\end{itemize}

  

\begin{verbatim}
                   iris.preds$class
iris$Species[-train] setosa versicolor virginica
          setosa         22          0         0
          versicolor      0         28         0
          virginica       0          1        24
\end{verbatim}


\begin{shortquiz}{Janeway}
The APER for the above table is:
\begin{answers}[aper]{4}
\Ans1 $0.013$ &
\Ans0 $0.036$ &
\Ans0 $0.042$ &
\Ans0 $0.007$
\end{answers}
\begin{solution}

\begin{displaymath}
APER = \frac{ \mbox{Number of errors}}{\mbox{Total number of data
    points}} = \frac{1}{75}
\end{displaymath}
\end{solution}
\end{shortquiz}


\newpage

\subsection{Do we need all discriminants}

One key question is to ask whether we need all the available
discriminant functions.

%% within correct here????

One check is by examining Wilk's lambda ($\frac{|\boldsymbol{SSCP}_{Within}|}{|\boldsymbol{SSCP}_{Total}|}$); which can also be found from $\prod_{j-1}^{J}\frac{1}{1+\lambda_{j}}$.   However, we could consider the following from Bartlett:

\begin{equation}
\Lambda^{2} = \left( \sum_{k=1}^{g} n_{k} - 1 - \frac{1}{2}(p + g) \right) \ln (1 + \lambda_{j}),
\end{equation}

Under $H_{0}$ this has a $\chi^{2}$ distribution with $p + g - 2j$ degrees of freedom where $p$ is the number of variables, $g$ is the number of groups and $j$ indexes the eigenvalue. 

We hope we only need a few:- ideally two as these are easily plotted!

Do note that we might investigate the number of discriminant functions
as part of a discriminant analysis, not just as a means to building a
better classifier.


\begin{shortquiz}{Sulu}
The eigenvalues from the iris analysis were reported as $1268.96,
9.33$.   What's the contribution to Wilks Lambda for the second
discriminant function
\begin{answers}[bartlett]{4}
\Ans0 337.41 &
\Ans1 140.83 &
\Ans0 5.45 &
\Ans0 13.45
\end{answers}
\begin{solution}
We have 
\begin{displaymath}
(150-1-\frac{1}{2}7) \log(1 + 9.33) = 140.83
\end{displaymath}

Given that we have $p=4$ variables, $g=3$ groups and we are looking at
eigenvalue $j=2$ we have $\nu=4 + 2 - 2 \times 2$

Hence the critical value of the $\chi^{2}_{2}=7.81$

We would consider that this discriminant function is ``significant''.
\end{solution}
\end{shortquiz}


\newpage

\subsection{Variable selection}

Another key question is to determine whether we need all the available
variables.   Quite often, we need to carry out variable selection.

\begin{itemize}
\item A tolerance test may be used prior to the analysis to remove multicollinear and singular variables
\item Parsimony: you may over-fit to many variables - using less may yield a classifier that fits better in the future
\end{itemize}

\begin{itemize}
\item Examine the ``standardised'' coefficients, perhaps it is safe to remove variables with small standardised coefficients.   
\item Examine the confusion matrix from classifiers with different numbers of variables
\end{itemize}


\newpage

\section{Summary}


\begin{itemize}
\item Disciminant analysis is one (the original?) method for supervised classification: many more available such as classification trees, nearest neighbour methods, learning vector quantization, neural networks, belief networks
\item We have considered methods based upon Fisher's discriminant method.   These find solutions which maximise a particular ratio of between to within group SSCPs.
\item We can use discriminant analysis in a descriptive manner, and
  may (or may not) wish to use hypothesis tests to assist in
  determing how many discriminant functions are informative
\item Alternatively, having found the discriminant functions, we can
  use these to build a classifier.   Such classifier rules need to be
  tested and we need to be particularly cautious about over-fitting
  and exaggerating the performance of our classifier.
\end{itemize}


\newpage

\subsection{Typical Exam type questions}
  
\begin{itemize}
\item Given some loadings $\boldsymbol{a}$, calculate the value of a discriminant function for some data points
\item Interpret a plot of the canonical discriminant functions
\item Explain how these functions are used in classification
\item Explain how to create a classification rule, and limitations in the method
\item Explain where the discriminant functions come from
\item Anything else you would like to suggest
\end{itemize}

\newpage

\subsection{Labwork}
  
  \begin{itemize}
  \item Worked example of discriminant analysis with the wines data - look at the effect of removing variables on the APER, plot the discriminant functions
\item Worked example with the iris data - to look at the way you can use a test and training set
\end{itemize}


 \end{document} 