\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate Methods) Week 4: Introduction to Cluster
Analysis}
\author{Paul Hewson}
\date{23rd October 2008}


\newtheorem{df}{Definition}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}
\maketitle
\tableofcontents

%\section{Background}



\section{Rationale for cluster analysis}

Some people distinguish between ``dissecting'' data (where you are
trying to break the data down into groups for convenience purposes)
and ``clustering'' where you are attempting to recover some ``true''
underlying structure.


\begin{figure}
\begin{center}
\includegraphics[width = 0.7\textwidth]{../images/partclust}
\caption{Artificial data suggesting a difference between ``clustering'' and ``dissecting''}
\label{partclust}
\end{center}
\end{figure}


So we need to inject a massive note of caution here.   ``Cluster
analysis'' is a set of algorithms which aim to find groups in data.
These techniques will find groups and strucures whether or not these
really exist.   It should only therefore be considered an
exploratory/descriptive technique, and great care should be taken with
the interpretation of any results from cluster analysis.

If we were being a bit more ``statistical'' we might consider mixture
models, which does allow some kind of probability structure to be
associated with the exercise.   But, for better or for worse,
``cluster analysis'' seems to be firmly associated with the canon of
multivariate methods.

\newpage

\section{Methods for cluster analysis}


There are a wide range of algorithms that have been developed to investigate clustering within data.   These can be considered in a number of ways:

\begin{itemize}
\item Hierarchical Methods
  \begin{itemize}
  \item Agglomerative clustering (\verb+hclust()+, \verb+agnes()+)
  \item Divisive clustering (\verb+diana()+, \verb+mona()+)
  \end{itemize}
\item Partitioning methods (\verb+kmeans()+, \verb+pam()+, \verb+clara()+)
\end{itemize}


So with hierarchical methods, you either start at the bottom (every
individual in its own group) and work your way up a family tree
(agglomerative), or start at the top (one group) and work your way
down.   Or, more recently a number of proposals have been made in the
literature for hybrid methods, where you go a couple of steps up, and
then one step down (or vice versa).   Alternatively, we consider
partitioning methods, where instead of a ``family tree'', we have some
idea as to the number of groups that may exist in the data.

\newpage


\subsection{Hierarchical Clustering}

We will only consider agglomerative methods in detail.   Consider the following distance matrix

\begin{tabular}{r|ccccc}
 & a & b & c & d & e\\
\hline
a & 0  &   &   &   &\\
b & 2  & 0 &   &   &\\
c & 6  & 5 & 0 &   & \\
d & 10 & 9 & 4 & 0 &  \\
e & 9  & 8 & 5 & 3 & 0\\
\end{tabular}


At the moment, each individual is in it's own cluster!   We now need
to consider strategies for determining which of these should be
grouped with other individuals and when.   The first decision is
obvious, $(a)$ and $(b)$ are the closest and will be grouped first.
But then what.   $(a)$ is six units away from $(c)$ but, $(b)$ is only
five units away from $(c)$.   Which is the distance that matters now?

\newpage

\subsubsection{Nearest neighbour / single linkage}


\begin{itemize}
\item Use \texttt{method = "single"} instruction in the call to \texttt{hclust()}
\item Finds ``friends of friends'' to join each cluster (c.f. minimum spanning trees).
\item Decision to merge groups is based on the distance of the \emph{nearest} member of the group to the \emph{nearest} other object.
\end{itemize}

In our example, with a distance of 2, inviduals $a$ and $b$ are the most similar.

%\begin{table}
\begin{tabular}{r|ccccc}
 & $a$ & $b$ & $c$ & $d$ & $e$\\
\hline
$a$ & 0  &   &   &   &\\
$b$ & 2  & 0 &   &   &\\
$c$ & 6  & 5 & 0 &   & \\
$d$ & 10 & 9 & 4 & 0 &  \\
$e$ & 9  & 8 & 5 & 3 & 0\\
\end{tabular}
%\end{table}

We therefore merge these into a cluster at level 2:
\newpage

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e
\end{tabular}
%\end{table}

and we now need to re-write our distance matrix, whereby:

{\color{blue}\begin{eqnarray*}
d_{([ab][c])} = min(d_{ac},d_{bc}) = d_{bc} = 5\\
d_{([ab][d])} = min(d_{ad},d_{bd}) = d_{bd} = 9\\
d_{([ab][e])} = min(d_{ae},d_{be}) = d_{be} = 8
\end{eqnarray*}}

This gives us a new distance matrix

%\begin{table}
\begin{tabular}{r|ccccc}
 & $(ab)$ & $c$ & $d$ & $e$\\
\hline
$(ab)$ & 0 &   &   &  \\
$c$    & {\color{blue}5} & 0 &   &  \\
$d$    & {\color{blue}9} & 4 & 0 &  \\
$e$    & {\color{blue}8} & 5 & 3 & 0\\
\end{tabular}
%\end{table}



And now we can see what to merge next?

\newpage

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & $a$ $b$ $c$ $d$ $e$\\
2 & $(ab)$ $c$ $d$ $e$\\
3 & $(ab)$ $c$ $(de)$
\end{tabular}
%\end{table}

So, find the minimum distance from $d$ and $e$ to the other objects and reform the distance matrix:

%\begin{table}
\begin{tabular}{r|ccc}
 & $(ab)$ & $c$ & $(de)$\\
\hline
$(ab)$ & 0 &   &    \\
$c$    & 5 & 0 &   \\
$(de)$ & {\color{blue}8} & {\color{blue}4} & 0  \\
\end{tabular}
%\end{table}

And again, clearly, the next merger is between $(de)$ and $c$, at a height of 4, the final merger will take place at a height of 5.

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & $a$ $b$ $c$ $d$ $e$\\
2 & $(ab)$ $c$ $d$ $e$\\
3 & $(ab)$ $c$ $(de)$\\
4 & $(ab)$ $(cde)$\\
5 & $(abcde)$
\end{tabular}
%\end{table}

\newpage

\subsubsection{Furthest neighbour / complete linkage}

But that isn't the only way of deciding on the distance between
``merged'' objects.   We could consider the furthest distance.

\begin{itemize}
\item Use the \texttt{method = "complete"} instruction in the call to \texttt{hclust()}
\item Finds ``similar'' clusters.
\item Objects are merged when the \emph{furthest} member of the group is close enough to the new object.
\end{itemize}

%\begin{table}
\begin{tabular}{r|ccccc}
 & a & b & c & d & e\\
\hline
a & 0  &   &   &   &\\
b & 2  & 0 &   &   &\\
c & 6  & 5 & 0 &   & \\
d & 10 & 9 & 4 & 0 &  \\
e & 9  & 8 & 5 & 3 & 0\\
\end{tabular}
%\end{table}

Starts as before, merge $a$ and $b$ as these are the nearest:

\newpage

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e
\end{tabular}
%\end{table}


Life changes now when we calculate the new distance matrix:

{\color{blue}\begin{eqnarray*}
d_{([ab][c])} = max(d_{ac},d_{bc}) = d_{ac} = 6\\
d_{([ab][d])} = max(d_{ad},d_{bd}) = d_{ad} = 10\\
d_{([ab][e])} = max(d_{ae},d_{be}) = d_{ae} = 9
\end{eqnarray*}}

%\begin{table}
\begin{tabular}{r|ccccc}
 & (ab) & c & d & e\\
\hline
(ab) & 0  &   &   &  \\
c    & {\color{blue}6}  & 0 &   &  \\
d    & {\color{blue}10} & 4 & 0 &  \\
e    & {\color{blue}9}  & 5 & 3 & 0\\
\end{tabular}
%\end{table}

So what do we merge next?

\newpage


%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e\\
3 & (ab) c (de)
\end{tabular}
%\end{table}

And reforming the new distance matrix:

%\begin{table}
\begin{tabular}{r|ccc}
 & (ab) & c & (de)\\
\hline
(ab) & 0 &   &    \\
c    & 6 & 0 &   \\
(de) & {\color{blue}10} & {\color{blue}5} & 0  \\
\end{tabular}
%\end{table}

Compare the next merge with the same step before, but compare the heights (noting this is a very artificial example)

\newpage

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e\\
3 & (ab) c (de)\\
5 & (ab) (cde)
\end{tabular}
%\end{table}

and the final distance matrix:

%\begin{table}
\begin{tabular}{r|cc}
 & (ab) & (cde)\\
\hline
(ab)  & 0   &     \\
(cde) & 10 &  0  \\
\end{tabular}
%\end{table}

So the final merge this time is at height 10.

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e\\
3 & (ab) c (de)\\
5 & (ab) (cde)\\
10 & (abcde)
\end{tabular}
%\end{table}

This is a very artificial example.   Merges happen in the same order, but at different heights.   In more realistic examples you would expect to see some different mergers taking place

\newpage

\subsubsection{Average link}

And perhaps obviously, a ``middle way'' suggests that we take the
average distance.   This is the last example we will work by hand

\begin{itemize}
\item  Requires \texttt{agnes()} in package \texttt{cluster}
\item Use with the \texttt{method="average"} instruction.
\item Merge two groups is the average distance between them is small enough
\end{itemize}


Again, we start by merging $a$ and $b$, but again the reduced distance matrix will be different:

{\color{blue}\begin{eqnarray*}
d_{([ab][c])} = (d_{ac} + d_{bc})/2  = 5.5\\
d_{([ab][d])} = (d_{ad} + d_{bd})/2  = 9.5\\
d_{([ab][e])} = (d_{ae}+ d_{be})/2  = 8.5
\end{eqnarray*}}


%\begin{table}
\begin{tabular}{r|ccccc}
 & (ab) & c & d & e\\
\hline
(ab) & 0  &   &   &  \\
c    & {\color{blue}5.5}  & 0 &   &  \\
d    & {\color{blue}9.5} & 4 & 0 &  \\
e    & {\color{blue}8.5}  & 5 & 3 & 0\\
\end{tabular}
%\end{table}


Merge $d$ and $e$, at height 3:

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e\\
3 & (ab) c (de)
\end{tabular}
%\end{table}

\newpage
Again, need to recalculate distances:

%\begin{table}
\begin{tabular}{r|ccc}
 & (ab) & c & (de)\\
\hline
(ab) & 0 &   &    \\
c    & 5.5 & 0 &   \\
(de) & {\color{blue}9} & {\color{blue}4.5} & 0  \\
\end{tabular}
%\end{table}


after merging $(de)$ and $c$:

%\begin{table}
\begin{tabular}{r|cc}
 & (ab) & (cde)\\
\hline
(ab)  & 0   &     \\
(cde) & 7.8 &  0  \\
\end{tabular}
%\end{table}

our final merge will take place at height $7.8$.

%\begin{table}
\begin{tabular}{ll}
Distance & Groups\\
\hline
0 & a b c d e\\
2 & (ab) c d e\\
3 & (ab) c (de)\\
4.5 & (ab) (cde)\\
7.8 & (abcde)
\end{tabular}
%\end{table}


\newpage

\subsubsection{Plotting the results}

The dendrogam is the standard plot for these kinds of results:

\begin{figure}
\includegraphics[width = 0.7\textwidth]{../images/democlust}
\caption{Dendrograms from three basic cluster methods}
\label{simpleclust}
\end{figure}

In this case, the basic shape and order of merges was the same - only
the heights of the merge points differed.   But don't be misled by
this simple example!   Also, what would happen with the
Peter,Jacqueline,Lieve,Ila data if we used different distance measures?

\newpage


Here's a slightly more interesting example:

\begin{tabular}{l|rrrrr}
  & a & b & c  & d & e \\
\hline
a & 0 &   &    &   &   \\
b & 4 & 0 &    &   &   \\
c & 6 & 9 & 0  &   &   \\
d & 1 & 7 & 10 & 0 &   \\
e & 6 & 3 & 5  & 8 & 0
\end{tabular}


\begin{shortquiz}{Hierarchical Clustering}
\begin{questions}
\item The first merge for any method will be between:
\begin{answers}[c1]{5}
\Ans0 (ab) &
\Ans0 (ac) &
\Ans1 (ad) &
\Ans0 (ae) &
\Ans0 (bc) \\
\Ans0 (bd) &
\Ans0 (be) &
\Ans0 (cd) &
\Ans0 (ce) &
\Ans0 (de)
\end{answers}
\begin{solution}
Cleraly the distance between a and d is the lowest (1), and these are
the first to be merged.
\end{solution}
\newpage
\item Now assume we are using single linkage.   What will be the
  distance between (ad) and b:
\begin{answers}[c3]{3}
\Ans1 4 &
\Ans0 3.5 &
\Ans0 7
\end{answers}
\begin{solution}
For single linkage, we want the distance between the nearest entities:
\begin{displaymath}
d_{([ad][b])} = min(d_{ab},d_{bd}) = d_{ab} = 4\\
\end{displaymath}
\end{solution}
\item Now assume we are using complete linkage.   What will be the
  distance between (ad) and b:
\begin{answers}[c3]{3}
\Ans0 4 &
\Ans0 3.5 &
\Ans1 7
\end{answers}
\begin{solution}
For complete linkage, we want the distance between the furthest entities:
\begin{displaymath}
d_{([ad][b])} = max(d_{ab},d_{bd}) = d_{ad} = 7\\
\end{displaymath}
\end{solution}
\item Now assume we are using average linkage.   What will be the
  distance between (ad) and b:
\begin{answers}[c3]{3}
\Ans0 4 &
\Ans1 5.5 &
\Ans0 7
\end{answers}
\begin{solution}
For average linkage, we want the averagedistance between the entities:
\begin{displaymath}
d_{([ad][b])} = mmean(d_{ab},d_{bd}) = \frac{1}{2}(d_{ab} + d_{bd}) = 5.5\\
\end{displaymath}
\end{solution}
\end{questions}
\end{shortquiz}

You really should complete the clustering exercise for all three
methods now, before attempting the next question.

\begin{shortquiz}{Dendrograms}
Consider the following figure:

%\includegraphics[width = 0.9\textwidth]{../images/jw12p6}
Figure temporarily misplaced, still looking sorry.

\begin{questions}
\item The dendrogram in figure X has been generated by:
\begin{answers}[d1]{3}
\Ans0 Single linkage &
\Ans0 Average linkage &
\Ans1 Complete linkage
\end{answers}
\item The dendrogram in figure Y has been generated by:
\begin{answers}[d1]{3}
\Ans0 Single linkage &
\Ans1 Average linkage &
\Ans0 Complete linkage
\end{answers}
\item The dendrogram in figure Z has been generated by:
\begin{answers}[d1]{3}
\Ans1 Single linkage &
\Ans0 Average linkage &
\Ans0 Complete linkage
\end{answers}
\item Which linkage method can produce ``snake-like'' clusters:
\begin{answers}[d1]{3}
\Ans1 Single linkage &
\Ans0 Average linkage &
\Ans0 Complete linkage
\end{answers}

\end{questions}
\end{shortquiz}





\newpage

\subsubsection{Alternative methods}



\begin{itemize}
\item Cluster ``analysis'' is an \emph{algorithmically} guided exploratory data analysis
\item Many other methods proposed: Lance-Williams recurrence formula
  attempts to encapsulate many of these 
  \item Ward's method:

\begin{itemize}
\item Minimises the error sum of squares:
\begin{displaymath}
ESS_{k} = \sum_{i+1}^{n_{k}} \sum_{j=1}^{p} (x_{ki,j} - \bar{\boldsymbol{x}}_{k,j})^{2}
\end{displaymath}
where $ \bar{\boldsymbol{x}}_{k,j}$ is the mean of cluster $k$ with respect to variable $j$ and $x_{ki,j}$ is the value of $j$ for each object $i$ in cluster $k$.   The total error sum of squares given by $\sum_{k=1}^{K} ESS_{k}$ for all clusters $k$.   
\item Ward's method tends to give spherical clusters (whether reasonable or not).
\end{itemize}

  
\end{itemize}


\begin{table}
\tiny
\begin{tabular}{lrrrr}
Method & R call &  $\alpha_{k}$ & $\beta$ & $\gamma$ \\
\hline
Single link (nearest neighbour) & \verb+method = "single"+ & $\frac{1}{2}$ & 0 & $-\frac{1}{2}$ \\
Complete link (furthest neighbour) &  \verb+method = "complete"+ & $\frac{1}{2}$ & 0 & $\frac{1}{2}$ \\
Group average link &  \verb+method = "average"+ & $N_{l}|(N_{l} + N_{m})$ & 0 & 0\\
Weighted average link &  \verb+method = "mcquitty"+ & $\frac{1}{2}$ & 0 & 0 \\
Centroid &   \verb+method = "centroid"+ & $N_{l}|(N_{l} + N_{m})$ &  $- N_{l} N_{m}|(N_{l} + N_{m})^2$ & 0\\
%Sum of squares &  \verb+method = ""+ &  $(N_{i} + N_{K})|(N_{i} + N_{j} + N_{k})$ & $-(N_{K} + N_{k})|(N_{i} + N_{j} + N_{K})$
Incremental sum of squares &  \verb+method = "ward"+ & $\frac{N_{k} + N_{m}}{N_{k} + N_{l} + N_{m}}$ &  
$\frac{N_{k} + N_{l}}{N_{k} + N_{l} + N_{m}}$ & 0 \\
Median &  \verb+method = "median"+ & $\frac{1}{2}$ & $-\frac{1}{4}$ & 0\\
%Flexible & $\frac{1}{2}(1-\beta)$ & $\beta(<1)$ & 0\\
\end{tabular}
\end{table}
where $N_{k}, N_{l}\ \mbox{and}\ N_{m}$ are the cluster sizes when $C_{k}$ is joined to the other two clusters considered.





\newpage

\subsection{Problems with hierarchical clustering}

There are several well known problems, such as reversals in the dendrogram and (with single link clustering): chaining

\begin{figure}
\includegraphics[width = 0.7\textwidth]{../images/chaining}
\caption{Demonstration of ``chaining'' with single link clustering}
\label{chaining}
\end{figure}



\subsection{Hierarchical clustering in R}




\begin{enumerate}
\item Create a distance matrix
\item Apply the cluster algorithm
\item Plot the results (dendrogam)
\item Cut the tree at a suitable point if you want distinct groups, plot the original data with this classification
\item Get some measures of fit
\end{enumerate}


\begin{verbatim}
> USArrests.dist <- dist(USArrests, 
       method = "manhattan")
> USArrests.hclust <- hclust(USArrests.dist, 
       method = "complete")
> plot(USArrests.hclust)
> US.cut <- cutree(USArrests.hclust, k=5)
> plot(USArrests, col = US.cut, pch = US.cut)
\end{verbatim}


\begin{figure}
\includegraphics[width = 0.7\textwidth]{../images/hclust}
\caption{Dendrogram following complete linkage cluster analysis of US Arrests data}
\label{hclust}
\end{figure}
\newpage




\begin{shortquiz}{Interpretation}
\begin{questions}
\item Have a look at the dendrogram for the US Arrests (previous slide).
How many sub-groups of states do you think there should be:
\begin{answers}[i1]{5}
\Ans1 $1$ &
\Ans1 $2$ &
\Ans1 $3$ &
\Ans1 $4$ & 
\Ans1 $\geq 5$
\end{answers}
\begin{solution}
Try clicking on one the other answers - there are no right or wrong
answers here - more a subjective judgement as to how you may interpret
the output.   But there are a few things we could check - for example
if our data were purely random noise we shouldn't go around trying to
intpret that.   And we ought to make sure that our data aren't too
complex to summarise by a dendrogram.
\end{solution}
\item If you assume (rightly or wrongly) a three cluster solution, how
  might you interpret the three groups:
\begin{answers}[i2]{1}
\Ans1 Urban, rural, semi-urban states
\Ans1 Southern, mid-western and north eastern states
\Ans1 A random grouping of states found by an untested algorithm
\end{answers}
\begin{solution}
I was a bit flippant when I set this question up.   I do think that
there seems to be a very rural southern states grouping, and a very
urban, modern, liberal leaning north eastern state grouping.   We
might have an interesting 3 group cluster solution that you can
interpret.   But do be careful.
\end{solution}
\end{questions}
\end{shortquiz}
 

\newpage
\section{Measure of fit}


\subsection{Cophenetic Correlation}

The cophenetic correlation can be used as some kind of measure of the goodness of fit of a particular dendrogram.

\begin{equation}
\rho_{Cophenetic} = \frac{\sum_{i=1, j=1, i <j}^{n} (d_{ij} - \bar{d})(h_{ij} - \bar{h})}{
\left(\sum_{i=1, j=1, i <j}^{n} (d_{ij} - \bar{d})^{2}(h_{ij} - \bar{h})^{2} \right)^{0.5}}
\end{equation}

Easily extracted in R, but less clear what it means.   A value {\color{red}below 0.6} implies some distortion in the dendrogram.


\newpage


\section*{}

%\includegraphics[width=0.4\textwidth]{../images/DavidWishart}
Home page of \href{http://www.st-andrews.ac.uk/~dw4/}{\color{blue}
  David Wishart}: originator of ``Clustan''.   What books has he
authored, and which to which Scottish product has Clustan been
extensively applied?

\newpage


\section{k-means clustering}

A very different approach

\begin{itemize}
\item Aimed at finding ``more homogenous'' subgroups within the data.
\item We specify at the start how many clusters we are looking for, 
\item Ideally provide some clue as to where those clusters might be
\item Given a number of $k$ starting points, the data are classified, the centroids recalculated and the process iterates until stable.

\end{itemize}


\begin{figure}
\includegraphics[width = 0.7\textwidth]{../images/kmeansdemo}
\label{kmeansdemo}
\caption{Demonstration of kmeans algorithm, points are classified according to seed, then position of seed is recalculated}
\end{figure}

\subsection{k-means clustering in R}

Very easily applied

\begin{verbatim}
> US.km <- kmeans(USArrests, centers = 5)
> plot(USArrests, col = US.km$cluster, 
    pch = US.km$cluster)
\end{verbatim}

But you could compare the Rand index from this solution with that from hierarchical clustering






\section{Measures of ``fit''}




\subsection{Rand Statistic}


\begin{itemize}
\item Rand's $C$ statistic compares the way two clustering schemes partition the object space\footnote{W.M.Rand (1971) ``Objective criteria for the evaluation of clustering methods'' \textit{Journal of the American Statistical Association} \textbf{66}:846-850} i.e. compares cluster solutions in terms of group membership
\item $0 \leq C \leq 1$, where a value of 1 implies perfect agreement
\item Applied to the classes you identify (in the USArrests data we decided 5 classes were appropriate) and can be used with other clustering methods
\end{itemize}




\section{Summary}

\begin{itemize}
\item Cluster analysis is all about finding groups of individuals who are more ``alike'' than others
\item Vast number of applications
\item Hierarchical clustering is based upon a distance\footnote{by whatever method} matrix; - we have worked examples with nearest / furthest neighbour and group average but there are popular alternatives (Ward's)
\item k-means is another approach to clustering
\item Can extract and plot group memberships
\item Cophenetic correlation can assess how much disortion is caused by hierarchical clustering; Rand index compares two cluster solutions
\item Algorithm based approach - unpopular with some!   Each method has advantages and disadvantages
\end{itemize}



\begin{shortquiz}{Overview}
\begin{questions}
\item Which technique produces a dendrogram
\begin{answers}[r1]{2}
\Ans0 k-means
\Ans1 Hierarchical clustering
\end{answers}
\item Regardless of the data your provide, clusters will always be
  created
\begin{answers}[r2]{2}
\Ans1 True &
\Ans0 False
\end{answers}
\item The Rand index measures:
\begin{answers}[r3]{1}
\Ans0 The similarity between a distance matrix and a representation of
a distance matrix (such as a dendrogram)\\
\Ans1 The agreement between different classifications methods in terms
of assignment to a certain number of groups
\end{answers}
\item The Cophenetic correlation measures:
\begin{answers}[r3]{1}
\Ans1 The similarity between a distance matrix and a representation of
a distance matrix (such as a dendrogram)\\
\Ans0 The agreement between different classifications methods in terms
of assignment to a certain number of groups
\end{answers}
\item Hierarchical clustering analysis always uses the Euclidean
  distance
\begin{answers}[r4]{2}
\Ans0 True & 
\Ans1 False 
\end{answers}
\item K-means clustering uses the Euclidean distance
\begin{answers}[r5]{2}
\Ans1 True &
\Ans0 False
\end{answers}
\item We produced a dendrogram using Nearest and Furthest Neighbour
  clustering.   We measured the cophenetic correlation as $0.43$ and
  $0.55$ respectively.   We should therefore use the results of:
\begin{answers}[r6]{1}
\Ans0 Nearest neighbour (single) linkage \\
\Ans0 Furthest neighbour (complete) linkage \\
\Ans1 Neither method
\end{answers}
\begin{solution}
Although the cophenetic correlation is not a well understood measure,
and we certainly do not have any idea of its distribution under any
kind of null hypotheses, convention suggests that values below 0.6 are
worrying.   We might be tempted therefore to reject both clustering
solutions as being of poor quality.
\end{solution}
\end{questions}
\end{shortquiz}



\subsection{A sanity check:}

\begin{itemize}
\item $S_{k,n}$, the number of ways of partitioning $n$ objects into $k$ groups is given by:
\begin{displaymath}
S_{k,n} = \frac{1}{k!} \sum_{j=1}^{k} \left( \begin{array}{c}k \\ j \end{array} \right) (-1)^{k-j} j^{n} \approx_{n \to \infty} \frac{k^{n}}{k!}
\end{displaymath} 
a second type Stirling number.   
\item Where $k$ is not specified we have $\sum_{k=1}^{K} S_{k,n}$ partitions.  
\item For $n= 50$ and $k=2$ this is in the order of $6 \times 10^{29}$
\end{itemize}


\begin{itemize}
\item These Stirling numbers assume you have \emph{one} method
\item As you've seen, there are many different distance measures, many different clustering algorithms
\item Outside our world, there are also numerous other methods for unsupervised classification
\item Projection and scaling methods may also be used in an attempt to visualise groupings (more on this later)
\item Some statisticians prefer mixture models
\end{itemize}

\newpage


\subsection{Further reading}

\begin{itemize}
\item See the online reading list for a few useful resources, including links to animations
\item Manly chapter 9
\item Johnson and Wichern chapter 12
\end{itemize}


\newpage

\subsection{Common exam questions}

\begin{itemize}
\item Explain why we use cluster analysis, give some examples where it has been useful
\item Explaining about different types of cluster analysis, and demonstrate awareness of the various sub-methods
\item Explain and work out distances, especially Gower's, Euclidean, Minkowski, Manhattan etc.
\item Explain and work through a simple cluster analysis using nearest, furthest or group average linkage
\item Justify a choice of cluster solution plots, diagnostics (cophenetic, Rand)
\item Interpret some cluster results in context
\item Anything else you might like to suggest \ldots
\end{itemize}


\subsection{Lab. work}

\begin{itemize}
\item You've been given notes for a worked example using \texttt{milk} data in \texttt{flexclust} library on the chemical composition of mammalian milk from a number of species
\item This example is a bit obvious, all methods seem to lead to the same solutions, but check out the methods - distance (choice of measure), hierarchical cluster (choice of method), graphics (dendrogram, then cut at a suitable point), graphics (look at groupings on original data), diagnostics
\item Then carry out a cluster analysis using \texttt{nutrient} data from \texttt{flexclust}
\item Interpret a cluster analysis of the class (use \texttt{daisy()} to get Gower's out)!
\end{itemize}


\end{document}
