\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage{mathrsfs}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate) Week 9: Principal Co-ordinates Analysis
  / Multidimensional Scaling}
\author{Paul Hewson}
\date{4th December 2008}

\newtheorem{df}{Definition}
\newtheorem{theorem}{Theorem}


\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}


\maketitle

\tableofcontents

\section{Outline}

\subsection{Aims of this week}

\begin{itemize}
\item To introduce multidimensional scaling / principal co-ordinates analysis as a means of finding a low dimensional representation of data points based upon a distance matrix
\item To consider relationships with principal component analysis
\end{itemize}



\begin{shortquiz}{Q and R techniques}
\begin{questions}
\item Principal co-ordinates analysis is:
\begin{answers}[pco]{2}
\Ans1 A ``Q-technique'' &
\Ans0 An ``R-technique'' 
\end{answers}
\item Principal components analysis is:
\begin{answers}[pca]{2}
\Ans0 A ``Q-technique'' &
\Ans1 An ``R-technique''
\end{answers}
\end{questions}
\end{shortquiz}


\newpage

\section{Rationale}


\subsection{Reasons for principal co-ordinate analysis}

Clustering techniques partition an $n \times n$ matrix of dissimilarities into groups of like individuals.

\begin{itemize}
\item Scaling lets us visualise the relative distances bewteen individuals in a low dimensional structure.  
\item \emph{principal co-ordinates analysis} also known as classical or metric scaling is one variant bearing a strong relationship to principal component analysis.
\item There are also techniques for \emph{non-metric} scaling
\end{itemize}


\newpage

\subsubsection{A familiar method:}
  
\includegraphics[width = 0.7\textwidth]{../images/hclustmilk}
  

\newpage

\subsubsection{A 2 dimensional approximation based on the distance matrix:}
  
  \includegraphics[width = 0.7\textwidth]{../images/pcomilk}
  

\newpage


\subsection{One approach}


\subsubsection{Distance matrices}
  
Classical \emph{metric} multidimensional scaling is based on the $n \times n$ distance or dissimilarity matrix. 
Consider (any) distance matrix $\boldsymbol{\Delta}$.   

\begin{itemize} 
\item $\boldsymbol{\Delta}$ is metric if its elements satisfy the metric (triangle) inequality $\delta_{ij} \leq \delta_{ik} + \delta_{kj}$ for all $i$, $j$, $k$.
\end{itemize}

e.g. the matrix $\boldsymbol{\Delta}$ of \emph{Euclidean} distances between objects, such that $\delta_{ij}$ is the distance between points $i$ and $j$.   
\begin{itemize}
\item These $n$ points can be represented in $n-1$ dimensional space.   
\item We are looking for a configuration of $n$ points such that the distance $d_{ij}$ between points $i$ and $j$ equals the dissimilarity $\delta_{ij}$ for all $i$, $j$.   
\item The dimensionality of this configuration is $q$ such that we are seeking to reconstruct an $n \times q$ matrix $\boldsymbol{X}$.
\end{itemize}


\newpage

\subsubsection{(Euclidean) distance matrix}

We can very easily get from an $n \times p$ matrix $\boldsymbol{X}$ to a Euclidean distance matrix.   

\begin{itemize}
\item First, form the $n \times n$ matrix $\boldsymbol{Q}$ where $\boldsymbol{Q} = \boldsymbol{X}\boldsymbol{X}^{T}$;
\item i.e. 
\begin{equation}
q_{rs} = \sum_{j = 1}^{p} x_{rj}x_{sj}
\end{equation}
\item We know that the Euclidean distance is given by:
\begin{eqnarray*}
\delta_{rs}^{2} &=& \sum_{j=1}^{p} (x_{rj} - x_{sj})^{2} \\
 &=& \sum_{j=1}^{p} (x_{rj}^{2}) + \sum_{j=1}^{p} (x_{sj}^{2} - 2) \sum_{j=1}^{p} x_{rj}x_{sj} \\
&=&  q_{rr} + q_{ss} - 2q_{rs}
\end{eqnarray*}
\end{itemize}

\newpage

\subsubsection{Reversing this process}

So given $\boldsymbol{X}$ we can find $\boldsymbol{Q} = \boldsymbol{X}\boldsymbol{X}^{T}$ and hence find the Euclidean distance.
%proof pg 106

\begin{itemize}
\item Note that our recovered $n \times q$ matrix is not uniquely defined - it is only defined up to {\color{red}\emph{translation}}, {\color{red}\emph{rotation}} and {\color{red}\emph{reflection}}.   
\item $\therefore$ assume $\boldsymbol{X}$ has been centred (i.e. make column means zero, such that $\sum_{i=1}^{n} y_{ij} = 0)$.    
\item Also note that we don't necessarily want to recover an $n \times p$ matrix, we can reconstruct a matrix with up to $n-1$ columns, but as with other dimension reduction techniques we hope to find an $n \times q$ matrix with $q < p$; clearly if $q = 2$ it will be easy to visualise the results.   
\end{itemize}

\newpage

\subsubsection{Not uniquely defined \ldots}


\includegraphics[width = 0.6\textwidth]{../images/eurodist}


\newpage


To recover $\boldsymbol{Q}$ from $\boldsymbol{D}$ we firstly note the following:

\begin{eqnarray}
\sum_{r=1}^{n} d_{rs}^{2} = trace(\boldsymbol{Q}) + n q_{ss}\\
\sum_{s=1}^{n} d_{rs}^{2} = n q_{rr} + trace(\boldsymbol{Q}) \\
\sum_{r=1}^{n} d_{rs}^{2} \sum_{s=1}^{n} d_{rs}^{2} = 2 n trace(\boldsymbol{Q})
\end{eqnarray}


\subsubsection{So the procedure is:}

By rearranging this and manipulating the equations above which lead us
to our distance matrix, we can recover elements of $\boldsymbol{Q}$
from $\boldsymbol{D}$ using a double centering procedure:


\begin{displaymath}
q_{ij} = -\frac{1}{2} ( d_{ij}^{2} - d_{i \cdot}^{2} -  d_{\cdot j}^{2} +  d_{\cdot \cdot}^{2})
\end{displaymath}

The dots denote \emph{means} taken over the relevant indices.

\newpage


% mention double centering here
So, to find $\boldsymbol{Q}$ given $\boldsymbol{D}$:

\begin{itemize}
\item Square it element by element
\item Double centre it 
  \begin{itemize}
  \item  subtract column means
  \item subtract row means
  \item add overall mean
  \end{itemize}
\item Multiply by $-\frac{1}{2}$.
\end{itemize}

\begin{shortquiz}
Given $\boldsymbol{D} = \left( \begin{array}{ll} 0 & 0.2 \\ 0.2 & 0 \end{array} \right)$ find $\boldsymbol{Q}$
\begin{answers}[findQ]{2}
\Ans0 $\left( \begin{array}{rr} -0.01 & 0.01 \\ 0.01 & -0.01 \end{array} \right)$ &
\Ans0 $\left( \begin{array}{rr} -0.02 & 0.02 \\ 0.02 & -0.02 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 0.02 & -0.02 \\ -0.02 & 0.02 \end{array} \right)$ &
\Ans1 $\left( \begin{array}{rr} 0.01 & -0.01 \\ -0.01 & 0.01 \end{array} \right)$
\end{answers}
\begin{solution}
Firstly we need to square each element of $\boldsymbol{D}$ giving $\boldsymbol{D}^2 = \left( \begin{array}{rr} 0.04 & 0 \\ 0 & 0.04 \end{array} \right)$.   Noting that the column means are all $0.02$, the row means are $0.02$ and the grand mean is also $0.0$ means that the double centering is rather simple.   We subtract $0.02$ from the entries in each row to remove row means, subtract $0.02$ from the entries in each column to remove the column mean and finally we add $0.02$ to add the grand mean.

This gives a double centred matrix $\left( \begin{array}{rr} -0.02 & 0.02 \\ 0.02 & -0.02 \end{array} \right)$.   All that remains is to multiple this matrix by the scalar $\frac{1}{2}$
\end{solution}
\end{shortquiz}


%some people approximate Q with a similarity matrix

\newpage

\subsubsection{What do we do with Q?}

\begin{itemize}
\item We have $\boldsymbol{Q}$ where $\boldsymbol{Q} = \boldsymbol{XX}^{T}$
\item All we need is to find a suitable $\boldsymbol{X}$, i.e. this is a matrix square root problem.   
\item Given Euclidean distances, $\boldsymbol{Q}$ is symmetric and we can do this using the \emph{spectral decomposition}
\end{itemize}

Spectral decomposition: $\boldsymbol{Q} = \boldsymbol{E \Lambda E}^{T}$ where $\boldsymbol{\Lambda} = \lambda_{1}, \lambda_{2}, \ldots, \lambda_{n}$, a diagnonal matrix of ordered eigenvalues and $\boldsymbol{E}$ is the matrix whose columns are the corresponding (normalised) eigenvectors.   

\begin{itemize}
\item If  $\boldsymbol{Q} = \boldsymbol{E \Lambda}^{\frac{1}{2}} \boldsymbol{\Lambda^{\frac{1}{2}} E}^{T}= \boldsymbol{X} \boldsymbol{X}^{T}$ then 
\item $\boldsymbol{X} =  \boldsymbol{E \Lambda}^{\frac{1}{2}}$ and we have recovered the co-ordinates from the inter-point distances.
\end{itemize}

Given:
\begin{displaymath}
\boldsymbol{X} = \left( \sqrt{\lambda_{1}} \left( \begin{array}{r} e_{11} \\ e_{12} \\ \vdots \\ e_{1n} \end{array} \right), \ldots,   \sqrt{\lambda_{n}} \left( \begin{array}{r} e_{n1} \\ e_{n2} \\ \vdots \\ e_{nn} \end{array} \right) \right)
\end{displaymath}
if we want a one dimensional representation, we just use  $\left( \sqrt{\lambda_{1}} \boldsymbol{e}_{1} \right) $, for a two dimensional representation we would use  $\left( \sqrt{\lambda_{1}} \boldsymbol{e}_{1},   \sqrt{\lambda_{2}} \boldsymbol{e}_{2} \right)$

\newpage

\begin{shortquiz}
\begin{questions}
\item Consider the matrix $\boldsymbol{Q} = \left( \begin{array}{rr}7 & 2 \\ 2 & 7 \end{array} \right)$.   Find the eigenvalues of $\boldsymbol{Q}$
\begin{answers}[eigvalq]{4}
\Ans0 $9,3$ &
\Ans0 $7,5$ &
\Ans1 $9,5$ &
\Ans0 $7,3$
\end{answers}
\begin{solution}
Maybe I should add the solution here at some point?   Hopefully this is quite well understood by now
\end{solution}
\item Now find the eigenvalues of $\boldsymbol{Q}$
\begin{answers}[eigvecq]{2}
\Ans1 $\left( \begin{array}{rr} 0.707 & -0.707 \\ 0.707 & 0.707 \end{array} \right) $ &
\Ans0 $\left( \begin{array}{rr} -0.707 & 0.707 \\ 0.707 & 0.707 \end{array} \right) $ \\
\Ans0 $\left( \begin{array}{rr} 0.707 & 0.707 \\ -0.707 & 0.707 \end{array} \right) $ &
\Ans0 $\left( \begin{array}{rr} 0.707 & 0.707 \\ 0.707 & -0.707 \end{array} \right) $
\end{answers}
\begin {solution}
I need to work this through as there is probably at least one other solution
\end{solution}
\item Now find $z_{1}$
\begin{answers}[z]{2}
\Ans0 $0.707, 0.707$ & 
\Ans1 $2.121, 2.121$ \\
\Ans0 $6.363, 6.363$ &
\Ans0 $9,5$
\end{answers}
\begin{solution}
Assuming you agreed with the previous answer we need $\sqrt{\lambda} \boldsymbol{e}_1 = \sqrt{9} (0.707, 0.707)$.   These are our ``reconstructed'' data points for the first variable
\end{solution}
\item We can reflect the solution to obtain any other solution we feel like:
\begin{answers}[rotreftrans]{2}
\Ans1 True &
\Ans0 False
\end{answers}
\begin{solution}
Remember, PCO only gives us a solution defined as far as \emph{rotation, reflection and translation}
\end{solution}
\end{questions}
\end{shortquiz}


\newpage

\section{PCO and PCA}


A short diversion \ldots

Consider the centred data matrix $\boldsymbol{X}$; and remember that 
\begin{displaymath}
S = \frac{1}{n-1} \boldsymbol{X}^{T} \boldsymbol{X}
\end{displaymath}

\begin{itemize}
\item Principal components come from an eigenanalysis of $\boldsymbol{S}$.
\item For now, denote the eigenvalues of $\boldsymbol{S}$ by $\mu_{i}$ and associated eigenvectors by $\boldsymbol{a}_{i}$:
\end{itemize}

\newpage

\subsubsection{The relationship:}


\begin{eqnarray*}
\boldsymbol{S} \boldsymbol{a}_{i} &=& \mu_{i} \boldsymbol{a}_{i}\\
\frac{1}{n-1}\boldsymbol{X}^{T} \boldsymbol{X} \boldsymbol{a}_{i} &=& \mu_{i} \boldsymbol{a}_{i}\\
\boldsymbol{X}^{T} \boldsymbol{X} \boldsymbol{a}_{i} &=& (n-1)\mu_{i} \boldsymbol{a}_{i}\\
\boldsymbol{X}\boldsymbol{X}^{T} \boldsymbol{X} \boldsymbol{a}_{i} &=& (n-1)\mu_{i} \boldsymbol{X} \boldsymbol{a}_{i}\\
\boldsymbol{Q} \underbrace{\boldsymbol{X} \boldsymbol{a}_{i}}_{\boldsymbol{z}_{i}} &=& (n-1)\mu_{i} \underbrace{\boldsymbol{X} \boldsymbol{a}_{i}}_{\boldsymbol{z}_{i}}
\end{eqnarray*}

So $\boldsymbol{X} \boldsymbol{a}_{i} = \boldsymbol{z}_{i}$ is an eigenvector of $\boldsymbol{Q}$ with corresponding eigenvalue $(n-1) \mu_{i}$.   



\subsubsection{Normalising}
  
If we want a normalised eigenvector it may be worth noting that the length can be found as follows:

\begin{eqnarray*}
\lVert \boldsymbol{z}_{i} \rVert^{2} = \boldsymbol{z}_{i}^{T}\boldsymbol{z}_{i} = \boldsymbol{a}_{i} \boldsymbol{X}^{T} \boldsymbol{X} \boldsymbol{a}_{i} &=& (n-1) \boldsymbol{a}_{i}^{T} \boldsymbol{C} \boldsymbol{a}_{i}\\
 &=& (n-1) \boldsymbol{a}_{i}^{T} \mu_{i} \boldsymbol{a}_{i}\\
 &=& (n-1) \mu_{i} \frac{\boldsymbol{a}_{i}^{T} \boldsymbol{a}_{i}}{||\boldsymbol{a}_{i}||^{2}}\\
 &=& (n-1) \mu_{i}
\end{eqnarray*}

\begin{itemize}
\item So, $\lVert \boldsymbol{z}_{i} \rVert = \sqrt{(n-1) \mu_{i}}$.   
\item Hence a normalised eigenvector for $\boldsymbol{Q}$ takes the form $\frac{1}{(n-1)\mu_{i}} \boldsymbol{Xa}_{i}$ with eigenvalue $(n-1) \mu_{i}$
\end{itemize}


$\therefore$ eigenvalues and eigenvectors found from multidimensional scaling / principal co-ordinates analysis are related to those found from decomposition of the covariance of the scaled data matrix:

\begin{eqnarray*}
\lambda_{i} = (n-1) \mu_{i}\\
e_{i} = \frac{1}{\sqrt{\lambda_{i}}} \boldsymbol{Xa}_{i}
\end{eqnarray*}

Remember that $\boldsymbol{Z} = \boldsymbol{XA}^{T}$, where:

\begin{displaymath}
\boldsymbol{A} = \left( \begin{array}{c} \boldsymbol{a}_{1}^{T} \\  \boldsymbol{a}_{2}^{T} \\ \vdots \\ \boldsymbol{a}_{n}^{T}   \end{array} \right)
\end{displaymath}

So $\boldsymbol{Xa}_{i}$:

\begin{displaymath}
 \left( \boldsymbol{Xa}_{1} \boldsymbol{Xa}_{2}, \ldots, \boldsymbol{Xa}_{n} \right)
\end{displaymath}
in other words, this is our matrix of principal component scores.



\emph{This isn't just cute algebra: rRemember all the methods requiring $n > p$?   Scaling offers a route to dealing with this problem.}
  

\newpage

\section*{Bernard Flury 4/6/1951 to 6/7/1999}

%\includegraphics[width = 0.5\textwidth]{../images/flury}
Click \href{http://www.stat.ch/ass/flury.html}{\color{blue}HERE} for
information on Bernard Flury.

Studied psychology, mathematics, and statistics at the University of Berne, after a postdoc at Purdue and Stanford took a ``random walk around the world'' leading to full Professor at Indiana in 1995 and subsequent professorial appointments.   Developed the common principal component model as well as other work in multivariate statistics.


\newpage

\section{Assessing the quality of fit}


One measure of discrepancy is given by:

\begin{displaymath}
\varphi = \sum_{i=1}^{n} \sum_{j=1}^{n} (\delta_{ij}^{2} - d_{ij}^{2})
\end{displaymath}

i.e. the sum of the square of the distance and the reconstructed distance.   It can be shown that:


\begin{displaymath}
\varphi = 2n(\lambda_{q+1} + \ldots + \lambda_{n})
\end{displaymath}

\begin{shortquiz}
For the \texttt{eurodist} data there are $n=21$ cities.   The first 11
eigenvalues are positive, namely: $ 19,538,380$; $ 11,856,560$; 
$1,528,844$; $ 1,118,742$, $789,347$; $581,655$; $262,319$; $192,597$,
$145,084$;  $107,967$; $51,394$.   If we retain only two components,
find $\varphi$
\begin{answers}[varphi]{4}
\Ans0  $200,673,906$ &
\Ans1  $200,692,996$ &
\Ans0  $200,673,894$ &
\Ans0  $200,654,921$ 
\end{answers}
\begin{solution}
We need to find :
\begin{displaymath}
2 \times 21 \times (1,528,844 + 1,118,742 + 789,347 + 581,655 +
262,319 +192,597 + 145,084 + 107,967 + 51,394 + 0 + 0 \ldots)
\end{displaymath}
This gives a measure of the discrepancy between our 2 dimensional
projection and the true inter-city distances in terms of sums of
squares (of the distances in kilometers)
\end{solution}
\end{shortquiz}


\newpage

\subsection{Percent of variation explained / retained:}

This has clear analogies with the percent trace measure used in principal components analysis:

\begin{equation}
\frac{\sum_{i=1}^{q} \lambda_{i}}{\sum_{i=1}^{p} \lambda_{i}}
\end{equation}



\begin{shortquiz}
Consider again the \texttt{eurodist} data there are $n=21$ cities, and
the first 11
(non-negative) eigenvalues $19,538,380$; $11,856,560$; 
$1,528,844$; $ 1,118,742$, $789,347$; $581,655$; $262,319$; $192,597$,
$145,084$;  $107,967$; $51,394$.   If we retain only two components,
find the proportion of variance explained
\begin{answers}[propexpl]{4}
\Ans1  $86.9\%$ &
\Ans0  $87.2\%$ &
\Ans0  $54.0\%$ &
\Ans0  $91.0\%$ 
\end{answers}
\begin{solution}
We need to find :
\begin{displaymath}
\frac{19,538,380 + 11,856,560}{\mbox{Sum of all the eigenvalues}} = 0.869
\end{displaymath}
\end{solution}
\end{shortquiz}

\newpage

\subsection{Some complications}

\begin{itemize}
\item If $\boldsymbol{\Delta}$ is based on a measure other than the Euclidean then our reconstructed $\boldsymbol{Q}$ may not be positive semi-definite (in other words we find some negative eigenvalues and imaginary co-ordinates).   
\end{itemize}


If $\boldsymbol{Q} = \sum_{i=1}^{q} \lambda_{i} \boldsymbol{e}_{i} \boldsymbol{e}_{i}^{T}$ then one discrepancy measure:

\begin{equation}
\varphi \prime = trace(\boldsymbol{Q} - \hat{\boldsymbol{Q}})^{2}
\end{equation}.   

or we could use:

\begin{equation}
\frac{\sum_{i=1}^{q} \lambda_{i}}{\sum_{i=1}^{p} \lvert \lambda_{i} \rvert}
\end{equation}

or even:

\begin{equation}
\frac{\sum_{i=1}^{q} \lambda_{i}^{2}}{\sum_{i=1}^{p} \lambda_{i}^{2}}
\end{equation}


\subsection{Extensions: Sammon mapping}

Classical metrical scaling works on orthogonal projections, and has the attractive property of an exact analytical solution.   This is quite restrictive, Sammon (1969) suggested minimising the discrepancy measure:

\begin{displaymath}
\varphi \prime \prime =  \sum_{i=1}^{n} \sum_{j=1}^{n} (\delta_{ij} - d_{ij})^{2}
\end{displaymath}

This has no analytical solution, and \emph{numerical} methods must be used.   



A set of disparities are generated:

\begin{equation}
\label{rsssammon}
\hat{d}_{ij} = a + b \delta_{ij}
\end{equation}

(familiar formula?).   Residual sums of squares from \ref{rsssammon} yields another discrepancy measure.   This measure can (should / must) be normalised with reference to its size $\sum_{i=1}^{n} \sum_{j=1}^{n} d_{ij}^{2}$, giving what is called the \textbf{{\color{red}st}}andardised \textbf{{\color{red}re}}sidual \textbf{{\color{red}s}}um of \textbf{{\color{red}s}}quares


\subsection{STRESS}

\begin{equation}
STRESS = \left( \frac{\sum_{i=1}^{n} \sum_{j=1}^{n} (d_{ij} - \hat{d}_{ij})^{2}}{\sum_{i=1}^{n} \sum_{j=1}^{n} d_{ij}^{2}} \right) ^{\frac{1}{2}}
\end{equation}


\begin{equation}
SSTRESS = \left( \frac{\sum_{i=1}^{n} \sum_{j=1}^{n} (d_{ij}^{2} - \hat{d}_{ij}^{2})^{2}}{\sum_{i=1}^{n} \sum_{j=1}^{n} d_{ij}^{4}} \right) ^{\frac{1}{2}}
\end{equation}

Normalisation means that both these measures take on values between 0 and 1, lower values indicate better fit.   Values below 0.1 are usually considered adequate, but Kruskal, (1964) suggests values below 0.2 give a poor fit, values below 0.1 a fair fit, values below 0.05 a good fit, values below 0.025 an excellent fit.

\newpage

\begin{shortquiz}
A STRESS value of $0.04$ denotes:
\begin{answers}[stress]{1}
\Ans0 A fair fit \\
\Ans1 A good fit \\
\Ans0 An excellent fit \\
\Ans0 A statistically significant fit 
\end{answers}
\begin{solution}
Careful here: stress has nothing whatsoever to do with statistical significance.   The values have an entirely \emph{heuristic} explanation only, based on the judgement of one authority in the technique.   Everyone else might have different opinions
\end{solution}
\end{shortquiz}

\newpage

\section{Summary}


\begin{itemize}
\item Metric multidimensional scaling / Principal Co-ordinates Analysis gives a low dimensional representation via the distance matrix   
\item PCO and PCA are inter-related; similar concepts apply in assessing quality of fit
\item Non-metric scaling may be needed if your distance matrix is not metric - several solutions e.g. Sammon mapping
\item Very useful technique in many applications.
\end{itemize}


\newpage

\subsection{Typical Exam type questions}
  
\begin{itemize}
\item Explaining what PCO is about
\item Explaining the relationship between PCA and PCO
\item Interpreting a plot derived from PCO or non-metric scaling
\item Assessing the quality of a reduced dimension approximation
\end{itemize}

\newpage

\subsection{Labwork}
  
  \begin{itemize}
  \item Consider a PCO analysis of the \texttt{milk} data we considered when examining cluster analysis, to include assessing quality of fit
  \item Consider further examples (the class data, or some Johnson and Wichern examples) - can you \emph{assess} quality of fit (not just report it)
\end{itemize}


\subsection{Further reading}

\begin{itemize}
\item Relevant chapter in Manly, or (parts of) chapter 11 in Johnson and Wichern
\item Venables and Ripley (2002) describe scaling (in fact, it's their software we're using here)
\end{itemize}


 \end{document} 