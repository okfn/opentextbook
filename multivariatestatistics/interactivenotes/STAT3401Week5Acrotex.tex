\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate Methods) Week 5}
\author{Paul Hewson}
\date{30th October 2008}


\newtheorem{df}{Definition}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}

\maketitle

\tableofcontents



\section{Outline}

\subsection{Aims of this week}

\begin{itemize}
\item To introduce ``Principal Components'' as a means for finding a lower dimension projection of data
\item To outline other reasons for attempting principal component analysis
\item To discuss how to assess the adequacy of a reduced dimension projection
\end{itemize}


\newpage

\section{Rationale}


\subsection{Reasons for principal component analysis}

\begin{itemize}
\item Reduced dimensionality - ideally to 2 or 3 dimensions where visualisation is trivial, or in some applications (e.g. allometry) to one dimension.
\item To find projections which have maximised variance (c.f. discriminant analysis where we sought projections which maximised group separation)
\item Sphering data
\end{itemize}


%\newpage


%\includegraphics[width = 0.5\textwidth]{../images/lancet}
%Eigengenes\footnote{T.O.Nielsen, R.B.West, S.C.Linn, O.Alter, M.A.Knowling, J.X.O'Connell, S.Zhu, M.Fero, G.Sherlock, J.R.Pollack, P.O.Brown, D.Botstein, M.van de Rijn (2002) ``Molecular characterisation of soft tissue studies: A gene expression study'' \textit{Lancet} \textbf{359}:1301-1307}


\newpage

\subsection{Some computational issues}

Here's a couple of points that you need to be aware of, but don't make too much of them.

\begin{itemize}
\item This is a technique based on finding eigenvalues and eigenvectors - do note that the \emph{sign is arbitrary!}
\item Be careful as \textbf{R} actually uses the singular value decomposition in some of its calculations (for computational reasons) and may return the square root of the eigenvalues
\item Otherwise, this is conceptually a very straightforward technique
\end{itemize}

What this means is that you can be sitting next to someone who gets a different answer to you, in that all the eigenvectors can have the opposite sign.

\newpage

\section{A justification}

I will run though another justification in class (based on Pythagoras theorem) which provides another rationale for using eigenanalysis in principal component analysis.   But this does explain another important property.   What we need to take from this justification is the idea that the {\color{red} principal component $z_{i1}$ is a linear combination / projection which contains maximum variance} (subject to the projecting eigenvectors being orthonormal).   $z_{i2}$ contains maximum variance from an orthonormal projection matrix (the eigenvectors) subject to being orthogonal to $z_{i1}$.   And so on.


\subsection{Maximising variance}

\begin{itemize}
\item Consider a random vector $\boldsymbol{x} \in \mathbb{R}^{P}$
\item We wish to find the vector of transformed variables $\boldsymbol{z} = \boldsymbol{\alpha}^{T}(\boldsymbol{x} - \boldsymbol{\bar{x}})$,
\item We wish to find $\alpha$ such that the $\mbox{Var}(z)$ is maximised. 
\item By convention, we insist that $\boldsymbol{\alpha}$ is orthonormal, i.e. in addition to being a vector of unit length we require that $\boldsymbol{\alpha}^{T}\boldsymbol{\alpha} = 1$.   
\end{itemize}


To find an expression for $\mbox{Var}(z)$ in terms of the original variables $\boldsymbol{x}$, firstly by considering the following relationship:
\begin{displaymath}
\mbox{Var}(\boldsymbol{z}) = \mbox{Var}(\boldsymbol{\alpha}^{T}\boldsymbol{x}) = E(\boldsymbol{\alpha}^{T}\boldsymbol{x})^{2}
\end{displaymath}
Hence,
\begin{displaymath}
\sum_{i=1}^{n}(\boldsymbol{z}_{i} - \boldsymbol{\bar{z}})^{2} = \sum_{i=1}^{n}\boldsymbol{\alpha}^{T}\left[(\boldsymbol{x}_{i} - \boldsymbol{\bar{x}})(\boldsymbol{x}_{i} - \boldsymbol{\bar{x}})^{T}\right] \boldsymbol{\alpha}
\end{displaymath}
and premultiplying both sides by $\frac{1}{n-1}$ gives us an estimate of the variance of the transformed variables in terms of the original variables.  To find $\boldsymbol{\alpha}$ in order to maximise the variance we therefore wish to find:  
\begin{equation}
\mbox{max}(\boldsymbol{\alpha}^{T}\boldsymbol{\hat{\Sigma}}\boldsymbol{\alpha})
\end{equation}
subject to the side condition that $\boldsymbol{\alpha}^{T}\boldsymbol{\alpha}= 1$.   

\begin{itemize}
\item We wish to maximise $\mbox{Var}(z_{1}) = \boldsymbol{\alpha}_{1}^{T}\boldsymbol{\hat{\Sigma}}\boldsymbol{\alpha}_{1}$
\item  it can be shown that this problem can be specified by incorporating a Lagrange multiplier.  
\item Consider maximising the expression $\varphi$ given below, where $\lambda$ is a Lagrange multiplier:
\begin{equation}
\varphi_{1} = \boldsymbol{\alpha}_{1}^{T}\boldsymbol{\hat{\Sigma}}\boldsymbol{\alpha}_{1} - \lambda_{1}(\boldsymbol{\alpha}_{1}^{T}\boldsymbol{\alpha}_{1} - 1)
\end{equation}
which can be solved by differentiation with respect to $\boldsymbol{\alpha}_{1}$.   
\end{itemize}


\subsubsection{First principal component}

The first differential yields a system of linear equations:
\begin{equation}
\frac{\partial \varphi_{1}}{\partial \boldsymbol{\alpha}_{1}} = 2 \Sigma \boldsymbol{\alpha}_{1} - 2 \lambda \boldsymbol{\alpha}_{1}
\end{equation}
Setting this derivative equal to zero gives:
\begin{equation}
\boldsymbol{\Sigma} \boldsymbol{\alpha}_{1} = \lambda_{1} \boldsymbol{\alpha}_{1}
\end{equation}

The previous equation is easily rearranged to give:
\begin{equation}
(\boldsymbol{\Sigma} - \lambda_{1} \boldsymbol{I})\boldsymbol{\alpha} = 0.
\end{equation}

\begin{itemize}
\item $p$ equations and $p$ unknowns, 
\item Non-trivial solutions (where $\boldsymbol{\alpha} \neq \boldsymbol{0}$) -  note that if  $\boldsymbol{a}_{1}^{T}\boldsymbol{a}= 1$ then $\boldsymbol{\Sigma} - \lambda \boldsymbol{I}$ is singular.   This implies that:
\begin{equation}
|\boldsymbol{\Sigma} - \lambda_{1} \boldsymbol{I}| = 0
\end{equation}
\end{itemize}

In other words, $\lambda_{1}$ can be found as the largest eigenvalue of $\boldsymbol{\Sigma}$, and $\boldsymbol{\alpha}_{1}$ as the corresponding eigenvector.


\subsubsection{Second principal component}

For the second principal component, we add a constraint to ensure orthogonality.

With the side condition that $\mbox{Cor}(z_{1},z_{2}) = 0$, we solve:

\begin{equation}
\varphi_{2} = \boldsymbol{\alpha}_{2}^{T}\boldsymbol{\hat{\Sigma}}\boldsymbol{\alpha}_{2} - \lambda_{2}(\boldsymbol{\alpha}_{2}^{T}\boldsymbol{\alpha}_{2} - 1) - \mu(\alpha_{1}^{T}\boldsymbol{\Sigma}\boldsymbol{\alpha_{2}})
\end{equation}
where differentiating now gives:
\begin{equation}
\label{pca2diff}
\frac{\partial \varphi_{2}}{\partial \boldsymbol{\alpha}_{2}} = 2 \Sigma \boldsymbol{\alpha}_{2} - 2 \lambda_{2} \boldsymbol{\alpha}_{2} - \mu \boldsymbol{\Sigma} \boldsymbol{\alpha}_{1}
\end{equation}


\begin{itemize}
\item Note that we assume $\mbox{Cor}(z_{1},z_{2}) = 0$ 
\item $\therefore$ we take $\boldsymbol{\alpha}_{2}^{T}\boldsymbol{\Sigma}\boldsymbol{\alpha}_{1} = \boldsymbol{\alpha}_{1}^{T}\boldsymbol{\Sigma}\boldsymbol{\alpha}_{2} = 0$ implying that $\boldsymbol{\alpha}_{1}^{T}\boldsymbol{\alpha}_{2} = \boldsymbol{\alpha}_{2}^{T}\boldsymbol{\alpha}_{1} = 0$ 
\item Hence we can premultiply our equation in \ref{pca2diff} by $\boldsymbol{\alpha}_{2}$ giving:
\begin{equation}
\label{pca2diff2}
2 \boldsymbol{\alpha}_{2}^{T} \Sigma \boldsymbol{\alpha}_{2} - 2 \boldsymbol{\alpha}_{2}^{T} \lambda_{2} \boldsymbol{\alpha}_{2} - \mu \boldsymbol{\alpha}_{2}^{T} \boldsymbol{\Sigma} \boldsymbol{\alpha}_{1}
\end{equation}
which implies that $\mu=0$, and that $\lambda_{2}$ is also an eigenvalue of $\boldsymbol{\Sigma}$.
\end{itemize}



\subsubsection{Subsequent principal components}

\begin{itemize}
\item This process can be considered formally to the $p$th principal component.   Hopefully we have at least demonstrated the pattern that emerges.   Principal components which maximise the variance of the linear combinations can be formed from a simple eigen decomposition of the covariance matrix.
\item It is also possible to give a justification from a geometric perspective (which is how Pearson introduced the technique in 1901)
\end{itemize}


Well, what a lot of fun that was.   Just make sure you take onboard the idea of projections contaiing maximum variance.

The derivation based on Pythagoras' theorem tells us that our principal components are the best lower dimensional projection in terms of MSE.

Or you could think of it in this way:

%\includegraphics[width = 0.4\textwidth]{../../cartoons/gm_pca}
\href{http://www.vias.org/science_cartoons/pca.html}{\color{blue}Principal Components}


\newpage


\section{Principal component analysis}

Having covered the background, let's get down the business of principal component analysis.

\begin{itemize}
\item PCA is an eigenanalysis of $\boldsymbol{\Sigma}$ (whether that is the correlation \emph{or} the covariance matrix)
\item Provided $\boldsymbol{\Sigma}$ is symmetric (which will always be the case for correlation and covariance matrices), the normalised eigenvectors corresponding to unequal eigenvalues are orthonormal.
\item In practice, we don't know $\Sigma$ and we have to estimate it from our data with a suitable estimate - it is usual to use the unbiased estimator $\boldsymbol{S} = \frac{1}{n-1} \boldsymbol{X}^{T}\boldsymbol{X}$, where $\boldsymbol{X}$ is a matrix of mean centred data, but do note that one of the \textbf{R} functions actually uses $\boldsymbol{S} = \frac{1}{n} \boldsymbol{X}^{T}\boldsymbol{X}$.  
\end{itemize}


\newpage

\subsection{Data analytic properties}

Because of the formal derivation of this technique, we can state some key properties of the technique.


\begin{theorem}
The first $k$ principal components have smaller mean squared departure from the population (or sample) variables than any other $k$-dimensional subspace.
\end{theorem}

This property\footnote{This property follows from the Pythagagoras theorem justification} is rather important when considering the dimension reducing properties of principal component as it \emph{does not require any distributional assumptions}.  

\newpage

\subsubsection{Definition of principal component analysis}

\input{../defs/prcompdef.tex}
This just gives a very formal definition, $\boldsymbol{E}$ are our eigenvectors.   The key thing you should watch is that this technique requires \emph{mean centred variables}.

\newpage

\subsubsection{Key properties of principal component analysis}

\input{../defs/prcompbasics}

Taken with our definition above, this tells us that the principal components (the projected variables $z_{ij}$) have zero mean, and a variance given by their corresponding eigenvalues.   You can also see that each subsequent eigenvalue is smaller than the previous one, i.e. the variance of the first principal component is larger than the second, the second larger than the third and so on.   And finally, each principal component is orthogonal to the others.

\subsubsection{Proportion of variance explained}

This is a little bit of a statement of the obvious (what we've just said in the previous definition).   But it is really rather important.   So here goes:

\input{../defs/eigentrace}

In other words, equation~\ref{eigentrace} indicates that the \emph{total variance} can be explained by the sum of the eigenvalues.   In the case of principal components formed from the correlation matrix $\boldsymbol{R}$ this will be equal to the number of variables.


We can even plot the eigenvalues.  This might help us investigate the way the variance is explained by different principal components.

\begin{figure}
\begin{center}
\includegraphics[width = 0.5\textwidth]{../images/HeptathScree} 
\caption{Scree plot displaying the amount of variation explained by each of the seven principal components formed from the correlation matrix of the Sydney Heptathalon data}
\label{screeplot}
\end{center}
\end{figure}



\newpage
\section{How many dimensions?}


One key question in principal components is determining how many dimensions (principal components) we need to retain. 

\begin{itemize}
\item We want a lower dimensional projection in order to visualise our data
\item We want to carry out some kind of narrative interpretation of the eigenvectors, and need to know how many principal components are ``interesting''.
\end{itemize}

There have been a number of proposals to help determine the number of dimensions to retain:



\begin{itemize}
\item Retain those components that retain greater than a pre-determined proportion of variance $\frac{\sum_{j=1}^{q} \lambda_{j}}{\sum_{j=1}^{p} \lambda_{j}}$
\item Change in slope of the scree plot (Catell, 1966); possibly supplemented by simulations (Horn, 1965)

\item Broken stick (Joliffe, 1986): if any unit is randomly divided into $p$ segments, the expected length of the $k$th longest segment is:

\begin{equation}
\label{stick}
l_{k} = \left(\frac{1}{p} \right) \sum_{i=k}^{p} \left( \frac{1}{i} \right)
\end{equation}
If we assume that the total variance, $trace{\boldsymbol{S}} = \sum_{j=1}^{p}$, is the ``stick'', we can use this approach to estimate an expected size of each eigenvalue.

\item Kaiser criterion (retain all principal components explaining greater than average variance), for a correlation matrix this means retaining all values greater than 1.


\item Distributional theory for the eigenvalues.   For example we know that:

\begin{displaymath}
  \frac{\hat{\lambda_{i}}}{(1 + z_{\alpha/2} \sqrt{2/n})} \leq \lambda
  \leq  \frac{\hat{\lambda_{i}}}{(1 - z_{\alpha/2} \sqrt{2/n})}
\end{displaymath}
where $n$ is the sample size, and $z_{\alpha/2}$ is the value of a z variable.


\item Cross-validation

\item Bootstrapping

\end{itemize}



\newpage

\subsubsection{Generalised variance}

I'm not sure we do very much with this anymore, but still, here's an interesting property.

\input{../defs/eigenproducts}
Theorem \ref{th:eigenproduct} indicates that we can find an estimate of the \emph{generalised variance} from the product of the eigenvalues.   Along with theorem \ref{th:eigentrace} we find that the generalised variance and the sum of variances are unchanged by the principal component transformation.   


\newpage

\subsubsection{Scale-invariance}

This point is rather vital:

\begin{itemize}
\item Flury (1997) describes this as an anti-property.   
\item You get a very different answer depending whether you take the covariance or correlation matrix.   
\item Great care needs to be taken in deciding which is the appropriate matrix to use in any given situation
\end{itemize}


\newpage


\subsubsection{Sphericity}

This point is more a caution than anything else.   If we have spherical data (i.e. uncorrelated) and ran principal components analysis we would get completely different answers from each sample of data.

\begin{figure}
\includegraphics[width = 0.9\textwidth]{../images/pcastability}
\caption{Artificially generated data indicating stablity of pca solution}
\end{figure}


\newpage

\subsubsection{Consolidation}

Here are a few questions about the key properties of principal components:


\begin{shortquiz}{Properties}
\begin{questions}
\item Assume that the eigenvalues of a \emph{covariance} matrix are 3.7, 1.3, 0.8, 0.1, 0.1.   What is the variance of the \emph{second} principal component
\begin{answers}[pc1]{4}
\Ans0 3.7 & 
\Ans1 1.3 &
\Ans0 0.8 &
\Ans0 0.1 
\end{answers}
\begin{solution}
The eigenvalues provide information on the variance of the associated principal component.
\end{solution}
\item Assume that the eigenvalues of a \emph{covariance} matrix are 3.7, 1.3, 0.8, 0.1, 0.1.   What is the \emph{proportion} of variance explained by the first \emph{principal components}?
\begin{answers}[pc2]{4}
\Ans0 3.7 & 
\Ans0 1.3 &
\Ans0 5 &
\Ans1 $\frac{5}{6}$ 
\end{answers}
\begin{solution}
The total variance is the sum of the eigenvalues, i.e. $\sum_{j=1}^{p} \lambda_{j} = 3.8 + 1.2 + 0.8 + 0.1 + 0.1 = 6$.   The total variance explained by the first two eigenvalues is $\sum_{j=1}^{q} \lambda_{j} = 3.8 + 1.2 = 5$.   Hence the proportion of variance explained by the first two principal components is $\frac{\sum_{j=1}^{q} \lambda_{j}}{\sum_{j=1}^{p} \lambda_{j}}$
\end{solution}
\newpage
\item The results from the eigenanalysis of a $2 \times 2$ correlation matrix produce eigenvectors as follows: $\left( \begin{array}{rr} 0.707 & 0.707 \\ -0.707 & 0.707 \end{array} \right)$.   An expression for the first principal component is therefore given as: 
\begin{answers}[pc3]{1}
\Ans1 $z_{i} = 0.707 x_{i1} - 0.707 x_{i2}$ \\   
\Ans0 $z_{i} = 0.707 x_{i1} + 0.707 x_{i2}$ \\
\end{answers}
\item A psychologist tells us that we should always base a principal component analysis on the correlation matrix.   Do you reply:
\begin{answers}[pc4]{1}
\Ans0 The psychologist is correct; you should always base the analysis on the correlation matrix
\Ans0 The psychologist is incorrect; it doesn't matter whether you use the correlation matrix or the covariance matrix
\Ans1 The psychologist is not entirely correct; sometimes it may be necessary to use a correlation matrix but sometimes it may be possible to use a covariance matrix
\end{answers}
\begin{solution}
The key thing to note here is that it does make a big difference whether you use a covariance or a correlation matrix.   The difficulty with using a covariance matrix is that if you have one variable that has a much higher variance than the others, this variable will dominate the first principal component (due to the variance maximising properties of principal component analysis).   However, if you avoided this by \emph{always} insisting that you used a correlation matrix you may find it is rather a brutal way of standardising your data.   We will discuss this more in the computer tutorial - for example we will find that taking logs of the turtle data gives us data with comparable variance whereas the USArrests data will be dominated by one variable.

The other thing you should note is that if you change units (e.g. changing from pounds and feet to kilograms and metres) you will also get a different solution.   Isn't that interesting!
\end{solution}
\newpage
\item According to the Kaiser criteria, how many principal components should you retain:
\begin{answers}[pc5]{5}
\Ans0 1 &
\Ans1 2 &
\Ans0 3 &
\Ans0 4 &
\Ans0 5
\end{answers}
\begin{solution}
The total variance here is $\sum_{j=1}^{p} \lambda_{j} = 6$.   The average variance explained by each component is therefore $\frac{6}{5}=1.2$.   The first two components exceed this value and therefore would be retained according to the Kaiser criteria.
\end{solution}
\item If you produced a screeplot, how many principal components would you retain
\begin{answers}[pc6]{5}
\Ans0 1 &
\Ans0 2 &
\Ans1 3&
\Ans0 4 &
\Ans0 5
\end{answers}
\begin{solution}
The scree plot is given by:

\includegraphics[width = 0.4\textwidth]{../images/simplescree}

I would be tempted to retain three components.   But you might note that there is quite a lot of subjectivity in this!
\end{solution}
\item If the sample size were 20, what would be the 95\% confidence interval for the third principal component:
\begin{answers}[pc7]{2}
\Ans0 $(0.894, 1.104)$ &
\Ans0 $(0.494, 0.904)$ \\
\Ans1 $(0.494, 2.104)$ &
\Ans0 $(0.894, 2.104)$ 
\end{answers}
\begin{solution}
We need to calculate:
\begin{displaymath}
  \frac{\hat{\lambda_{i}}}{(1 + z_{\alpha/2} \sqrt{2/n})} \leq \lambda
  \leq  \frac{\hat{\lambda_{i}}}{(1 - z_{\alpha/2} \sqrt{2/n})}
\end{displaymath}
where $n=20$ is the sample size, and $z_{\alpha/2}=1.96$ is the value of a z variable for the 95\% confidence interval.   Accordingly we need to calculate $\frac{0.8}{ (1 + 1.96 * \sqrt{2 / 20})} = 0.4938862$ and $\frac{ 0.8 }{ (1 - 1.96 * \sqrt{2 / 20})} = 2.104$.   


What's really interesting about this result is that our 95\% confidence interval overlaps 1.2 - the threshold value for the Kaiser criteria.   So whilst we would discard this principal component under the Kaiser criteria, when we think about the sample variation we shouldn't be so sure we can do that!
\end{solution}

\end{questions}
\end{shortquiz}

\newpage


\section{Examples}

\subsection{Painted turtles}



%\begin{figure}
%\includegraphics[width = 0.3\textwidth]{../images/turtle}
%\caption{Painted turtle \textit{Chrysemys picta}}
%\end{figure}
Further information (and pictures) on Painted turtles is available
from \href{http://en.wikipedia.org/wiki/Painted_Turtle}{\color{blue} HERE}

%http://people.hofstra.edu/faculty/Russell_L_Burke/HerpKey/regional_turtles.htm

We will examine some rather well studied data on Painted Turtles (different sub-species depicted).   The basic question is whether or not turtles have the same overall shape?   If they do, we can explain the data with a single principal component, if not we need to use more than one principal component.



\newpage

\subsection{Heptathalon}

Another data analysis that could be performed concerns some Heptathalon data.   This is intended as an example which gives you chance to think about ``standardising'' variables.   Do you just use the correlation matrix (yes) but what else do you need to do with your data?

\begin{itemize}
\item In three running events, better performance is indicated by a lower measure (time), 
\item whereas in the jumping and throwing events good performance is indicated by a higher measure (distance).   
\item It seems sensible to introduce a scale reversal so that good performance is in some way at the top of any given scale.   A convenient way of doing this is to multiply the times of the running events by $-1$.
\item The data are clearly highly incompatible (running 800 metres tends to take rather longer than running 100 metres)
\end{itemize}

The question we may wish to address is to determine whether winning athletes better in certain events?



\subsection{Reification}

This is the game that gives principal components a bad name.   If we examined a correlation matrix derived from the heptathalon data we might get the following eigenvectors.



\begin{verbatim}
$vectors
      [,1]   [,2]   [,3]   [,4]
[1,] -0.465  0.288  0.328 -0.003  
[2,] -0.245 -0.564 -0.107 -0.610  
[3,] -0.419 -0.071 -0.521  0.235 
[4,] -0.433 -0.022  0.518  0.357 
[5,] -0.463  0.115  0.124 -0.480 
[6,] -0.322  0.381 -0.568  0.091 
[7,] -0.201 -0.658 -0.032  0.452 
\end{verbatim}


You might find that all the loadings on the first principal component are similar(ish) in magnitude, and all are negative.   You could find if you did this analysis yourself that these were all positive.   So we might interpret this as some kind of general athletic performance ``component''.  The second and third components are rather more interesting.   You can see in the second component that the loadings are negative for the 2nd, 3rd, 4th and 7th variables, and positive for the others.   You can also see that the loading for the 3rd and 4th variable is close to zero.   So in ``interpreting'' this component you might name it according to the contrast between the 1st, 5th and 6th variables and the 2nd and 7th variables.

\newpage


\section{Principal component scores}

\begin{equation}
\label{pcascore}
\boldsymbol{z} =  \boldsymbol{E}^{T}(\boldsymbol{x}-\boldsymbol{\mu})
\end{equation}

It is possible (although somewhat unusual in this context) to find scores from the standardised data

\begin{equation}
\boldsymbol{z} =  \boldsymbol{E}^{T}(\boldsymbol{x}-\boldsymbol{\mu})  \boldsymbol{\Lambda}^{-1/2}
\end{equation}
which essentially leaves us looking at the correlation between the standardised data and the principal component scores.  




\subsection{Mahalanobis distance}

There's an interesting relationship between the Mahalanobis distance and the principal components.

\begin{theorem}
\begin{equation}
\label{Qstats}
\delta^{2}(\boldsymbol{x}, \boldsymbol{\mu}) = (\boldsymbol{x} -  \boldsymbol{\mu}) \boldsymbol{\Sigma}^{-1} (\boldsymbol{x} -  \boldsymbol{\mu}) = \sum_{j=1}^{p} \frac{z_{j}^{2}}{\lambda_{j}}
\end{equation}
where $\boldsymbol{z} = (z_{1}, \ldots, z_{p})^{T} = \boldsymbol{E}(\boldsymbol{x}- \boldsymbol{\mu})$, and $\boldsymbol{\Sigma} = \boldsymbol{E \Lambda E}^{T}$ as above.   
\end{theorem}

\begin{itemize}
\item i.e. use principal component representation to partition the (squared) Mahalanobis distance
\item $\delta_{a}^{2} =  \sum_{j=1}^{q} \frac{z_{j}^{2}}{\lambda_{j}}$ corresponding to the distance encapsulated in our first $q$ principal components, and $\delta_{b}^{2} =  \sum_{j=q+1}^{p} \frac{z_{j}^{2}}{\lambda_{j}}$ corresponding to the distances encapsulated by the last $(p-q)$ principal components.   
\item Flury (1997) indicates that these can be represented by $\chi^{2}$ random variables with respectively $q$ and $p-q$ degrees of freedom 
\end{itemize}


\newpage

\section{Summary}


\begin{itemize}
\item Principal components can find an ``optimal'' reduced dimension projection, without making distributional assumptions about original data.   
\item Principal components explain maximum variation, subject to being orthogonal to previous components.
\item Numerous techniques available to held determine whether a reduced dimension projection is adequate
\item Care needed in deciding how to use the data; i.e. whether to use covariance matrix or correlation matrix and if the former whether to transform the variables first
\end{itemize}

\newpage

\subsection{Typical Exam type questions}
  
\begin{itemize}

\item Determining the number of principal components to retain
\item Demonstrating expertise in calculating covariance matrix, correlation matrix and finding eigenvalues and normalised eigenvectors

\item Demonstrating how to use these eigenvectors to form principal components, interpreting the loadings (with or without the aid of a biplot)
\item Explaining the rationale for principal component analysis

\end{itemize}

\newpage

\subsection{Labwork}
  
  \begin{itemize}
  \item Be very clear about the difference between \texttt{prcomp()} and \texttt{princomp()}
  \item Heptathalon analysis; worked in detail in lab. notes - is the proposed rescaling sensible; what do you learn about medal winning heptathletes
\item Do consider a principal component analysis of the US Arrests data as suggested.   It might help clarify the difference between covariance and correlation based analysis.
  \item Turtle analysis - mainly because they are cute animals.
\end{itemize}


\subsection{Further reading}

\begin{itemize}
\item Relevant chapter in Manly, or chapter 8 in Johnson and Wichern are very good.   J\&W in particular talk a lot (too much) about sample and population principal components.
\item Flury (1997) and Krzanowski also give very good accounts of this technique
\end{itemize}


\NaviBarOff

 \end{document} 