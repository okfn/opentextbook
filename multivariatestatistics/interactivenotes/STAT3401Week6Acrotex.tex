\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage{mathrsfs}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%


\title{STAT3401 (Multivariate) Week 6: Hotelling's T$^{2}$}
\author{Paul Hewson}
\date{13th November 2008}

\newtheorem{df}{Definition}
\newtheorem{theorem}{Theorem}


\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}


\maketitle


\section{Outline}

\subsection{Aims of this week}

\begin{itemize}
\item Explore the relationship between univariate t-tests and analagous tests for mean vectors (Hotelling's T$^{2}$ test)
\item Examine relationships between Mahalanobis distance and T$^{2}$ distribution
\item Revise confidence intervals and introduce confidence ellipses
\end{itemize}


\newpage

\section{Inference}

\subsection{Hypothesis tests}

Do note that there is a general procedure for obtaining a test statistic 

\begin{itemize}
\item Likelihood ratio tests (in any situation, multivariate or univariate)
\item Union-intersection tests (designed for multivariate problems but we won't consider it any further here)
\end{itemize}

You have (implicitly at least) met Likelihood Ratio Tests.   But rather conveniently, these both lead to the same test for mean vectors!!!

\newpage

\subsection{Likelihood Ratio Tests for the mean vector}

The multivariate normal likelihood, with parameters $\boldsymbol{\mu}$ and $\boldsymbol{\Sigma}$ is given by:
\begin{equation}
\mathscr{L}(\boldsymbol{x}|\boldsymbol{\mu, \Sigma}) = \frac{1}{(2 \pi)^{np/2}  \lvert \hat{\boldsymbol{\Sigma}} \rvert^{n/2}}  e^{- \frac{1}{2} \sum_{i=1}^{n}(\boldsymbol{x}_{i} - \boldsymbol{\mu})^{T}\boldsymbol{\Sigma}^{-1}(\boldsymbol{x}_{i} - \boldsymbol{\mu})}
\end{equation}

which is maximised taking $\hat{\boldsymbol{\Sigma}} = \frac{1}{n} \sum_{i=1}^{n}(\boldsymbol{x}_{i} - \bar{\boldsymbol{x}})(\boldsymbol{x}_{i} - \bar{\boldsymbol{x}})^{T}$ and $\hat{\boldsymbol{\mu}} = \bar{\boldsymbol{x}}$.   A little algebra shows that $\sum_{i=1}^{n}(\boldsymbol{x}_{i} - \hat{\boldsymbol{\mu}})^{T} \hat{\boldsymbol{\Sigma}}^{-1}(\boldsymbol{x}_{i} - \hat{\boldsymbol{\mu}}) = np$ so we can express the maximum as:

\begin{equation}
max \mathscr{L} (\boldsymbol{\mu},\boldsymbol{\Sigma}) = \frac{1}{(2 \pi)^{np/2} \lvert \hat{\boldsymbol{\Sigma}} \rvert^{n/2}} e^{-np/2}
\end{equation}

Under the hypothesis $H_{0}: \boldsymbol{\mu} = \boldsymbol{\mu}_{0}$

\begin{equation}
\mathscr{L} (\boldsymbol{\mu}_{0},\boldsymbol{\Sigma}) = \frac{1}{(2 \pi)^{np/2} \lvert \boldsymbol{\Sigma} \rvert^{n/2}} e^{- \frac{1}{2} \sum_{i=1}^{n}(\boldsymbol{x}_{i} - \boldsymbol{\mu}_{0})\boldsymbol{\Sigma}^{-1}(\boldsymbol{x}_{i} - \boldsymbol{\mu}_{0})^{T}}
\end{equation}

so $\boldsymbol{\mu}_{0}$ is now fixed but $\boldsymbol{\Sigma}$ can be varied to find the most likely value.   As before, this can be rearranged to give:

\begin{equation}
max \mathscr{L} (\boldsymbol{\mu}_{0},\boldsymbol{\Sigma}) = \frac{1}{(2 \pi)^{np/2} \lvert \hat{\boldsymbol{\Sigma}}_{0} \rvert^{n/2}} e^{-np/2}
\end{equation}


where $\hat{\boldsymbol{\Sigma}}_{0} = \frac{1}{n} \sum_{i=1}^{n} (\boldsymbol{x}_{i} - \boldsymbol{\mu}_{0})(\boldsymbol{x}_{i} - \boldsymbol{\mu}_{0})^{T}$

To determine whether $\boldsymbol{\mu}_{0}$ is plausible, we wish to compare $\mathscr{L} (\boldsymbol{\mu}_{0},\boldsymbol{\Sigma})$ with $\mathscr{L} (\boldsymbol{\mu},\boldsymbol{\Sigma})$, conventionally performed by means of the likelihood ratio statistic:

\begin{equation}
\Lambda = \frac{max \mathscr{L} (\boldsymbol{\mu}_{0},\boldsymbol{\Sigma})}{max \mathscr{L} (\boldsymbol{\mu},\boldsymbol{\Sigma})} = \left(\frac{\lvert \hat{\boldsymbol{\Sigma}} \rvert }{\lvert \hat{\boldsymbol{\Sigma}}_{0} \rvert } \right)^{n/2}
\end{equation}


\newpage

\subsection{Wilk's Lambda}

In general, a likelihood ratio test of $H_{0}: \mu = \mu_{0}$ rejects $H_{0}$ if:

\begin{equation}
\Lambda = \frac{max \mathscr{L}(\mu_{0})}{max \mathscr{L}(\mu)} < c
\end{equation}
for a suitable constant $c$.   

\begin{itemize}
\item For each test we need to find a sampling distribution for $\Lambda$, 
\item For large samples, $-2 \log \Lambda$ is approximated by $\chi^{2}$ with degrees of freedom equivalent to the difference in dimension of the two parameter spaces
\item It turns out that a little algebra turns the likelihood ratio into something much simpler when considering mean vectors
\end{itemize}


\newpage

\subsection{t-test}

Let's take a moment to revise the (Univariate) t-test

Consider testing the univariate hypothesis $H_{0}: \mu = \mu_{0}$:

\begin{displaymath}
t = \sqrt{n} \frac{\bar{x}-\mu}{s}
\end{displaymath}
and recall that
\begin{displaymath}
\Delta(\bar{x}, \mu) = \frac{\lvert \bar{x}-\mu \rvert }{s}
\end{displaymath}
So it should come as no surprise that a multivariate test on mean vectors will be based on the distance measures!


\newpage

\subsection{Remember Multivariate distance?}

 Two vectors  $\boldsymbol{x}_{1}$ and $\boldsymbol{x}_{2}$, with a common covariance matrix $\boldsymbol{\Sigma}$ the multivariate standard distance is given by:

\begin{displaymath}
\Delta(\boldsymbol{x}_{1},\boldsymbol{x}_{2}) = \sqrt{(\boldsymbol{x}_{1} - \boldsymbol{x}_{2})^{T}\boldsymbol{\Sigma}^{-1}(\boldsymbol{x}_{1} - \boldsymbol{x}_{2}) }
\end{displaymath}

Depending on whichever textbook is consulted, this multivariate standard distance may be referred to as the \emph{statistical distance}, the \emph{elliptical distance} or the \emph{Mahalanobis distance}.   

\newpage

\subsubsection{Mahalanobis distance}

Originally proposed by Mahalanobis:1930 as a measure of distance between two populations:
\begin{displaymath}
\Delta(\boldsymbol{\mu}_{1},\boldsymbol{\mu}_{2}) = \sqrt{(\boldsymbol{\mu}_{1} - \boldsymbol{\mu}_{2})^{T}\boldsymbol{\Sigma}^{-1}(\boldsymbol{\mu}_{1} - \boldsymbol{\mu}_{2}) }
\end{displaymath}

obvious sample analogue:

\begin{displaymath}
\Delta(\boldsymbol{\bar{x}}_{1},\boldsymbol{\bar{x}}_{2}) = \sqrt{(\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2})^{T}\boldsymbol{S}^{-1}(\boldsymbol{\bar{x}}_{1} - \boldsymbol{\bar{x}}_{2}) }
\end{displaymath}
where $\boldsymbol{S}$ is the pooled estimate of $\boldsymbol{\Sigma}$ given by $\boldsymbol{S} = \left[ (n_{1}-1) \boldsymbol{S}_{1} +  (n_{2}-1) \boldsymbol{S}_{2} \right] / (n_{1} + n_{2} - 2)$.

Consider the distance between $\boldsymbol{x}$, a vector of random variables with mean $\boldsymbol{\mu}$ and covariance matrix $\boldsymbol{\Sigma}$ and its mean:

\begin{displaymath}
\Delta(\boldsymbol{x},\boldsymbol{\mu}) = \sqrt{(\boldsymbol{x} - \boldsymbol{\mu})^{T}\boldsymbol{\Sigma}^{-1}(\boldsymbol{x} - \boldsymbol{\mu}) }
\end{displaymath}

or the sample analogue (estimating $\boldsymbol{\mu}$ by $\hat{\boldsymbol{x}}$ and $\boldsymbol{\Sigma}$ by $\boldsymbol{S} = \frac{1}{n-1} \boldsymbol{X}^{T}\boldsymbol{X}$).



\newpage

\section{One sample T$^{2}$ tests}

\subsection{Hotelling's T$^{2}$ test for a single sample}

Consider testing $H_{0}: \boldsymbol{\mu} = \boldsymbol{\mu}_{0}$, where $\boldsymbol{\mu}_{0}$ is a predetermined mean vector (such as $\boldsymbol{0}$).   In this case, Hotelling's T$^{2}$ is given by:

\begin{equation}
\label{t2}
T^{2} = n (\boldsymbol{\mu}_{0} - \bar{\boldsymbol{x}})^{T} \boldsymbol{S}^{-1}  (\boldsymbol{\mu}_{0} - \bar{\boldsymbol{x}})
\end{equation}
where $n$ is the sample size, $\boldsymbol{\mu}_{0}$ is the hypothesised mean, $\bar{\boldsymbol{x}}$ and $\boldsymbol{S}$ are the sample mean and covariance matrices respectively.   


\newpage

\subsection{The T$^{2}$ distribution}

\begin{itemize}
\item The statistic given above follows a T$^{2}$ distribution,
\item however, there is a simple relationship between the T$^{2}$ and $F$ distribution
\end{itemize}





\begin{theorem}
If $\boldsymbol{x}_{i}$, $i = 1, \ldots n$ represent a sample from a $p$ variate normal distribution with mean $\boldsymbol{\mu}$ and covariance $\boldsymbol{\Sigma}$, provided $\boldsymbol{\Sigma}$ is positive definite and $n > p$, given sample estimators for mean and covariance $\bar{\boldsymbol{x}}$ and $\boldsymbol{S}$ respectively, then:
\begin{equation}
F = \left(\frac{n}{n-1}\right) \left(\frac{n-p}{p}\right)  (\boldsymbol{\mu}_{0} - \bar{\boldsymbol{x}})^{T} \boldsymbol{S}^{-1}  (\boldsymbol{\mu}_{0} - \bar{\boldsymbol{x}})
\end{equation}
follows an $F$-distribution with $p$ and $(n-p)$ degrees of freedom.   
\end{theorem}


\newpage

\subsection{Footnotes to Hotelling's T$^{2}$ test}

\begin{itemize}
\item Note the requirement that $n > p$, i.e. that $\boldsymbol{S}$ is non-singular which clearly limits the use of this test in bio-informatic applications
\item To carry out a test on $\boldsymbol{\mu}$, we determine whether $F \leq F_{(1-\alpha),p,n-p}$, the $(1-\alpha)$ quantile of the $F$ distribution on $p$ and $n-p$ degrees of freedom and reject the null hypothesis if our test statistic exceeds this value.  
\end{itemize}

We will not consider the one sample T$^{2}$ test any further, but will now examine the two-sampled test.

\newpage

\subsection{Worked example}

Consider the data matrix $\boldsymbol{X} = \left( \begin{array}{rr} 6 & 9 \\
10 & 6 \\
8 & 3 \end{array} \right)$.   Evaluate $H_{0}: \boldsymbol{\mu}_{0}^{T} = (9,5)$\\[1cm]


\begin{shortquiz}{A one sample test}
\begin{questions}
\item Find $\boldsymbol{\bar{x}}^{T}$
\begin{answers}[xbar]{2}
\Ans0 $(8\ 3)$ & 
\Ans1 $(8\ 6)$ \\
\Ans0 $(4\ 6)$ &
\Ans0 $(4\ 3)$
\end{answers}
\begin{solution}
$\boldsymbol{\bar{x}} = \left( \begin{array}{r} \bar{x}_{1} \\ \bar{x}_{2} \end{array} \right)  = \left( \begin{array}{r} \frac{6 + 10 + 8}{3} \\ \frac{9 + 6 + 3}{3} \end{array} \right) = \left( \begin{array}{r} 8 \\ 6 \end{array} \right)$
\end{solution}
\newpage
\item Find $\boldsymbol{S}$
\begin{answers}[s]{1}
\Ans1 $\left( \begin{array}{rr} 4 & -3 \\ -3 & 9 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 4 & 3 \\ 3 & 9 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 6 & -3 \\ -3 & 9 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 4 & -3 \\ -3 & 6 \end{array} \right)$
\end{answers}
\begin{solution}
$s_{11} = \frac{(6-8)^2 + (10-8)^2 + (8-8)^2}{2} = 4$, 
$s_{12} = \frac{(6-8)(9-6) + (10-8)(6-6) + (8-8)(3-6)}{2} = -3$ and
$s_{22} = \frac{(9-6)^2 + (6-6)^2 + (3-6)^2}{2}=9$.   (OK, I know this isn't the matrix calculations, but it's what they use in J\&W.
\end{solution}
\newpage
\item Find $\boldsymbol{S}^{-1}$
\begin{answers}[sinv]{1}
\Ans0 $\left( \begin{array}{rr} 1/3 & -1/9 \\ -1/9 & 4/27 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 7/27 & 1/3 \\ 1/3 & 1/27 \end{array} \right)$ \\
\Ans0 $\left( \begin{array}{rr} 1/9 & 1/27 \\ 1/27 & 4/9 \end{array} \right)$ \\
\Ans1 $\left( \begin{array}{rr} 1/3 & 1/9 \\ 1/9 & 4/27 \end{array} \right)$
\end{answers}
\begin{solution}
 $\boldsymbol{S}^{-1} = \frac{1}{4 \times 9 - (-3) \times (-3)}\left( \begin{array}{rr} 4 & -3 \\ -3 & 9 \end{array} \right) = \left( \begin{array}{rr} 1/3 & 1/9 \\ 1/9 & 4/27 \end{array} \right)$
\end{solution}
\newpage
\item Evaluate the $T^{2}$ test statistic:
\begin{answers}[t2]{4}
\Ans0 $\frac{5}{9}$ &
\Ans0 $\frac{2}{3}$ &
\Ans1 $\frac{7}{9}$ &
\Ans0 $\frac{8}{9}$
\end{answers}
\begin{solution}
\begin{displaymath}
T^2 = 3(8-9, 6-5)\left( \begin{array}{rr} 1/3 & 1/9 \\ 1/9 & 4/27 \end{array} \right) \left( \begin{array}{r} 8-9 \\ 6-5 \end{array} \right) 
\end{displaymath}

\begin{displaymath}
T^2 = 3(-1, 1) \left( \begin{array}{r} -2/9 \\ 1/27 \end{array} \right) = \frac{7}{9}
\end{displaymath}
\end{solution}
\item And now, find the corresponding value of the $F$ distribution
\begin{answers}[f]{4}
\Ans0 $4/54$ & 
\Ans0 $5/54$ & 
\Ans0 $6/54$ & 
\Ans1 $7/54$
\end{answers}
\begin{solution}
$\frac{1}{(n-1)} \\frac{(n-p)}{n} T^2 = \frac{1}{2} \frac{1}{3} \frac{7}{9} = \frac{7}{54}$
\end{solution}
\item Do you reject the Null Hypothesis
\begin{answers}[sig]{2}
\Ans0 Yes & 
\Ans1 No
\end{answers}
\begin{solution}
We have to consider an F distribution on $\nu_{1}=p=2$ and $\nu_2=n-p=1$ degrees of freedom.   You should find that the critical value at $\alpha = 0.1$ is 49.5 (see table 4 the appendix in J\&W) or that at $\alpha = 0.05$ is 199.5 (if I remember the handout).   Clearly the critical value is much larger than the observed value of the test statistic, and we have no evidence to reject the null hypothesis.   This doesn't mean we can say that the mean vector is definitely  $\boldsymbol{\mu}_{0}^{T} = (9,5)$, merely that we have insufficient evidence to rule out that possibility (we have a very small sample size here!
\end{solution}
\end{questions}
\end{shortquiz}


\newpage

\subsection{Two sample T$^{2}$ test}


Analagous to the univariate context, we wish to determine whether the mean vectors are comparable, more formally:
\begin{equation}
H_{0}: \boldsymbol{\mu}_{1} = \boldsymbol{\mu}_{2}
\end{equation}


The T$^{2}$ statistic can be calculated as:

\begin{equation}
\label{hotelling}
T^{2} = \left(\frac{n_{1}n_{2}}{n_{1}+n_{2}}\right)(\boldsymbol{\bar{x}_{1}} - \boldsymbol{\bar{x}_{2}})^{T}\boldsymbol{S}^{-1}(\boldsymbol{\bar{x}_{1}} - \boldsymbol{\bar{x}_{2}})
\end{equation}
where $S^{-1}$ is the inverse of the pooled correlation matrix given by:
\begin{displaymath}
\label{poolcov}
\boldsymbol{S} = \frac{(n_{1} - 1) \boldsymbol{S_{1}} + (n_{2} - 1) \boldsymbol{S_{2}}}{n_{1} + n_{2} - 2}
\end{displaymath}
given the sample estimates for covariance, $\boldsymbol{S_{1}}$ and $\boldsymbol{S_{2}}$ in the two samples.   


\newpage

\subsection{T$^{2}$ distribution in the two sample case}

Again, there is a simple relationship between the test statistic, $T^2$, and the $F$ distribution:   

\begin{theorem}
%make theorm flury 404, proof seber, anderson
If $\boldsymbol{x}_{1i}$, $i = 1, \ldots n_{1}$ and $\boldsymbol{x}_{2i}$, $i = 1, \ldots n_{2}$ represent independent samples from two $p$ variate normal distribution with mean vectors $\boldsymbol{\mu}_{1}$ and  $\boldsymbol{\mu}_{2}$ but with common covariance matrix $\boldsymbol{\Sigma}$, provided $\boldsymbol{\Sigma}$ is positive definite and $n > p$, given sample estimators for mean and covariance $\bar{\boldsymbol{x}}$ and $\boldsymbol{S}$ respectively, then:
\begin{displaymath}
F = \frac{(n_{1} + n_{2} - p - 1) T^{2}}{(n_{1} + n_{2} - 2)p}
\end{displaymath}
has an $F$ distribution on $p$ and $(n_{1}+n_{2}-p-1)$ degrees of freedom.  
\end{theorem}


\begin{itemize}
\item Essentially, we compute the test statistic, and see whether it falls within the $(1-\alpha)$ quantile of the F distribution on those degrees of freedom.\item Note again that to ensure non-singularity of $\boldsymbol{S}$, we require that $n_{1}+n_{2} > p$.
\end{itemize}

\newpage


\subsection{Wilk's Lambda}

\emph{What was all that stuff about likelihood ratio's about?}

It turns out that it is possible to show that:

\begin{equation}
\Lambda^{2/n} =  \left(\frac{\lvert \hat{\boldsymbol{\Sigma}} \rvert }{\lvert \hat{\boldsymbol{\Sigma}}_{0} \rvert } \right) = \left( 1 + \frac{T^{2}}{n-1} \right)^{-1}
\end{equation}

It is also possible to obtain the T$^{2}$ via union intersection methods.   This is nice because it tells us a lot about the properties of the test!

\newpage

\section*{}

\subsection*{Harold Hotelling}
  
%  \includegraphics[width = 0.2\textwidth]{../images/hotell}
A biography (and links to pictures) of Harold Hotelling is available \href{http://en.wikipedia.org/wiki/Harold_Hotelling}{\color{blue}HERE} 
 
Born 1895 Minnesota, died 1973.   First degree in Journalism, PhD in Mathematics from Princeton University.   Studied with R.A.Fisher, set up statistics program at Columbia; numerous fundamental contributions including confidence intervals - as well as several multivariate contributions such as T$^{2}$, canonical correlation, practical methods for principal components



\newpage

\section{Confidence ellipses}


Essentially, we wish to find a region of squared Mahlanobis distance such that:
\begin{displaymath}
Pr \left( (\boldsymbol{\bar{x}} - \boldsymbol{\mu})^{T} \boldsymbol{S}^{-1} (\boldsymbol{\bar{x}} - \boldsymbol{\mu}) \right) \leq c^{2} 
\end{displaymath}
and we can find $c^{2}$ as follows:
\begin{displaymath}
c^{2} = \left( \frac{n-1}{n} \right) \left( \frac{p}{n-p} \right) F_{(1-\alpha), p, (n-p)}
\end{displaymath}
where $F_{(1-\alpha), p, (n-p)}$ is the $(1-\alpha)$ quantile of the $F$ distribution with $p$ and $n-p$ degrees of freedom, $p$ represents the number of variables and $n$ the sample size.


\begin{figure}
\begin{center}
\includegraphics[width = 0.5\textwidth]{../images/cdellipse}
\caption{Constant density ellipse for mean vector for difference in head width and breadth, and univariate confidence intervals for the mean of each variable}
\label{cdellipse}
\end{center}
\end{figure}


\begin{itemize}
\item The centroid of the ellipse is at $\bar{\boldsymbol{x}}$
\item The half length of the semi-major axis is given by:
\begin{displaymath}
\sqrt{\lambda_{1}} \sqrt{\frac{p(n-1)}{n(n-p)}F_{p, n-p}(\alpha)}
\end{displaymath} where $\lambda_{1}$ is the first eigenvalue of $\boldsymbol{S}$
\item The half length of the semi-minor axis is given by:
\begin{displaymath}
\sqrt{\lambda_{2}} \sqrt{\frac{p(n-1)}{n(n-p)}F_{p, n-p}(\alpha)}
\end{displaymath} where $\lambda_{2}$ is the second eigenvalue of $\boldsymbol{S}$
\item The ratio of these two eigenvalues gives you some idea of the elongation of the ellipse
\end{itemize}

\newpage

\subsection{Final thoughts on confidence intervals}

\begin{itemize}
\item In addition to the (joint) confidence ellipse, it is possible to consider \emph{simultaneous} confidence intervals - univariate confidence intervals based on a linear combination which could be considered as shadows of the confidence ellipse
\item It is also possible to carry out bonferroni adjustments of these simultaneous intervals
\end{itemize}


\newpage

\begin{shortquiz}
\begin{questions}
\item Which method for constructing hypothesis tests yields a test statistic which looks at the ratio of the determinant of the covariance matrix under the null to the determinant of the covariance matrix assuming the sample mean vectors
\begin{answers}[lrt1]{2}
\Ans1 Likelihood ratio test &
\Ans0 Union intersection test
\end{answers}
\item Twice the negative log of a test statistic given by $ = \left(\frac{\lvert \hat{\boldsymbol{\Sigma}} \rvert }{\lvert \hat{\boldsymbol{\Sigma}}_{0} \rvert } \right)^{n/2}$ follows which distribution under the Null hypothesis:
\begin{answers}[lrt2]{4}
\Ans0 $z$ &
\Ans0 $F$ &
\Ans0 $t$ &
\Ans1 $\chi^2$
\end{answers}
\item Multiplication by a constant (which depends on the sample size and number of variables) transforms the $T^2$ test statistic into which of the following distribution:
\begin{answers}[lrt3]{4}
\Ans0 $z$ &
\Ans1 $F$ &
\Ans0 $t$ &
\Ans0 $\chi^2$
\end{answers}
\newpage
\item In conducting an $T^2$ Hypothesis test, we compare the test statistic calculate from our data with a critical value for a given size of test (e.g. $\alpha = 0.05$).   Under which circumstances would we reject $H_{0}$:
\begin{answers}[lrt4]{1}
\Ans0 If the calculated test statistic was \emph{smaller} than the critical value under the null
\Ans1 If the calculated test statistic was \emph{larger} than the critical value under the null
\end{answers}
\item If we had a computer and wished to calculate the $p-value$ for our $T^2$ test, this amounts to doing which of the following:
\begin{answers}[lrt5]{1}
\Ans1 Calculating the proportion of the null hypothesis distribution (the area under the curve) which exceeds the calculated value of a test statistic \\
\Ans0 Calculating the proportion of the null hypothesis distribution (the area under the curve) which lies below the calculated value of a test statistic \end{answers}
\newpage
\item In a two sample $T^2$ test, a $p-value$ of $0.01$ is regarded as statistically significant.   We would reject $H_0$ and consider that we had evidence to indicate that $\boldsymbol{\bar{x}}_{1}$ is significantly different from  $\boldsymbol{\bar{x}}_{2}$ 
\begin{answers}[lrt6]{2}
\Ans1 True &
\Ans0 False
\end{answers}
\item In a one sample $T^2$ test, a $p-value$ of $0.6$ is regarded as \emph{not} statistically significant.   We would \emph{not} reject $H_0$ and conclude that we had evidence to indicate that $\boldsymbol{\bar{x}}$ was $\boldsymbol{\bar{\mu}}_{0}$
\begin{answers}[lrt7]{2}
\Ans0 True &
\Ans1 False
\end{answers}
\begin{solution}
Most of the statement is true.   A p-value of $0.6$ is indeed regarded as \emph{not significant} and we do not have evidence to reject the null hypothesis.   The mistake here is in stating that we therefore conclude that  $\boldsymbol{\bar{x}}$ was $\boldsymbol{\bar{\mu}}_{0}$.   The fundamental point is that can never \emph{prove} the null.   All we can say is that we didn't find evidence to reject the null: maybe this was because  $\boldsymbol{\bar{x}}$ was indeed $\boldsymbol{\bar{\mu}}_{0}$, or maybe because our sample size wasn't large enough to tell us it was something different!   Absence of evidence isn't evidence of absence.
\end{solution}
\end{questions}
\end{shortquiz}



\newpage

\section{Summary}


\begin{itemize}
\item T$^{2}$ test is based upon Mahalanobis distance and can be used for inference on mean vectors - this test can be derived via a variety of routes
\item Difference between univariate and multivariate inference, especially when considering confidence ellipses
\item Having determined that there is a significant difference between mean vectors, you may wish to conduct a number of follow up investigations and even carry out discriminant analysis
\end{itemize}

\newpage

\subsection{Exam type questions}

\begin{itemize}
\item Explain why we carry out inference on a mean vector rather than analysing one variable at a time
\item Interpret confidence ellipses
\item Interpret hypothesis test results
\item Conduct a simple hypothesis test (or part of one)
\item Anything else you may suggest
\end{itemize}


\newpage

\subsection{Further reading}

Johnson and Wichern (2002) Chapter 5 is very good.   Flury (1997) chapters 5-7 are also very good.  

\newpage

\subsection{This weeks lab.}

\begin{itemize}
\item Conduct T$^{2}$ test for difference between flea beetle species
\item Construct simultaneous confidence ellipses
\end{itemize}


 \end{document} 