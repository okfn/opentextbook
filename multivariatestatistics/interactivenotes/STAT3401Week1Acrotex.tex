\documentclass{article}
%\usepackage[pdftex]{hyperref}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amssymb}
\usepackage[pdftex,designi,nodirectory,navibar,usesf]{web} %removed usetemplates
\usepackage[pdftex]{exerquiz}
%\template{../img/logo.pdf}
\definecolor{Blue}{cmyk}{1,1,0.,0.}
\definecolor{plum}{cmyk}{.5,1,0.,0.}
\definecolor{orange}{cmyk}{0.5,0.51,1,0}
\definecolor{Red}{rgb}{1.0,0.2,0.25}
\definecolor{Green}{rgb}{.1,.6,.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{STAT3401: Introduction to Multivariate Methods}
\author{Paul Hewson}
\date{2nd October 2008}
\authorColor{blue}



\newtheorem{df}{Definition}

\university{
\null\hspace{-.6cm}\raisebox{.25cm}{\includegraphics[width=0.5cm]{../images/logo.pdf}}
\hspace{.5cm}\raisebox{.7cm}
{School of Mathematics and Statistics}\hspace{1cm}
%\includegraphics[width=0.9cm]{../EBP/img/logo.pdf}
 }


\email{paul.hewson@plymouth.ac.uk}
\version{0.1} 
\copyrightyears{2008}


\optionalPageMatter{\vfill
  \begin{center}
  \fcolorbox{blue}{webyellow}{
   \begin{minipage}{.67\linewidth}
   \noindent \textcolor{red}{\textbf{Overview:}}
   This webfile is intended to support the lectures on Multivariate Methods, and should be supplemented with a good textbook.   These notes are meant to be slightly interactive, mysterious
   green dots, squares and boxes appear which you can click on to
   answer questions and check solutions.
  \end{minipage}}
  \end{center}}

\begin{document}


\maketitle


\section{Outline}

\subsection{Aims of week 1}

\begin{itemize}
\item To explain what we mean by multivariate methods
\item To provide an overview of the material we will cover
\item To introduce the notation, to start thinking about linear combinations
\item To provide some ideas on how to conduct a multivariate
  exploratory data analysis (eda)
\end{itemize}


\newpage

\subsection{Coursework}

\begin{itemize}
\item The coursework will be handed out soon.   There will be a data
  analysis exercise and a number of past paper type exam questions to
  help consolidate learning as we go through the term.  
\end{itemize}



Performing a multivariate data analysis is one of the learning
outcomes.   However, the computer labs are intended to be used in two ways:

\begin{itemize}
\item To consolidate our understanding of the principles behind the
  methods we are meeting
\item To allow us to carry out data analysis.   
%\item If you have a clash consider other times we could meet.
\end{itemize}


\newpage


\subsection{Books}

\begin{itemize}
\item There is an online booklist: use it
\item You \emph{will need access to} a good multivariate text book.   Johnson and Wichern is the recommended text this year, but there are plenty more available.   Find one that suits (as long as it covers the right topics)
\item Don't just read the book, try the exercises!   Many of our
  problems will be taken from Johnson and Wichern
\end{itemize}



\newpage

\subsection{Topics we shall cover}

\begin{itemize}
\item The multivariate ``canon'' has not yet been established: books
  vary in the material they cover
\item We will consider 1. Cluster Analysis, 2. Principal Components
  Analysis, 3. Hotellings T$^{2}$, 4. MANOVA, 5. Factor Analysis, 6. Discriminant
  Analysis, 7. Canonical correlation and 8. Scaling (principal co-ordinates
  analysis).  I will set one of these topics as a ``self study topic''
\item For the first few weeks, we will revisit the tools we need in
  order to understand what's going on.   Specifically, we shall revise
  some simple linear algebra (matrices), consider measures of distance
  and meet a few of the many beautiful ideas around the multivariate Normal distribution.
\end{itemize}

\newpage


\subsection{Diagnostic test}

In the mean time, try a diagnostic test:

\begin{shortquiz}{Diagnostics}
\begin{questions}
\item If $Z \sim N(0,1)$, what is the distribution of $Z^{2}$
\begin{answers}[whatschi]{4}
\Ans0 $F$ &
\Ans0 $t$ &
\Ans1 $\chi^2_{1}$ &
\Ans0 $\chi^2_{p}$
\end{answers}
\item If $Z_{1}, Z_{2}, \ldots, Z_{p} \sim_{iid} N(0,1)$, what is the distribution of $Z_{1}^{2} + Z_{2}^{2} + \ldots + Z_{p}^{2}$
\begin{answers}[whatschip]{4}
\Ans0 $N(0, 1^p)$ &
\Ans0 $t$ &
\Ans0 $\chi^2_{1}$ &
\Ans1 $\chi^2_{p}$
\end{answers}
\item What is:
 \begin{equation}
r = \frac{\sum_{i=1}^{N}(x_{i} - \bar{x})(y_{i} - \bar{y})}
{\sqrt{\sum_{i=1}^{N}(x_{i} - \bar{x})^{2}}\sqrt{\sum_{i=1}^{N}(y_{i} - \bar{y})^{2}}}
\end{equation}
\begin{answers}[vardef]{2}
\Ans0 Variance & 
\Ans1 Covariance \\
\Ans0 Correlation &
\Ans0 Standard deviation 
\end{answers}
\newpage
\item What is the sample estimate of
  $\frac{Cov(X,Y)}{\sqrt{Var(X)}\sqrt{Var(Y)}}$ also known as?
\begin{answers}[cordef]{2}
\Ans0 Variance & 
\Ans0 Covariance \\
\Ans1 Correlation &
\Ans0 Standard deviation 
\end{answers}
\item If $\bar{x}_{1}$ is the mean of group 1, and $\bar{x}_{2}$ is the mean of group 2, what test do we use to find whether $\bar{x}_{1} = \bar{x}_{1}$
\begin{answers}[ttest]{2}
\Ans0 Z-test & 
\Ans1 t-test \\
\Ans0 F-test &
\Ans0 ANOVA 
\end{answers}
\item What test do you use if you have more than 2 groups
\begin{answers}[anova]{2}
\Ans0 Z-test & 
\Ans0 t-test \\
\Ans0 F-test &
\Ans1 ANOVA 
\end{answers}
\item If $X_{1} \sim \chi^{2}_{\nu_{1}}$ and $X_{2} \sim \chi^{2}_{\nu_{2}}$ what is the distribution of $X_{1} / X_{2}$
\begin{answers}[fdef]{4}
\Ans1 $F$ &
\Ans0 $t$ &
\Ans0 $\chi^2_{1}$ &
\Ans0 $\chi^2_{p}$
\end{answers}
\end{questions}
\end{shortquiz}



\newpage
\section{Some essentials}


One way in which multivariate analysis differs is that we consider the
data as a whole.   I do not consider multiple regression as a
multivariate technique.   We are going to consider techniques which
look at a number of variable together.   We are also going to consider
techniques which examine a number of rows together.   But is it always
clear what belongs on a row, and what belongs in a column?

\subsection{Data}

\begin{itemize}
\item Multivariate data arise whenever more than one variable (\textit{response, attribute}) is measured on each individual (\textit{unit, subject}) in a sample.
\item The variables are usually denoted $x_{1}, x_{2}, \ldots, x_{p}$, and collectively as the vector $\boldsymbol{x}^{T} = (x_{1}, x_{2}, \ldots, x_{p})$. (Vectors are generally columns; ${T}$ denotes transpose).
\item There are different types of variables: numerical (either continuous or discrete), categorical (either nominal or ordinal), and binary (presence/absence or 2-state categorical).
\end{itemize}


\subsection{Summarising the data}

Let's start with a gentle introduction to some matrix terminology.

\begin{itemize}
\item The values observed on the $i$th individual can be written as the vector $\boldsymbol{x}^{T}_{i} = (x_{i1}, x_{i2},\ldots , x_{ip})$
\item If all the individual vectors are written as the rows of a table, the result is called the data matrix and is usually denoted by $\boldsymbol{X}$. The element $x_{ij}$ of this matrix contains the value of variable $x_{j}$ observed on individual $i$.
\end{itemize}

And now, lets fix our ideas by thinking about some real data.


\begin{itemize}
\item To describe a multivariate data set it would help to be able to look at it, i.e. to have a pictorial representation.
\item First, let us assume that all the variables are purely numerical.
\item When there are just two such variables, a scatter diagram is a familiar idea for representing the data.
\item The $n$ individuals are represented by $n$ points on a graph; the coordinates of point $i$ are given by $(x_{i1}, x_{i2})$.
\end{itemize}

\subsubsection{Ryan and Joiner Black Cherry Tree data}

Here's some rather over-used data (but I do like black cherries).   A
sample of the data is given here:

\begin{table}[ht]
\begin{center}
\begin{tabular}{rrrr}
\hline
 & Girth & Height & Volume \\
\hline
1 & 8.30 & 70.00 & 10.30 \\
2 & 8.60 & 65.00 & 10.30 \\
3 & 8.80 & 63.00 & 10.20 \\
4 & 10.50 & 72.00 & 16.40 \\
5 & 10.70 & 81.00 & 18.80 \\
6 & 10.80 & 83.00 & 19.70 \\
\ldots & \ldots & \ldots & \ldots \\
\hline
\end{tabular}
\end{center}
\end{table}


%par(las = 1, bty = "n")
%plot(trees$Girth, trees$Volume, xlab = "Girth", ylab = "Volume", main = "Ryan and Joiner Tree Data", pch = 16, col = "red")
 
It's pretty clear in this case that the columns should be Girth,
Height and Volume, and that the rows should be the individual trees.
We already know the standard method for illustration:

\newpage

\textbf{Scatterplot}

\includegraphics[width = 0.6\textwidth]{../images/scatterT}


\newpage

\textbf{Some features of a scatter diagram}

\begin{itemize}
\item The centroid of the points occurs at $(\bar{x}_{1}, \bar{x}_{2})$, where $\bar{x}_{j}$ is the mean (i.e. arithmetic average $\frac{1}{n} \sum_{i=1}^{n}x_{ij}$) of the values on variable $x_{j}$. Here, this is $(\bar{x}_{1}, \bar{x}_{2})) = (13.24, 30.17)$
\item The spread of the points along an axis is measured either by the variance or the standard deviation of the corresponding variable. The variance of $x_{j}$ is $\frac{1}{n-1}\sum_{i=1}^{n} (x_{ij} - \bar{x}_{j})^{2}$ and its standard deviation is the square root of the variance. For Table 2, the variance of $x_{1}$ is $9.84$ and its standard deviation is $\sqrt{9.84} = 3.14$, while the variance of $x_{2}$ is $270.2$ and its standard deviation is $\sqrt{270.2} = 16.4$
\item The association between the two variables is measured by their covariance
$\frac{1}{n-1}(x_{i1} -  \bar{x}_{1})(x_{i2} -  \bar{x}_{2})$. Here, this is 49.9.
\end{itemize}


%\subsection{Correlation and Covariance}

Now, we measure the linear association between two variables with the
covariance.   If we standardise that in terms of the variance of the
two variables we have the correlation.

\newpage

\textbf{Bivariate Correlation}

\begin{itemize}
\item \texttt{cor()} in R.
\item You can also get Kendall's and Spearman's coefficients by adding \verb+method = "kendall"+ and \verb+method = "spearman"+ to the function call.
\end{itemize}

There's a nice demo in R if you want to revise your ideas of what correlation is all about:

\begin{verbatim}
library(TeachingDemos)
run.cor.examp()
\end{verbatim}


\newpage

\textbf{On the other hand (A correlation paradox)}

\begin{itemize}
\item The following paradox, based on an article in ``The American Statistician'' by Langford et al. 2001.
\item If $x$ and $y$ are positively correlated, and $x$ and $z$ are positively correlated, what will be the sign of the correlation between $y$ and $z$?
\item For an interesting little demonstration of this paradox, create three random variables $u$, $v$ and $w$ according to any recipe you like (e.g. \verb+u <- rnorm(100)+).   Then form $x$, $y$ and $z$ from these variables as follows:
\end{itemize}

\begin{verbatim}
> x <- u + v
> y <- u + w
> z <- v - w
> X <- cbind(x,y,z)
\end{verbatim}


Now look at \texttt{cor(X)} and \texttt{pairs(X)}

\newpage
\begin{shortquiz}{Correlation paradox: }
What's going on here:
\begin{answers}[corpar]{1}
\Ans0 The correlation between x, y and z are positive \\
\Ans1 The correlation between x and y, x and z is positive, but the
correlation between y and z is negative \\
\Ans0 The correlation between x and y, y and z is positiv, but the
correlation between x and z is negative \\
\Ans0 The correlation between x and z, y and z is positive, but the
correlation between y and z is negative
\end{answers}
\begin{solution}
This might sound like an odd result, but it makes perfect geometric
sense.   Try the following exercise.   Consider $x = (1,4,7)$ and $y =
2,5,6$.   If you were to plot a conventional scatterplot you would
plot points at $(1,2)$, $(4,5)$, $(7,6$).   But if you know attempt to
plot $\vec{x}$ and $\vec{y}$, i.e. the three dimensional points
$(1,4,7)$ and $(2,5,6)$ you could attempt to measure the angle between
these vectors.   The cosine of this angle is the correlation.
Unfortunately, it's a little harder to even imagine this vector for a
conventional data set (with tens if not hundreds of points), but
that's what you've been measuring whenever you work out the
correlation coefficient.   And if you think about the correlation
paradox, you will appreciate that by having two modestly correlated
variables (i.e. with angles in the order of 50 or more degrees), when
you measure the angle between the two outermost variables it will be
greater than 90 and the cosine will be negative.
\end{solution}
\end{shortquiz}


\newpage

\subsection{Conventional ideas around correlation}

Just by way of a recap, what do you estimate the correlation
coefficients to be for these following data points.



\useBeginQuizButton
\useEndQuizButton
\begin{quiz}{Correlation}
%Revision of $p$ values and confidence intervals}
{\color{red} Consider the following diagrams:}
\begin{questions}
\item What is the correlation in figure 1
\begin{center}
\includegraphics[width = 0.3\textwidth]{../images/correx1}
\end{center}
\begin{answers}[c1]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans1 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 2
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx2}
\end{center}
\begin{answers}[c2]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans1 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 3
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx3}
\end{center}
\begin{answers}[c3]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans1  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 4
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx4}
\end{center}
\begin{answers}[c4]{4}
\Ans1 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 5
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx5}
\end{center}
\begin{answers}[c5]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans1  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 6
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx6}
\end{center}
\begin{answers}[c6]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans1 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 7
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx7}
\end{center}
\begin{answers}[c7]{4}
\Ans0 -0.8 & \Ans0 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans1 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
%%
\newpage
\item What is the correlation in figure 8
\begin{center}
\includegraphics[width = 0.4\textwidth]{../images/correx8}
\end{center}
\begin{answers}[c1]{4}
\Ans0 -0.8 & \Ans1 -0.7 & \Ans0 -0.5 & \Ans0 0 \\
\Ans0  0.3 & \Ans0 0.8 & \Ans0  0.9 & \Ans0 0.95
\end{answers}
\end{questions}
\end{quiz}

\ScoreField{Correlation}
\eqButton{Correlation}

\newpage

\subsection{Extension beyond bivariate}

You should be very comfortable dealing with bivariate concepts.   Now
let's introduce some terminology that we will be using throughout in a
fully 
\emph{multivariate} setting.   Firstly, note that conventionally, we
have $p$ variables $x_{1}, x_{2}, \ldots, x_{p}$ observed on $n$ individuals

\begin{itemize}
\item $p$ variable means can be collected into the \emph{mean vector} $\boldsymbol{\bar{x}}^{T} = (\bar{x}_{1}, \bar{x}_{2}, \ldots, \bar{x}_{p})$
\item Variances of the $p$ variables collcted on the diagonal and the $\frac{1}{2}p(p-1)$ covariances between every pair of variables collected in off-diagonal of the \emph{variance covariance} matrix $\boldsymbol{S}$.
\item Correlations between every pair of variables can be put in the offdiagonal position of the \emph{correlation} matrix $\boldsymbol{R}$ (diagonal elements are all $1$)
\item Mean centering or standardising is conducted by carrying out the appropriate operation on each variable in turn.   
\item Covariance matrix of the standardised data is the same of the correlation matrix of the original data
\end{itemize}


We no longer have bivariate correlation coefficients (or covariance),
we now have a Covariance (or even more formally the variance
covariance) matrix.   This is what it looks like for the \texttt{tree} data

\begin{table}[ht]
\begin{center}
\begin{tabular}{rrrr}
\hline
 & Girth & Height & Volume \\
\hline
Girth & 9.85 & 10.38 & 49.89 \\
Height & 10.38 & 40.60 & 62.66 \\
Volume & 49.89 & 62.66 & 270.20 \\
\hline
\end{tabular}
\end{center}
\end{table}

And the corresponding Correlation matrix for \texttt{tree} data:

% latex table generated in R 2.3.1 by xtable 1.3-0 package
% Mon Jul 17 20:51:11 2006
\begin{table}[ht]
\begin{center}
\begin{tabular}{rrrr}
\hline
 & Girth & Height & Volume \\
\hline
Girth & 1.00 & 0.52 & 0.97 \\
Height & 0.52 & 1.00 & 0.60 \\
Volume & 0.97 & 0.60 & 1.00 \\
\hline
\end{tabular}
\end{center}
\end{table}

\newpage
\subsection{Partial correlation coefficient}

We also have more subtle ways of measuring the association between variables

\begin{itemize}
\item For quantitative data, the correlation matrix $\boldsymbol{R}$ shows all pairwise correlations between variables (may be too many to interpret)
\item The \emph{partial correlation} $r_{ij,k}$ is the correlation between $x_{i}$ and $x_{j}$ when $x_{k}$ is held at a constant value.  Any number of values can be held fixed:
\begin{displaymath}
r_{ij,k} = \frac{r_{ij} - r_{ik}r_{jk}}{\sqrt{(1-r_{ik}^{2})(1 - r_{jk}^{2})}}
\end{displaymath}
\begin{displaymath}
r_{ij,kl} = \frac{r_{ij,k} - r_{il,k}r_{jl,k}}{\sqrt{(1-r_{il,k}^{2})(1 - r_{jl,k}^{2})}},\ \mbox{etc.}
\end{displaymath}
\item A partial correlation will show whether a high correlation between two variables is caused by their mutual correlation with one of more other variables
\end{itemize}





\newpage

\section{Multivariate questions}


``Multivariate statistics'' covers a vast and rapidly expanding
discipline; developments are taking place for the analysis of gene
expression data as well as psychometrics to name but two.   We
therefore need to narrow down our definition to make life manageable.

For the purposes of this course, our definition of multivariate is as follows:

\begin{itemize}
\item We don't count multiple regression as a multivariate technique.
\item Relationships between variables (principal components, factor
  analysis, canonical correlation)
\item Relationships between individuals (cluster analysis,
  discriminant analysis, Hotelling's $T^2$ test, MANOVA, principal
  coordinates analysis.
\end{itemize}


There are some overlaps between the techniques.   Also, to make life
manageable, $6\frac{1}{3}$ of the techniques we will consider involve
eigenanalysis (either of the covariance or the correlation matrix, or
of the ratio of between and within ``covariance'' matrices).   


Firstly, the techniques we shall cover looking at the differences
between individuals are as follows:
\newpage
\begin{itemize}
\item Differences between Groups
  \begin{itemize}
  \item Can we tell if a vector of means $\bar{\boldsymbol{x}}_{1}$ is
    different from another vector $\bar{\boldsymbol{x}}_{2}$
    ({\color{red}Hotelling's $T^2$})
  \item What if we have more than two groups ({\color{red}MANOVA})?
  \end{itemize}
\item Classification
  \begin{itemize} 
  \item Can we find individuals that are more alike than other
    individuals ({\color{red}Cluster Analysis})
  \item If we already knew the groupings, could we investigate which
    variables were most important in telling the groups apart.   Could
    we use this information to find a rule that lets us classify new
    observations ({\color{red}Discriminant analysis})
  \end{itemize}
\item Visualisation
\begin{itemize}
\item Do we have some way of visualising the similiarities and
  dissimilarities between individuals ({\color{red} Scaling /
    Principal Co-ordinates Analysis})
\end{itemize}
\end{itemize}

\newpage
And the techniques which are about examining the variables are as follows:

\begin{itemize}
\item Dimension Reduction
  \begin{itemize} 
  \item Can we represent our data in less dimensions
    ({\color{red}Principal Components Analysis})
  \end{itemize}
\item Relationships between variables
  \begin{itemize}
  \item Can we model the relationships between variables
    ({\color{red}Factor analysis})
  \item If we have a set of variables $\boldsymbol{X}$, can we find a
    projection that is correlated to a projection of variables
    $\boldsymbol{Y}$ ({\color{red}Canonical correlation})
  \end{itemize}
\end{itemize}



\newpage

\useBeginQuizButton
\useEndQuizButton
\begin{quiz}{Summary}
%Revision of $p$ values and confidence intervals}
{\color{red} Have a good think about the types of analysis we have
  just described, and identify which methods we would use in the
  following situations:}
\begin{questions}
\item A bank keeps information on customers to whom it has lent
  money.   It collects information at the time of making the loan
  (income, tenure, age and so on).   It knows which of these customers
  have repaid their loans and which have defaulted.   How can it
  better understand the difference between the two groups - and how
  might it be able to develop a means for predicting which people will
  default in the future
\begin{answers}[da]{4}
\Ans0 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans1 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\newpage
\item A manufacturer is producing a thin plastic file (by extrusion).
  A number of features of the film are important, such as
  transparancy, tear resistance and adhesiveness.   It wishes to carry
  out an experiment examining the effects of extruding at three
  different rates, and at four different pressures.    Which technique
  might be appropriate:
\begin{answers}[manova]{4}
\Ans0 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans1 MANOVA &
\Ans0 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\newpage
\item A psychologist is carrying out a lengthy survey of people's
  attitudes to road risk.   There are a number of questions, each
  having a continuous response.   The psychologist wishes to
  understand the relationship between these variables (attitudes to
  the law, attitudes to other people in society and so on).  Which
  technique might be appropriate:
\begin{answers}[fa]{4}
\Ans0 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans0 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans1 Factor analysis
\end{answers}
\newpage
\item
A gymnasium has collected information on a number of physical
attributes of its members, such as height, age, weight, resting heart
rate.   It want't to see how this group of variables might relate to
performance on a number of the pieces of equipment (e.g. average speed
on running machine, longest distance on rowing machine, highest weight
lifted and so on).   Which technique might be appropriate:
\begin{answers}[cc]{4}
\Ans0 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans0 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans1 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\newpage
\item A biologist has carried out an experiment using a ``microarray
  chip''.   These devices tell you which of 64,000 genes a cell in the
  body of an organism
  might be expressing at any given given.   The biologist doesn't have
  any 64,000 dimensional paper, and even the $64,000 \times 64,000$
  dimensional scatterplot is a little crowded.   What technique might
  be appropriate in order to start examining these data
\begin{answers}[pca]{4}
\Ans1 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans0 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\newpage
\item A retail outlet collects information on its customers through
  loyalty cards.   It wishes to know whether there are different
  sub-groups of customer who are more alike in some way.  Knowing this
  would help the company target its marketing information more
  effectively.   Which technique might be the most appropriate:
\begin{answers}[clus]{4}
\Ans0 Principal Components analysis &
\Ans0 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans0 Discriminant analysis \\
\Ans1 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\newpage
\item A statistics lecturer has some very old data on painted
  turtles.   The main reason for persevering with such old data is
  because it is a chance to live up a lecture slightly by including
  photos of these rather stunning creatures.   A number of body
  measurements have been taken.   How might we determine whether there
  is a difference between male and female turtles over all these
  measurements together:
\begin{answers}[ht]{4}
\Ans0 Principal Components analysis &
\Ans1 Hotellings $T^2$ test &
\Ans0 MANOVA &
\Ans0 Discriminant analysis \\
\Ans0 Cluster analysis &
\Ans0 Principal co-ordinates analysis &
\Ans0 Canonical correlation &
\Ans0 Factor analysis
\end{answers}
\end{questions}
\end{quiz}

\ScoreField{Summary}
\eqButton{Summary}

\newpage
\section{Some eda}


Finally, let's recap by mentioning a few useful computing routines (in \textbf{R}):


For convenience, we will illustrate these with some inbuilt data, on
US Arrests:\\
\texttt{data(USArrests)}\\
\texttt{USArrests}\\
\texttt{head(USArrests)}

The data look something like this:
  
\begin{displaymath}
\begin{array}{lcccc}
State & Murder & Assault & Rape &  UrbanPop(\%)\\
\end{array}
\end{displaymath}
\begin{displaymath}
\left[ \begin{array}{lrrrr}
Alabama       &   13.2  &   236  &     21.2 &  58\\
Alaska         &  10.0   &  263  &    44.5 & 48\\
Arizona        &   8.1  &   294   &    31.0 &  80\\
Arkansas     &     8.8 &    190  &     19.5 &  50\\
$\ldots$ & $\ldots$ & $\ldots$ & $\ldots$ \\
\end{array}
\right]
\end{displaymath}

\newpage

The Variance-covariance matrix $\boldsymbol{\Sigma}$ is conventionally estimated by the sample covariance matrix $\boldsymbol{S}$.\\
\texttt{cov(USArrests)}

\begin{tabular}{lrrrr}
          &  Murder  & Assault &  UrbanPop    &  Rape\\
Murder &   18.97 & 291.06 &   4.39 &  22.99\\
Assault & 291.06 & 6945.17 & 312.28 & 519.27\\
UrbanPop &   4.39 & 312.28 & 209.53 & 55.77\\
Rape  &    22.99 & 519.27 & 55.77 &  87.73\\
\end{tabular}

(And do note here that \texttt{cov.wt(USArrests)} uses $1/n$ rather
than $1/(n-1)$ if that ever worries you.   The correlation matrix is estimated by the sample correlation matrix $\boldsymbol{R}$.)\\

The correlation matrix can be obtained as follows:\\
\texttt{cor(USArrests)}

\begin{tabular}{lrrrr}
    &         Murder &  Assault &  UrbanPop  &    Rape\\
Murder &  1.00 & 0.80 & 0.07 & 0.56\\
Assault &  0.80 & 1.00 & 0.26 & 0.67\\
UrbanPop & 0.07 & 0.26 &  1.00 & 0.41\\
Rape &    0.56 & 0.67 &  0.41 & 1.00\\
\end{tabular}

\newpage
The basic scatter plot is easy enough to produce in \textbf{R}:

\includegraphics[width = 0.5\textwidth]{../images/scatter}

\texttt{plot(USArrests[,1], USArrests[,2])}

or we can just use the default, which produces a pairwise scatterplot
of all variables in the data frame (this is sometimes called a
draftsman plot):

\newpage

\includegraphics[width = 0.6\textwidth]{../images/pairs}

\texttt{pairs(USArrests)}

\newpage

\textbf{R} being \textbf{R}, there are a number of enhancements to the
basic scatterplot, such as (1) Scatter plot matrix

\texttt{library(lattice)}\\
\texttt{splom(USArrests)}

\includegraphics[width = 0.5\textwidth]{../images/splomUSArrests}

\newpage

as well as the following rather (Over)-elaborate scatter plot matrix

\texttt{library(cwhplot)}\\
\texttt{library(grid)}\\
\texttt{pltSplomT(USArrests)}


\includegraphics[width = 0.5\textwidth]{../images/cwhplotUSArrests}

\newpage

A little gimmicky, but still useful are the 3d scatterplots

\texttt{library(scatterplot3d)}\\
\texttt{s3d <- scatterplot3d(USArrests[,-3], type=''h'',  
highlight.3d=TRUE,
angle=55, scale.y=0.7, pch=16, main = ``USArrests'')}

\includegraphics[width = 0.4\textwidth]{../images/scat3dUSArrests}

\newpage

And we will meet another enhancement that lets you plot groupwise
densities (and may mention \texttt{rgl}).   However, we're kind of
familiar with all this stuff.   What about ``relationships'' between
individuals

\begin{itemize}
\item A number of proposals have been made
\item These make more sense when considering groups
\end{itemize}

\newpage

Firstly, consider an irresistable gimmick (Chernoff's faces) which is  based on the idea that we find it easier to spot similarities in
human faces:

\texttt{library(TeachingDemos)}\\
\texttt{faces(USArrests)}

\includegraphics[width = 0.6\textwidth]{../images/faces}


\newpage

A little more sensible, and something we will revisit
later are heatmaps

\texttt{heatmap(as.matrix(USArrests))}

\includegraphics[width = 0.4\textwidth]{../images/heatmapUSArrests}

which can get very convoluted:

\includegraphics[width = 0.4\textwidth]{../images/hopach}

\newpage
And those dendrograms can get much prettier if we want:

\includegraphics[width = 0.4\textwidth]{../images/fancyhclust}


\newpage

However, a somewhat more sensible plot for these is the ``star'' plot:





%stars(USArrests, col.segments = c("red", "green", "blue", "pink"),
%draw.segment


\texttt{stars(USArrests)}

\includegraphics[width = 0.6\textwidth]{../images/stars}

\newpage

But now, we consider something actually rather useful.   The proposal
dates from 1970, and works perfectly well on paper (you don't have to
have dynamic computer graphics).  It's named after its inventor, the
Andrew's curve:


It consists of a fourier transform of the data:
\begin{displaymath}
f_{x}(t) = x_{1}/\sqrt 2 + x_{2} \sin 2 + x_{3} \cos t + x_{4} \sin 2t + x_{5} \cos 2t + \ldots
\end{displaymath}

and is actually rather good at revealing groupings in the data:


As we will find out later, there are three groups in the
\texttt{data(Iris)} data.


\includegraphics[width = 0.6\textwidth]{../images/andrewsIris}

\newpage
Rather simpler than the Andrew's curve are parallel co-ordinate plots:

\texttt{library(MASS)}\\
\texttt{parcoord(iris[,-5], col = as.numeric(iris[,5]))}


\includegraphics[width = 0.5\textwidth]{../images/parcoordIris}

Although these seem very simple and obvious, they are still quite powerful.

\newpage

Just to finish off, try the following dynamic 3d graphic:

\texttt{source(``rglellipsoid.R'')}\\
\texttt{scatter3d(iris\$Sepal.Length, iris\$Sepal.Width, iris\$Petal.Width,
           ellipsoid=TRUE, surface=FALSE,
           groups=as.factor(iris\$Species))}

\includegraphics[width = 0.4\textwidth]{../images/rgl}




\newpage


\section{Linear Combinations}

\begin{displaymath}
z_{1i} = \alpha_{11} x_{1i} + \alpha_{12} x_{2i} + \ldots + \alpha_{1p} x_{pi}
\end{displaymath}
\begin{displaymath}
z_{2i} = \alpha_{21} x_{1i} + \alpha_{22} x_{2i} + \ldots + \alpha_{2p} x_{pi}
\end{displaymath}
\begin{displaymath}
\vdots = \vdots
\end{displaymath}
\begin{displaymath}
z_{pi} = \alpha_{p1} x_{1i} + \alpha_{p2} x_{2i} + \ldots + \alpha_{pp} x_{pi}
\end{displaymath}

Many techniques are concerned with finding out useful linear combinations for a given task!

\newpage


\section{Lab. work}


Do also start thinking about linear combinations:

\begin{itemize}
\item Simulate some multivariate normal data
  \begin{itemize}
  \item \texttt{library(MASS)}
  \item \texttt{covmat <- matrix(c(1,0.7,0.7, 0.7,1, 0.7, 0.7, 0.7, 1),3,3)}
  \item \texttt{X <- mvrnorm(1000, c(0,0,0), covmat )}
  \end{itemize}
\item Check univariate normality for each margin e.g 
  \begin{itemize}
  \item \texttt{hist(X[,2])}
  \item \texttt{qqnorm(X[,2])}
  \item \texttt{qqline(X[,2], col = ``red'')}
  \end{itemize}
\item Check univariate normality of a few linear combinations e.g. 
  \begin{itemize}
  \item \texttt{Z <- 2 * X[,1] + 0.2 * X[,2] + 0.15 * X[,3]}
  \end{itemize}
\end{itemize}

We will consider multivariate normality in a couple of weeks.


Also this week, get comfortable with multivariate exploratory data analysis

\begin{itemize}
\item Some of the slides above, the defaults were modified!
\item \texttt{library(gclus)}, \texttt{?cpairs}, \texttt{?cparcoord} 
\item \texttt{data(wines)}, also in \texttt{library(Flury)} look at \texttt{data(turtles)} and \texttt{data(flea.beetles)} 
\end{itemize}

Save your code!   It will save you a lot of work over the next term!


\newpage

\section{Summary}

\begin{itemize}
\item You should now understand what material we will cover in this module, and be able to find suitable books to support your learning
\item You should have a basic understanding of the notation.   By next week make sure you understand the idea of a linear combination
\item You should be comfortable carrying out an exploratory data analysis - save the bits of code you need to be able to create the graphs you may be interested in\end{itemize}

\NaviBarOff


 \end{document} 