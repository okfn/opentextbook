%% This file generated automatically.
%% Do not edit by hand.
%%
\flashcard{%
\begin{definition}{M}{Matrix}{matrix}
An $m\times n$ \define{matrix} is a rectangular layout of numbers from $\complex{\null}$ having $m$ rows and $n$ columns.  We will use upper-case Latin letters from the start of the alphabet ($A,\,B,\,C,\dotsc$) to denote matrices and squared-off brackets to delimit the layout.  Many use large parentheses instead of brackets --- the distinction is not important.  Rows of a matrix will be referenced starting at the top and working down (i.e.\ row 1 is at the top) and columns will be referenced starting from the left (i.e.\ column 1 is at the left).  For a matrix $A$, the notation $\matrixentry{A}{ij}$ will refer to the complex number in row $i$ and column $j$ of $A$.
\denote{M}{Matrix}{$A$}{matrix}
\denote{MC}{Matrix Components}{$\matrixentry{A}{ij}$}{matrix components}
\end{definition}
}
%
\flashcard{%
\begin{definition}{CV}{Column Vector}{vector!column}
A \define{column vector} of \define{size} $m$ is an ordered list of $m$ numbers, which is written in order vertically, starting at the top and proceeding to the bottom.  At times, we will refer to a column vector as simply a \define{vector}.  Column vectors will be written in bold, usually with lower case Latin letter from the end of the alphabet such as $\vect{u}$, $\vect{v}$, $\vect{w}$, $\vect{x}$, $\vect{y}$, $\vect{z}$.  Some books like to write vectors with arrows, such as $\vec{u}$.  Writing by hand, some like to put arrows on top of the symbol, or a tilde underneath the symbol, as in $\underset{\sim}{\textstyle u}$.  To refer to the \define{entry} or \define{component} that is number $i$ in the list that is the vector $\vect{v}$ we write $\vectorentry{\vect{v}}{i}$.
\denote{CV}{Column Vector}{$\vect{v}$}{vector}
\denote{CVC}{Column Vector Components}{$\vectorentry{\vect{v}}{i}$}{vector component}
\end{definition}
}
%
\flashcard{%
\begin{definition}{ZCV}{Zero Column Vector}{zero column vector}
The \define{zero vector} of size $m$ is the column vector of size $m$ where each entry is the number zero,
%
\begin{align*}
\zerovector=
\colvector{0\\0\\0\\\vdots\\0}
\end{align*}
%
or defined much more compactly, $\vectorentry{\zerovector}{i}=0$ for $1\leq i\leq m$.
\denote{ZCV}{Zero Column Vector}{$\zerovector$}{zero column vector}
\end{definition}
}
%
\flashcard{%
\begin{definition}{CM}{Coefficient Matrix}{coefficient matrix}
For a system of linear equations, 
\begin{align*}
a_{11}x_1+a_{12}x_2+a_{13}x_3+\dots+a_{1n}x_n&=b_1\\
a_{21}x_1+a_{22}x_2+a_{23}x_3+\dots+a_{2n}x_n&=b_2\\
a_{31}x_1+a_{32}x_2+a_{33}x_3+\dots+a_{3n}x_n&=b_3\\
\vdots&\\
a_{m1}x_1+a_{m2}x_2+a_{m3}x_3+\dots+a_{mn}x_n&=b_m
\end{align*}
the \define{coefficient matrix} is the $m\times n$ matrix
\begin{equation*}
A=
\begin{bmatrix}
a_{11}&a_{12}&a_{13}&\dots&a_{1n}\\
a_{21}&a_{22}&a_{23}&\dots&a_{2n}\\
a_{31}&a_{32}&a_{33}&\dots&a_{3n}\\
\vdots&\\
a_{m1}&a_{m2}&a_{m3}&\dots&a_{mn}\\
\end{bmatrix}
\end{equation*}
\end{definition}
}
%
\flashcard{%
\begin{definition}{VOC}{Vector of Constants}{vector!of constants}
For a system of linear equations, 
\begin{align*}
a_{11}x_1+a_{12}x_2+a_{13}x_3+\dots+a_{1n}x_n&=b_1\\
a_{21}x_1+a_{22}x_2+a_{23}x_3+\dots+a_{2n}x_n&=b_2\\
a_{31}x_1+a_{32}x_2+a_{33}x_3+\dots+a_{3n}x_n&=b_3\\
\vdots&\\
a_{m1}x_1+a_{m2}x_2+a_{m3}x_3+\dots+a_{mn}x_n&=b_m
\end{align*}
the \define{vector of constants} is the column vector of size $m$ 
\begin{equation*}
\vect{b}=
\begin{bmatrix}
b_1\\
b_2\\
b_3\\
\vdots\\
b_m\\
\end{bmatrix}
\end{equation*}
\end{definition}
}
%
\flashcard{%
\begin{definition}{SOLV}{Solution Vector}{solution vector}
For a system of linear equations, 
\begin{align*}
a_{11}x_1+a_{12}x_2+a_{13}x_3+\dots+a_{1n}x_n&=b_1\\
a_{21}x_1+a_{22}x_2+a_{23}x_3+\dots+a_{2n}x_n&=b_2\\
a_{31}x_1+a_{32}x_2+a_{33}x_3+\dots+a_{3n}x_n&=b_3\\
\vdots&\\
a_{m1}x_1+a_{m2}x_2+a_{m3}x_3+\dots+a_{mn}x_n&=b_m
\end{align*}
the \define{solution vector} is the column vector of size $n$
\begin{equation*}
\vect{x}=
\begin{bmatrix}
x_1\\
x_2\\
x_3\\
\vdots\\
x_n\\
\end{bmatrix}
\end{equation*}
\end{definition}
}
%
\flashcard{%
\begin{definition}{LSMR}{Matrix Representation of a Linear System}{linear system!matrix representation}
If $A$ is the coefficient matrix of a system of linear equations and $\vect{b}$ is the vector of constants, then we will write $\linearsystem{A}{\vect{b}}$ as a shorthand expression for the  system of linear equations, which we will refer to as the \define{matrix representation} of the linear system.
\denote{LSMR}{ Matrix Representation of a Linear System}{$\linearsystem{A}{\vect{b}}$}{linear system!matrix representation}
\end{definition}
}
%
\flashcard{%
\begin{definition}{AM}{Augmented Matrix}{matrix!augmented}
Suppose we have a system of $m$ equations in $n$ variables, with coefficient matrix $A$ and vector of constants $\vect{b}$.  Then the \define{augmented matrix} of the system of equations is the $m\times(n+1)$ matrix whose first $n$ columns are the columns of $A$ and whose last column (number $n+1$) is the column vector $\vect{b}$.  This matrix will be written as $\augmented{A}{\vect{b}}$.
\denote{AM}{Augmented Matrix}{$\augmented{A}{\vect{b}}$}{augmented matrix}
\end{definition}
}
%
\flashcard{%
\begin{definition}{RO}{Row Operations}{row operations}
The following three operations will transform an $m\times n$ matrix into a different matrix of the same size, and each is known as a \define{row operation}.
%
\begin{enumerate}
\item Swap the locations of two rows.
\item Multiply each entry of a single row by a nonzero quantity.
\item Multiply each entry of one row by some quantity, and add these values to the entries in the same columns of a second row.  Leave the first row the same after this operation, but replace the second row by the new values.
\end{enumerate}
We will use a symbolic shorthand to describe these row operations:
\begin{enumerate}
\item $\rowopswap{i}{j}$:\quad Swap the location of rows $i$ and $j$.
\item $\rowopmult{\alpha}{i}$:\quad Multiply row $i$ by the nonzero scalar $\alpha$.
\item $\rowopadd{\alpha}{i}{j}$:\quad Multiply row $i$ by the scalar $\alpha$ and add to row $j$.
\end{enumerate}
\denote{RO}{Row Operations}{$\rowopswap{i}{j}$, $\rowopmult{\alpha}{i}$, $\rowopadd{\alpha}{i}{j}$}{row operations}
\end{definition}
}
%
\flashcard{%
\begin{definition}{REM}{Row-Equivalent Matrices}{row-equivalent matrices}
Two matrices, $A$ and $B$, are \define{row-equivalent} if one can be obtained from the other by a sequence of row operations.
\end{definition}
}
%
\flashcard{%
\begin{theorem}{REMES}{Row-Equivalent Matrices represent Equivalent Systems}{row-equivalent matrices}
Suppose that $A$ and $B$ are row-equivalent augmented matrices.  Then the systems of linear equations that they represent are equivalent systems.
\end{theorem}
}
%
\flashcard{%
\begin{definition}{RREF}{Reduced Row-Echelon Form}{reduced row-echelon form}
A matrix is in \define{reduced row-echelon form} if it meets all of the following conditions:
\begin{enumerate}
\item A row where every entry is zero lies below any row that contains a nonzero entry.
\item The leftmost nonzero entry of a row is equal to 1.
\item The leftmost nonzero entry of a row is the only nonzero entry in its column.
\item Consider any two different leftmost nonzero entries, one located in row $i$, column $j$ and the other located in row $s$, column $t$.  If $s>i$, then $t>j$.
\end{enumerate}
A row of only zero entries will be called a \define{zero row} and the leftmost nonzero entry of a nonzero row will be called a \define{leading 1}.  The number of nonzero rows will be denoted by $r$.\par
%
A column containing a leading 1 will be called a \define{pivot column}.  The set of column indices for all of the pivot columns will be denoted by $D=\set{d_1,\,d_2,\,d_3,\,\ldots,\,d_r}$ where $d_1<d_2<d_3<\cdots<d_r$, while the columns that are not pivot colums will be denoted as $F=\set{f_1,\,f_2,\,f_3,\,\ldots,\,f_{n-r}}$ where $f_1<f_2<f_3<\cdots<f_{n-r}$.
\denote{RREFA}{Reduced Row-Echelon Form Analysis}{$r$, $D$, $F$}{reduced row-echelon form!analysis}
\end{definition}
}
%
\flashcard{%
\begin{theorem}{REMEF}{Row-Equivalent Matrix in Echelon Form}{row-reduced matrices}
Suppose $A$ is a matrix.  Then there is a matrix $B$ so that
\begin{enumerate}
\item $A$ and $B$ are row-equivalent.
\item $B$ is in reduced row-echelon form.
\end{enumerate}
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{RREFU}{Reduced Row-Echelon Form is Unique}{reduced row-echelon form!unique}
Suppose that $A$ is an $m\times n$ matrix and that $B$ and $C$ are $m\times n$ matrices that are row-equivalent to $A$ and in reduced row-echelon form.  Then $B=C$.
\end{theorem}
}
%
\flashcard{%
\begin{definition}{RR}{Row-Reducing}{row-reduce!the verb}
To \define{row-reduce} the matrix $A$ means to apply row operations to $A$ and arrive at a row-equivalent matrix $B$ in reduced row-echelon form.
\end{definition}
}
%
