%% This file generated automatically.
%% Do not edit by hand.
%%
\flashcard{%
\begin{definition}{LT}{Linear Transformation}{linear transformation}
A \define{linear transformation}, $\ltdefn{T}{U}{V}$, is a function that carries elements of the vector space $U$ (called the \define{domain}) to the vector space $V$ (called the \define{codomain}), and which has two additional properties
%
\begin{enumerate}
\item $\lt{T}{\vect{u}_1+\vect{u}_2}=\lt{T}{\vect{u}_1}+\lt{T}{\vect{u}_2}$ for all $\vect{u}_1,\,\vect{u}_2\in U$
\item $\lt{T}{\alpha\vect{u}}=\alpha\lt{T}{\vect{u}}$ for all $\vect{u}\in U$ and all $\alpha\in\complex{\null}$
\end{enumerate}
%
\denote{LT}{Linear Transformation}{$\ltdefn{T}{U}{V}$}{linear transformation}
\end{definition}
}
%
\flashcard{%
\begin{theorem}{LTTZZ}{Linear Transformations Take Zero to Zero}{linear transformation!zero vector}
Suppose $\ltdefn{T}{U}{V}$ is a linear transformation.  Then $\lt{T}{\zerovector}=\zerovector$.
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{MBLT}{Matrices Build Linear Transformations}{linear transformations!from matrices}
Suppose that $A$ is an $m\times n$ matrix.  Define a function $\ltdefn{T}{\complex{n}}{\complex{m}}$ by $\lt{T}{\vect{x}}=A\vect{x}$.  Then $T$ is a linear transformation.
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{MLTCV}{Matrix of a Linear Transformation, Column Vectors}{matrix!of a linear transformation}
\index{linear transformation!matrix of}
Suppose that $\ltdefn{T}{\complex{n}}{\complex{m}}$ is a linear transformation.  Then there is an $m\times n$ matrix $A$ such that $\lt{T}{\vect{x}}=A\vect{x}$.
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{LTLC}{Linear Transformations and Linear Combinations}{linear transformation!linear combination}
\index{linear combination!linear transformation}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $\vectorlist{u}{t}$ are vectors from $U$ and $\scalarlist{a}{t}$ are scalars from $\complex{\null}$.  Then
%
\begin{equation*}
\lt{T}{\lincombo{a}{u}{t}}
=
a_1\lt{T}{\vect{u}_1}+
a_2\lt{T}{\vect{u}_2}+
a_3\lt{T}{\vect{u}_3}+\cdots+
a_t\lt{T}{\vect{u}_t}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{LTDB}{Linear Transformation Defined on a Basis}{linear transformation!defined on a basis}
Suppose $B=\set{\vectorlist{u}{n}}$ is a basis for the vector space $U$ and $\vectorlist{v}{n}$ is a list of vectors from the vector space $V$ (which are not necessarily distinct).   Then there is a unique linear transformation, $\ltdefn{T}{U}{V}$, such that $\lt{T}{\vect{u}_i}=\vect{v}_i$, $1\leq i\leq n$.
%
\end{theorem}
}
%
\flashcard{%
\begin{definition}{PI}{Pre-Image}{pre-image}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation.  For each $\vect{v}$, define the \define{pre-image} of $\vect{v}$ to be the subset of $U$ given by
%
\begin{equation*}
\preimage{T}{\vect{v}}=\setparts{\vect{u}\in U}{\lt{T}{\vect{u}}=\vect{v}}
\end{equation*}
%
\end{definition}
}
%
\flashcard{%
\begin{definition}{LTA}{Linear Transformation Addition}{linear transformation!addition}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{U}{V}$ are two linear transformations with the same domain and codomain.  Then their \define{sum} is the function $\ltdefn{T+S}{U}{V}$ whose outputs are defined by
%
\begin{equation*}
\lt{(T+S)}{\vect{u}}=\lt{T}{\vect{u}}+\lt{S}{\vect{u}}
\end{equation*}
%
\end{definition}
}
%
\flashcard{%
\begin{theorem}{SLTLT}{Sum of Linear Transformations is a Linear Transformation}{linear transformation!addition}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{U}{V}$ are two linear transformations with the same domain and codomain.  Then $\ltdefn{T+S}{U}{V}$ is a linear transformation.
\end{theorem}
}
%
\flashcard{%
\begin{definition}{LTSM}{Linear Transformation Scalar Multiplication}{linear transformation!scalar multiplication}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation and $\alpha\in\complex{\null}$.  Then the \define{scalar multiple} is the function $\ltdefn{\alpha T}{U}{V}$ whose outputs are defined by
%
\begin{equation*}
\lt{(\alpha T)}{\vect{u}}=\alpha\lt{T}{\vect{u}}
\end{equation*}
%
\end{definition}
}
%
\flashcard{%
\begin{theorem}{MLTLT}{Multiple of a Linear Transformation is a Linear Transformation}{linear transformation!addition}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation and $\alpha\in\complex{\null}$.  Then $\ltdefn{(\alpha T)}{U}{V}$ is a linear transformation.
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{VSLT}{Vector Space of Linear Transformations}{vector space!linear transformations}
\index{linear transformation!vector space of}
Suppose that $U$ and $V$ are vector spaces.  Then the set of all linear transformations from $U$ to $V$, $\vslt{U}{V}$ is a vector space when the operations are those given in \acronymref{definition}{LTA} and \acronymref{definition}{LTSM}.
\end{theorem}
}
%
\flashcard{%
\begin{definition}{LTC}{Linear Transformation Composition}{linear transformation!composition}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{V}{W}$ are linear transformations.  Then the \define{composition} of $S$ and $T$ is the function $\ltdefn{(\compose{S}{T})}{U}{W}$ whose outputs are defined by
%
\begin{equation*}
\lt{(\compose{S}{T})}{\vect{u}}=\lt{S}{\lt{T}{\vect{u}}}
\end{equation*}
%
\end{definition}
}
%
\flashcard{%
\begin{theorem}{CLTLT}{Composition of Linear Transformations is a Linear Transformation}{linear transformation!composition}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{V}{W}$ are linear transformations.  Then $\ltdefn{(\compose{S}{T})}{U}{W}$ is a linear transformation.
\end{theorem}
}
%
