%% This file generated automatically.
%% Do not edit by hand.
%%
\flashcard{%
\begin{definition}{MR}{Matrix Representation}{matrix representation}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $B=\set{\vectorlist{u}{n}}$ is a basis for $U$ of size $n$, and $C$ is a basis for $V$ of size $m$.  Then the \define{matrix representation} of $T$ relative to $B$ and $C$ is the $m\times n$ matrix,
%
\begin{equation*}
\matrixrep{T}{B}{C}=\left[
\left.\vectrep{C}{\lt{T}{\vect{u}_1}}\right|
\left.\vectrep{C}{\lt{T}{\vect{u}_2}}\right|
\left.\vectrep{C}{\lt{T}{\vect{u}_3}}\right|
\ldots
\left|\vectrep{C}{\lt{T}{\vect{u}_n}}\right.
\right]
\end{equation*}
%
\denote{MR}{Matrix Representation}{$\matrixrep{T}{B}{C}$}{matrix representation}
%
\end{definition}
}
%
\flashcard{%
\begin{theorem}{FTMR}{Fundamental Theorem of Matrix Representation}{matrix representation}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $B$ is a basis for $U$, $C$ is a basis for $V$ and $\matrixrep{T}{B}{C}$ is the matrix representation of $T$ relative to $B$ and $C$.  Then, for any $\vect{u}\in U$,
%
\begin{equation*}
\vectrep{C}{\lt{T}{\vect{u}}}=\matrixrep{T}{B}{C}\left(\vectrep{B}{\vect{u}}\right)
\end{equation*}
%
or equivalently
%
\begin{equation*}
\lt{T}{\vect{u}}=\vectrepinv{C}{\matrixrep{T}{B}{C}\left(\vectrep{B}{\vect{u}}\right)}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{MRSLT}{Matrix Representation of a Sum of Linear Transformations}{matrix representation!sum of linear transformations}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{U}{V}$ are linear transformations, $B$ is a basis of $U$ and $C$ is a basis of $V$.  Then
%
\begin{equation*}
\matrixrep{T+S}{B}{C}=\matrixrep{T}{B}{C}+\matrixrep{S}{B}{C}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{MRMLT}{Matrix Representation of a Multiple of a Linear Transformation}{matrix representation!multiple of a linear transformation}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $\alpha\in\complex{\null}$, $B$ is a basis of $U$ and $C$ is a basis of $V$.  Then
%
\begin{equation*}
\matrixrep{\alpha T}{B}{C}=\alpha\matrixrep{T}{B}{C}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{MRCLT}{Matrix Representation of a Composition of Linear Transformations}{matrix representation!composition of linear transformations}
Suppose that $\ltdefn{T}{U}{V}$ and $\ltdefn{S}{V}{W}$ are linear transformations, $B$ is a basis of $U$, $C$ is a basis of $V$, and $D$ is a basis of $W$.  Then
%
\begin{equation*}
\matrixrep{\compose{S}{T}}{B}{D}=\matrixrep{S}{C}{D}\matrixrep{T}{B}{C}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{KNSI}{Kernel and Null Space Isomorphism}{kernel!isomorphic to null space}
\index{null space!isomorphic to kernel}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $B$ is a basis for $U$ of size $n$, and $C$ is a basis for $V$.  Then the kernel of $T$ is isomorphic to the null space of $\matrixrep{T}{B}{C}$,
%
\begin{equation*}
\krn{T}\isomorphic\nsp{\matrixrep{T}{B}{C}}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{RCSI}{Range and Column Space Isomorphism}{range!isomorphic to column space}
\index{column space!isomorphic to range}
Suppose that $\ltdefn{T}{U}{V}$ is a linear transformation, $B$ is a basis for $U$ of size $n$, and $C$ is a basis for $V$ of size $m$.  Then the range of $T$ is isomorphic to the column space of $\matrixrep{T}{B}{C}$,
%
\begin{equation*}
\rng{T}\isomorphic\csp{\matrixrep{T}{B}{C}}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{IMR}{Invertible Matrix Representations}{matrix representation!invertible}
Suppose that $\ltdefn{T}{U}{V}$ is an invertible linear transformation, $B$ is a basis for $U$ and $C$ is a basis for $V$. Then the matrix representation of $T$ relative to $B$ and $C$, $\matrixrep{T}{B}{C}$ is an invertible matrix, and 
%
\begin{equation*}
\matrixrep{\ltinverse{T}}{C}{B}=\inverse{\left(\matrixrep{T}{B}{C}\right)}
\end{equation*}
%
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{IMILT}{Invertible Matrices, Invertible Linear Transformation}{invertible linear transformation!defined by invertible matrix}
Suppose that $A$ is a square matrix of size $n$ and $\ltdefn{T}{\complex{n}}{\complex{n}}$ is the linear transformation defined by $\lt{T}{\vect{x}}=A\vect{x}$.  Then $A$ is invertible matrix if and only if $T$ is an invertible linear transformation.
\end{theorem}
}
%
\flashcard{%
\begin{theorem}{NME9}{Nonsingular Matrix Equivalences, Round 9}{nonsingular matrix!equivalences}
Suppose that $A$ is a square matrix of size $n$.  The following are equivalent.
%
\begin{enumerate}
\item $A$ is nonsingular.
\item $A$ row-reduces to the identity matrix.
\item The null space of $A$ contains only the zero vector, $\nsp{A}=\set{\zerovector}$.
\item The linear system $\linearsystem{A}{\vect{b}}$ has a unique solution for every possible choice of $\vect{b}$.
\item The columns of $A$ are a linearly independent set.
\item $A$ is invertible.
\item The column space of $A$ is $\complex{n}$, $\csp{A}=\complex{n}$.
\item The columns of $A$ are a basis for $\complex{n}$.
\item The rank of $A$ is $n$, $\rank{A}=n$.
\item The nullity of $A$ is zero, $\nullity{A}=0$.
\item The determinant of $A$ is nonzero, $\detname{A}\neq 0$.
\item $\lambda=0$ is not an eigenvalue of $A$.
\item The linear transformation $\ltdefn{T}{\complex{n}}{\complex{n}}$ defined by $\lt{T}{\vect{x}}=A\vect{x}$ is invertible.
\end{enumerate}
\end{theorem}
}
%
