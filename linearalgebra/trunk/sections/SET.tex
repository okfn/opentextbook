%%%%(c)
%%%%(c)  This file is a portion of the source for the textbook
%%%%(c)
%%%%(c)    A First Course in Linear Algebra
%%%%(c)    Copyright 2004 by Robert A. Beezer
%%%%(c)
%%%%(c)  See the file COPYING.txt for copying conditions
%%%%(c)
%%%%(c)
%%%%%%%%%%%
%%
%%  Section SET
%%  Sets
%%
%%%%%%%%%%%
%
\begin{definition}{SET}{Set}{set}
A \define{set} is an unordered collection of objects.  If $S$ is a set and $x$ is an object that is in the set $S$, we write $x\in S$.  If $x$ is not in $S$, then we write $x\not\in S$.  We refer to the objects in a set as its \define{elements}.
\denote{SETM}{Set Membership}{$x\in S$}{set!membership}
\end{definition}
%
Hard to get much more basic than that.   Notice that the objects in a set can be {\em anything}, and there is no notion of order among the elements of the set.  A set can be finite as well as infinite.  A set can contain other sets as its objects.  At a primitive level, a set is just a way to break up some class of objects into two groupings:  those objects in the set, and those objects not in the set.
%
\begin{example}{SETM}{Set membership}{set!membership}
From the set of all possible symbols, construct the following set of three symbols,
%
\begin{align*}
S&=\set{\blacksquare,\,\blacklozenge,\,\bigstar}
\end{align*}
%
Then the statement $\blacksquare\in S$ is true, while the statement $\blacktriangle\in S$ is false.  However, then the statement $\blacktriangle\not\in S$ is true.
\end{example}
%
A portion of a set is known as a subset.  Notice how the following definition uses an implication (if whenever\dots then\dots).  Note too how the definition of a subset relies on the definition of a set through the idea of set membership.
%
\begin{definition}{SSET}{Subset}{subset}
\index{set!subset}
If $S$ and $T$ are two sets, then $S$ is a subset of $T$, written $S\subseteq T$ if whenever $x\in S$ then $x\in T$.
\denote{SSET}{Subset}{$S\subseteq T$}{subset}
\end{definition}
%
If we want to disallow the possibility that $S$ is the same as $T$, we use the notation $S\subset T$ and we say that $S$ is a \define{proper subset} of $T$.   We'll do an example, but first we'll define a special set.
%
%
\begin{definition}{ES}{Empty Set}{set!empty}
\index{empty set}
The empty set is the set with no elements.  Its is denoted by $\emptyset$.
\denote{ES}{Empty Set}{$\emptyset$}{empty set}
\end{definition}
%
%
\begin{example}{SSET}{Subset}{indesxstring}
If $S=\set{\blacksquare,\,\blacklozenge,\,\bigstar}$, $T=\set{\bigstar,\,\blacklozenge}$, $R=\set{\blacktriangle,\,\bigstar}$, then
%
\begin{align*}
%
T&\subseteq S
&
R&\not\subseteq T
&
\emptyset&\subseteq S
\\
T&\subset S
&
S&\subseteq S
&
S&\not\subset S
%
\end{align*}
%
\end{example}
%
What does it mean for two sets to be equal?  They must be the same.  Well, that explanation is not really too helpful, is it?  How about:  If $A\subseteq B$ and $B\subseteq A$, then $A$ equals $B$.  This gives us something to work with, if $A$ is a subset of $B$, and {\em vice versa}, then they must really be the same set.  We will now make the symbol ``$=$'' do double-duty and extend its use to statements like $A=B$, where $A$ and $B$ are sets.  Here's the definition, which we will reference often.
%
\begin{definition}{SE}{Set Equality}{set!equality}
Two sets, $S$ and $T$, are equal, if $S\subseteq T$ and $T\subseteq S$.  In this case, we write $S=T$.
\denote{SE}{Set Equality}{$S=T$}{set!equality}
\end{definition}
%
Sets are typically written inside of braces, as $\set{\ }$, as we have seen above.  However, when sets have more than a few elements, a description will typically have two components.  The first is a description of the general type of objects contained in a set, while the second is some sort of restriction on the properties the objects have.  Every object in the set must be of the type described in the first part and it must satisfy the restrictions in the second part.  Conversely, any object of the proper type for the first part, that also meets the conditions of the second part, will be in the set.  These two parts are set off from each other somehow, often with a vertical bar ($\vert$) or a colon (:).\par
%
I like to think of sets as clubs.  The first part is some description of the type of people who {\em might} belong to the club, the basic objects.  For example, a bicycle club would describe its members as being people who like to ride bicycles.  The second part is like a membership committee, it restricts the people who are allowed in the club.  Continuing with our bicycle club analogy, we might decide to limit ourselves to ``serious'' riders and only have members who can document having ridden 100 kilometers or more in a single day at least one time.\par
%
The restrictions on membership can migrate around some between the first and second part, and there may be several ways to describe the same set of objects.  Here's a more mathematical example, employing the set of all integers, ${\mathbb Z}$, to describe the set of even integers.
%
\begin{align*}
E
&=\setparts{x\in{\mathbb Z}}{x\text{ is an even number}}\\
&=\setparts{x\in{\mathbb Z}}{2\text{ divides }x\text{ evenly}}\\
&=\setparts{2k}{k\in{\mathbb Z}}
\end{align*}
%
Notice how this set tells us that its objects are integer numbers (not, say, matrices or functions, for example) and just those that are even.  So we can write that $10\in E$, while $17\not\in E$ once we check the membership criteria.  We also recognize the question
%
\begin{align*}
\begin{bmatrix}
1&-3&5\\
2&0&3
\end{bmatrix}
\in E\text{?}
\end{align*}
%
as being simply ridiculous.\par
%
\subsect{SC}{Set Cardinality}
%
On occasion, we will be interested in the number of elements in a finite set.  Here's the definition and the associated notation.
%
\begin{definition}{C}{Cardinality}{set!cardinality}
\index{set!size}
Suppose $S$ is a finite set.  Then the number of elements in $S$ is called the \define{cardinality} or \define{size} of $S$, and is denoted $\card{S}$.
%
\denote{C}{Cardinality}{$\card{S}$}{set!cardinality}
\end{definition}
%
\begin{example}{CS}{Cardinality and Size}{set!cardinality}
If $S=\set{\blacklozenge,\,\bigstar,\,\blacksquare}$, then $\card{S}=3$.
\end{example}
%
\subsect{SO}{Set Operations}
%
In this subsection we define and illustrate the three most common basic ways to manipulate sets to create other sets.  Since much of linear algebra is about sets, we will use these often.
%
\begin{definition}{SU}{Set Union}{set!union}
Suppose $S$ and $T$ are sets.  Then the \define{union} of $S$ and $T$, denoted $S\cup T$, is the set whose elements are those that are elements of $S$ or of $T$, or both.  More formally,
%
\begin{align*}
x\in S\cup T\text{ if and only if }x\in S\text{ or }x\in T
\end{align*}
%
\denote{SU}{Set Union}{$S\cup T$}{set!union}
\end{definition}
%
Notice that the use of the word ``or'' in this definition is meant to be non-exclusive.  That is, it allows for $x$ to be an element of both $S$ and $T$ and still qualify for membership in $S\cup T$.
%
\begin{example}{SU}{Set union}{set!union}
If $S=\set{\blacklozenge,\,\bigstar,\,\blacksquare}$ and $T=\set{\blacklozenge,\,\bigstar,\,\blacktriangle}$ then $S\cup T=\set{\blacklozenge,\,\bigstar,\,\blacksquare,\,\blacktriangle}$.
\end{example}
%
\begin{definition}{SI}{Set Intersection}{set!intersection}
Suppose $S$ and $T$ are sets.  Then the \define{intersection} of $S$ and $T$, denoted $S\cap T$, is the set whose elements are only those that are elements of $S$ and of $T$.  More formally,
%
\begin{align*}
x\in S\cap T\text{ if and only if }x\in S\text{ and }x\in T
\end{align*}
%
\denote{SI}{Set Intersection}{$S\cap T$}{set!intersection}
\end{definition}
%
%
\begin{example}{SI}{Set intersection}{set!intersection}
If $S=\set{\blacklozenge,\,\bigstar,\,\blacksquare}$ and $T=\set{\blacklozenge,\,\bigstar,\,\blacktriangle}$ then $S\cap T=\set{\blacklozenge,\,\bigstar}$.
\end{example}
%
The union and intersection of sets are operations that begin with two sets and produce a third, new, set.  Our final operation is the set complement, which we usually think of as an operation that takes a single set and creates a second, new, set.  However, if you study the definition carefully, you will see that it needs to be computed {\em relative} to some ``universal'' set.
%
\begin{definition}{SC}{Set Complement}{set!complement}
Suppose $S$ is a set that is a subset of a universal set $U$.  Then the \define{complement} of $S$, denoted $\setcomplement{S}$, is the set whose elements are those that are elements of $U$ and not elements of $S$.  More formally,
%
\begin{align*}
x\in\setcomplement{S}\text{ if and only if }x\in U\text{ and }x\not\in S
\end{align*}
%
\denote{SC}{Set Complement}{$\setcomplement{S}$}{set!complement}
\end{definition}
%
Notice that there is nothing at all special about the universal set.  This is simply a term that suggests that $U$ contains all of the possible objects we are considering.  Often this set will be clear from the context, and we won't think much about it, nor reference it in our notation.  In other cases (rarely in our work in this course) the exact nature of the universal set must be made explicit, and reference to it will possibly be carried through in our choice of notation.
%
\begin{example}{SC}{Set complement}{set!complement}
If $U=\set{\blacklozenge,\,\bigstar,\,\blacksquare,\,\blacktriangle}$ and $S=\set{\blacklozenge,\,\bigstar,\,\blacksquare}$ then $\setcomplement{S}=\set{\blacktriangle}$.
\end{example}
%
There are many more natural operations that can be performed on sets, such as an exclusive-or and the symmetric difference.  Many of these can be defined in terms of the union, intersection and complement.  We will not have much need of them in this course, and so we will not give precise descriptions here in this preliminary section.\par
%
There is also an interesting variety of basic results that describe the interplay of these operations with each other.  We mention just two as an example, these are known as DeMorgan's Laws.
%
\begin{align*}
\setcomplement{\left(S\cup T\right)}&=\setcomplement{S}\cap\setcomplement{T}\\
\setcomplement{\left(S\cap T\right)}&=\setcomplement{S}\cup\setcomplement{T}\\
\end{align*}
%
Besides having an appealing symmetry, we mention these two facts, since constructing the proofs of each is a useful exercise that will require a solid understanding of all but one of the definitions presented in this section.  Give it a try.
%
%
%  End of  set.tex