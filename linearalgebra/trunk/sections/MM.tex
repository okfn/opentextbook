%%%%(c)
%%%%(c)  This file is a portion of the source for the textbook
%%%%(c)
%%%%(c)    A First Course in Linear Algebra
%%%%(c)    Copyright 2004 by Robert A. Beezer
%%%%(c)
%%%%(c)  See the file COPYING.txt for copying conditions
%%%%(c)
%%%%(c)
%%%%%%%%%%%
%%
%%  Section MM
%%  Matrix Multiplication
%%
%%%%%%%%%%%
%
We know how to add vectors and how to multiply them by scalars.  Together, these operations give us the possibility of making linear combinations.  Similarly, we know how to add matrices and how to multiply matrices by scalars.  In this section we mix all these ideas together and produce an operation known as ``matrix multiplication.''  This will lead to some results that are both surprising and central.  We begin with a definition of how to multiply a vector by a matrix.
%
\subsect{MVP}{Matrix-Vector Product}
%
We have repeatedly seen the importance of forming linear combinations of the columns of a matrix.  As one example of this, the oft-used \acronymref{theorem}{SLSLC}, said that every solution to a system of linear equations gives rise to a linear combination of the column vectors of the coefficient matrix that equals the vector of constants.  This theorem, and others, motivate the following central definition.
%
\begin{definition}{MVP}{Matrix-Vector Product}{matrix!product with vector}
\index{vector!product with matrix}
Suppose $A$ is an $m\times n$ matrix with columns $\vectorlist{A}{n}$ and $\vect{u}$ is a vector of size $n$.  Then the \define{matrix-vector product} of $A$ with $\vect{u}$ is the linear combination
%
\begin{equation*}
A\vect{u}=
\vectorentry{\vect{u}}{1}\vect{A}_1+
\vectorentry{\vect{u}}{2}\vect{A}_2+
\vectorentry{\vect{u}}{3}\vect{A}_3+
\cdots+
\vectorentry{\vect{u}}{n}\vect{A}_n
\end{equation*}
%
\denote{MVP}{Matrix-Vector Product}{A\vect{u}}{matrix-vector product}
\end{definition}
%
So, the matrix-vector product is yet another version of ``multiplication,'' at least in the sense that we have yet again overloaded juxtaposition of two symbols as our notation.  Remember your objects, an $m\times n$ matrix times a vector of size $n$ will create a vector of size $m$.  So if $A$ is rectangular, then the size of the vector changes.  With all the linear combinations we have performed so far, this computation should now seem second nature.
%
\begin{example}{MTV}{A matrix times a vector}{matrix-vector product}
%
Consider
%
\begin{align*}
A=
\begin{bmatrix}
1 & 4 & 2 & 3 & 4\\
-3 & 2 & 0 & 1 & -2\\
1 & 6 & -3 & -1 & 5
\end{bmatrix}
&&
\vect{u}=\colvector{2\\1\\-2\\3\\-1}
\end{align*}
%
Then
%
\begin{equation*}
A\vect{u}=
2\colvector{1\\-3\\1}+
1\colvector{4\\2\\6}+
(-2)\colvector{2\\0\\-3}+
3\colvector{3\\1\\-1}+
(-1)\colvector{4\\-2\\5}
=
\colvector{7\\1\\6}.
\end{equation*}
%
\end{example}
%
We can now represent systems of linear equations compactly with a matrix-vector product (\acronymref{definition}{MVP}) and column vector equality (\acronymref{definition}{CVE}).  This finally yields a very popular alternative to our unconventional $\linearsystem{A}{\vect{b}}$ notation.
%
\begin{theorem}{SLEMM}{Systems of Linear Equations as Matrix Multiplication}{matrix multiplication!systems of linear equations}
The set of solutions to the linear system $\linearsystem{A}{\vect{b}}$ equals the set of solutions for $\vect{x}$ in the vector equation $A\vect{x}=\vect{b}$.
\end{theorem}
%
\begin{proof}
This theorem says that two sets (of solutions) are equal.  So we need to show that one set of solutions is a subset of the other, and vice versa (\acronymref{definition}{SE}).  Let $\vectorlist{A}{n}$ be the columns of $A$.  Both of these set inclusions then follow from the following chain of equivalences,
%
\begin{align*}
&\vect{x}\text{ is a solution to }\linearsystem{A}{\vect{b}}\\
&\iff
\vectorentry{\vect{x}}{1}\vect{A}_1+
\vectorentry{\vect{x}}{2}\vect{A}_2+
\vectorentry{\vect{x}}{3}\vect{A}_3+
\cdots+
\vectorentry{\vect{x}}{n}\vect{A}_n
=\vect{b}&&\text{\acronymref{theorem}{SLSLC}}\\
&\iff
\vect{x}\text{ is a solution to }A\vect{x}=\vect{b}&&\text{\acronymref{definition}{MVP}}
\end{align*}
%
\end{proof}
%
\begin{example}{MNSLE}{Matrix notation for systems of linear equations}{linear systems!notation}
Consider the system of linear equations from \acronymref{example}{NSLE}.
%
\begin{align*}
2x_1+4x_2-3x_3+5x_4+x_5&=9\\
3x_1+x_2+x_4-3x_5&=0\\
-2x_1+7x_2-5x_3+2x_4+2x_5&=-3
\end{align*}
%
has coefficient matrix
%
\begin{equation*}
A=
\begin{bmatrix}
2 & 4 & -3 & 5 & 1\\
3 & 1 & 0 & 1 & -3\\
-2 & 7 & -5 & 2 & 2
\end{bmatrix}
\end{equation*}
%
and vector of constants
%
\begin{equation*}
\vect{b}=\colvector{9\\0\\-3}
\end{equation*}
%
and so will be described compactly by the vector equation $A\vect{x}=\vect{b}$.
%
\end{example}
%
The matrix-vector product is a very natural computation.  We have motivated it by its connections with systems of equations, but here is a another example.
%
\begin{example}{MBC}{Money's best cities}{best cities!money magazine}
Every year {\sl Money} magazine selects several cities in the United States as the ``best'' cities to live in, based on a wide arrary of statistics about each city.  This is an example of how the editors of {\sl Money} might arrive at a single number that consolidates the statistics about a city.  We will analyze Los Angeles, Chicago and New York City, based on four criteria:  average high temperature in July (Farenheit), number of colleges and universities in a 30-mile radius, number of toxic waste sites in the Superfund environmental clean-up program and a personal crime index based on FBI statistics (average = 100, smaller is safer).  It should be apparent how to generalize the example to a greater number of cities and a greater number of statistics.\par
%
We begin by building a table of statistics.  The rows will be labeled with the cities, and the columns with statistical categories.  These values are from {\sl Money}'s website in early 2005.\par
%
\begin{center}
\begin{tabular}{||l||c|c|c|c||}\hline
City & Temp & Colleges & Superfund & Crime\\\hline\hline
Los Angeles & 77 & 28 & 93 & 254\\\hline
Chicago & 84 & 38 & 85 & 363\\\hline
New York & 84 & 99 & 1 & 193\\\hline\hline
\end{tabular}
\end{center}
%
Conceivably these data might reside in a spreadsheet.  Now we must combine the statistics for each city.  We could accomplish this by weighting each category, scaling the values and summing them.  The sizes of the weights would depend upon the numerical size of each statistic generally, but more importantly, they would reflect the editors opinions or beliefs about which statistics were most important to their readers.  Is the crime index more important than the number of colleges and universities?  Of course, there is no right answer to this question.\par
%
Suppose the editors finally decide on the following weights to employ:  temperature, $0.23$; colleges, $0.46$; Superfund, $-0.05$; crime, $-0.20$.  Notice how negative weights are used for undesirable statistics.  Then, for example, the editors would compute for Los Angeles,
%
\begin{equation*}
(0.23)(77) + (0.46)(28) + (-0.05)(93) + (-0.20)(254) = -24.86
\end{equation*}
%
This computation might remind you of an inner product, but we will produce the computations for all of the cities as a matrix-vector product.  Write the table of raw statistics as a matrix
%
\begin{equation*}
T=
\begin{bmatrix}
77 & 28 & 93 & 254\\
84 & 38 & 85 & 363\\
84 & 99 & 1 & 193
\end{bmatrix}
\end{equation*}
%
and the weights as a vector
%
\begin{equation*}
\vect{w}=\colvector{0.23\\0.46\\-0.05\\-0.20}
\end{equation*}
%
then the matrix-vector product (\acronymref{definition}{MVP}) yields
%
\begin{equation*}
%
T\vect{w}=
(0.23)\colvector{77\\84\\84}+
(0.46)\colvector{28\\38\\99}+
(-0.05)\colvector{93\\85\\1}+
(-0.20)\colvector{254\\363\\193}
=
\colvector{-24.86\\-40.05\\26.21}
%
\end{equation*}
%
This vector contains a single number for each of the cities being studied, so the editors would rank New York best, Los Angeles next, and Chicago third.  Of course, the mayor's offices in Chicago and Los Angeles are free to counter with a different set of weights that cause their city to be ranked best.  These alternative weights would be chosen to play to each cities' strengths, and minimize their problem areas.\par
%
If a speadsheet were used to make these computations, a row of weights would be entered somewhere near the table of data and the formulas in the spreadsheet would effect a matrix-vector product.  This example is meant to illustrate how ``linear'' computations (addition, multiplication) can be organized as a matrix-vector product.\par
%
Another example would be the matrix of numerical scores on examinations and exercises for students in a class.  The rows would correspond to students and the columns to exams and assignments.  The instructor could then assign weights to the different exams and assignments, and via a matrix-vector product, compute a single score for each student.
%
\end{example}
%
Later (much later) we will need the following theorem, which is really a technical lemma (see \acronymref{technique}{LC}).  Since we are in a position to prove it now, we will.  But you can safely skip it for the moment, if you promise to come back later to study the proof when the theorem is employed.  At that point you will also be able to understand the comments in the paragraph following the proof.
%
\begin{theorem}{EMMVP}{Equal Matrices and Matrix-Vector Products}{equal matrices!via equal matrix-vector products}
Suppose that $A$ and $B$ are $m\times n$ matrices such that $A\vect{x}=B\vect{x}$ for every $\vect{x}\in\complex{n}$.  Then $A=B$.
\end{theorem}
%
\begin{proof}
We are assuming $A\vect{x}=B\vect{x}$ for all $\vect{x}\in\complex{n}$, so we can employ this equality for {\em any} choice of the vector $\vect{x}$.  However, we'll limit our use of this equality to the standard unit vectors, $\vect{e}_j$, $1\leq j\leq n$ (\acronymref{definition}{SUV}).  For all $1\leq j\leq n$, $1\leq i\leq m$,
%
\begin{align*}
\matrixentry{A}{ij}
&=
0\matrixentry{A}{i1}+
\cdots+
0\matrixentry{A}{i,j-1}+
1\matrixentry{A}{ij}+
0\matrixentry{A}{i,j+1}+
\cdots+
0\matrixentry{A}{in}\\
%
&=
\matrixentry{A}{i1}\vectorentry{\vect{e}_j}{1}+
\matrixentry{A}{i2}\vectorentry{\vect{e}_j}{2}+
\matrixentry{A}{i3}\vectorentry{\vect{e}_j}{3}+
\cdots+
\matrixentry{A}{in}\vectorentry{\vect{e}_j}{n}
&&\text{\acronymref{definition}{SUV}}\\
%
&=
\vectorentry{A\vect{e}_j}{i}&&\text{\acronymref{definition}{MVP}}\\
%
&=
\vectorentry{B\vect{e}_j}{i}&&\text{\acronymref{definition}{CVE}}\\
%
&=
\matrixentry{B}{i1}\vectorentry{\vect{e}_j}{1}+
\matrixentry{B}{i2}\vectorentry{\vect{e}_j}{2}+
\matrixentry{B}{i3}\vectorentry{\vect{e}_j}{3}+
\cdots+
\matrixentry{B}{in}\vectorentry{\vect{e}_j}{n}&&\text{\acronymref{definition}{MVP}}\\
%
&=
0\matrixentry{B}{i1}+
\cdots+
0\matrixentry{B}{i,j-1}+
1\matrixentry{B}{ij}+
0\matrixentry{B}{i,j+1}+
\cdots+
0\matrixentry{B}{in}
&&\text{\acronymref{definition}{SUV}}\\
%
&=\matrixentry{B}{ij}
%
\end{align*}
%
So by \acronymref{definition}{ME} the matrices $A$ and $B$ are equal, as desired.
%
\end{proof}
%
You might notice that the hypotheses of this theorem could be weakened (i.e.\  made less restrictive).   We could suppose the equality of the matrix-vector products for just the standard unit vectors (\acronymref{definition}{SUV}) or any other spanning set (\acronymref{definition}{TSVS}) of $\complex{n}$ (\acronymref{exercise}{LISS.T40}).  However, in practice, when we apply this theorem we will only need this weaker form.  (If we made the hypothesis less restricive, we would call the theorem stronger.)
%
\subsect{MM}{Matrix Multiplication}
%
We now define how to multiply two matrices together.  Stop for a minute and think about how you might define this new operation.\par
%
Many books would present this definition much earlier in the course.  However, we have taken great care to delay it as long as possible and to present as many ideas as practical based mostly on the notion of linear combinations.  Towards the conclusion of the course, or when you perhaps take a second course in linear algebra, you may be in a position to appreciate the reasons for this.  For now, understand that matrix multiplication is a central definition and perhaps you will appreciate its importance more by having saved it for later.
%
\begin{definition}{MM}{Matrix Multiplication}{matrix multiplication}
\index{vector!product with matrix}
Suppose $A$ is an $m\times n$ matrix and $B$ is an $n\times p$ matrix with columns $\vectorlist{B}{p}$.  Then the \define{matrix product} of $A$ with $B$ is the $m\times p$ matrix where column $i$ is the matrix-vector product $A\vect{B}_i$.  Symbolically,
%
\begin{equation*}
AB=A\matrixcolumns{B}{p}=\left[A\vect{B}_1|A\vect{B}_2|A\vect{B}_3|\ldots|A\vect{B}_p\right].
\end{equation*}
%
\end{definition}
%
\begin{example}{PTM}{Product of two matrices}{matrix!product}
Set
%
\begin{align*}
A=
\begin{bmatrix}
1 & 2 & -1 & 4 & 6\\
0 & -4 & 1 & 2 & 3\\
-5 & 1 & 2 & -3 & 4
\end{bmatrix}
&&
B=
\begin{bmatrix}
1 & 6 & 2 & 1\\
-1 & 4 & 3 & 2\\
1 & 1 & 2 & 3\\
6 & 4 & -1 & 2\\
1 & -2 & 3 & 0
\end{bmatrix} & 
\end{align*}
%
Then
%
\begin{equation*}
AB=
\left[
A\colvector{1\\-1\\1\\6\\1}
\left\lvert A\colvector{6\\4\\1\\4\\-2}\right.
\left\lvert A\colvector{2\\3\\2\\-1\\3}\right.
\left\lvert A\colvector{1\\2\\3\\2\\0}\right.
\right]
=
\begin{bmatrix}
28 & 17 & 20 & 10\\
20 & -13 & -3 & -1\\
-18 & -44 & 12 & -3
\end{bmatrix}.
\end{equation*}
%
\end{example}
%
Is this the definition of matrix multiplication you expected?  Perhaps our previous operations for matrices caused you to think that we might multiply two matrices of the {\em same} size, {\em entry-by-entry}?  Notice that our current definition uses matrices of different sizes (though the number of columns in the first must equal the number of rows in the second), and the result is of a third size.  Notice too in the previous example that we cannot even consider the product $BA$, since the sizes of the two matrices in this order aren't right.\par
But it gets weirder than that.  Many of your old ideas about ``multiplication'' won't apply to matrix multiplication, but some still will.  So make no assumptions, and don't do anything until you have a theorem that says you can.  Even if the sizes are right, matrix multiplication is not commutative --- order matters.
%
\begin{example}{MMNC}{Matrix multiplication is not commutative}{matrix multiplication!noncommutative}
Set
%
\begin{align*}
A=
\begin{bmatrix}
1 & 3\\
-1 & 2
\end{bmatrix}
&&
B=
\begin{bmatrix}
4&0\\
5&1
\end{bmatrix}.
\end{align*}
%
Then we have two square, $2\times 2$ matrices, so \acronymref{definition}{MM} allows us to multiply them in either order.  We find
%
\begin{align*}
AB=
\begin{bmatrix}
19 & 3\\
6 & 2
\end{bmatrix}
&&
BA=
\begin{bmatrix}
4 & 12\\
4 & 17
\end{bmatrix}
\end{align*}
%
and $AB\neq BA$.  Not even close.  It should not be hard for you to construct other pairs of matrices that do not commute (try a couple of $3\times 3$'s).  Can you find a pair of non-identical matrices that {\em do} commute?
%
\end{example}
%
\computenote{\boolean{hasmathematica}}
{Matrix multiplication is fundamental, so it is a natural procedure for any computational device.}
{
\ifthenelse{\boolean{hasmathematica}}{\computedevicetopic{MMA}{Mathematica}{mathematica}{MM}{Matrix Multiplication}{matrix multiplication}}{\relax}
}{
\ifthenelse{\boolean{hasmathematica}}{\quad\acronymref{computation}{MM.MMA}}{\relax}
}
%
\subsect{MMEE}{Matrix Multiplication, Entry-by-Entry}
%
While certain ``natural'' properties of multiplication don't hold, many more do.  In the next subsection, we'll state and prove the relevant theorems.  But first, we need a theorem that provides an alternate means of multiplying two matrices.  In many texts, this would be given as the {\em definition} of matrix multiplication.  We prefer to turn it around and have the following formula as a consequence of our definition.  It will prove useful for proofs of matrix equality, where we need to examine products of matrices, entry-by-entry.
%
\begin{theorem}{EMP}{Entries of Matrix Products}{matrix multiplication!entry-by-entry}
Suppose $A$ is an $m\times n$ matrix and $B=$ is an $n\times p$ matrix.  Then for $1\leq i\leq m$, $1\leq j\leq p$, the individual entries of $AB$ are given by
%
\begin{align*}
\matrixentry{AB}{ij}
&=
\matrixentry{A}{i1}\matrixentry{B}{1j}+
\matrixentry{A}{i2}\matrixentry{B}{2j}+
\matrixentry{A}{i3}\matrixentry{B}{3j}+
\cdots+
\matrixentry{A}{in}\matrixentry{B}{nj}\\
&=
\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}
\end{align*}
%
\end{theorem}
%
\begin{proof}
Denote the columns of  $A$ as the vectors $\vectorlist{A}{n}$ and the columns of  $B$ as the vectors $\vectorlist{B}{p}$.  Then for $1\leq i\leq m$, $1\leq j\leq p$,
%
\begin{align*}
\matrixentry{AB}{ij}
&=\vectorentry{A\vect{B}_j}{i}&&\text{\acronymref{definition}{MM}}\\
%
&=\vectorentry{
\vectorentry{\vect{B}_j}{1}\vect{A}_1+
\vectorentry{\vect{B}_j}{2}\vect{A}_2+
\vectorentry{\vect{B}_j}{3}\vect{A}_3+
\cdots+
\vectorentry{\vect{B}_j}{n}\vect{A}_n
}{i}&&\text{\acronymref{definition}{MVP}}\\
%
&=
\vectorentry{\vectorentry{\vect{B}_j}{1}\vect{A}_1}{i}+
\vectorentry{\vectorentry{\vect{B}_j}{2}\vect{A}_2}{i}+
\vectorentry{\vectorentry{\vect{B}_j}{3}\vect{A}_3}{i}+
\cdots+
\vectorentry{\vectorentry{\vect{B}_j}{n}\vect{A}_n}{i}
&&\text{\acronymref{definition}{CVA}}\\
%
&=
\vectorentry{\vect{B}_j}{1}\vectorentry{\vect{A}_1}{i}+
\vectorentry{\vect{B}_j}{2}\vectorentry{\vect{A}_2}{i}+
\vectorentry{\vect{B}_j}{3}\vectorentry{\vect{A}_3}{i}+
\cdots+
\vectorentry{\vect{B}_j}{n}\vectorentry{\vect{A}_n}{i}
&&\text{\acronymref{definition}{CVSM}}\\
%
&=
\matrixentry{B}{1j}\matrixentry{A}{i1}+
\matrixentry{B}{2j}\matrixentry{A}{i2}+
\matrixentry{B}{3j}\matrixentry{A}{i3}+
\cdots+
\matrixentry{B}{nj}\matrixentry{A}{in}
&&\text{\acronymref{notation}{ME}}\\
%
&=
\matrixentry{A}{i1}\matrixentry{B}{1j}+
\matrixentry{A}{i2}\matrixentry{B}{2j}+
\matrixentry{A}{i3}\matrixentry{B}{3j}+
\cdots+
\matrixentry{A}{in}\matrixentry{B}{nj}
&&\text{\acronymref{property}{CMCN}}\\
%
&=\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}
%
\end{align*}
%
\end{proof}
%
\begin{example}{PTMEE}{Product of two matrices, entry-by-entry}{matrix!product}
Consider again the two matrices from \acronymref{example}{PTM}
%
\begin{align*}
A=
\begin{bmatrix}
1 & 2 & -1 & 4 & 6\\
0 & -4 & 1 & 2 & 3\\
-5 & 1 & 2 & -3 & 4
\end{bmatrix}
&&
B=
\begin{bmatrix}
1 & 6 & 2 & 1\\
-1 & 4 & 3 & 2\\
1 & 1 & 2 & 3\\
6 & 4 & -1 & 2\\
1 & -2 & 3 & 0
\end{bmatrix} & 
\end{align*}
%
Then suppose we just wanted the entry of $AB$ in the second row, third column:
%
\begin{align*}
\matrixentry{AB}{23}
=&
\matrixentry{A}{21}\matrixentry{B}{13}+
\matrixentry{A}{22}\matrixentry{B}{23}+
\matrixentry{A}{23}\matrixentry{B}{33}+
\matrixentry{A}{24}\matrixentry{B}{43}+
\matrixentry{A}{25}\matrixentry{B}{53}\\
=&(0)(2)+(-4)(3)+(1)(2)+(2)(-1)+(3)(3)=-3
\end{align*}
%
Notice how there are 5 terms in the sum, since 5 is the common dimension of the two matrices (column count for $A$, row count for $B$).  In the conclusion of \acronymref{theorem}{EMP}, it would be the index $k$ that would run from 1 to 5 in this computation.   Here's a bit more practice.\par
%
The entry of third row, first column:
%
\begin{align*}
\matrixentry{AB}{31}
=&
\matrixentry{A}{31}\matrixentry{B}{11}+
\matrixentry{A}{32}\matrixentry{B}{21}+
\matrixentry{A}{33}\matrixentry{B}{31}+
\matrixentry{A}{34}\matrixentry{B}{41}+
\matrixentry{A}{35}\matrixentry{B}{51}\\
=&(-5)(1)+(1)(-1)+(2)(1)+(-3)(6)+(4)(1)=-18
\end{align*}
%
To get some more practice on your own, complete the computation of the other 10 entries of this product.  Construct some other pairs of matrices (of compatible sizes) and compute their product two ways.  First use \acronymref{definition}{MM}.  Since linear combinations are straightforward for you now, this should be easy to do and to do correctly.  Then do it again, using \acronymref{theorem}{EMP}.  Since this process may take some practice, use your first computation to check your work.
%
\end{example}
%
\acronymref{theorem}{EMP} is the way many people compute matrix products by hand.  It will also be very useful for the theorems we are going to prove shortly.  However, the definition (\acronymref{definition}{MM}) is frequently the most useful for its connections with deeper ideas like the null space and the upcoming column space.\par
%
\subsect{PMM}{Properties of Matrix Multiplication}
%
In this subsection, we collect properties of matrix multiplication and its interaction with 
the zero matrix (\acronymref{definition}{ZM}),
the identity matrix (\acronymref{definition}{IM}),
matrix addition (\acronymref{definition}{MA}),
scalar matrix multiplication (\acronymref{definition}{MSM}),
the inner product (\acronymref{definition}{IP}),
conjugation (\acronymref{theorem}{MMCC}),
and 
the transpose (\acronymref{definition}{TM}).  
Whew!  Here we go.  These are great proofs to practice with, so try to concoct the proofs before reading them, they'll get progressively more complicated as we go.
%
\begin{theorem}{MMZM}{Matrix Multiplication and the Zero Matrix}{matrix multiplication!zero matrix}
Suppose $A$ is an $m\times n$ matrix.  Then\\
1.\quad  $A\zeromatrix_{n\times p}=\zeromatrix_{m\times p}$\\
2.\quad $\zeromatrix_{p\times m}A=\zeromatrix_{p\times n}$
\end{theorem}
%
\begin{proof}
We'll prove (1) and leave (2) to you.  Entry-by-entry,
%
\begin{align*}
\matrixentry{A\zeromatrix_{n\times p}}{ij}
&=
\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{\zeromatrix_{n\times p}}{kj}
&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{k=1}^{n}\matrixentry{A}{ik}0
&&\text{\acronymref{definition}{ZM}}\\
&=\sum_{k=1}^{n}0=0.
\end{align*}
%
So every entry of the product is the scalar zero, i.e.\ the result is the zero matrix.
%
\end{proof}
%
\begin{theorem}{MMIM}{Matrix Multiplication and Identity Matrix}{matrix multiplication!identity matrix}
Suppose $A$ is an $m\times n$ matrix.  Then\\
1.\quad  $AI_n=A$\\
2.\quad  $I_mA=A$
\end{theorem}
%
\begin{proof}
Again, we'll prove (1) and leave (2) to you.  Entry-by-entry,
%
\begin{align*}
\matrixentry{AI_n}{ij}=&
\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{I_n}{kj}
&&\text{\acronymref{theorem}{EMP}}\\
%
&=\matrixentry{A}{ij}\matrixentry{I_n}{jj}+\sum_{\substack{k=1\\k\neq j}}^{n}\matrixentry{A}{ik}\matrixentry{I_n}{kj}
&&\text{\acronymref{property}{CACN}}\\
%
&=\matrixentry{A}{ij}(1)+\sum_{k=1, k\neq j}^{n}\matrixentry{A}{ik}(0)
&&\text{\acronymref{definition}{IM}}\\
%
&=\matrixentry{A}{ij}+\sum_{k=1, k\neq j}^{n}0\\
&=\matrixentry{A}{ij}
\end{align*}
%
So the matrices $A$ and $AI_n$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
\end{proof}
%
It is this theorem that gives the identity matrix its name.  It is a matrix that behaves with matrix multiplication like the scalar 1 does with scalar multiplication.  To multiply by the identity matrix is to have no effect on the other matrix.
%
\begin{theorem}{MMDAA}{Matrix Multiplication Distributes Across Addition}{matrix multiplication!distributivity}
Suppose $A$ is an $m\times n$ matrix and $B$ and $C$ are $n\times p$ matrices and $D$ is a $p\times s$ matrix.    Then\\
1.\quad  $A(B+C)=AB+AC$\\
2.\quad  $(B+C)D=BD+CD$
\end{theorem}
%
\begin{proof}
We'll do (1), you do (2).  Entry-by-entry,
%
\begin{align*}
\matrixentry{A(B+C)}{ij}
&=
\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B+C}{kj}&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{k=1}^{n}\matrixentry{A}{ik}(\matrixentry{B}{kj}+\matrixentry{C}{kj})&&\text{\acronymref{definition}{MA}}\\
&=\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}+\matrixentry{A}{ik}\matrixentry{C}{kj}
&&\text{\acronymref{property}{DCN}}\\
&=\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}+\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{C}{kj}&&\text{\acronymref{property}{CACN}}\\
&=\matrixentry{AB}{ij}+\matrixentry{AC}{ij}&&\text{\acronymref{theorem}{EMP}}\\
&=\matrixentry{AB+AC}{ij}&&\text{\acronymref{definition}{MA}}
\end{align*}
%
So the matrices $A(B+C)$ and $AB+AC$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
\end{proof}
%
\begin{theorem}{MMSMM}{Matrix Multiplication and Scalar Matrix Multiplication}{matrix multiplication!scalar matrix multiplication}
Suppose $A$ is an $m\times n$ matrix and $B$ is an $n\times p$ matrix.  Let $\alpha$ be a scalar.  Then $\alpha(AB)=(\alpha A)B=A(\alpha B)$.
\end{theorem}
%
\begin{proof}
These are equalities of matrices.  We'll do the first one, the second is similar and will be good practice for you.
%
\begin{align*}
\matrixentry{\alpha(AB)}{ij}
&=\alpha\matrixentry{AB}{ij}&&\text{\acronymref{definition}{MSM}}\\
&=\alpha\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{k=1}^{n}\alpha\matrixentry{A}{ik}\matrixentry{B}{kj}&&\text{\acronymref{property}{DCN}}\\
&=\sum_{k=1}^{n}\matrixentry{\alpha A}{ik}\matrixentry{B}{kj}&&\text{\acronymref{definition}{MSM}}\\
&=\matrixentry{(\alpha A)B}{ij}&&\text{\acronymref{theorem}{EMP}}
\end{align*}
%
So the matrices $\alpha(AB)$ and $(\alpha A)B$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
\end{proof}
%
\begin{theorem}{MMA}{Matrix Multiplication is Associative }{matrix multiplication!associativity}
Suppose $A$ is an $m\times n$ matrix, $B$ is an $n\times p$ matrix and $D$ is a $p\times s$ matrix.  Then  $A(BD)=(AB)D$.
\end{theorem}
%
\begin{proof}
A matrix equality, so we'll go entry-by-entry, no surprise there.
%
\begin{align*}
\matrixentry{A(BD)}{ij}
&=\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{BD}{kj}
&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{k=1}^{n}\matrixentry{A}{ik}\left(\sum_{\ell=1}^{p}\matrixentry{B}{k\ell}\matrixentry{D}{\ell j}\right)
&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{k=1}^{n}\sum_{\ell=1}^{p}\matrixentry{A}{ik}\matrixentry{B}{k\ell}\matrixentry{D}{\ell j}
&&\text{\acronymref{property}{DCN}}\\
%
\intertext{We can switch the order of the summation since these are finite sums,}
%
&=\sum_{\ell=1}^{p}\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{k\ell}\matrixentry{D}{\ell j}
&&\text{\acronymref{property}{CACN}}\\
%
\intertext{As $\matrixentry{D}{\ell j}$ does not depend on the index $k$, we can factor it out of the inner sum,}
%
&=\sum_{\ell=1}^{p}\matrixentry{D}{\ell j}\left(\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{k\ell}\right)
&&\text{\acronymref{property}{DCN}}\\
&=\sum_{\ell=1}^{p}\matrixentry{D}{\ell j}\matrixentry{AB}{i\ell}
&&\text{\acronymref{theorem}{EMP}}\\
&=\sum_{\ell=1}^{p}\matrixentry{AB}{i\ell}\matrixentry{D}{\ell j}
&&\text{\acronymref{property}{CMCN}}\\
&=\matrixentry{(AB)D}{ij}&&\text{\acronymref{theorem}{EMP}}
\end{align*}
%
So the matrices $(AB)D$ and $A(BD)$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
%
\end{proof}
%
The statement of our next theorem is technically inaccurate.  If we upgrade the vectors $\vect{u},\,\vect{v}$ to matrices with a single column, then the expression $\transpose{\vect{u}}\conjugate{\vect{v}}$ is a $1\times 1$ matrix, though we will treat this small matrix as if it was simply the scalar quantity in its lone entry.  When we apply \acronymref{theorem}{MMIP} there should not be any confusion.
%
\begin{theorem}{MMIP}{Matrix Multiplication and Inner Products}{matrix multiplication!inner product}
If we consider the vectors $\vect{u},\,\vect{v}\in\complex{m}$ as $m\times 1$ matrices then
%
\begin{equation*}
\innerproduct{\vect{u}}{\vect{v}}=\transpose{\vect{u}}\conjugate{\vect{v}}
\end{equation*}
%
\end{theorem}
%
\begin{proof}
%
\begin{align*}
\innerproduct{\vect{u}}{\vect{v}}
%
&=\sum_{k=1}^{m}\vectorentry{\vect{u}}{k}\conjugate{\vectorentry{\vect{v}}{k}}
&&\text{\acronymref{definition}{IP}}\\
%
&=\sum_{k=1}^{m}\matrixentry{\vect{u}}{k1}\conjugate{\matrixentry{\vect{v}}{k1}}
&&\text{Column vectors as matrices}\\
%
&=\sum_{k=1}^{m}\matrixentry{\transpose{\vect{u}}}{1k}\conjugate{\matrixentry{\vect{v}}{k1}}
&&\text{\acronymref{definition}{TM}}\\
%
&=\sum_{k=1}^{m}\matrixentry{\transpose{\vect{u}}}{1k}\matrixentry{\conjugate{\vect{v}}}{k1}
&&\text{\acronymref{definition}{CCCV}}\\
%
&=\matrixentry{\transpose{\vect{u}}\conjugate{\vect{v}}}{11}
&&\text{\acronymref{theorem}{EMP}}\\
\end{align*}
%
To finish we just blur the distinction between a $1\times 1$ matrix ($\transpose{\vect{u}}\conjugate{\vect{v}}$) and its lone entry.
%
\end{proof}
%
\begin{theorem}{MMCC}{Matrix Multiplication and Complex Conjugation}{matrix multiplication!complex conjugation}
Suppose $A$ is an $m\times n$ matrix and $B$ is an $n\times p$ matrix.  Then $\conjugate{AB}=\conjugate{A}\,\conjugate{B}$.
\end{theorem}
%
\begin{proof}
%
To obtain this matrix equality, we will work entry-by-entry,
%
\begin{align*}
\matrixentry{\conjugate{AB}}{ij}
%
&=\conjugate{\matrixentry{AB}{ij}}
&&\text{\acronymref{definition}{CM}}\\
%
&=\conjugate{\sum_{k=1}^{n}\matrixentry{A}{ik}\matrixentry{B}{kj}}
&&\text{\acronymref{theorem}{EMP}}\\
%
&=\sum_{k=1}^{n}\conjugate{\matrixentry{A}{ik}\matrixentry{B}{kj}}
&&\text{\acronymref{theorem}{CCRA}}\\
%
&=\sum_{k=1}^{n}\conjugate{\matrixentry{A}{ik}}\,\,\conjugate{\matrixentry{B}{kj}}
&&\text{\acronymref{theorem}{CCRM}}\\
%
&=\sum_{k=1}^{n}\matrixentry{\conjugate{A}}{ik}\matrixentry{\conjugate{B}}{kj}
&&\text{\acronymref{definition}{CCM}}\\
%
&=\matrixentry{\conjugate{A}\,\conjugate{B}}{ij}
&&\text{\acronymref{theorem}{EMP}}
%
\end{align*}
%
So the matrices $\conjugate{AB}$ and $\conjugate{A}\,\conjugate{B}$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
%
\end{proof}
%
Another theorem in this style, and its a good one.  If you've been practicing with the previous proofs you should be able to do this one yourself.
%
\begin{theorem}{MMT}{Matrix Multiplication and Transposes}{matrix multiplication!transposes}
Suppose $A$ is an $m\times n$ matrix and $B$ is an $n\times p$ matrix.  Then $\transpose{(AB)}=\transpose{B}\transpose{A}$.
\end{theorem}
%
\begin{proof}
This theorem may be surprising but if we check the sizes of the matrices involved, then maybe it will not seem so far-fetched.  First, $AB$ has size $m\times p$, so its transpose has size $p\times m$.  The product of $\transpose{B}$ with $\transpose{A}$ is a $p\times n$ matrix times an $n\times m$ matrix, also resulting in a $p\times m$ matrix.  So at least our objects are compatible for equality (and would not be, in general, if we didn't reverse the order of the matrix multiplication).\par
%
Here we go again, entry-by-entry,
%
\begin{align*}
\matrixentry{\transpose{(AB)}}{ij}=&\matrixentry{AB}{ji}
&&\text{\acronymref{definition}{TM}}\\
%
&=\sum_{k=1}^{n}\matrixentry{A}{jk}\matrixentry{B}{ki}
&&\text{\acronymref{theorem}{EMP}}\\
%
&=\sum_{k=1}^{n}\matrixentry{B}{ki}\matrixentry{A}{jk}
&&\text{\acronymref{property}{CMCN}}\\
%
&=\sum_{k=1}^{n}\matrixentry{\transpose{B}}{ik}\matrixentry{\transpose{A}}{kj}
&&\text{\acronymref{definition}{TM}}\\
%
&=\matrixentry{\transpose{B}\transpose{A}}{ij}
&&\text{\acronymref{theorem}{EMP}}
%
\end{align*}
%
So the matrices $\transpose{(AB)}$ and $\transpose{B}\transpose{A}$ are equal, entry-by-entry, and by the definition of matrix equality (\acronymref{definition}{ME}) we can say they are equal matrices.
%
\end{proof}
%
This theorem seems odd at first glance, since we have to switch the order of $A$ and $B$.  But if we simply consider the sizes of the matrices involved, we can see that the switch is necessary for this reason alone.  That the individual entries of the products then come along to be equal is a bonus.\par
%
As the adjoint of a matrix is a composition of a conjugate and a transpose, its interaction with matrix multiplication is similar to that of a transpose.  Here's the last of our long list of basic properties of matrix multiplication.
%
\begin{theorem}{MMAD}{Matrix Multiplication and Adjoints}{matrix multiplication!adjoints}
Suppose $A$ is an $m\times n$ matrix and $B$ is an $n\times p$ matrix.  Then $\adjoint{(AB)}=\adjoint{B}\adjoint{A}$.
\end{theorem}
%
\begin{proof}
%
\begin{align*}
\adjoint{(AB)}
&=\transpose{\left(\conjugate{AB}\right)}&&\text{\acronymref{definition}{A}}\\
&=\transpose{\left(\conjugate{A}\,\conjugate{B}\right)}&&\text{\acronymref{theorem}{MMCC}}\\
&=\transpose{\left(\conjugate{B}\right)}\transpose{\left(\conjugate{A}\right)}&&\text{\acronymref{theorem}{MMT}}\\
&=\adjoint{B}\adjoint{A}&&\text{\acronymref{definition}{A}}
\end{align*}
%
\end{proof}
%
Notice how none of these proofs above relied on writing out huge general matrices with lots of ellipses (``\dots'') and trying to formulate the equalities a whole matrix at a time.  This messy business is a ``proof technique'' to be avoided at all costs.  Notice too how the proof of \acronymref{theorem}{MMAD} does not use an entry-by-entry approach, but simply builds on previous results about matrix multiplication's interaction with conjugation and transposes.\par
%
These theorems, along with \acronymref{theorem}{VSPM} and the other results in \acronymref{section}{MO}, give you the ``rules'' for how matrices interact with the various operations we have defined on matrices (addition, scalar multiplication, matrix multiplication, conjugation, transposes and adjoints).  Use them and use them often.  But don't try to do anything with a matrix that you don't have a rule for.  Together, we would informally call all these operations, and the attendant theorems, ``the algebra of matrices.''  Notice, too, that every column vector is just a $n\times 1$ matrix, so these theorems apply to column vectors also.  Finally, these results, taken as a whole, may make us feel that the definition of matrix multiplication is not so unnatural.
%
\subsect{HM}{Hermitian Matrices}
%
The adjoint of a matrix has a basic property when employed in a matrix-vector product as part of an inner product.  At this point, you could even use the following result as a motivation for the definition of an adjoint.
%
\begin{theorem}{AIP}{Adjoint and Inner Product}{adjoint!inner product}
Suppose that $A$ is an $m\times n$ matrix and $\vect{x}\in\complex{n}$, $\vect{y}\in\complex{m}$.  Then $\innerproduct{A\vect{x}}{\vect{y}}=\innerproduct{\vect{x}}{\adjoint{A}\vect{y}}$.
\end{theorem}
%
\begin{proof}
%
\begin{align*}
\innerproduct{A\vect{x}}{\vect{y}}
&=\transpose{\left(A\vect{x}\right)}\conjugate{\vect{y}}&&\text{\acronymref{theorem}{MMIP}}\\
&=\transpose{\vect{x}}\transpose{A}\conjugate{\vect{y}}&&\text{\acronymref{theorem}{MMT}}\\
&=\transpose{\vect{x}}\transpose{\left(\conjugate{\conjugate{A}}\right)}\conjugate{\vect{y}}&&\text{\acronymref{theorem}{CCM}}\\
&=\transpose{\vect{x}}\conjugate{\left(\transpose{\left(\conjugate{A}\right)}\right)}\conjugate{\vect{y}}&&\text{\acronymref{theorem}{MCT}}\\
&=\transpose{\vect{x}}\conjugate{\left(\adjoint{A}\right)}\conjugate{\vect{y}}&&\text{\acronymref{definition}{A}}\\
&=\transpose{\vect{x}}\conjugate{\left(\adjoint{A}\vect{y}\right)}&&\text{\acronymref{theorem}{MMCC}}\\
&=\innerproduct{\vect{x}}{\adjoint{A}\vect{y}}&&\text{\acronymref{theorem}{MMIP}}
\end{align*}
%
\end{proof}
%
Sometimes a matrix is equal to its adjoint (\acronymref{definition}{A}), and these matrices have interesting properties.  One of the most common situations where this occurs is when a matrix has only real number entries.  Then we are simply talking about symmetric matrices (\acronymref{definition}{SYM}), so you can view this as a generalization of a symmetric matrix.
%
\begin{definition}{HM}{Hermitian Matrix}{hermitian}
The square matrix $A$ is \define{Hermitian} (or \define{self-adjoint}) if $A=\adjoint{A}$.
\end{definition}
%
Again, the set of real matrices that are Hermitian is exactly the set of symmetric matrices.  In \acronymref{section}{PEE} we will uncover some amazing properties of Hermitian matrices, so when you get there, run back here  to remind yourself of this definition.  Further properties will also appear in various sections of the Topics (\acronymref{part}{T}).  Right now we prove a fundamental result about Hermitian matrices, matrix vector products and inner products.  As a characterization, this could be employed as a definition of a Hermitian matrix and some authors take this approach.\par
%
%
\begin{theorem}{HMIP}{Hermitian Matrices and Inner Products}{Hermitian matrix!inner product}
Suppose that $A$ is a square matrix of size $n$.  Then $A$ is Hermitian if and only if $\innerproduct{A\vect{x}}{\vect{y}}=\innerproduct{\vect{x}}{A\vect{y}}$ for all $\vect{x},\,\vect{y}\in\complex{n}$.
\end{theorem}
%
\begin{proof}
($\Rightarrow$)\quad  This is the ``easy half'' of the proof, and makes the rationale for a definition of Hermitian matrices most obvious.  Assume $A$ is Hermitian,
%
\begin{align*}
\innerproduct{A\vect{x}}{\vect{y}}
&=\innerproduct{\vect{x}}{\adjoint{A}\vect{y}}&&\text{\acronymref{theorem}{AIP}}\\
&=\innerproduct{\vect{x}}{A\vect{y}}&&\text{\acronymref{definition}{HM}}\\
\end{align*}
%
($\Leftarrow$)\quad  This ``half'' will take a bit more work.  Assume that $\innerproduct{A\vect{x}}{\vect{y}}=\innerproduct{\vect{x}}{A\vect{y}}$ for all $\vect{x},\,\vect{y}\in\complex{n}$.   Choose any $\vect{x}\in\complex{n}$.  We want to show that $A=\adjoint{A}$ by establishing that $A\vect{x}=\adjoint{A}\vect{x}$.  With only this much motivation, consider the inner product,
%
\begin{align*}
\innerproduct{A\vect{x}-\adjoint{A}\vect{x}}{A\vect{x}-\adjoint{A}\vect{x}}
%
&=\innerproduct{A\vect{x}-\adjoint{A}\vect{x}}{A\vect{x}}-
\innerproduct{A\vect{x}-\adjoint{A}\vect{x}}{\adjoint{A}\vect{x}}
&&\text{\acronymref{theorem}{IPVA}}\\
%
&=\innerproduct{A\vect{x}-\adjoint{A}\vect{x}}{A\vect{x}}-
\innerproduct{A\left(A\vect{x}-\adjoint{A}\vect{x}\right)}{\vect{x}}
&&\text{\acronymref{theorem}{AIP}}\\
%
&=\innerproduct{A\left(A\vect{x}-\adjoint{A}\vect{x}\right)}{\vect{x}}-
\innerproduct{A\left(A\vect{x}-\adjoint{A}\vect{x}\right)}{\vect{x}}
&&\text{Hypothesis}\\
%
&=0&&\text{\acronymref{property}{AICN}}
%
\end{align*}
%
Because this inner product equals zero, and has the same vector in each argument ($A\vect{x}-\adjoint{A}\vect{x}$), \acronymref{theorem}{PIP} gives the conclusion that $A\vect{x}-\adjoint{A}\vect{x}=\zerovector$.  With $A\vect{x}=\adjoint{A}\vect{x}$ for all $\vect{x}\in\complex{n}$, \acronymref{theorem}{EMMVP} says $A=\adjoint{A}$, which is the defining property of a Hermitian matrix (\acronymref{definition}{HM}).
%
\end{proof}
%
So, informally, Hermitian matrices are those that can be tossed around from one side of an inner product to the other with reckless abandon.  We'll see later what this buys us.
%
%  End  mm.tex










