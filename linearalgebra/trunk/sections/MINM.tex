%%%%(c)
%%%%(c)  This file is a portion of the source for the textbook
%%%%(c)
%%%%(c)    A First Course in Linear Algebra
%%%%(c)    Copyright 2004 by Robert A. Beezer
%%%%(c)
%%%%(c)  See the file COPYING.txt for copying conditions
%%%%(c)
%%%%(c)
%%%%%%%%%%%
%%
%%  Section MINM
%%  Matrix Inverses and Nonsingular Matrices
%%
%%%%%%%%%%%
%
We saw in \acronymref{theorem}{CINM} that if a square matrix $A$ is nonsingular, then there is a matrix $B$ so that $AB=I_n$.  In other words, $B$ is halfway to being an inverse of $A$.  We will see in this section that $B$ automatically fulfills the second condition ($BA=I_n$).  \acronymref{example}{MWIAA} showed us that the coefficient matrix from \acronymref{archetype}{A} had no inverse.  Not coincidentally, this coefficient matrix is singular.  We'll make all these connections precise now.  Not many examples or definitions in this section, just theorems.
%
\subsect{NMI}{Nonsingular Matrices are Invertible}
%
We need a couple of technical results for starters.  Some books would call these minor, but essential, results ``lemmas.''  We'll just call 'em theorems.  
\techniqueinline{LC}{Lemmas and Corollaries}{lemma}
{\relax}
{See \acronymref{technique}{LC} for more on the distinction.}\par
%
The first of these technical results is interesting in that the hypothesis says something about a product of two square matrices and the conclusion then says the same thing about each individual matrix in the product.
%
\begin{theorem}{NPNT}{Nonsingular Product has Nonsingular Terms}{nonsingular matrix!product of nonsingular matrices}
%
Suppose that $A$ and $B$ are square matrices of size $n$ and the product $AB$ is nonsingular.  Then $A$ and $B$ are both nonsingular.
\end{theorem}
%
\begin{proof}
We'll do the proof in two parts, each as a proof by contradiction (\acronymref{technique}{CD}).  Establishing that $B$ is nonsingular is the easier part, so we will do it first, but in reality, we will {\em need} to know that $B$ is nonsingular when we prove that $A$ is nonsingular.\par
%
You can also think of this proof as being a study of four possible conclusions in the table below.  One of the four rows {\em must} happen (the list is exhaustive).  In the proof we learn that the first three rows lead to contradictions, and so are impossible.  That leaves the fourth row as a certainty, which is our desired conclusion.
%
\begin{center}
\begin{tabular}{l|l|c}
\multicolumn{1}{c}{$A$}&
\multicolumn{1}{c}{$B$}&
\multicolumn{1}{c}{Case}\\\hline\hline
Singular&Singular&1\\\hline
Nonsingular&Singular&1\\\hline
Singular&Nonsingular&2\\\hline
Nonsingular&Nonsingular&
\end{tabular}
\end{center}
%
Case 1.  Suppose $B$ is singular.  Then there is a nonzero vector $\vect{z}$ that is a solution to $\homosystem{B}$.  So
%
\begin{align*}
(AB)\vect{z}
&=A(B\vect{z})&&\text{\acronymref{theorem}{MMA}}\\
&=A\zerovector&&\text{\acronymref{theorem}{SLEMM}}\\
&=\zerovector&&\text{\acronymref{theorem}{MMZM}}\\
\end{align*}
%
Because $\vect{z}$ is a nonzero solution to $\homosystem{AB}$,  we conclude that $AB$ is singular (\acronymref{definition}{NM}).  This is a contradiction, so $B$ is nonsingular, as desired.\par
%
Case 2.  Suppose $A$ is singular.  Then there is a nonzero vector $\vect{y}$ that is a solution to $\homosystem{A}$.  Now consider the linear system $\linearsystem{B}{\vect{y}}$.  Since we know $B$ is nonsingular from Case 1, the system has a unique solution (\acronymref{theorem}{NMUS}), which we will denote as $\vect{w}$.  We first claim $\vect{w}$ is not the zero vector either.  Assuming the opposite, suppose that $\vect{w}=\zerovector$ (\acronymref{technique}{CD}).  Then
%
\begin{align*}
\vect{y}
&=B\vect{w}&&\text{\acronymref{theorem}{SLEMM}}\\
&=B\zerovector&&\text{Hypothesis}\\
&=\zerovector&&\text{\acronymref{theorem}{MMZM}}
%
\intertext{contrary to $\vect{y}$ being nonzero.  So $\vect{w}\neq\zerovector$.  The pieces are in place, so here we go,}
%
(AB)\vect{w}
&=A(B\vect{w})&&\text{\acronymref{theorem}{MMA}}\\
&=A\vect{y}&&\text{\acronymref{theorem}{SLEMM}}\\
&=\zerovector&&\text{\acronymref{theorem}{SLEMM}}\\
\end{align*}
%
So $\vect{w}$ is a nonzero solution to $\homosystem{AB}$,  and thus we can say  that $AB$ is singular (\acronymref{definition}{NM}).  This is a contradiction, so $A$ is nonsingular, as desired.\par
%
\end{proof}
%
This is a powerful result, because it allows us to begin with a hypothesis that something complicated (the matrix product $AB$) has the property of being nonsingular, and we can then conclude that the simpler constituents ($A$ and $B$ individually) then also have the property of being nonsingular.  If we had thought that the matrix product was an artificial construction, results like this would make us begin to think twice.\par
%
The contrapositive of this result is equally interesting.  It says that if either $A$ or $B$ (or both) is a singular matrix, then the product $AB$ is also singular.  Notice how the negation of the theorem's conclusion ($A$ and $B$ both nonsingular) becomes the statement ``at least one of $A$ and $B$ is singular.''  (See \acronymref{technique}{CP}.)
%
%  TODO: Insert reference to a technique about negations.
%
\begin{theorem}{OSIS}{One-Sided Inverse is Sufficient}{matrix inverse!one-sided}
Suppose $A$ and $B$ are  square matrices of size $n$ such that $AB=I_n$.  Then $BA=I_n$.
\end{theorem}
%
\begin{proof}
The matrix $I_n$ is nonsingular (since it row-reduces easily to $I_n$, \acronymref{theorem}{NMRRI}).  So $A$ and $B$ are nonsingular by \acronymref{theorem}{NPNT}, so in particular $B$ is nonsingular.  We can therefore apply \acronymref{theorem}{CINM} to assert the existence of a matrix $C$ so that $BC=I_n$.  This application of \acronymref{theorem}{CINM} could be a bit confusing, mostly because of the names of the matrices involved.  $B$ is nonsingular, so there must be a ``right-inverse'' for $B$, and we're calling it $C$.\par
%
Now
%
\begin{align*}
BA
&=(BA)I_n&&\text{\acronymref{theorem}{MMIM}}\\
&=(BA)(BC)&&\text{\acronymref{theorem}{CINM}}\\
&=B(AB)C&&\text{\acronymref{theorem}{MMA}}\\
&=BI_nC&&\text{Hypothesis}\\
&=BC&&\text{\acronymref{theorem}{MMIM}}\\
&=I_n&&\text{\acronymref{theorem}{CINM}}
\end{align*}
%
which is the desired conclusion.
%
\end{proof}
%
So \acronymref{theorem}{OSIS} tells us that if $A$ is nonsingular, then the matrix $B$ guaranteed by \acronymref{theorem}{CINM} will be both a ``right-inverse'' and a ``left-inverse'' for $A$, so $A$ is invertible and $\inverse{A}=B$.\par
%
So if you have a nonsingular matrix, $A$, you can use the procedure described in \acronymref{theorem}{CINM} to find an inverse for $A$.  If $A$ is singular, then the procedure in \acronymref{theorem}{CINM} will fail as the first $n$ columns of $M$ will not row-reduce to the identity matrix.  However, we can say a bit more.  When $A$ is singular, then $A$ does not have an inverse (which is very different from saying that the procedure in \acronymref{theorem}{CINM} fails to find an inverse).
This may feel like we are splitting hairs, but its important that we do not make unfounded assumptions.  These observations motivate the next theorem.
%
\begin{theorem}{NI}{Nonsingularity is Invertibility}{matrix inverse!nonsingular matrix}
\index{nonsingular matrix!matrix inverse}
Suppose that $A$ is a square matrix.  Then $A$ is nonsingular if and only if $A$ is invertible.
\end{theorem}
%
\begin{proof}
($\Leftarrow$)  Suppose $A$ is invertible, and suppose that $\vect{x}$ is any solution to the homogeneous system $\homosystem{A}$.  Then
%
\begin{align*}
\vect{x}
&=I_n\vect{x}&&\text{\acronymref{theorem}{MMIM}}\\
&=\left(\inverse{A}A\right)\vect{x}&&\text{\acronymref{definition}{MI}}\\
&=\inverse{A}\left(A\vect{x}\right)&&\text{\acronymref{theorem}{MMA}}\\
&=\inverse{A}\zerovector&&\text{\acronymref{theorem}{SLEMM}}\\
&=\zerovector&&\text{\acronymref{theorem}{MMZM}}
\end{align*}
%
So the {\em only} solution to $\homosystem{A}$ is the zero vector, so by \acronymref{definition}{NM}, $A$ is nonsingular.\par
%
($\Rightarrow$)  Suppose now that $A$ is nonsingular.  By \acronymref{theorem}{CINM} we find $B$ so that $AB=I_n$.  Then \acronymref{theorem}{OSIS} tells us that $BA=I_n$.  So $B$ is $A$'s inverse, and by construction, $A$ is invertible.
%
\end{proof}
%
So for a square matrix, the properties of having an inverse and of having a trivial null space are one and the same.  Can't have one without the other.  
%
\begin{theorem}{NME3}{Nonsingular Matrix Equivalences, Round 3}{nonsingular matrix!equivalences}
Suppose that $A$ is a square matrix of size $n$.  The following are equivalent.
%
\begin{enumerate}
\item $A$ is nonsingular.
\item $A$ row-reduces to the identity matrix.
\item The null space of $A$ contains only the zero vector, $\nsp{A}=\set{\zerovector}$.
\item The linear system $\linearsystem{A}{\vect{b}}$ has a unique solution for every possible choice of $\vect{b}$.
\item The columns of $A$ are a linearly independent set.
\item $A$ is invertible.
\end{enumerate}
\end{theorem}
%
\begin{proof}
We can update our list of equivalences for nonsingular matrices (\acronymref{theorem}{NME2}) with the equivalent condition from \acronymref{theorem}{NI}.
\end{proof}
%
In the case that $A$ is a nonsingular coefficient matrix of a system of equations, the inverse allows us to very quickly compute the unique solution, for any vector of constants.
%
\begin{theorem}{SNCM}{Solution with Nonsingular Coefficient Matrix}{coefficient matrix!nonsingular}
Suppose that $A$ is nonsingular.  Then the unique solution to $\linearsystem{A}{\vect{b}}$ is $\inverse{A}\vect{b}$.
\end{theorem}
%
\begin{proof}
By \acronymref{theorem}{NMUS} we know already that $\linearsystem{A}{\vect{b}}$ has a unique solution for every choice of $\vect{b}$.  We need to show that the expression stated is indeed a solution ({\em the} solution).  That's easy, just ``plug it in'' to the corresponding vector equation representation (\acronymref{theorem}{SLEMM}),
%
\begin{align*}
A\left(\inverse{A}\vect{b}\right)
&=\left(A\inverse{A}\right)\vect{b}&&\text{\acronymref{theorem}{MMA}}\\
&=I_n\vect{b}&&\text{\acronymref{definition}{MI}}\\
&=\vect{b}&&\text{\acronymref{theorem}{MMIM}}
\end{align*}
%
Since $A\vect{x}=\vect{b}$ is true when we substitute $\inverse{A}\vect{b}$ for $\vect{x}$, $\inverse{A}\vect{b}$ is a (the!) solution to $\linearsystem{A}{\vect{b}}$.
%
\end{proof}
%
\subsect{UM}{Unitary Matrices}
%
Recall that the adjoint of a matrix is $\adjoint{A}=\transpose{\left(\conjugate{A}\right)}$ (\acronymref{definition}{A}).
%
\begin{definition}{UM}{Unitary Matrices}{matrix!unitary}
Suppose that $U$ is a square matrix of size $n$ such that $\adjoint{U}U=I_n$.  Then we say $U$ is \define{unitary}.
\end{definition}
%
This condition may seem rather far-fetched at first glance.  Would there be {\em any} matrix that behaved this way?  Well, yes, here's one.
%
\begin{example}{UM3}{Unitary matrix of size 3}{unitary!size 3}
%
\begin{equation*}
U=
\begin{bmatrix}
%
\frac{1 + i }{{\sqrt{5}}} &
   \frac{3 + 2\,i }{{\sqrt{55}}} &  
   \frac{2+2i}{\sqrt{22}} \\
%    
\frac{1 - i }{{\sqrt{5}}} & 
   \frac{2 + 2\,i }{{\sqrt{55}}} &
   \frac{-3 + i }{{\sqrt{22}}} \\
%   
\frac{i }{{\sqrt{5}}} & 
   \frac{3 - 5\,i }{{\sqrt{55}}} & 
   -\frac{2}{\sqrt{22}}
\end{bmatrix}
\end{equation*}
%
The computations get a bit tiresome, but if you work your way through the computation of $\adjoint{U}U$, you {\em will} arrive at the $3\times 3$ identity matrix $I_3$.
%
\end{example}
%
%  Above example from Mathematica input:
%  GramSchmidt[{{1 + I, 1 - I, I}, {I, 1 + I, -I}, {I, -1 + I, 1}},
%                       InnerProduct -> (Conjugate[#1].#2 &)] // Simplify
%
%
Unitary matrices do not have to look quite so gruesome.  Here's a larger one that is a bit more pleasing.
%
\begin{example}{UPM}{Unitary permutation matrix}{unitary!permutation matrix}
The matrix
%
\begin{equation*}
P=
\begin{bmatrix}
0&1&0&0&0\\
0&0&0&1&0\\
1&0&0&0&0\\
0&0&0&0&1\\
0&0&1&0&0
\end{bmatrix}
\end{equation*}
%
is unitary as can be easily checked.  Notice that it is just a rearrangement of the columns of the $5\times 5$ identity matrix, $I_5$ (\acronymref{definition}{IM}).\par
%
An interesting exercise is to build another $5\times 5$ unitary matrix, $R$, using a different rearrangement of the columns of $I_5$.  Then form the product $PR$.  This will be another unitary matrix (\acronymref{exercise}{MINM.T10}).  If you were to build all $5!=5\times 4\times 3\times 2\times 1=120$ matrices of this type you would have a set that remains closed under matrix multiplication.  It is an example of another algebraic structure known as a \define{group} since together the set and the one operation (matrix multiplication here) is closed, associative, has an identity ($I_5$), and inverses (\acronymref{theorem}{UMI}).  Notice though that the operation in this group is not commutative!
%
\end{example}
%
If a matrix $A$ has only real number entries (we say it is a \define{real matrix}) then the defining property of being unitary simplifies to $\transpose{A}A=I_n$.  In this case we, and everybody else, calls the matrix \define{orthogonal}, so you may often encounter this term in your other reading when the complex numbers are not under consideration.\par
%
Unitary matrices have easily computed inverses.  They also have columns that form orthonormal sets.  Here are the theorems that show us that unitary matrices are not as strange as they might initially appear.
%
\begin{theorem}{UMI}{Unitary Matrices are Invertible}{matrix!unitary is invertible}
Suppose that $U$ is a unitary matrix of size $n$.  Then $U$ is nonsingular, and $\inverse{U}=\adjoint{U}$.
\end{theorem}
%
\begin{proof}
By \acronymref{definition}{UM}, we know that $\adjoint{U}U=I_n$.  The matrix $I_n$ is nonsingular (since it row-reduces easily to $I_n$, \acronymref{theorem}{NMRRI}).  So by \acronymref{theorem}{NPNT}, $U$ and $\adjoint{U}$ are both nonsingular matrices.\par
%
The equation $\adjoint{U}U=I_n$ gets us halfway to an inverse of $U$, and \acronymref{theorem}{OSIS} tells us that then $U\adjoint{U}=I_n$ also.  So $U$ and $\adjoint{U}$ are inverses of each other (\acronymref{definition}{MI}).
%
\end{proof}
%
\begin{theorem}{CUMOS}{Columns of Unitary Matrices are Orthonormal Sets}{unitary matrices!columns}
Suppose that $A$ is a square matrix of size $n$ with columns $S=\set{\vectorlist{A}{n}}$.  Then $A$ is a unitary matrix if and only if $S$ is an orthonormal set.
\end{theorem}
%
\begin{proof}
The proof revolves around recognizing that a typical entry of the product $\adjoint{A}A$ is an inner product of columns of $A$.  Here are the details to support this claim.
%
\begin{align*}
\matrixentry{\adjoint{A}A}{ij}
&=\sum_{k=1}^{n}\matrixentry{\adjoint{A}}{ik}\matrixentry{A}{kj}
&&\text{\acronymref{theorem}{EMP}}\\
%
&=\sum_{k=1}^{n}\matrixentry{\transpose{\left(\conjugate{A}\right)}}{ik}\matrixentry{A}{kj}
&&\text{\acronymref{theorem}{EMP}}\\
%
&=\sum_{k=1}^{n}\matrixentry{\,\conjugate{A}\,}{ki}\matrixentry{A}{kj}
&&\text{\acronymref{definition}{TM}}\\
%
&=\sum_{k=1}^{n}\conjugate{\matrixentry{A}{ki}}\matrixentry{A}{kj}
&&\text{\acronymref{definition}{CCM}}\\
%
&=\sum_{k=1}^{n}\matrixentry{A}{kj}\conjugate{\matrixentry{A}{ki}}
&&\text{\acronymref{property}{CMCN}}\\
%
&=\sum_{k=1}^{n}\vectorentry{\vect{A}_j}{k}\conjugate{\vectorentry{\vect{A}_i}{k}}\\
%
&=\innerproduct{\vect{A}_j}{\vect{A}_i}
&&\text{\acronymref{definition}{IP}}
%
\end{align*}
%
We now employ this equality in a chain of equivalences,
%
\begin{align*}
&\text{$S=\set{\vectorlist{A}{n}}$ is an orthonormal set}\\
&\iff \innerproduct{\vect{A}_j}{\vect{A}_i}=
\begin{cases}
0 &\text{if $i\neq j$}\\
1 & \text{if $i=j$}
\end{cases}&&\text{\acronymref{definition}{ONS}}\\
%
&\iff \matrixentry{\adjoint{A}A}{ij}=
\begin{cases}
0 &\text{if $i\neq j$}\\
1 & \text{if $i=j$}
\end{cases}\\
%
&\iff \matrixentry{\adjoint{A}A}{ij}=\matrixentry{I_n}{ij},\ 1\leq i\leq n,\ 1\leq j\leq n
&&\text{\acronymref{definition}{IM}}\\
%
&\iff \adjoint{A}A=I_n
&&\text{\acronymref{definition}{ME}}\\
%
&\iff \text{$A$ is a unitary matrix}
&&\text{\acronymref{definition}{UM}}
%
\end{align*}
%
\end{proof}
%
%
\begin{example}{OSMC}{Orthonormal set from matrix columns}{orthonormal!matrix columns}
The matrix
%
\begin{equation*}
U=
\begin{bmatrix}
%
\frac{1 + i }{{\sqrt{5}}} &
   \frac{3 + 2\,i }{{\sqrt{55}}} &  
   \frac{2+2i}{\sqrt{22}} \\
%    
\frac{1 - i }{{\sqrt{5}}} & 
   \frac{2 + 2\,i }{{\sqrt{55}}} &
   \frac{-3 + i }{{\sqrt{22}}} \\
%   
\frac{i }{{\sqrt{5}}} & 
   \frac{3 - 5\,i }{{\sqrt{55}}} & 
   -\frac{2}{\sqrt{22}}
\end{bmatrix}
\end{equation*}
%
from \acronymref{example}{UM3} is a unitary matrix.  By \acronymref{theorem}{CUMOS}, its columns
%
\begin{equation*}
\set{
%
\colvector{
\frac{1 + i }{{\sqrt{5}}}\\
   \frac{1 - i }{{\sqrt{5}}}\\
   \frac{i }{{\sqrt{5}}}
},\,
%
\colvector{
\frac{3 + 2\,i }{{\sqrt{55}}}\\
   \frac{2 + 2\,i }{{\sqrt{55}}}\\
   \frac{3 - 5\,i }{{\sqrt{55}}}
},\,
%
\colvector{
\frac{2+2i}{\sqrt{22}}\\ 
   \frac{-3 + i }{{\sqrt{22}}}\\ 
   -\frac{2}{\sqrt{22}}
}   
%
}
\end{equation*}
%
form an orthonormal set.  You might find checking the six inner products of pairs of these vectors easier than doing the matrix product $\adjoint{U}U$.  Or, because the inner product is anti-commutative (\acronymref{theorem}{IPAC}) you only need check  three inner products (see \acronymref{exercise}{MINM.T12}).
%
\end{example}
%
When using vectors and matrices that only have real number entries, orthogonal matrices are those matrices with inverses that equal their transpose.  Similarly, the inner product is the familiar dot product.  Keep this special case in mind as you read the next theorem.
%
\begin{theorem}{UMPIP}{Unitary Matrices Preserve Inner Products}{unitary matrix!inner product}
Suppose that $U$ is a unitary matrix of size $n$ and $\vect{u}$ and $\vect{v}$ are two vectors from $\complex{n}$.  Then
%
\begin{align*}
\innerproduct{U\vect{u}}{U\vect{v}}&=\innerproduct{\vect{u}}{\vect{v}}
&
&\text{and}
&
\norm{U\vect{v}}&=\norm{\vect{v}}
\end{align*}
%
\end{theorem}
%
\begin{proof}
%
\begin{align*}
\innerproduct{U\vect{u}}{U\vect{v}}
&=\transpose{(U\vect{u})}\conjugate{U\vect{v}}
&&\text{\acronymref{theorem}{MMIP}}\\
%
&=\transpose{\vect{u}}\transpose{U}\conjugate{U\vect{v}}
&&\text{\acronymref{theorem}{MMT}}\\
%
&=\transpose{\vect{u}}\transpose{U}\conjugate{U}\conjugate{\vect{v}}
&&\text{\acronymref{theorem}{MMCC}}\\
%
&=\transpose{\vect{u}}\transpose{\left(\conjugate{\conjugate{U}}\right)}\conjugate{U}\conjugate{\vect{v}}
&&\text{\acronymref{theorem}{CCT}}\\
%
&=\transpose{\vect{u}}\conjugate{\transpose{\left(\conjugate{U}\right)}}\conjugate{U}\conjugate{\vect{v}}
&&\text{\acronymref{theorem}{MCT}}\\
%
&=\transpose{\vect{u}}\conjugate{\transpose{\left(\conjugate{U}\right)}U}\conjugate{\vect{v}}
&&\text{\text{\acronymref{theorem}{MMCC}}}\\
%
&=\transpose{\vect{u}}\conjugate{\adjoint{U}U}\conjugate{\vect{v}}&&\text{\text{\acronymref{definition}{A}}}\\
%
&=\transpose{\vect{u}}\conjugate{I_n}\conjugate{\vect{v}}
&&\text{\acronymref{definition}{UM}}\\
%
&=\transpose{\vect{u}}I_n\conjugate{\vect{v}}
&&\acronymref{definition}{IM}\\
%
&=\transpose{\vect{u}}\conjugate{\vect{v}}
&&\text{\acronymref{theorem}{MMIM}}\\
%
&=\innerproduct{\vect{u}}{\vect{v}}
&&\text{\acronymref{theorem}{MMIP}}\\
%
\end{align*}
%
The second conclusion is just a specialization of the first conclusion.
%
\begin{align*}
\norm{U\vect{v}}
&=\sqrt{\norm{U\vect{v}}^2}\\
&=\sqrt{\innerproduct{U\vect{v}}{U\vect{v}}}&&\text{\acronymref{theorem}{IPN}}\\
&=\sqrt{\innerproduct{\vect{v}}{\vect{v}}}\\
&=\sqrt{\norm{\vect{v}}^2}&&\text{\acronymref{theorem}{IPN}}\\
&=\norm{\vect{v}}
\end{align*}
%
\end{proof}
%
Aside from the inherent interest in this theorem, it makes a bigger statement about unitary matrices.  When we view vectors geometrically as directions or forces, then the norm equates to a notion of length.  If we transform a vector by multiplication with a unitary matrix, then the length (norm) of that vector stays the same.  If we consider column vectors with two or three slots containing only real numbers, then the inner product of two such vectors is just the dot product, and this quantity can be used to compute the angle between two vectors.  When two vectors are multiplied (transformed) by the same unitary matrix, their dot product is unchanged and their individual lengths are unchanged.  The results in the angle between the two vectors remaining unchanged.\par
%
A ``unitary transformation'' (matrix-vector products with unitary matrices) thus preserve geometrical relationships among vectors representing directions, forces, or other physical quantities.  In the case of a two-slot vector with real entries, this is simply a rotation.  These sorts of computations are exceedingly important in computer graphics such as games and real-time simulations, especially when increased realism is achieved by performing many such computations quickly.  We will see unitary matrices again in subsequent sections (especially \acronymref{theorem}{OD}) and in each instance, consider the interpretation of the unitary matrix as a sort of geometry-preserving transformation.  Some authors use the term \define{isometry} to highlight this behavior.  We will speak loosely of a unitary matrix as being a sort of generalized rotation.\par
%
A final reminder:  the terms ``dot product,'' ``symmetric matrix'' and ``orthogonal matrix'' used in reference to vectors or matrices with real number entries correspond to the terms inner product, Hermitian matrix and unitary matrix when we generalize to include complex number entries, so keep that in mind as you read elsewhere.
%
%  End  MINM.tex








