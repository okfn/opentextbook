%%%%(c)
%%%%(c)  This file is a portion of the source for the textbook
%%%%(c)
%%%%(c)    A First Course in Linear Algebra
%%%%(c)    Copyright 2004 by Robert A. Beezer
%%%%(c)
%%%%(c)  See the file COPYING.txt for copying conditions
%%%%(c)
%%%%(c)
%%%%%%%%%%%
%%
%%  Section CNO
%%  Complex Number Arithmetic
%%
%%%%%%%%%%%
%
In this section we review of the basics of working with complex numbers.\par
%
\subsect{CNA}{Arithmetic with complex numbers}
%
A complex number is a linear combination of $1$ and $i=\sqrt{-1}$, typically written in the form $a+bi$.  Complex numbers can be added, subtracted, multiplied and divided, just like we are used to doing with real numbers, including the restriction on division by zero.  We will not define these operations carefully, but instead illustrate with examples.
%
\begin{example}{ACN}{Arithmetic of complex numbers}{complex arithmetic}
%
\begin{align*}
(2+5i)+(6-4i)&=(2+6)+(5+(-4))i=8+i\\
(2+5i)-(6-4i)&=(2-6)+(5-(-4))i=-4+9i\\
(2+5i)(6-4i)&=(2)(6)+(5i)(6)+(2)(-4i)+(5i)(-4i)=12+30i-8i-20i^2\\
&=12+22i-20(-1)=32+22i
\intertext{Division takes just a bit more care.  We multiply the denominator by a complex number chosen to produce a real number and then we can produce a complex number as a result.}
\frac{2+5i}{6-4i}&=\frac{2+5i}{6-4i}\frac{6+4i}{6+4i}=\frac{-8+38i}{52}=-\frac{8}{52}+\frac{38}{52}i=-\frac{2}{13}+\frac{19}{26}i
\end{align*}
%
\end{example}
%
In this example, we used $6+4i$ to convert the denominator in the fraction to a real number.  This number is known as the conjugate, which we define in the next section.
%
We will often exploit the basic properties of complex number addition, subtraction, multiplication and division, so we will carefully define the two basic operations, together with a definition of equality, and then collect nine basic properties  in a theorem.
%
\begin{definition}{CNE}{Complex Number Equality}{complex numbers!equality}
The complex numbers $\alpha=a+bi$ and $\beta=c+di$ are \define{equal}, denoted $\alpha=\beta$, if $a=c$ and $b=d$.
\denote{CNE}{Complex Number Equality}{$\alpha=\beta$}{complex numbers!equality}
\end{definition}
%
%
\begin{definition}{CNA}{Complex Number Addition}{complex numbers!addition}
The \define{sum} of the complex numbers $\alpha=a+bi$ and $\beta=c+di$ , denoted $\alpha+\beta$, is $(a+c)+(b+d)i$.
\denote{CNA}{Complex Number Addition}{$\alpha+\beta$}{complex numbers!addition}
\end{definition}
%
%
\begin{definition}{CNM}{Complex Number Multiplication}{complex numbers!multiplication}
The \define{product} of the complex numbers $\alpha=a+bi$ and $\beta=c+di$ , denoted $\alpha\beta$, is $(ac-bd)+(ad+bc)i$.
\denote{CNM}{Complex Number Multiplication}{$\alpha\beta$}{complex numbers!multiplication}
\end{definition}
%
\begin{theorem}{PCNA}{Properties of Complex Number Arithmetic}{complex numbers!arithmetic properties}
The operations of addition and multiplication of complex numbers have the following properties.
%
\begin{itemize}
%
\item\property{ACCN}{Additive Closure, Complex Numbers}{addtive closure!complex numbers}
If $\alpha,\beta\in\complexes$, then $\alpha+\beta\in\complexes$.
%
\item\property{MCCN}{Multiplicative Closure, Complex Numbers}{multiplicative closure!complex numbers}
If $\alpha,\beta\in\complexes$, then $\alpha\beta\in\complexes$.
%
\item\property{CACN}{Commutativity of Addition, Complex Numbers}{additive commutativity!complex numbers}
For any $\alpha,\,\beta\in\complexes$, $\alpha+\beta=\beta+\alpha$.
%
\item\property{CMCN}{Commutativity of Multiplication, Complex Numbers}{multiplicative commuativity!complex numbers}
For any $\alpha,\,\beta\in\complexes$, $\alpha\beta=\beta\alpha$.
%
\item\property{AACN}{Additive Associativity, Complex Numbers}{additive associativity!complex numbers}
For any $\alpha,\,\beta,\,\gamma\in\complexes$, $\alpha+\left(\beta+\gamma\right)=\left(\alpha+\beta\right)+\gamma$.
%
\item\property{MACN}{Multiplicative Associativity, Complex Numbers}{multiplicative associativity!complex numbers}
For any $\alpha,\,\beta,\,\gamma\in\complexes$, $\alpha\left(\beta\gamma\right)=\left(\alpha\beta\right)\gamma$.
%
\item\property{DCN}{Distributivity, Complex Numbers}{distributivity!complex numbers}
For any $\alpha,\,\beta,\,\gamma\in\complexes$, $\alpha(\beta+\gamma)=\alpha\beta+\alpha\gamma$.
%
\item\property{ZCN}{Zero, Complex Numbers}{zero!complex numbers}
There is a complex number $0=0+0i$ so that for any $\alpha\in\complexes$, $0+\alpha=\alpha$.
%
\item\property{OCN}{One, Complex Numbers}{one!complex numbers}
There is a complex number $1=1+0i$ so that for any $\alpha\in\complexes$, $1\alpha=\alpha$.
%
\item\property{AICN}{Additive Inverse, Complex Numbers}{additive inverse!complex numbers}
For every $\alpha\in\complexes$ there exists $-\alpha\in\complexes$ so that $\alpha+\left(-\alpha\right)=0$.
%
\item\property{MICN}{Multiplicative Inverse, Complex Numbers}{multiplicative inverse!complex numbers}
For every $\alpha\in\complexes$, $\alpha\neq 0$ there exists $\frac{1}{\alpha}\in\complexes$ so that $\alpha\left(\frac{1}{\alpha}\right)=1$.
%
\end{itemize}
\end{theorem}
%
\begin{proof}
We could derive each of these properties of complex numbers with a proof that builds on the identical properties of the real numbers.  The only proof that might be at all interesting would be to show \acronymref{property}{MICN} since we would need to trot out a conjugate.  For this property, and especially for the others, we might be tempted to construct proofs of the identical properties for the reals.  This would take us way too far afield, so we will draw a line in the sand right here and just agree that these nine fundamental behaviors are true.  OK?\par
%
Mostly we have stated these nine properties carefully so that we can make reference to them later in other proofs.  So we will be linking back here often.
%
\end{proof}







\subsect{CCN}{Conjugates of Complex Numbers}
%
\begin{definition}{CCN}{Conjugate of a Complex Number}{complex number !conjugate}
The \define{conjugate} of the complex number $c=a+bi\in\complex{\null}$  is the complex number $\conjugate{c}=a-bi$.
\denote{CCN}{Conjugate of a Complex Number}{$\conjugate{c}$}{conjugate}
\end{definition}
%
\begin{example}{CSCN}{Conjugate of some complex numbers}{complex number!conjugate}
%
\begin{align*}
\conjugate{2+3i}=2-3i&&\conjugate{5-4i}=5+4i&&
\conjugate{-3+0i}=-3+0i&&\conjugate{0+0i}=0+0i
\end{align*}
%
\end{example}
%
Notice how the conjugate of a real number leaves the number unchanged.  The conjugate enjoys some basic properties that are useful when we work with linear expressions involving addition and multiplication.
%
\begin{theorem}{CCRA}{Complex Conjugation Respects Addition}{conjugate!addition}
Suppose that $c$ and $d$ are complex numbers.  Then $\conjugate{c+d}=\conjugate{c}+\conjugate{d}$.
\end{theorem}
%
\begin{proof}
Let $c=a+bi$ and $d=r+si$.  Then
%
\begin{equation*}
\conjugate{c+d}=\conjugate{(a+r)+(b+s)i}=(a+r)-(b+s)i=(a-bi)+(r-si)=\conjugate{c}+\conjugate{d}
\end{equation*}
%
\end{proof}
%
\begin{theorem}{CCRM}{Complex Conjugation Respects Multiplication}{conjugate!multiplication}
Suppose that $c$ and $d$ are complex numbers.  Then $\conjugate{cd}=\conjugate{c}\conjugate{d}$.
\end{theorem}
%
\begin{proof}
Let $c=a+bi$ and $d=r+si$.  Then
%
\begin{align*}
\conjugate{cd}&=\conjugate{(ar-bs)+(as+br)i}=(ar-bs)-(as+br)i\\
&=(ar-(-b)(-s))+(a(-s)+(-b)r)i=(a-bi)(r-si)=\conjugate{c}\conjugate{d}
\end{align*}
%
\end{proof}
%
\begin{theorem}{CCT}{Complex Conjugation Twice}{conjugate!twice}
Suppose that $c$ is a complex number.  Then $\conjugate{\conjugate{c}}=c$.
\end{theorem}
%
\begin{proof}
Let $c=a+bi$.  Then
%
\begin{equation*}
\conjugate{\conjugate{c}}=\conjugate{a-bi}=a-(-bi)=a+bi=c
\end{equation*}
%
\end{proof}
%
\subsect{MCN}{Modulus of a Complex Number}
%
We define one more operation with complex numbers that may be new to you.
%
\begin{definition}{MCN}{Modulus of a Complex Number}{complex number !modulus}
The \define{modulus} of the complex number $c=a+bi\in\complex{\null}$, is the nonnegative real number
%
\begin{equation*}
\modulus{c}=\sqrt{c\conjugate{c}}=\sqrt{a^2+b^2}.
\end{equation*}
%
\end{definition}
%
\begin{example}{MSCN}{Modulus of some complex numbers}{complex number!modulus}
%
\begin{align*}
\modulus{2+3i}=\sqrt{13}&&\modulus{5-4i}=\sqrt{41}&&
\modulus{-3+0i}=3&&\modulus{0+0i}=0
\end{align*}
%
\end{example}
%
The modulus can be interpreted as a version of the absolute value for complex numbers, as is suggested by the notation employed.  You can see this in how $\modulus{-3}=\modulus{-3+0i}=3$.  Notice too how the modulus of the complex zero, $0+0i$, has value $0$.
%
%  End  cno.tex

