%%%%(c)
%%%%(c)  This file is a portion of the source for the textbook
%%%%(c)
%%%%(c)    A First Course in Linear Algebra
%%%%(c)    Copyright 2004 by Robert A. Beezer
%%%%(c)
%%%%(c)  See the file COPYING.txt for copying conditions
%%%%(c)
%%%%(c)
Another proof technique is known as ``proof by contradiction'' and it can be a powerful (and satisfying) approach.  Simply put, suppose you wish to prove the implication, ``If $A$, then $B$.''   As usual, we assume that $A$ is true, but we also make the additional assumption that $B$ is false.  If our original implication is true, then these twin assumptions should lead us to a logical inconsistency.  In practice we assume the negation of $B$ to be true (see \acronymref{technique}{N}).  So we argue from the assumptions $A$ and $\text{not}(B)$ looking for some obviously false conclusion such as $1=6$, or a set is simultaneously empty and nonempty, or a matrix is both nonsingular and singular.\par
%
You should be careful about formulating proofs that look like proofs by contradiction, but really aren't.  This happens when you assume $A$ and $\text{not}(B)$ and proceed to give a ``normal'' and direct proof that $B$ is true by only using the assumption that $A$ is true.  Your last step is to then claim that $B$ is true and you then appeal to the assumption that $\text{not}(B)$ is true, thus getting the desired contradiction.  Instead, you could have avoided the overhead of a proof by contradiction and just run with the direct proof.  This stylistic flaw is known, quite graphically, as ``setting up the strawman to knock him down.''\par
%
Here is a simple example of a proof by contradiction.  There are direct proofs that are just about as easy, but this will demonstrate the point, while narrowly avoiding knocking down the straw man.\par\medskip
%
{\bf Theorem}:  If $a$ and $b$ are odd integers, then their product, $ab$, is odd.\par\medskip
%
{\bf Proof}:  To begin a proof by contradiction, assume the hypothesis, that $a$ and $b$ are odd.  Also assume the negation of the conclusion, in this case, that $ab$ is even.  Then there are integers, $j$, $k$, $\ell$ so that $a=2j+1$, $b=2k+1$, $ab=2\ell$.  Then
%
\begin{align*}
0
&=ab-ab\\
&=(2j+1)(2k+1)-(2\ell)\\
&=4jk+2j+2k-2\ell+1\\
&=2\left(2jk+j+k-\ell\right)+1\\
\end{align*}
%
Notice how we used both our hypothesis and the negation of the conclusion in the second line.   Now divide the integer on each end of this string of equalities by 2.  On the left we get a remainder of 0, while on the right we see that the remainder will be 1.  Both remainders cannot be correct, so this is our desired contradiction.  Thus, the conclusion (that $ab$ is odd) is true.\par\medskip
%
Again, we do not offer this example as the {\em best} proof of this fact about even and odd numbers, but rather it is a simple illustration of a proof by contradiction.  You can find examples of proofs by contradiction in 
\acronymref{theorem}{RREFU},  % RREF is unique
\acronymref{theorem}{NMUS},  % Nonsingular matrices and unique solutions
\acronymref{theorem}{NPNT},    % Nonsingular product, nonsingular terms
\acronymref{theorem}{TTMI},     % 2x2 matrix inverse
\acronymref{theorem}{GSP},   % Gram-Schmidt
\acronymref{theorem}{ELIS},       % Extending linearly independent sets
\acronymref{theorem}{EDYES},    % Equal dimensions -> equal subspaces
\acronymref{theorem}{EMHE},   % Every matrix has an eigenvalue
\acronymref{theorem}{EDELI},     % Eigenvectors with distinct eigenvalues
and
\acronymref{theorem}{DMFE},      % Diagonalizable matrices, large eigenspaces
%
in addition to several examples and solutions to exercises.

