Author(s): Rob Beezer

Title: A First Course in Linear Algebra

Source URL: <http://linear.ups.edu/>

Download URL(s): <http://linear.ups.edu/download/fcla-source-1.32.tar.gz>

License(s): GFDL

Tags: mathematics


#############################################################
# Mirrored at Open Text Book <http://www.opentextbook.org>. #
#############################################################
