Econometrics notes/textbook written by Michael Creel [1]. Michael was kind
enough to make available these source files as well as to dual license the
material under the GPL and Creative Commons Attribution Share-Alike 2.5. 

[1]: http://idea.uab.es/~mcreel/
