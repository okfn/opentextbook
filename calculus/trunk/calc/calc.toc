\ttl@change@i {\@ne }{chapter}{0mm}{\addvspace {4mm}\sffamily }{\contentsmargin {0mm}\Large \thecontentslabel \enspace \Large }{\contentsmargin {0mm}\Large }{}\relax 
\ttl@change@v {chapter}{}{}{}\relax 
\contentsline {chapter}{\numberline {1}Rates of Change}{9}
\contentsline {section}{\numberline {1.1}Change in discrete steps}{9}
\contentsline {subsection}{\numberline {1.1.1}Two sides of the same coin}{9}
\contentsline {subsection}{\numberline {1.1.2}Some guesses}{11}
\contentsline {section}{\numberline {1.2}Continuous change}{12}
\contentsline {subsection}{\numberline {1.2.1}A derivative}{14}
\contentsline {subsection}{\numberline {1.2.2}Properties of the derivative}{15}
\contentsline {subsection}{\numberline {1.2.3}Higher-order polynomials}{16}
\contentsline {subsection}{\numberline {1.2.4}The second derivative}{16}
\contentsline {subsection}{\numberline {1.2.5}Maxima and minima}{18}
\contentsline {section}{Problems}{21}
\contentsline {chapter}{\numberline {2}To infinity --- and beyond!}{23}
\contentsline {section}{\numberline {2.1}Infinitesimals}{23}
\contentsline {section}{\numberline {2.2}Safe use of infinitesimals}{26}
\contentsline {section}{\numberline {2.3}The product rule}{30}
\contentsline {section}{\numberline {2.4}The chain rule}{33}
\contentsline {section}{\numberline {2.5}Exponentials and logarithms}{34}
\contentsline {subsection}{\numberline {2.5.1}The exponential}{34}
\contentsline {subsection}{\numberline {2.5.2}The logarithm}{36}
\contentsline {section}{\numberline {2.6}Quotients}{37}
\contentsline {section}{\numberline {2.7}Differentiation on a computer}{38}
\contentsline {section}{\numberline {2.8}Continuity}{41}
\contentsline {section}{\numberline {2.9}Limits}{42}
\contentsline {subsection}{\numberline {2.9.1}L'H\^{o}pital's rule}{45}
\contentsline {subsection}{\numberline {2.9.2}Another perspective on indeterminate forms}{47}
\contentsline {subsection}{\numberline {2.9.3}Limits at infinity}{48}
\contentsline {section}{Problems}{50}
\contentsline {chapter}{\numberline {3}Integration}{55}
\contentsline {section}{\numberline {3.1}Definite and indefinite integrals}{55}
\contentsline {section}{\numberline {3.2}The fundamental theorem of calculus}{58}
\contentsline {section}{\numberline {3.3}Properties of the integral}{59}
\contentsline {section}{\numberline {3.4}Applications}{60}
\contentsline {subsection}{\numberline {3.4.1}Averages}{60}
\contentsline {subsection}{\numberline {3.4.2}Work}{61}
\contentsline {subsection}{\numberline {3.4.3}Probability}{61}
\contentsline {section}{Problems}{67}
\contentsline {chapter}{\numberline {4}Techniques}{69}
\contentsline {section}{\numberline {4.1}Newton's method}{69}
\contentsline {section}{\numberline {4.2}Implicit differentiation}{70}
\contentsline {section}{\numberline {4.3}Taylor series}{71}
\contentsline {section}{\numberline {4.4}Methods of integration}{76}
\contentsline {subsection}{\numberline {4.4.1}Change of variable}{76}
\contentsline {subsection}{\numberline {4.4.2}Integration by parts}{78}
\contentsline {subsection}{\numberline {4.4.3}Partial fractions}{79}
\contentsline {section}{Problems}{83}
\contentsline {chapter}{\numberline {5}Complex number techniques}{85}
\contentsline {section}{\numberline {5.1}Review of complex numbers}{85}
\contentsline {section}{\numberline {5.2}Euler's formula}{88}
\contentsline {section}{\numberline {5.3}Partial fractions revisited}{90}
\contentsline {section}{Problems}{91}
\contentsline {chapter}{\numberline {6}Improper integrals}{93}
\contentsline {section}{\numberline {6.1}Integrating a function that blows up}{93}
\contentsline {section}{\numberline {6.2}Limits of integration at infinity}{94}
\contentsline {section}{Problems}{96}
\contentsline {chapter}{\numberline {7}Iterated integrals}{97}
\contentsline {section}{\numberline {7.1}Integrals inside integrals}{97}
\contentsline {section}{\numberline {7.2}Applications}{99}
\contentsline {section}{\numberline {7.3}Polar coordinates}{101}
\contentsline {section}{\numberline {7.4}Spherical and cylindrical coordinates}{102}
\contentsline {section}{Problems}{104}
\ttl@change@i {\@ne }{chapter}{0mm}{\addvspace {4mm}\sffamily }{\contentsmargin {0mm}\large \thecontentslabel \enspace \large }{\contentsmargin {0mm}\large }{\quad \contentspage }\relax 
\ttl@change@v {chapter}{}{}{}\relax 
\contentsline {chapter}{\numberline {A}Detours}{107}
\contentsline {chapter}{\numberline {B}Answers and solutions}{115}
\contentsline {chapter}{\numberline {C}Photo Credits}{133}
\contentsline {chapter}{\numberline {D}Reference}{135}
\contentsline {section}{\numberline {D.1}Review}{135}
\contentsline {subsection}{\numberline {D.1.1}Algebra}{135}
\contentsline {subsection}{\numberline {D.1.2}Geometry, area, and volume}{135}
\contentsline {subsection}{\numberline {D.1.3}Trigonometry with a right triangle}{135}
\contentsline {subsection}{\numberline {D.1.4}Trigonometry with any triangle}{135}
\contentsline {section}{\numberline {D.2}Hyperbolic functions}{135}
\contentsline {section}{\numberline {D.3}Calculus}{136}
\contentsline {subsection}{\numberline {D.3.1}Rules for differentiation}{136}
\contentsline {subsection}{\numberline {D.3.2}Integral calculus}{136}
\contentsline {subsection}{\numberline {D.3.3}Table of integrals}{136}
\contentsfinish 
